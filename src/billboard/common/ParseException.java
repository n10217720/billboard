package billboard.common;

/**
 * A generic exception, raised whenever a parsing-related error occurs.
 */
public class ParseException extends Exception {
    /**
     * Construct a new parse exception with the provided message.
     * @param message The message to display on when the error gets thrown.
     */
    public ParseException(String message){
        super(message);
    }
}
