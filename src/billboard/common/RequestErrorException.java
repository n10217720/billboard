package billboard.common;

import billboard.common.api.ErrorResponse;

/**
 * An exception for requests that fail due to an error response being returned by the server.
 */
public class RequestErrorException extends RequestFailedException {
    private ErrorResponse errorResponse;

    /**
     * Construct a new RequestErrorException based of a provided error response object.
     * @param errorResponse The errorResponse object.
     */
    public RequestErrorException(ErrorResponse errorResponse) {
        super("Request failed due to server returning error response");
        this.errorResponse = errorResponse;
    }

    /**
     * Returns the error response that was returned by the server.
     *
     * @return The error response that was returned by the server.
     */
    public ErrorResponse getErrorResponse() {
        return this.errorResponse;
    }
}
