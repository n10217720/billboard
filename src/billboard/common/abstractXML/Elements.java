package billboard.common.abstractXML;

import billboard.common.ParseException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.List;

/**
 * Several XML elements with identical names and text-only content, used for both document parsing and generation.
 */
public class Elements extends ElementBase {
    private List<String> childContents;

    /**
     * Creates several XML elements, each with content as defined by or parsed into {@code childContents}.
     *
     * @param name The name of every XML element.
     * @param childContents The contents of the elements created or parsed.
     */
    public Elements(String name, List<String> childContents) {
        super(name);
        this.childContents = childContents;
    }

    @Override
    protected void buildInto(Document document, Node parent) {
        for (String childContent : this.childContents) {
            org.w3c.dom.Element element = document.createElement(this.name);
            element.setTextContent(childContent);

            parent.appendChild(element);
        }
    }

    @Override
    protected void parseFromParent(Element parent) throws ParseException {
        NodeList children = parent.getElementsByTagName(this.name);
        for (int i = 0; i < children.getLength(); ++i) {
            Element child = (Element) children.item(i);

            this.childContents.add(child.getTextContent());
        }
    }
}
