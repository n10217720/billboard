package billboard.common.abstractXML;

/**
 * An attribute on an XML element, used for building an XML document.
 */
public class Attribute {
    protected String name;
    protected String value;

    protected StringOutput valueOutput;

    /**
     * Creates an attribute with a specified name and value.
     *
     * @param name The attribute's name.
     * @param value The attribute's value.
     */
    public Attribute(String name, String value) {
        this.name = name;
        this.value = value;
    }

    /**
     * Creates an attribute for parsing with a specified name and value output.
     *
     * @param name The attribute's name.
     * @param value A {@link StringOutput} in which to store the attributes's value.
     */
    public Attribute(String name, StringOutput value) {
        this.name = name;
        this.valueOutput = value;
    }
}
