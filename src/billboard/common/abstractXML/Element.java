package billboard.common.abstractXML;

import billboard.common.ParseException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * An XML element, used for building an XML document.
 */
public class Element extends ElementBase {
    private String textContent;
    private Attribute[] attributes;
    private ElementBase[] children;

    private StringOutput textContentOutput;

    private boolean optional = false;

    /**
     * Creates an XML element with the specified tag name and text content.
     *
     * @param name The element's tag name.
     * @param textContent The element's text content.
     */
    public Element(String name, String textContent) {
        this(name, new Attribute[] {}, textContent);
    }

    /**
     * Creates an XML element with the specified tag name, attributes and text content.
     *
     * @param name The XML element's tag name.
     * @param attributes The element's attributes.
     * @param textContent The element's text content.
     */
    public Element(String name, Attribute[] attributes, String textContent) {
        super(name);
        this.attributes = attributes;
        this.textContent = textContent;
        this.children = new Element[] {};
    }

    /**
     * Creates an XML element for parsing with the specified tag name and text content output.
     *
     * @param name The element's tag name.
     * @param textContent A {@link StringOutput} in which to store the element's text content.
     */
    public Element(String name, StringOutput textContent) {
        this(name, new Attribute[] {}, textContent);
    }

    /**
     * Creates an XML element for parsing with the specified tag name, attributes and text content output.
     *
     * @param name The XML element's tag name.
     * @param attributes The element's attributes.
     * @param textContent A {@link StringOutput} in which to store the element's text content.
     */
    public Element(String name, Attribute[] attributes, StringOutput textContent) {
        super(name);
        this.attributes = attributes;
        this.children = new Element[] {};
        this.textContentOutput = textContent;
    }

    /**
     * Creates an XML element with the specified tag name and children.
     *
     * @param name The element's tag name.
     * @param children The element's children.
     */
    public Element(String name, ElementBase[] children) {
        this(name, new Attribute[] {}, children);
    }

    /**
     * Creates an XML element with the specified tag name, children and attributes.
     *
     * @param name The XML element's tag name.
     * @param attributes The element's attributes.
     * @param children The element's children.
     */
    public Element(String name, Attribute[] attributes, ElementBase[] children) {
        super(name);
        this.attributes = attributes;
        this.children = children;
    }

    /**
     * Marks a specified element as optional.
     */
    public Element markOptional() {
        this.optional = true;
        return this;
    }

    @Override
    protected void buildInto(Document document, Node parent) {
        org.w3c.dom.Element element = document.createElement(this.name);
        element.setTextContent(this.textContent);

        for (Attribute attribute : this.attributes) {
            element.setAttribute(attribute.name, attribute.value);
        }

        for (ElementBase child : this.children) {
            child.buildInto(document, element);
        }

        parent.appendChild(element);
    }

    /**
     * Returns a {@link org.w3c.dom.Element} of this {@link Element}, containing its text content or the recursively
     * created content of its children.
     *
     * @return A {@link org.w3c.dom.Element} of this {@link Element}, containing its associated content.
     */
    public org.w3c.dom.Element getElement() {
        return (org.w3c.dom.Element) this.getDocument().getElementsByTagName(this.name).item(0);
    }

    /**
     * Returns a {@link org.w3c.dom.Document} with this element as the root element, containing its text content or the
     * recursively created content of its children.
     *
     * @return A {@link org.w3c.dom.Document} with this element as its root element, containing its associated content.
     */
    public Document getDocument() {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
            System.exit(1);
        }

        Document document = dBuilder.newDocument();
        this.buildInto(document, document);

        return document;
    }

    @Override
    protected void parseFromParent(org.w3c.dom.Element parent) throws ParseException {
        org.w3c.dom.Element element = (org.w3c.dom.Element) parent.getElementsByTagName(this.name).item(0);

        if (element != null) {
            parseFromInternal(element);
        } else if (!this.optional) {
            throw new ParseException(String.format("Could not find required element %s", this.name));
        }
    }

    /**
     * True implementation of {@code parseFrom} which parses the element from an {@link org.w3c.dom.Element} rather than
     * a document.
     *
     * @param element The element to parse.
     * @throws ParseException The requested child elements and attributes could not be parsed from the provided element.
     */
    private void parseFromInternal(org.w3c.dom.Element element) throws ParseException {
        // If this element is intended only to contain child elements, it won't have a textContentOutput since there's
        // no text content anyway.
        if (this.textContentOutput != null) {
            this.textContentOutput.value = element.getTextContent();
        }

        for (Attribute attribute : this.attributes) {
            attribute.valueOutput.value = element.getAttribute(attribute.name);
        }

        for (ElementBase child : this.children) {
            child.parseFromParent(element);
        }
    }

    /**
     * Parses a {@link org.w3c.dom.Element}, populating relevant {@link StringOutput}s with their values.
     *
     * If a {@link ParseException} is not thrown, then it is guaranteed that all {@link StringOutput}s contain non-null
     * values.
     *
     * @param element The element to parse.
     * @throws ParseException The requested child elements and attributes could not be parsed from the provided element.
     */
    public void parseElement(org.w3c.dom.Element element) throws ParseException {
        parseFromInternal(element);
    }

    /**
     * Parses a {@link org.w3c.dom.Document}, populating relevant {@link StringOutput}s with their values.
     *
     * If a {@link ParseException} is not thrown, then it is guaranteed that all {@link StringOutput}s contain non-null
     * values.
     *
     * @param document The document to parse.
     * @throws ParseException The requested elements and attributes could not be parsed from the provided document.
     */
    public void parseDocument(Document document) throws ParseException {
        org.w3c.dom.Element rootElement = (org.w3c.dom.Element) document.getElementsByTagName(this.name).item(0);

        if (rootElement == null) {
            throw new ParseException(String.format("Could not find expected root element %s", this.name));
        }

        parseFromInternal(rootElement);
    }
}
