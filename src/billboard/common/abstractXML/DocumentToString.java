package billboard.common.abstractXML;

import org.w3c.dom.Document;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.StringWriter;

/**
 * Provides a convenient method for converting an XML document into its string representation.
 */
public class DocumentToString {
    private static Transformer transformer;

    static {
        try {
            transformer = TransformerFactory.newDefaultInstance().newTransformer();
        } catch (TransformerConfigurationException e) {
            // Our application is completely non-functional without a transformer. Quit here, because we can't run
            // anyway.
            System.err.println("Could not create required Transformer");
            e.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Converts an XML document to string, stripping newlines.
     *
     * @param document The document to convert into a string.
     * @return The string representation of the provided document.
     */
    public static String documentToString(Document document) {
        try {
            StringWriter stringWriter = new StringWriter();
            transformer.transform(new DOMSource(document), new StreamResult(stringWriter));
            return stringWriter.getBuffer().toString().replaceAll("\n|\r", "");
        } catch (TransformerException e) {
            e.printStackTrace();
            return null;
        }
    }
}
