package billboard.common.abstractXML;

import billboard.common.ParseException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.List;

/**
 * Several raw XML elements with identical names and text-only content, used for both document parsing and generation.
 */
public class RawElements extends ElementBase {
    private List<Element> elements;

    /**
     * Creates a set of raw elements with the specified name.
     *
     * The name is ignored when building a document/element, as the name of the element itself is used.
     *
     * @param name The element's tag name.
     * @param elements The contents of the elements to be created or parsed.
     */
    public RawElements(String name, List<Element> elements) {
        super(name);

        this.elements = elements;
    }

    @Override
    protected void buildInto(Document document, Node parent) {
        for (Element element : this.elements) {
            // A document can't add an element from another document without adopting it first.
            document.adoptNode(element);

            parent.appendChild(element);
        }
    }

    @Override
    protected void parseFromParent(Element parent) throws ParseException {
        NodeList childNodes = parent.getElementsByTagName(this.name);
        for (int i = 0; i < childNodes.getLength(); ++i) {
            Element child = (org.w3c.dom.Element) childNodes.item(i);
            this.elements.add(child);
        }
    }
}
