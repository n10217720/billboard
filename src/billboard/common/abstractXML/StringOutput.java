package billboard.common.abstractXML;

/**
 * A wrapper around {@link String} allowing a value to be returned to a caller.
 */
public class StringOutput {
    protected String value;

    /**
     * Returns the string value which has been stored.
     *
     * @return The string value that has been stored.
     */
    public String getValue() {
        return this.value;
    }
}
