package billboard.common.abstractXML;

import billboard.common.ParseException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 * An abstract XML base class, in which different XML element type classes derive from.
 */
public abstract class ElementBase {
    protected String name;

    /**
     * Creates an element with the specified name.
     *
     * @param name The element's tag name.
     */
    public ElementBase(String name) {
        this.name = name;
    }

    /**
     * Builds this element into the provided parent node, that is, creates a child of the parent node based on this
     * {@link Element}.
     *
     * @param document The document this element and the parent node are associated with.
     * @param parent The desired parent node for this element.
     */
    protected abstract void buildInto(Document document, Node parent);

    /**
     * Given an {@link org.w3c.dom.Element}, parses out the information required for this element (including any
     * children) from the child of the element with the correct name.
     *
     * @param parent The parent element to parse this element's data from.
     * @throws ParseException This element's attributes, children or text content could not be parsed.
     */
    protected abstract void parseFromParent(org.w3c.dom.Element parent) throws ParseException;
}
