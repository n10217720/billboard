package billboard.common.abstractXML;

import billboard.common.ParseException;
import billboard.common.TestUtils;
import org.junit.jupiter.api.Test;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TestAbstractXML {
    @Test
    public void getDocumentElementTextContent() {
        org.w3c.dom.Document d = new Element("name", "content").getDocument();

        org.w3c.dom.Element el = (org.w3c.dom.Element) d.getElementsByTagName("name").item(0);
        assertEquals("content", el.getTextContent());
    }

    @Test
    public void getDocumentElementTextContentAttributes() {
        org.w3c.dom.Document d = new Element("name", new Attribute[] {
                new Attribute("attrName1", "attrValue1"),
                new Attribute("attrName2", "attrValue2")
        }, "content").getDocument();

        org.w3c.dom.Element el = (org.w3c.dom.Element) d.getElementsByTagName("name").item(0);
        assertEquals("content", el.getTextContent());
        assertEquals("attrValue1", el.getAttribute("attrName1"));
        assertEquals("attrValue2", el.getAttribute("attrName2"));
    }

    @Test
    public void getDocumentElementChildren() {
        org.w3c.dom.Document d = new Element("name", new Element[] {
                new Element("child1", "childContent1"),
                new Element("child2", "childContent2"),
        }).getDocument();

        org.w3c.dom.Element elName = (org.w3c.dom.Element) d.getElementsByTagName("name").item(0);
        org.w3c.dom.Element elChild1 = (org.w3c.dom.Element) elName.getElementsByTagName("child1").item(0);
        org.w3c.dom.Element elChild2 = (org.w3c.dom.Element) elName.getElementsByTagName("child2").item(0);
        assertEquals("childContent1", elChild1.getTextContent());
        assertEquals("childContent2", elChild2.getTextContent());
    }

    @Test
    public void getDocumentElementChildrenAttributes() {
        org.w3c.dom.Document d = new Element(
                "name",
                new Attribute[] {
                        new Attribute("attrName1", "attrValue1"),
                        new Attribute("attrName2", "attrValue2")
                },
                new Element[] {
                        new Element("child1", "childContent1"),
                        new Element("child2", "childContent2")
                }
        ).getDocument();

        org.w3c.dom.Element elName = (org.w3c.dom.Element) d.getElementsByTagName("name").item(0);
        org.w3c.dom.Element elChild1 = (org.w3c.dom.Element) elName.getElementsByTagName("child1").item(0);
        org.w3c.dom.Element elChild2 = (org.w3c.dom.Element) elName.getElementsByTagName("child2").item(0);
        assertEquals("attrValue1", elName.getAttribute("attrName1"));
        assertEquals("attrValue2", elName.getAttribute("attrName2"));
        assertEquals("childContent1", elChild1.getTextContent());
        assertEquals("childContent2", elChild2.getTextContent());
    }

    @Test
    public void getDocumentElementNested() {
        org.w3c.dom.Document d = new Element("name", new Element[] {
                new Element("child", new Element[] {
                        new Element("grandchild", "grandchildContent")
                }),
        }).getDocument();

        org.w3c.dom.Element elName = (org.w3c.dom.Element) d.getElementsByTagName("name").item(0);
        org.w3c.dom.Element elChild = (org.w3c.dom.Element) elName.getElementsByTagName("child").item(0);
        org.w3c.dom.Element elGrandchild = (org.w3c.dom.Element) elChild.getElementsByTagName("grandchild").item(0);
        assertEquals("grandchildContent", elGrandchild.getTextContent());
    }

    @Test
    public void getDocumentElementsNone() {
        org.w3c.dom.Document d = new Element("name", new ElementBase[] {
                new Elements("child", List.of())
        }).getDocument();

        org.w3c.dom.Element elName = (org.w3c.dom.Element) d.getElementsByTagName("name").item(0);
        NodeList elChildList = elName.getElementsByTagName("child");
        assertEquals(0, elChildList.getLength());
    }

    @Test
    public void getDocumentElementsOne() {
        org.w3c.dom.Document d = new Element("name", new ElementBase[] {
                new Elements("child", List.of("content1"))
        }).getDocument();

        org.w3c.dom.Element elName = (org.w3c.dom.Element) d.getElementsByTagName("name").item(0);
        NodeList elChildList = elName.getElementsByTagName("child");
        assertEquals(1, elChildList.getLength());

        assertEquals("content1", elChildList.item(0).getTextContent());
    }

    @Test
    public void getElement() {
        org.w3c.dom.Element elName = new Element(
                "name",
                new Attribute[] {
                        new Attribute("attrName1", "attrValue1"),
                        new Attribute("attrName2", "attrValue2")
                },
                new Element[] {
                        new Element("child1", "childContent1"),
                        new Element("child2", "childContent2")
                }
        ).getElement();

        assertEquals("name", elName.getTagName());
        org.w3c.dom.Element elChild1 = (org.w3c.dom.Element) elName.getElementsByTagName("child1").item(0);
        org.w3c.dom.Element elChild2 = (org.w3c.dom.Element) elName.getElementsByTagName("child2").item(0);
        assertEquals("attrValue1", elName.getAttribute("attrName1"));
        assertEquals("attrValue2", elName.getAttribute("attrName2"));
        assertEquals("childContent1", elChild1.getTextContent());
        assertEquals("childContent2", elChild2.getTextContent());
    }

    @Test
    public void getDocumentElementsMany() {
        org.w3c.dom.Document d = new Element("name", new ElementBase[] {
                new Elements("child", List.of("content1", "content2", "content3"))
        }).getDocument();

        org.w3c.dom.Element elName = (org.w3c.dom.Element) d.getElementsByTagName("name").item(0);
        NodeList elChildList = elName.getElementsByTagName("child");
        assertEquals(3, elChildList.getLength());

        assertEquals("content1", elChildList.item(0).getTextContent());
        assertEquals("content2", elChildList.item(1).getTextContent());
        assertEquals("content3", elChildList.item(2).getTextContent());
    }

    @Test
    public void getDocumentRawElements() {
        org.w3c.dom.Element child1 = new Element("child", "content1").getElement();
        org.w3c.dom.Element child2 = new Element("child", "content2").getElement();
        org.w3c.dom.Element child3 = new Element("child", "content3").getElement();

        org.w3c.dom.Document d = new Element("name", new ElementBase[] {
                new RawElements("child", List.of(child1, child2, child3))
        }).getDocument();

        org.w3c.dom.Element elName = (org.w3c.dom.Element) d.getElementsByTagName("name").item(0);
        NodeList elChildList = elName.getElementsByTagName("child");
        assertEquals(3, elChildList.getLength());

        assertEquals("content1", elChildList.item(0).getTextContent());
        assertEquals("content2", elChildList.item(1).getTextContent());
        assertEquals("content3", elChildList.item(2).getTextContent());
    }

    @Test
    public void parseDocumentElementTextContent() throws ParseException {
        org.w3c.dom.Document d = TestUtils.fromXMLString("<name>content</name>");

        StringOutput textContent = new StringOutput();
        new Element("name", textContent).parseDocument(d);

        assertEquals("content", textContent.getValue());
    }

    @Test
    public void parseDocumentElementTextContentAttributes() throws ParseException {
        org.w3c.dom.Document d = TestUtils.fromXMLString(
                "<name attrName1=\"attrValue1\" attrName2=\"attrValue2\">content</name>"
        );

        StringOutput textContent = new StringOutput();
        StringOutput attrValue1 = new StringOutput();
        StringOutput attrValue2 = new StringOutput();
        new Element("name", new Attribute[] {
                new Attribute("attrName1", attrValue1),
                new Attribute("attrName2", attrValue2),
        }, textContent).parseDocument(d);

        assertEquals("content", textContent.getValue());
        assertEquals("attrValue1", attrValue1.getValue());
        assertEquals("attrValue2", attrValue2.getValue());
    }

    @Test
    public void parseDocumentElementChildren() throws ParseException {
        org.w3c.dom.Document d = TestUtils.fromXMLString(
                "<name><child1>childContent1</child1><child2>childContent2</child2></name>"
        );

        StringOutput childContent1 = new StringOutput();
        StringOutput childContent2 = new StringOutput();
        new Element("name", new Element[] {
                new Element("child1", childContent1),
                new Element("child2", childContent2),
        }).parseDocument(d);

        assertEquals("childContent1", childContent1.getValue());
        assertEquals("childContent2", childContent2.getValue());
    }

    @Test
    public void parseDocumentElementChildrenAttributes() throws ParseException {
        org.w3c.dom.Document d = TestUtils.fromXMLString(
                "<name attrName1=\"attrValue1\" attrName2=\"attrValue2\">" +
                        "<child1>childContent1</child1><child2>childContent2</child2>" +
                        "</name>"
        );

        StringOutput childContent1 = new StringOutput();
        StringOutput childContent2 = new StringOutput();
        new Element("name", new Element[] {
                new Element("child1", childContent1),
                new Element("child2", childContent2),
        }).parseDocument(d);

        assertEquals("childContent1", childContent1.getValue());
        assertEquals("childContent2", childContent2.getValue());
    }

    @Test
    public void parseDocumentElementOptionalChildMissing() throws ParseException {
        org.w3c.dom.Document d = TestUtils.fromXMLString(
                "<name attrName1=\"attrValue1\" attrName2=\"attrValue2\">" +
                        "<child1>childContent1</child1>" +
                        "</name>"
        );

        StringOutput childContent1 = new StringOutput();
        StringOutput childContent2 = new StringOutput();
        new Element("name", new Element[] {
                new Element("child1", childContent1),
                new Element("child2", childContent2).markOptional(),
        }).parseDocument(d);

        assertEquals("childContent1", childContent1.getValue());
        assertNull(childContent2.getValue());
    }

    @Test
    public void parseDocumentElementOptionalChildPresent() throws ParseException {
        org.w3c.dom.Document d = TestUtils.fromXMLString(
                "<name attrName1=\"attrValue1\" attrName2=\"attrValue2\">" +
                        "<child1>childContent1</child1><child2>childContent2</child2>" +
                        "</name>"
        );

        StringOutput childContent1 = new StringOutput();
        StringOutput childContent2 = new StringOutput();
        new Element("name", new Element[] {
                new Element("child1", childContent1),
                new Element("child2", childContent2).markOptional(),
        }).parseDocument(d);

        assertEquals("childContent1", childContent1.getValue());
        assertEquals("childContent2", childContent2.getValue());
    }

    @Test
    public void parseDocumentElementsNone() throws ParseException {
        org.w3c.dom.Document d = TestUtils.fromXMLString(
                "<name></name>"
        );

        ArrayList<String> childContents = new ArrayList<>();
        new Element("name", new ElementBase[] {
                new Elements("child", childContents)
        }).parseDocument(d);

        assertEquals(0, childContents.size());
    }

    @Test
    public void parseDocumentElementsOne() throws ParseException {
        org.w3c.dom.Document d = TestUtils.fromXMLString(
                "<name>" +
                        "<child>content1</child>" +
                        "</name>"
        );

        ArrayList<String> childContents = new ArrayList<>();
        new Element("name", new ElementBase[] {
                new Elements("child", childContents)
        }).parseDocument(d);

        assertEquals(1, childContents.size());
        assertEquals("content1", childContents.get(0));
    }

    @Test
    public void parseDocumentElementsMany() throws ParseException {
        org.w3c.dom.Document d = TestUtils.fromXMLString(
                "<name>" +
                        "<child>content1</child>" +
                        "<child>content2</child>" +
                        "<child>content3</child>" +
                        "</name>"
        );

        ArrayList<String> childContents = new ArrayList<>();
        new Element("name", new ElementBase[] {
                new Elements("child", childContents)
        }).parseDocument(d);

        assertEquals(3, childContents.size());
        assertEquals("content1", childContents.get(0));
        assertEquals("content2", childContents.get(1));
        assertEquals("content3", childContents.get(2));
    }

    @Test
    public void parseDocumentRawElements() throws ParseException {
        org.w3c.dom.Document d = TestUtils.fromXMLString(
                "<name>" +
                        "<child>content1</child>" +
                        "<child>content2</child>" +
                        "<child>content3</child>" +
                        "</name>"
        );

        ArrayList<org.w3c.dom.Element> children = new ArrayList<>();
        new Element("name", new ElementBase[] {
                new RawElements("child", children)
        }).parseDocument(d);

        assertEquals(3, children.size());
        assertEquals("content1", children.get(0).getTextContent());
        assertEquals("content2", children.get(1).getTextContent());
        assertEquals("content3", children.get(2).getTextContent());
    }

    @Test
    public void parseDocumentThrowsOnIncorrectRootElement() throws ParseException {
        org.w3c.dom.Document d = TestUtils.fromXMLString("<name>content</name>");

        StringOutput textContent = new StringOutput();
        assertThrows(ParseException.class, () -> {
            new Element("xname", textContent).parseDocument(d);
        }, "Could not find expected root element \"name\"");
    }

    @Test
    public void parseDocumentThrowsOnMissingChild() throws ParseException {
        org.w3c.dom.Document d = TestUtils.fromXMLString("<name><child></child></name>");

        StringOutput textContent = new StringOutput();
        assertThrows(ParseException.class, () -> {
            new Element("name", new Element[] {
                    new Element("xchild", textContent)
            }).parseDocument(d);
        }, "Could not find required element \"xchild\"");
    }

    @Test
    public void parseElement() throws ParseException {
        org.w3c.dom.Document d = TestUtils.fromXMLString(
                "<name attrName1=\"attrValue1\" attrName2=\"attrValue2\">" +
                        "<child1>childContent1</child1><child2>childContent2</child2>" +
                        "</name>"
        );

        org.w3c.dom.Element e = (org.w3c.dom.Element) d.getElementsByTagName("name").item(0);

        StringOutput childContent1 = new StringOutput();
        StringOutput childContent2 = new StringOutput();
        new Element("name", new Element[] {
                new Element("child1", childContent1),
                new Element("child2", childContent2),
        }).parseElement(e);

        assertEquals("childContent1", childContent1.getValue());
        assertEquals("childContent2", childContent2.getValue());
    }
}
