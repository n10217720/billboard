package billboard.common.api;

import org.w3c.dom.Document;

/**
 * An interface that marks a class as needing to be serialisable into XML.
 */
public interface XMLSerialisable {
    /**
     * Serialises this object, converting it to an XML document.
     *
     * @return This object as an XML document.
     */
    Document toXML();
}
