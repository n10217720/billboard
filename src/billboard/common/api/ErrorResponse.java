package billboard.common.api;

import billboard.common.ParseException;
import billboard.common.abstractXML.*;
import org.w3c.dom.Document;

import java.util.ArrayList;

/**
 * A class for responses in which a request may have failed, returning an error response document.
 */
public class ErrorResponse implements Response {
    private String message;

    /**
     * Creates a error response with a specified message.
     *
     * @param message The specific error response message.
     */
    public ErrorResponse(String message) {
        this.message = message;
    }

    /**
     * Returns the error message set for the response.
     *
     * @return The error message set for the response.
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * Serialise's this response into an XML document.
     * @see billboard.common.api.XMLSerialisable
     */
    @Override
    public Document toXML() {
        return new Element("response", new Attribute[] {
                new Attribute("status", "error")
        }, new Element[] {
                new Element("message", this.getMessage())
        }).getDocument();
    }

    /**
     * Deserialises an error response from XML.
     *
     * @param document The response XML document to deserialise.
     * @return The deserialised response.
     * @throws ParseException The response had an invalid type, or the document was in an invalid format.
     */
    public static ErrorResponse fromXML(Document document) throws ParseException {
        StringOutput status = new StringOutput();
        StringOutput message = new StringOutput();

        new Element("response",
                new Attribute[]{
                        new Attribute("status", status)
                },
                new Element[]{
                        new Element("message", message)
                }
        ).parseDocument(document);

        if(!status.getValue().equals("error")) {
            throw new ParseException(String.format("Invalid response status. Expected status was 'error', but received '%s'", status.getValue()));
        }

        return new ErrorResponse(message.getValue());
    }
}
