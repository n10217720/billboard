package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.abstractXML.Attribute;
import billboard.common.abstractXML.Element;
import billboard.common.abstractXML.StringOutput;
import billboard.common.api.Request;
import org.w3c.dom.Document;

/**
 * Method class for creating and serialising/de-serialising a deleteBillboard request.
 * Serialised request documents get sent to the server via the client.
 */
public class DeleteBillboardRequest implements Request<DeleteBillboardResponse> {

    private String sessionToken;
    private String billboardName;

    /**
     * Creates a delete billboard request.
     *
     * @param sessionToken The user's hashed and salted password.
     * @param billboardName The user's username.
     */
    public DeleteBillboardRequest(String sessionToken, String billboardName){
        this.sessionToken = sessionToken;
        this.billboardName = billboardName;
    }

    /**
     * Returns the session token for the user making the request.
     *
     * @return The session token for the user making the request.
     */
    public String getSessionToken(){
        return sessionToken;
    }

    /**
     * Returns the requested billboards name via String.
     *
     * @return The requested billboards name via String.
     */
    public String getBillboardName(){
        return billboardName;
    }

    /**
     * Serialise's this request into an XML document.
     * @see billboard.common.api.XMLSerialisable
     */
    @Override
    public Document toXML() {
        return new Element("request", new Attribute[] {
                new Attribute("type", "deleteBillboard"),
                new Attribute("sessionToken", this.getSessionToken())
        }, new Element[] {
                new Element("name", this.getBillboardName())
        }).getDocument();
    }

    /**
     * Deserialises a delete billboard request from XML.
     *
     * @param document The request XML document to deserialise.
     * @return The deserialised request.
     * @throws ParseException The request had an invalid type, or the document was in an invalid format.
     */
    public static DeleteBillboardRequest fromXML(Document document) throws ParseException {
        StringOutput type = new StringOutput();
        StringOutput sessionToken = new StringOutput();
        StringOutput billboardName = new StringOutput();

        new Element("request",
                new Attribute[]{
                        new Attribute("type", type),
                        new Attribute("sessionToken", sessionToken)
                },
                new Element[]{
                        new Element("name", billboardName)
                }
        ).parseDocument(document);

        if(!type.getValue().equals("deleteBillboard")) {
            throw new ParseException(String.format("Invalid request type. Expected type was 'deleteBillboard', but received '%s'", type.getValue()));
        }

        return new DeleteBillboardRequest(sessionToken.getValue(), billboardName.getValue());
    }

    /**
     * Returns an empty success response from XML
     * @see billboard.common.api.EmptySuccessResponse
     */
    @Override
    public DeleteBillboardResponse responseFromXML(Document document) {
        return new DeleteBillboardResponse();
    }


}
