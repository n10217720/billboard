package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.abstractXML.Attribute;
import billboard.common.abstractXML.Element;
import billboard.common.abstractXML.StringOutput;
import billboard.common.api.Request;
import org.w3c.dom.Document;

/**
 * Method class for creating and serialising/de-serialising a UserLogin request.
 * Serialised request documents get sent to the server via the client.
 */
public class UserLoginRequest implements Request<UserLoginResponse> {
    private String username;
    private String hashedPassword;

    /**
     * Creates a user login request.
     *
     * @param username The user's username.
     * @param hashedPassword The user's hashed and salted password.
     */
    public UserLoginRequest(String username, String hashedPassword) {
        this.username = username;
        this.hashedPassword = hashedPassword;
    }

    /**
     * Returns the username.
     *
     * @return The username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Returns the hashed password.
     *
     * @return The hashed password.
     */
    public String getHashedPassword() {
        return hashedPassword;
    }

    /**
     * Serialise's this request into an XML document.
     * @see billboard.common.api.XMLSerialisable
     */
    @Override
    public Document toXML() {
        return new Element("request",
                new Attribute[]{
                        new Attribute("type", "userLogin")
                },
                new Element[]{
                        new Element("username", this.getUsername()),
                        new Element("hashedPassword", this.getHashedPassword())
                }
        ).getDocument();
    }

    /**
     * Deserialises a user login request from XML.
     *
     * @param document The request XML document to deserialise.
     * @return The deserialised request.
     * @throws ParseException The request had an invalid type, or the document was in an invalid format.
     */
    public static UserLoginRequest fromXML(Document document) throws ParseException {
        StringOutput type = new StringOutput();
        StringOutput username = new StringOutput();
        StringOutput hashedPassword = new StringOutput();
        new Element("request",
                new Attribute[]{
                        new Attribute("type", type)
                },
                new Element[]{
                        new Element("username", username),
                        new Element("hashedPassword", hashedPassword),
                }
        ).parseDocument(document);

        if(!type.getValue().equals("userLogin")) {
            throw new ParseException(String.format("Invalid request type. Expected type was 'userLogin', but received '%s'", type.getValue()));
        }

        return new UserLoginRequest(username.getValue(), hashedPassword.getValue());
    }

    /**
     * Returns an appropriate for this request, response parsed from XML.
     * @see billboard.common.api.Request
     */
    @Override
    public UserLoginResponse responseFromXML(Document document) throws ParseException {
        return UserLoginResponse.fromXML(document);
    }
}
