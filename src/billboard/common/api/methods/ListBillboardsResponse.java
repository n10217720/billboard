package billboard.common.api.methods;

import billboard.common.BillboardMeta;
import billboard.common.ParseException;
import billboard.common.abstractXML.*;
import billboard.common.api.Response;
import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Method class for creating and serialising/de-serialising a ListBillboards response.
 * Serialised response documents get sent to the client via the server.
 */
public class ListBillboardsResponse implements Response {
    private List<BillboardMeta> billboards;

    /**
     * Creates a new list billboards response.
     *
     * @param billboards The metadata to return.
     */
    public ListBillboardsResponse(List<BillboardMeta> billboards) {
        this.billboards = billboards;
    }

    /**
     * Returns the billboard metadata.
     *
     * @return The billboard metadata.
     */
    public List<BillboardMeta> getBillboards() {
        return this.billboards;
    }

    /**
     * Serialise's this request into an XML document.
     * @see billboard.common.api.XMLSerialisable
     */
    @Override
    public Document toXML() {
        List<Element> elements = new ArrayList<>();
        for (BillboardMeta billboard : this.billboards) {
            elements.add(new Element("billboard", new Attribute[] {}, new Element[] {
                    new Element("name", billboard.getName()),
                    new Element("creator", billboard.getCreator()),
                    new Element("canEdit", String.valueOf(billboard.canEdit())),
                    new Element("canDelete", String.valueOf(billboard.canDelete()))
            }));
        }

        Element[] elementsArray = new Element[elements.size()];
        elements.toArray(elementsArray);

        return new Element("response", new Attribute[]{
                new Attribute("status", "success")
        }, elementsArray).getDocument();
    }

    public static ListBillboardsResponse fromXML(Document document) throws ParseException {
        StringOutput status = new StringOutput();
        List<org.w3c.dom.Element> billboards = new ArrayList<>();

        new Element("response",
                new Attribute[]{
                        new Attribute("status", status)
                },
                new ElementBase[]{
                        new RawElements("billboard", billboards)
                }
        ).parseDocument(document);

        if(!status.getValue().equals("success")) {
            throw new ParseException(String.format("Invalid response status. Expected status was 'success', but received '%s'", status.getValue()));
        }

        List<BillboardMeta> metadata = new ArrayList<>();

        for (org.w3c.dom.Element billboard : billboards) {
            StringOutput name = new StringOutput();
            StringOutput creator = new StringOutput();
            StringOutput canEdit = new StringOutput();
            StringOutput canDelete = new StringOutput();
            new Element("billboard", new Attribute[]{}, new ElementBase[] {
                    new Element("name", name),
                    new Element("creator", creator),
                    new Element("canEdit", canEdit),
                    new Element("canDelete", canDelete)
            }).parseElement(billboard);

            metadata.add(new BillboardMeta(
                    name.getValue(),
                    creator.getValue(),
                    canEdit.getValue().equals("true"),
                    canDelete.getValue().equals("true")
            ));
        }

        return new ListBillboardsResponse(metadata);
    }
}
