package billboard.common.api.methods;

import billboard.common.Billboard;
import billboard.common.ParseException;
import billboard.common.abstractXML.Attribute;
import billboard.common.abstractXML.DocumentToString;
import billboard.common.abstractXML.Element;
import billboard.common.abstractXML.StringOutput;
import billboard.common.api.Response;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Method class for creating and serialising/de-serialising a GetCurrentBillboard response.
 * Serialised response documents get sent to the client via the server.
 */
public class GetCurrentBillboardResponse implements Response {
    private Billboard billboard;

    /**
     * Creates a get current showing billboard response from a billboard's XML text.
     *
     * @param billboardText The current showing billboard's XML text.
     */
    public GetCurrentBillboardResponse(String billboardText) throws ParseException {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newDefaultInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document billboardDocument = db.parse(new ByteArrayInputStream(billboardText.getBytes(StandardCharsets.UTF_8)));

            this.billboard = Billboard.fromXML(billboardDocument);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            throw new ParseException("Could not parse billboard");
        }
    }

    /**
     * Creates a get current showing billboard response from a billboard object.
     *
     * @param billboard The current showing billboard object.
     */
    public GetCurrentBillboardResponse(Billboard billboard) {
        this.billboard = billboard;
    }

    /**
     * Returns the billboard that was retrieved.
     *
     * @return The billboard that was retrieved.
     */
    public Billboard getBillboard() {
        return billboard;
    }

    /**
     * Serialise's this request into an XML document.
     * @see billboard.common.api.XMLSerialisable
     */
    @Override
    public Document toXML() {
        String billboardText = DocumentToString.documentToString(this.billboard.toXML());

        return new Element("response",
                new Attribute[]{
                        new Attribute("status", "success")
                }, new Element[]{
                new Element("billboard", billboardText)
        }).getDocument();
    }

    /**
     * Deserialises a get current showing billboard response from XML.
     *
     * @param document The request XML document to deserialise.
     * @return The deserialised request.
     * @throws ParseException The request had an invalid type, or the document was in an invalid format.
     */
    public static GetCurrentBillboardResponse fromXML(Document document) throws ParseException {
        StringOutput status = new StringOutput();
        StringOutput billboardText = new StringOutput();

        new Element("response",
                new Attribute[]{
                        new Attribute("status", status)
                },
                new Element[]{
                        new Element("billboard", billboardText)
                }
        ).parseDocument(document);

        if(!status.getValue().equals("success")) {
            throw new ParseException(String.format("Invalid response status. Expected status was 'success', but received '%s'", status.getValue()));
        }

        return new GetCurrentBillboardResponse(billboardText.getValue());
    }
}
