package billboard.common.api.methods;

import billboard.common.BillboardMeta;
import billboard.common.ParseException;
import billboard.common.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestListBillboardsResponse {
    private ListBillboardsResponse response;
    private List<String> names;
    private List<String> creators;
    private List<Boolean> canEdits;
    private List<Boolean> canDeletes;
    private List<BillboardMeta> billboards;

    @BeforeEach
    public void beforeEach() {
        this.names = List.of("Billboard 1", "Billboard 2");
        this.creators = List.of("creator1", "creator2");
        this.canEdits = List.of(true, false);
        this.canDeletes = List.of(false, true);

        this.billboards = List.of(
                new BillboardMeta("Billboard 1", "creator1", true, false),
                new BillboardMeta("Billboard 2", "creator2", false, true)
        );

        this.response = new ListBillboardsResponse(this.billboards);
    }

    @Test
    public void getBillboards() {
        assertEquals(this.names, response.getBillboards().stream().map(BillboardMeta::getName).collect(Collectors.toList()));
        assertEquals(this.creators, response.getBillboards().stream().map(BillboardMeta::getCreator).collect(Collectors.toList()));
    }

    @Test
    public void toXML() {
        Document responseXML = this.response.toXML();

        Element responseEl = (Element) responseXML.getElementsByTagName("response").item(0);
        assertEquals("success", responseEl.getAttribute("status"));

        NodeList billboardEls = responseEl.getElementsByTagName("billboard");
        for (int i = 0; i < billboardEls.getLength(); ++i) {
            Element billboardEl = (Element) billboardEls.item(i);
            Element nameEl = (Element) billboardEl.getElementsByTagName("name").item(0);
            Element creatorEl = (Element) billboardEl.getElementsByTagName("creator").item(0);
            Element canEditEl = (Element) billboardEl.getElementsByTagName("canEdit").item(0);
            Element canDeleteEl = (Element) billboardEl.getElementsByTagName("canDelete").item(0);

            assertEquals(nameEl.getTextContent(), this.names.get(i));
            assertEquals(creatorEl.getTextContent(), this.creators.get(i));
            assertEquals(canEditEl.getTextContent(), this.canEdits.get(i).toString());
            assertEquals(canDeleteEl.getTextContent(), this.canDeletes.get(i).toString());
        }
    }

    @Test
    public void fromXML() throws ParseException {
        ListBillboardsResponse response = ListBillboardsResponse.fromXML(TestUtils.fromXMLString(
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                        "<response status=\"success\">\n" +
                        "\t<billboard>\n" +
                        "\t<name>Billboard 1</name>\n" +
                        "\t<creator>creator1</creator>\n" +
                        "\t<canEdit>true</canEdit>\n" +
                        "\t<canDelete>false</canDelete>\n" +
                        "\t</billboard>\n" +
                        "\t<billboard>\n" +
                        "\t<name>Billboard 2</name>\n" +
                        "\t<creator>creator2</creator>\n" +
                        "\t<canEdit>false</canEdit>\n" +
                        "\t<canDelete>true</canDelete>\n" +
                        "\t</billboard>\n" +
                        "</response>"
        ));

        assertEquals(this.names, response.getBillboards().stream().map(BillboardMeta::getName).collect(Collectors.toList()));
        assertEquals(this.creators, response.getBillboards().stream().map(BillboardMeta::getCreator).collect(Collectors.toList()));
        assertEquals(this.canEdits, response.getBillboards().stream().map(BillboardMeta::canEdit).collect(Collectors.toList()));
        assertEquals(this.canDeletes, response.getBillboards().stream().map(BillboardMeta::canDelete).collect(Collectors.toList()));
    }

    @Test
    public void fromXMLInvalidStatus() {
        assertThrows(ParseException.class, () -> {
            ListBillboardsResponse response = ListBillboardsResponse.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<response status=\"error\">\n" +
                            "\t<billboard>\n" +
                            "\t<name>Billboard 1</name>\n" +
                            "\t<creator>creator1</creator>\n" +
                            "\t<canEdit>true</canEdit>\n" +
                            "\t<canDelete>false</canDelete>\n" +
                            "\t</billboard>\n" +
                            "\t<billboard>\n" +
                            "\t<name>Billboard 2</name>\n" +
                            "\t<creator>creator2</creator>\n" +
                            "\t<canEdit>false</canEdit>\n" +
                            "\t<canDelete>true</canDelete>\n" +
                            "\t</billboard>\n" +
                            "</response>"
            ));
        });
    }

    @Test
    public void fromXMLNoResponseElement() {
        assertThrows(ParseException.class, () -> {
            ListBillboardsResponse response = ListBillboardsResponse.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<xresponse status=\"success\">\n" +
                            "\t<billboard>\n" +
                            "\t<name>Billboard 1</name>\n" +
                            "\t<creator>creator1</creator>\n" +
                            "\t<canEdit>true</canEdit>\n" +
                            "\t<canDelete>false</canDelete>\n" +
                            "\t</billboard>\n" +
                            "\t<billboard>\n" +
                            "\t<name>Billboard 2</name>\n" +
                            "\t<creator>creator2</creator>\n" +
                            "\t<canEdit>false</canEdit>\n" +
                            "\t<canDelete>true</canDelete>\n" +
                            "\t</billboard>\n" +
                            "</xresponse>"
            ));
        });
    }
}
