package billboard.common.api.methods;

import billboard.common.BillboardMeta;
import billboard.common.ParseException;
import billboard.common.ScheduledBillboard;
import billboard.common.abstractXML.*;
import billboard.common.api.Response;
import org.w3c.dom.Document;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Method class for creating and serialising/de-serialising a ViewSchedule response.
 * Serialised response documents get sent to the client via the server.
 */
public class ViewScheduleResponse implements Response {
    private List<ScheduledBillboard> scheduledBillboards;
    private List<String> billboardCreators;

    /**
     * Creates a new view schedule response.
     *
     * @param scheduledBillboards The scheduled billboards to return.
     * @param billboardCreators The scheduled billboard creators to return.
     */
    public ViewScheduleResponse(List<ScheduledBillboard> scheduledBillboards, List<String> billboardCreators){
        this.scheduledBillboards = scheduledBillboards;
        this.billboardCreators = billboardCreators;
    }

    /**
     * Returns the scheduled billboards.
     *
     * @return The list of scheduled billboard objects.
     */
    public List<ScheduledBillboard> getScheduledBillboards(){
        return scheduledBillboards;
    }

    /**
     * Returns the scheduled billboard creators.
     *
     * @return The list of scheduled billboard creators.
     */
    public List<String> getBillboardCreators(){
        return billboardCreators;
    }

    /**
     * Serialise's this request into an XML document.
     * @see billboard.common.api.XMLSerialisable
     */
    @Override
    public Document toXML() {
        List<Element> elements = new ArrayList<>();
        for(int i = 0; i < this.scheduledBillboards.size();  ++i){
            elements.add(new Element("entry", new Attribute[] {}, new Element[]{
                    new Element("billboardName", this.scheduledBillboards.get(i).getName()),
                    new Element("initialShowing", this.scheduledBillboards.get(i).getInitialShowing().toString()),
                    new Element("displayDuration", "" + this.scheduledBillboards.get(i).getDuration()),
                    new Element("recurrencePeriod", String.valueOf(this.scheduledBillboards.get(i).getRecurrencePeriod())),
                    new Element("creator", String.valueOf(this.billboardCreators.get(i)))
            }));
        }

        Element[] elementsArray = new Element[elements.size()];
        elements.toArray(elementsArray);

        return new Element("response", new Attribute[]{
                new Attribute("status", "success")
        }, elementsArray).getDocument();
    }

    /**
     * Deserialises a list users permissions response from XML.
     *
     * @param document The response XML document to deserialise.
     * @return The deserialised response.
     * @throws ParseException The request had an invalid type, or the document was in an invalid format.
     */
    public static ViewScheduleResponse fromXML(Document document) throws ParseException{
        StringOutput status = new StringOutput();
        List<org.w3c.dom.Element> scheduleEntries = new ArrayList<>();

        new Element("response",
                new Attribute[]{
                        new Attribute("status", status)
                },
                new ElementBase[]{
                        new RawElements("entry", scheduleEntries)
                }
        ).parseDocument(document);

        if(!status.getValue().equals("success")) {
            throw new ParseException(String.format("Invalid response status. Expected status was 'success', but received '%s'", status.getValue()));
        }

        List<ScheduledBillboard> scheduledBillboards = new ArrayList<>();
        List<String> creators = new ArrayList<>();

        for (org.w3c.dom.Element billboard : scheduleEntries) {
            StringOutput name = new StringOutput();
            StringOutput initialShowing = new StringOutput();
            StringOutput displayDuration = new StringOutput();
            StringOutput recurrencePeriod = new StringOutput();
            StringOutput creator = new StringOutput();
            new Element("entry", new Attribute[]{}, new ElementBase[] {
                    new Element("billboardName", name),
                    new Element("initialShowing", initialShowing),
                    new Element("displayDuration", displayDuration),
                    new Element("recurrencePeriod", recurrencePeriod),
                    new Element("creator", creator)
            }).parseElement(billboard);

            Duration recurrenceDuration = null;
            if(!recurrencePeriod.getValue().equals("null")){
                recurrenceDuration = Duration.parse(recurrencePeriod.getValue());
            }

            scheduledBillboards.add(new ScheduledBillboard(
                    name.getValue(),
                    ZonedDateTime.parse(initialShowing.getValue()),
                    Long.parseLong(displayDuration.getValue()),
                    recurrenceDuration
            ));
            creators.add(creator.getValue());
        }

        return new ViewScheduleResponse(scheduledBillboards, creators);
    }
}
