package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.TestUtils;
import billboard.server.Permission;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;

public class TestGetUserPermissionsResponse {
    private GetUserPermissionsResponse response;
    private HashSet<Permission> permissions;
    private ArrayList<String> permissionList;

    @BeforeEach
    public void beforeEach() {
        this.permissions = new HashSet<>(Arrays.asList(Permission.EDIT_USERS, Permission.CREATE_BILLBOARD));
        this.permissionList = new ArrayList<>(Arrays.asList("EDIT_USERS", "CREATE_BILLBOARD"));
        this.response = new GetUserPermissionsResponse(permissions);
    }

    @Test
    public void getPermission() {
        assertEquals(this.permissions, this.response.getPermissionList());
    }

    @Test
    public void toXML() {
        Document responseXML = this.response.toXML();

        Element responseEl = (Element) responseXML.getElementsByTagName("response").item(0);
        assertEquals("success", responseEl.getAttribute("status"));

        Element permissionsEl = (Element) responseEl.getElementsByTagName("permissions").item(0);
        assertEquals(2, permissionsEl.getChildNodes().getLength());

        NodeList children = permissionsEl.getElementsByTagName("permission");
        for (int i = 0; i < children.getLength(); ++i) {
            Element child = (Element) children.item(i);
            assertTrue(this.permissionList.contains(child.getTextContent()));
        }

    }
    @Test
    public void fromXML() throws ParseException {
        GetUserPermissionsResponse response = GetUserPermissionsResponse.fromXML(TestUtils.fromXMLString(
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                        "<response status=\"success\">\n" +
                            "\t<permissions>\n" +
                                "\t<permission>EDIT_USERS</permission>\n" +
                                "\t<permission>CREATE_BILLBOARD</permission>\n" +
                            "\t</permissions>\n" +
                        "</response>"
        ));

        assertEquals(this.permissions, response.getPermissionList());
    }

    @Test
    public void fromXMLInvalidStatus() {
        assertThrows(ParseException.class, () -> {
            GetUserPermissionsResponse response = GetUserPermissionsResponse.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<response status=\"error\">\n" +
                                "\t<permissions>\n" +
                                    "\t<permission>EDIT_USERS</permission>\n" +
                                    "\t<permission>CREATE_BILLBOARD</permission>\n" +
                                "\t</permissions>\n" +
                            "</response>"
            ));
        });
    }

    @Test
    public void fromXMLNoResponseElement() {
        assertThrows(ParseException.class, () -> {
            GetUserPermissionsResponse response = GetUserPermissionsResponse.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<xresponse status=\"success\">\n" +
                                "\t<permissions>\n" +
                                    "\t<permission>EDIT_USERS</permission>\n" +
                                    "\t<permission>CREATE_BILLBOARD</permission>\n" +
                                "\t</permissions>\n" +
                            "</xresponse>"
            ));
        });
    }

    @Test
    public void fromXMLNoPermissionsElement() {
        assertThrows(ParseException.class, () -> {
            GetUserPermissionsResponse response = GetUserPermissionsResponse.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<response status=\"success\">\n" +
                                "\t<xpermissions>\n" +
                                    "\t<permission>EDIT_USERS</permission>\n" +
                                    "\t<permission>CREATE_BILLBOARD</permission>\n" +
                                "\t</xpermissions>\n" +
                            "</response>"
            ));
        });
    }
}
