package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestListUsersResponse {
    private ListUsersResponse response;
    private ArrayList<String> usernames;

    @BeforeEach
    public void beforeEach() {
        this.usernames = new ArrayList<>(Arrays.asList("user", "admin"));
        this.response = new ListUsersResponse(usernames);
    }

    @Test
    public void getUsernames() {
        assertEquals(this.usernames, this.response.getUsernames());
    }

    @Test
    public void toXML() {
        Document responseXML = this.response.toXML();

        Element responseEl = (Element) responseXML.getElementsByTagName("response").item(0);
        assertEquals("success", responseEl.getAttribute("status"));

        Element usernamesEl = (Element) responseEl.getElementsByTagName("usernames").item(0);
        assertEquals(2, usernamesEl.getChildNodes().getLength());

        NodeList children = usernamesEl.getElementsByTagName("username");
        for (int i = 0; i < children.getLength(); ++i) {
            Element child = (Element) children.item(i);
            assertTrue(this.usernames.contains(child.getTextContent()));
        }

    }

    @Test
    public void fromXML() throws ParseException {
        ListUsersResponse response = ListUsersResponse.fromXML(TestUtils.fromXMLString(
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                        "<response status=\"success\">\n" +
                            "\t<usernames>\n" +
                                "\t<username>user</username>\n" +
                                "\t<username>admin</username>\n" +
                            "\t</usernames>\n" +
                        "</response>"
        ));

        assertEquals(this.usernames, response.getUsernames());
    }

    @Test
    public void fromXMLInvalidStatus() {
        assertThrows(ParseException.class, () -> {
            ListUsersResponse response = ListUsersResponse.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<response status=\"error\">\n" +
                                "\t<usernames>\n" +
                                    "\t<username>user</username>\n" +
                                    "\t<username>admin</username>\n" +
                                "\t</usernames>\n" +
                            "</response>"
            ));
        });
    }

    @Test
    public void fromXMLNoResponseElement() {
        assertThrows(ParseException.class, () -> {
            ListUsersResponse response = ListUsersResponse.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<xresponse status=\"success\">\n" +
                                "\t<usernames>\n" +
                                    "\t<username>user</username>\n" +
                                    "\t<username>admin</username>\n" +
                                "\t</usernames>\n" +
                            "</xresponse>"
            ));
        });
    }

    @Test
    public void fromXMLNoUsernamesElement() {
        assertThrows(ParseException.class, () -> {
            ListUsersResponse response = ListUsersResponse.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<response status=\"success\">\n" +
                                "\t<xusernames>\n" +
                                    "\t<username>user</username>\n" +
                                    "\t<username>admin</username>\n" +
                                "\t</xusernames>\n" +
                            "</response>"
            ));
        });
    }

}
