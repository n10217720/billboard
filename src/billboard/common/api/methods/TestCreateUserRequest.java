package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.TestUtils;
import billboard.server.Permission;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestCreateUserRequest {
    private CreateUserRequest request;
    private HashSet<Permission> permissions;
    private ArrayList<String> permissionListString;

    @BeforeEach
    public void beforeEach() {
        this.permissions = new HashSet<>(Arrays.asList(Permission.EDIT_USERS, Permission.CREATE_BILLBOARD));
        this.permissionListString = new ArrayList<>(Arrays.asList("EDIT_USERS", "CREATE_BILLBOARD"));
        this.request = new CreateUserRequest("ABCDEF", "username",
                "5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8", this.permissions);
    }

    @Test
    public void getUsername() {
        assertEquals("username", this.request.getUsername());
    }

    @Test
    public void getSessionToken() {
        assertEquals("ABCDEF", this.request.getSessionToken());
    }

    @Test
    public void getHashedPassword(){
        assertEquals("5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8", this.request.getHashedPassword());
    }

    @Test
    public void getPermissions(){
        assertEquals(this.permissions, this.request.getPermissions());
    }

    @Test
    public void toXML() {
        Document requestXml = this.request.toXML();

        Element requestEl = (Element) requestXml.getElementsByTagName("request").item(0);
        assertEquals("createUser", requestEl.getAttribute("type"));
        assertEquals("ABCDEF", requestEl.getAttribute("sessionToken"));

        Element usernameEl = (Element) requestEl.getElementsByTagName("username").item(0);
        assertEquals("username", usernameEl.getTextContent());

        Element hashedPasswordEl = (Element) requestEl.getElementsByTagName("hashedPassword").item(0);
        assertEquals("5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8", hashedPasswordEl.getTextContent());

        Element permissionsEl = (Element) requestEl.getElementsByTagName("permissions").item(0);
        assertEquals(2, permissionsEl.getChildNodes().getLength());

        NodeList children = permissionsEl.getElementsByTagName("permission");
        for (int i = 0; i < children.getLength(); ++i) {
            Element child = (Element) children.item(i);
            assertTrue(this.permissionListString.contains(child.getTextContent()));
        }
    }

    @Test
    public void fromXML() throws ParseException {
        CreateUserRequest request = CreateUserRequest.fromXML(TestUtils.fromXMLString(
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                        "<request type=\"createUser\" sessionToken=\"ABCDEF\">\n" +
                        "\t<username>username</username>\n" +
                        "\t<hashedPassword>5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8</hashedPassword>\n" +
                        "\t<permissions>\n" +
                        "\t<permission>EDIT_USERS</permission>\n" +
                        "\t<permission>CREATE_BILLBOARD</permission>\n" +
                        "\t</permissions>\n" +
                        "</request>"
        ));

        assertEquals("username", request.getUsername());
        assertEquals("ABCDEF", request.getSessionToken());
        assertEquals("5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8", request.getHashedPassword());
        assertEquals(this.permissions, request.getPermissions());
    }

    @Test
    public void fromXMLInvalidType() {
        assertThrows(ParseException.class, () -> {
            CreateUserRequest request = CreateUserRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"invalid\" sessionToken=\"ABCDEF\">\n" +
                            "\t<username>username</username>\n" +
                            "\t<hashedPassword>5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8</hashedPassword>\n" +
                            "\t<permissions>\n" +
                            "\t<permission>EDIT_USERS</permission>\n" +
                            "\t<permission>CREATE_BILLBOARD</permission>\n" +
                            "\t</permissions>\n" +
                            "</request>"
            ));
        });
    }

    @Test
    public void fromXMLNoRequestElement() {
        assertThrows(ParseException.class, () -> {
            CreateUserRequest request = CreateUserRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<xrequest type=\"createUser\" sessionToken=\"ABCDEF\">\n" +
                            "\t<username>username</username>\n" +
                            "\t<hashedPassword>5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8</hashedPassword>\n" +
                            "\t<permissions>\n" +
                            "\t<permission>EDIT_USERS</permission>\n" +
                            "\t<permission>CREATE_BILLBOARD</permission>\n" +
                            "\t</permissions>\n" +
                            "</xrequest>"
            ));
        });
    }

    @Test
    public void fromXMLNoUsernameElement() {
        assertThrows(ParseException.class, () -> {
            CreateUserRequest request = CreateUserRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"createUser\" sessionToken=\"ABCDEF\">\n" +
                            "\t<xusername>username</xusername>\n" +
                            "\t<hashedPassword>5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8</hashedPassword>\n" +
                            "\t<permissions>\n" +
                            "\t<permission>EDIT_USERS</permission>\n" +
                            "\t<permission>CREATE_BILLBOARD</permission>\n" +
                            "\t</permissions>\n" +
                            "</request>"
            ));
        });
    }

    @Test
    public void fromXMLNoHashedPasswordElement() {
        assertThrows(ParseException.class, () -> {
            CreateUserRequest request = CreateUserRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"createUser\" sessionToken=\"ABCDEF\">\n" +
                            "\t<username>username</username>\n" +
                            "\t<xhashedPassword>5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8</xhashedPassword>\n" +
                            "\t<permissions>\n" +
                            "\t<permission>EDIT_USERS</permission>\n" +
                            "\t<permission>CREATE_BILLBOARD</permission>\n" +
                            "\t</permissions>\n" +
                            "</request>"
            ));
        });
    }

    @Test
    public void fromXMLNoPermissionsElement() {
        assertThrows(ParseException.class, () -> {
            CreateUserRequest request = CreateUserRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"createUser\" sessionToken=\"ABCDEF\">\n" +
                            "\t<username>username</username>\n" +
                            "\t<hashedPassword>5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8</hashedPassword>\n" +
                            "\t<xpermissions>\n" +
                            "\t<permission>EDIT_USERS</permission>\n" +
                            "\t<permission>CREATE_BILLBOARD</permission>\n" +
                            "\t</xpermissions>\n" +
                            "</request>"
            ));
        });
    }
}
