package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.abstractXML.Attribute;
import billboard.common.abstractXML.Element;
import billboard.common.abstractXML.StringOutput;
import billboard.common.api.Request;
import org.w3c.dom.Document;

/**
 * Method class for creating and serialising/de-serialising a ListBillboards request.
 * Serialised request documents get sent to the server via the client.
 */
public class ListBillboardsRequest implements Request<ListBillboardsResponse> {
    private String sessionToken;

    /**
     * Creates a new list billboards request.
     *
     * @param sessionToken The session token of the user making the request.
     */
    public ListBillboardsRequest(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    /**
     * Returns the session token for the user making the request.
     *
     * @return The session token for the user making the request.
     */
    public String getSessionToken() {
        return sessionToken;
    }

    /**
     * Serialise's this request into an XML document.
     * @see billboard.common.api.XMLSerialisable
     */
    @Override
    public Document toXML() {
        return new Element("request", new Attribute[] {
                new Attribute("type", "listBillboards"),
                new Attribute("sessionToken", this.getSessionToken()),
        }, new Element[]{}).getDocument();
    }

    public static ListBillboardsRequest fromXML(Document document) throws ParseException {
        StringOutput type = new StringOutput();
        StringOutput sessionToken = new StringOutput();

        new Element("request", new Attribute[]{
                new Attribute("type", type),
                new Attribute("sessionToken", sessionToken)
        } ,new Element[]{}).parseDocument(document);

        if(!type.getValue().equals("listBillboards")) {
            throw new ParseException(String.format("Invalid request type. Expected type was 'listBillboards', but received '%s'", type.getValue()));
        }

        return new ListBillboardsRequest(sessionToken.getValue());
    }

    /**
     * Returns an appropriate for this request, response parsed from XML.
     * @see billboard.common.api.Request
     */
    @Override
    public ListBillboardsResponse responseFromXML(Document document) throws ParseException {
        return ListBillboardsResponse.fromXML(document);
    }
}
