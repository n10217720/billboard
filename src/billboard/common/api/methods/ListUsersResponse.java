package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.abstractXML.*;
import billboard.common.api.Response;
import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.List;

/**
 * Method class for creating and serialising/de-serialising a ListUsers response.
 * Serialised response documents get sent to the client via the server.
 */
public class ListUsersResponse implements Response {
    private List<String> usernames;

    /**
     * Creates a new list users response.
     *
     * @param usernames The list of all usernames in the system.
     */
    public ListUsersResponse(List<String> usernames){
        this.usernames = usernames;
    }

    /**
     * Returns the list of all usernames in the system.
     *
     * @return The list of all usernames in the system.
     */
    public List<String> getUsernames(){
        return usernames;
    }

    /**
     * Serialise's this request into an XML document.
     * @see billboard.common.api.XMLSerialisable
     */
    @Override
    public Document toXML() {
        return new Element("response",
                new Attribute[]{
                        new Attribute("status", "success")
                }, new Element[]{
                new Element("usernames", new ElementBase[]{
                        new Elements("username", getUsernames())
                })
        }).getDocument();
    }

    /**
     * Deserialises a list users permissions response from XML.
     *
     * @param document The response XML document to deserialise.
     * @return The deserialised response.
     * @throws ParseException The request had an invalid type, or the document was in an invalid format.
     */
    public static ListUsersResponse fromXML(Document document) throws ParseException{
        StringOutput status = new StringOutput();
        ArrayList<String> users = new ArrayList<>();

        new Element("response",
                new Attribute[]{
                        new Attribute("status", status)
                },
                new Element[]{
                        new Element("usernames", new ElementBase[]{
                                new Elements("username", users)
                        })
                }
        ).parseDocument(document);

        if(!status.getValue().equals("success")) {
            throw new ParseException(String.format("Invalid response status. Expected status was 'success', but received '%s'", status.getValue()));
        }

        return new ListUsersResponse(users);
    }
}
