package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.ScheduledBillboard;
import billboard.common.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

//Kept some of the tests here just in case, but some are probs unnecessary (we really only need a sessionToken, billboardName and time)
public class TestRemoveFromScheduleRequest {
    private RemoveFromScheduleRequest request;
    private ScheduledBillboard billboard;
    private ZonedDateTime initialShowing = ZonedDateTime.of(2020, 10, 4, 1, 45, 0, 0, ZoneId.systemDefault());
    private long duration = 5;
    private Duration occurrence = Duration.ofHours(23);

    @BeforeEach
    public void beforeEach() {
        billboard = new ScheduledBillboard("billboard", initialShowing, duration, occurrence);

        this.request = new RemoveFromScheduleRequest("ABCDEF", billboard);
    }

    @Test
    public void getSessionToken() {
        assertEquals("ABCDEF", this.request.getSessionToken());
    }

    @Test
    public void getScheduledBillboardRepeating(){
        assertEquals(billboard, this.request.getScheduledBillboard());
    }


    @Test
    public void toXML() {
        Document requestXML = this.request.toXML();

        Element requestEl = (Element) requestXML.getElementsByTagName("request").item(0);
        assertEquals("removeFromSchedule", requestEl.getAttribute("type"));
        assertEquals("ABCDEF", requestEl.getAttribute("sessionToken"));

        Element nameEl = (Element) requestEl.getElementsByTagName("name").item(0);
        assertEquals("billboard", nameEl.getTextContent());

        Element initialShowingEl = (Element) requestEl.getElementsByTagName("initialShowing").item(0);
        assertEquals("2020-10-04T01:45+10:00[Australia/Brisbane]", initialShowingEl.getTextContent());

        Element durationEl = (Element) requestEl.getElementsByTagName("displayDuration").item(0);
        assertEquals("" + duration, durationEl.getTextContent());

        Element recurrencePeriodEl = (Element) requestEl.getElementsByTagName("recurrencePeriod").item(0);
        assertEquals(occurrence.toString(), recurrencePeriodEl.getTextContent());

    }


    @Test
    public void fromXML() throws ParseException {
        RemoveFromScheduleRequest request = RemoveFromScheduleRequest.fromXML(TestUtils.fromXMLString(
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                        "<request type=\"removeFromSchedule\" sessionToken=\"ABCDEF\">\n" +
                        "\t<name>billboard</name>\n" +
                        "\t<initialShowing>2020-10-04T01:45+10:00[Australia/Brisbane]</initialShowing>\n" +
                        "\t<displayDuration>5</displayDuration>\n" +
                        "\t<recurrencePeriod>PT23H</recurrencePeriod>\n" +
                        "</request>"
        ));
        assertEquals("ABCDEF", request.getSessionToken());
        assertEquals("billboard", request.getScheduledBillboard().getName());
        assertEquals("2020-10-04T01:45+10:00[Australia/Brisbane]", request.getScheduledBillboard().getInitialShowing().toString());
        assertEquals("5", "" + request.getScheduledBillboard().getDuration());
        assertEquals("PT23H", request.getScheduledBillboard().getRecurrencePeriod().toString());
    }

    @Test
    public void fromXMLInvalidType() {
        assertThrows(ParseException.class, () -> {
            RemoveFromScheduleRequest request = RemoveFromScheduleRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"invalid\" sessionToken=\"ABCDEF\">\n" +
                            "\t<name>billboard</name>\n" +
                            "\t<initialShowing>2020-10-04T01:45+10:00[Australia/Brisbane]</initialShowing>\n" +
                            "\t<displayDuration>5</displayDuration>\n" +
                            "\t<recurrencePeriod>null</recurrencePeriod>\n" +
                            "</request>"
            ));
        });
    }

    @Test
    public void fromXMLNoRequestElement() {
        assertThrows(ParseException.class, () -> {
            RemoveFromScheduleRequest request = RemoveFromScheduleRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<xrequest type=\"removeFromSchedule\" sessionToken=\"ABCDEF\">\n" +
                            "\t<name>billboard</name>\n" +
                            "\t<initialShowing>2020-10-04T01:45+10:00[Australia/Brisbane]</initialShowing>\n" +
                            "\t<displayDuration>5</displayDuration>\n" +
                            "\t<recurrencePeriod>null</recurrencePeriod>\n" +
                            "</xrequest>"
            ));
        });
    }

    @Test
    public void fromXMLNoNameElement() {
        assertThrows(ParseException.class, () -> {
            RemoveFromScheduleRequest request = RemoveFromScheduleRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"removeFromSchedule\" sessionToken=\"ABCDEF\">\n" +
                            "\t<xname>billboard</xname>\n" +
                            "\t<initialShowing>2020-10-04T01:45+10:00[Australia/Brisbane]</initialShowing>\n" +
                            "\t<displayDuration>5</displayDuration>\n" +
                            "\t<recurrencePeriod>null</recurrencePeriod>\n" +
                            "</request>"
            ));
        });
    }

    @Test
    public void fromXMLInitialShowingElement() {
        assertThrows(ParseException.class, () -> {
            RemoveFromScheduleRequest request = RemoveFromScheduleRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"removeFromSchedule\" sessionToken=\"ABCDEF\">\n" +
                            "\t<name>billboard</name>\n" +
                            "\t<xinitialShowing>2020-10-04T01:45+10:00[Australia/Brisbane]</xinitialShowing>\n" +
                            "\t<displayDuration>5</displayDuration>\n" +
                            "\t<recurrencePeriod>null</recurrencePeriod>\n" +
                            "</request>"
            ));
        });
    }

    @Test
    public void fromXMLNoDisplayDurationElement() {
        assertThrows(ParseException.class, () -> {
            RemoveFromScheduleRequest request = RemoveFromScheduleRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"removeFromSchedule\" sessionToken=\"ABCDEF\">\n" +
                            "\t<name>billboard</name>\n" +
                            "\t<initialShowing>2020-10-04T01:45+10:00[Australia/Brisbane]</initialShowing>\n" +
                            "\t<xdisplayDuration>5</xdisplayDuration>\n" +
                            "\t<recurrencePeriod>null</recurrencePeriod>\n" +
                            "</request>"
            ));
        });
    }

    @Test
    public void fromXMLNoRecurrencePeriodElement() {
        assertThrows(ParseException.class, () -> {
            RemoveFromScheduleRequest request = RemoveFromScheduleRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"removeFromSchedule\" sessionToken=\"ABCDEF\">\n" +
                            "\t<name>billboard</name>\n" +
                            "\t<initialShowing>2020-10-04T01:45+10:00[Australia/Brisbane]</initialShowing>\n" +
                            "\t<displayDuration>5</displayDuration>\n" +
                            "\t<xrecurrencePeriod>null</xrecurrencePeriod>\n" +
                            "</request>"
            ));
        });
    }
}
