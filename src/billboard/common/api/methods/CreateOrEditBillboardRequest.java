package billboard.common.api.methods;

import billboard.common.Billboard;
import billboard.common.ParseException;
import billboard.common.abstractXML.Attribute;
import billboard.common.abstractXML.DocumentToString;
import billboard.common.abstractXML.Element;
import billboard.common.abstractXML.StringOutput;
import billboard.common.api.Request;
import org.w3c.dom.Document;

/**
 * Method class for creating and serialising/de-serialising a CreateOrEditBillboard request.
 * Serialised request documents get sent to the server via the client.
 */
public class CreateOrEditBillboardRequest implements Request<CreateOrEditBillboardResponse> {
    private String sessionToken;
    private String name;
    private String billboard;

    /**
     * Creates a new create or edit billboard request.
     *
     * @param sessionToken The session token of the logged in user.
     * @param name The name of the billboard.
     * @param billboard The billboard itself.
     */
    public CreateOrEditBillboardRequest(String sessionToken, String name, Billboard billboard) {
        this.sessionToken = sessionToken;
        this.name = name;
        this.billboard = DocumentToString.documentToString(billboard.toXML());
    }

    /**
     * Creates a new create or edit billboard request.
     *
     * @param sessionToken The session token of the logged in user.
     * @param name The name of the billboard.
     * @param billboard The billboard's XML string.
     */
    public CreateOrEditBillboardRequest(String sessionToken, String name, String billboard) {
        this.sessionToken = sessionToken;
        this.name = name;
        this.billboard = billboard;
    }

    /**
     * Returns the session token for the user making the request.
     *
     * @return The session token for the user making the request.
     */
    public String getSessionToken() {
        return sessionToken;
    }

    /**
     * Returns the billboard's name.
     *
     * @return The billboard's name.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the billboard, as an XML string.
     *
     * @return The billboard, as an XML string.
     */
    public String getBillboard() {
        return billboard;
    }

    /**
     * Deserialises a CreateOrEditBillboard request from XML.
     *
     * @param document The request XML document to deserialise.
     * @return The deserialised request.
     * @throws ParseException The request had an invalid type, or the document was in an invalid format.
     */
    public static CreateOrEditBillboardRequest fromXML(Document document) throws ParseException {
        StringOutput type = new StringOutput();
        StringOutput name = new StringOutput();
        StringOutput billboard = new StringOutput();
        StringOutput sessionToken = new StringOutput();
        new Element("request",
                new Attribute[]{
                        new Attribute("type", type),
                        new Attribute("sessionToken", sessionToken)
                },
                new Element[]{
                        new Element("name", name),
                        new Element("billboard", billboard)
                }
        ).parseDocument(document);

        if(!type.getValue().equals("createOrEditBillboard")){
            throw new ParseException(String.format("Invalid request type. Expected type was 'createOrEditBillboard', but received '%s'", type.getValue()));
        }

        return new CreateOrEditBillboardRequest(sessionToken.getValue(), name.getValue(), billboard.getValue());
    }

    /**
     * Serialise's this request into an XML document.
     * @see billboard.common.api.XMLSerialisable
     */
    @Override
    public Document toXML() {
        return new Element("request",
                new Attribute[]{
                        new Attribute("type", "createOrEditBillboard"),
                        new Attribute("sessionToken", this.getSessionToken())
                },
                new Element[]{
                        new Element("name", this.getName()),
                        new Element("billboard", this.getBillboard())
                }
        ).getDocument();
    }

    /**
     * Returns an empty success response from XML
     * @see billboard.common.api.EmptySuccessResponse
     */
    @Override
    public CreateOrEditBillboardResponse responseFromXML(Document document) {
        return new CreateOrEditBillboardResponse();
    }
}
