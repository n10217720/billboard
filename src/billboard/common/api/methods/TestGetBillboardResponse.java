package billboard.common.api.methods;

import billboard.common.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

public class TestGetBillboardResponse {
    private static final String BILLBOARD_XML_TEXT = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><billboard background=\"#FF0000\"><message colour=\"#FFFFFF\">Message</message></billboard>";
    private static final String BILLBOARD_XML_TEXT_ESCAPED = "&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;no&quot;?&gt;&lt;billboard background=&quot;#FF0000&quot;&gt;&lt;message colour=&quot;#FFFFFF&quot;&gt;Message&lt;/message&gt;&lt;/billboard&gt;";
    private Billboard billboard;
    private GetBillboardResponse response;

    @BeforeEach
    public void beforeEach() throws ParseException {
        this.billboard = new Billboard();
        this.billboard.backgroundColour = Color.RED;
        this.billboard.message = new ColouredText("Message", Color.WHITE);
        this.response = new GetBillboardResponse(BILLBOARD_XML_TEXT);
    }

    @Test
    public void getBillboard() {
        assertEquals(Color.RED, response.getBillboard().backgroundColour);
        assertEquals("Message", response.getBillboard().message.text);
        assertEquals(Color.WHITE, response.getBillboard().message.colour);
        assertNull(response.getBillboard().picture);
        assertNull(response.getBillboard().information);
    }

    @Test
    public void toXML() {
        Document responseXML = this.response.toXML();

        Element responseEl = (Element) responseXML.getElementsByTagName("response").item(0);
        assertEquals("success", responseEl.getAttribute("status"));

        Element billboardEl = (Element) responseEl.getElementsByTagName("billboard").item(0);
        assertEquals(BILLBOARD_XML_TEXT, billboardEl.getTextContent());
    }

    @Test
    public void fromXML() throws ParseException {
        GetBillboardResponse response = GetBillboardResponse.fromXML(TestUtils.fromXMLString(
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                        "<response status=\"success\">\n" +
                        "\t<billboard>" + BILLBOARD_XML_TEXT_ESCAPED + "</billboard>\n" +
                        "</response>"
        ));

        Billboard billboard = response.getBillboard();
        assertEquals(Color.RED, billboard.backgroundColour);
        assertEquals("Message", billboard.message.text);
        assertEquals(Color.WHITE, billboard.message.colour);
        assertNull(billboard.picture);
        assertNull(billboard.information);
    }

    @Test
    public void fromXMLInvalidStatus() {
        assertThrows(ParseException.class, () -> {
            GetBillboardResponse response = GetBillboardResponse.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<response status=\"error\">\n" +
                            "\t<billboard>" + BILLBOARD_XML_TEXT_ESCAPED + "</billboard>\n" +
                            "</response>"
            ));
        });
    }

    @Test
    public void fromXMLNoBillboardElement() {
        assertThrows(ParseException.class, () -> {
            GetBillboardResponse response = GetBillboardResponse.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<response status=\"success\">\n" +
                            "</response>"
            ));
        });
    }

    @Test
    public void fromXMLNoResponseElement() {
        assertThrows(ParseException.class, () -> {
            GetBillboardResponse response = GetBillboardResponse.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<xresponse status=\"success\">\n" +
                            "\t<billboard>" + BILLBOARD_XML_TEXT_ESCAPED + "</billboard>\n" +
                            "</xresponse>"
            ));
        });
    }
}
