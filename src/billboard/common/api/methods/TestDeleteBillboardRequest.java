package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestDeleteBillboardRequest {
    private DeleteBillboardRequest request;

    @BeforeEach
    public void beforeEach() {
        this.request = new DeleteBillboardRequest("ABCDEF", "billboard");
    }

    @Test
    public void getBillboardName() {
        assertEquals("billboard", this.request.getBillboardName());
    }

    @Test
    public void getSessionToken() {
        assertEquals("ABCDEF", this.request.getSessionToken());
    }

    @Test
    public void toXML() {
        Document requestXml = this.request.toXML();

        Element requestEl = (Element) requestXml.getElementsByTagName("request").item(0);
        assertEquals("deleteBillboard", requestEl.getAttribute("type"));
        assertEquals("ABCDEF", requestEl.getAttribute("sessionToken"));

        Element billboardName = (Element) requestEl.getElementsByTagName("name").item(0);
        assertEquals("billboard", billboardName.getTextContent());

    }

    @Test
    public void fromXML() throws ParseException {
        DeleteBillboardRequest request = DeleteBillboardRequest.fromXML(TestUtils.fromXMLString(
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                        "<request type=\"deleteBillboard\" sessionToken=\"ABCDEF\">\n" +
                        "\t<name>billboard</name>\n" +
                        "</request>"
        ));

        assertEquals("billboard", request.getBillboardName());
        assertEquals("ABCDEF", request.getSessionToken());
    }

    @Test
    public void fromXMLInvalidType() {
        assertThrows(ParseException.class, () -> {
            DeleteBillboardRequest request = DeleteBillboardRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"invalid\" sessionToken=\"ABCDEF\">\n" +
                            "\t<name>billboard</name>\n" +
                            "</request>"
            ));
        });
    }

    @Test
    public void fromXMLNoRequestElement() {
        assertThrows(ParseException.class, () -> {
            DeleteBillboardRequest request = DeleteBillboardRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<xrequest type=\"deleteBillboard\" sessionToken=\"ABCDEF\">\n" +
                            "\t<name>billboard</name>\n" +
                            "</xrequest>"
            ));
        });
    }

    @Test
    public void fromXMLNoBillboardNameElement() {
        assertThrows(ParseException.class, () -> {
            DeleteBillboardRequest request = DeleteBillboardRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"deleteBillboard\" sessionToken=\"ABCDEF\">\n" +
                            "\t<xname>billboard</xname>\n" +
                            "</request>"
            ));
        });
    }
}
