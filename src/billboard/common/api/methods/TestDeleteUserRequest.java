package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestDeleteUserRequest {
    private DeleteUserRequest request;

    @BeforeEach
    public void beforeEach() {
        this.request = new DeleteUserRequest("ABCDEF", "username");
    }

    @Test
    public void getUsername() {
        assertEquals("username", this.request.getUsername());
    }

    @Test
    public void getSessionToken() {
        assertEquals("ABCDEF", this.request.getSessionToken());
    }

    @Test
    public void toXML() {
        Document requestXml = this.request.toXML();

        Element requestEl = (Element) requestXml.getElementsByTagName("request").item(0);
        assertEquals("deleteUser", requestEl.getAttribute("type"));
        assertEquals("ABCDEF", requestEl.getAttribute("sessionToken"));

        Element usernameEl = (Element) requestEl.getElementsByTagName("username").item(0);
        assertEquals("username", usernameEl.getTextContent());

    }

    @Test
    public void fromXML() throws ParseException {
        DeleteUserRequest request = DeleteUserRequest.fromXML(TestUtils.fromXMLString(
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                        "<request type=\"deleteUser\" sessionToken=\"ABCDEF\">\n" +
                        "\t<username>username</username>\n" +
                        "</request>"
        ));

        assertEquals("username", request.getUsername());
        assertEquals("ABCDEF", request.getSessionToken());
    }

    @Test
    public void fromXMLInvalidType() {
        assertThrows(ParseException.class, () -> {
            DeleteUserRequest request = DeleteUserRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"invalid\" sessionToken=\"ABCDEF\">\n" +
                            "\t<username>username</username>\n" +
                            "</request>"
            ));
        });
    }

    @Test
    public void fromXMLNoRequestElement() {
        assertThrows(ParseException.class, () -> {
            DeleteUserRequest request = DeleteUserRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<xrequest type=\"deleteUser\" sessionToken=\"ABCDEF\">\n" +
                            "\t<username>username</username>\n" +
                            "</xrequest>"
            ));
        });
    }

    @Test
    public void fromXMLNoUsernameElement() {
        assertThrows(ParseException.class, () -> {
            DeleteUserRequest request = DeleteUserRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"deleteUser\" sessionToken=\"ABCDEF\">\n" +
                            "\t<xusername>username</xusername>\n" +
                            "</request>"
            ));
        });
    }
}
