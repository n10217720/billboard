package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestUserLoginResponse {
    private UserLoginResponse response;

    @BeforeEach
    public void beforeEach() {
        this.response = new UserLoginResponse("ABCDEF");
    }

    @Test
    public void getSessionToken() {
        assertEquals("ABCDEF", this.response.getSessionToken());
    }

    @Test
    public void toXML() {
        Document requestXML = this.response.toXML();

        Element requestEl = (Element) requestXML.getElementsByTagName("response").item(0);
        assertEquals("success", requestEl.getAttribute("status"));

        Element sessionTokenEl = (Element) requestEl.getElementsByTagName("sessionToken").item(0);
        assertEquals("ABCDEF", sessionTokenEl.getTextContent());
    }

    @Test
    public void fromXML() throws ParseException {
        UserLoginResponse response = UserLoginResponse.fromXML(TestUtils.fromXMLString(
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                        "<response status=\"success\">\n" +
                        "\t<sessionToken>ABCDEF</sessionToken>\n" +
                        "</response>"
        ));

        assertEquals("ABCDEF", response.getSessionToken());
    }

    @Test
    public void fromXMLInvalidStatus() {
        assertThrows(ParseException.class, () -> {
            UserLoginResponse response = UserLoginResponse.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<response status=\"error\">\n" +
                            "\t<message>Error message text</message>\n" +
                            "</response>"
            ));
        });
    }

    @Test
    public void fromXMLNoResponseElement() {
        assertThrows(ParseException.class, () -> {
            UserLoginResponse response = UserLoginResponse.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<xresponse status=\"error\">\n" +
                            "\t<sessionToken>ABCDEF</sessionToken>\n" +
                            "</xresponse>"
            ));
        });
    }

    @Test
    public void fromXMLNoSessionTokenElement() {
        assertThrows(ParseException.class, () -> {
            UserLoginResponse response = UserLoginResponse.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<response status=\"error\">\n" +
                            "\t<xsessionToken>ABCDEF</xsessionToken>\n" +
                            "</response>"
            ));
        });
    }
}
