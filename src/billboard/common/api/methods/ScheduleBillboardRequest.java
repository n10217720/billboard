package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.ScheduledBillboard;
import billboard.common.abstractXML.Attribute;
import billboard.common.abstractXML.Element;
import billboard.common.abstractXML.StringOutput;
import billboard.common.api.Request;
import org.w3c.dom.Document;

import java.sql.Timestamp;
import java.time.Duration;
import java.time.ZonedDateTime;

/**
 * Method class for creating and serialising/de-serialising a ScheduleBillboard request.
 * Serialised request documents get sent to the server via the client.
 */
public class ScheduleBillboardRequest implements Request<ScheduleBillboardResponse> {
    private String sessionToken;
    private ScheduledBillboard billboard;

    /**
     * Creates a new schedule billboard request.
     *
     * @param sessionToken The session token of the logged in user.
     * @param billboard The scheduledBillboard entry.
     */
    public ScheduleBillboardRequest(String sessionToken, ScheduledBillboard billboard){
        this.sessionToken = sessionToken;
        this.billboard = billboard;
    }

    /**
     * Returns the session token for the user making the request.
     *
     * @return The session token for the user making the request.
     */
    public String getSessionToken(){
        return sessionToken;
    }

    /**
     * Returns the schedule billboard entry object.
     *
     * @return The schedule billboard entry object.
     */
    public ScheduledBillboard getScheduledBillboard(){
        return billboard;
    }

    /**
     * Serialise's this request into an XML document.
     * @see billboard.common.api.XMLSerialisable
     */
    @Override
    public Document toXML() {
        return new Element("request",
                new Attribute[]{
                        new Attribute("type", "scheduleBillboard"),
                        new Attribute("sessionToken", this.getSessionToken())
                },
                new Element[]{
                        new Element("name", this.getScheduledBillboard().getName()),
                        new Element("initialShowing", this.getScheduledBillboard().getInitialShowing().toString()),
                        new Element("displayDuration", "" + this.getScheduledBillboard().getDuration()),
                        new Element("recurrencePeriod", String.valueOf(this.getScheduledBillboard().getRecurrencePeriod()))
                }).getDocument();
    }

    /**
     * Deserialises a schedule billboard request from XML.
     *
     * @param document The request XML document to deserialise.
     * @return The deserialised request.
     * @throws ParseException The request had an invalid type, or the document was in an invalid format.
     */
    public static ScheduleBillboardRequest fromXML(Document document) throws ParseException {
        StringOutput type = new StringOutput();
        StringOutput name = new StringOutput();
        StringOutput initialShowing = new StringOutput();
        StringOutput displayDuration = new StringOutput();
        StringOutput recurrencePeriod = new StringOutput();
        StringOutput sessionToken = new StringOutput();
        new Element("request",
                new Attribute[]{
                        new Attribute("type", type),
                        new Attribute("sessionToken", sessionToken)
                },
                new Element[]{
                        new Element("name", name),
                        new Element("initialShowing", initialShowing),
                        new Element("displayDuration", displayDuration),
                        new Element("recurrencePeriod", recurrencePeriod)
                }
        ).parseDocument(document);

        if(!type.getValue().equals("scheduleBillboard")){
            throw new ParseException(String.format("Invalid request type. Expected type was 'scheduleBillboard', but received '%s'", type.getValue()));
        }

        Duration recurrenceDuration = null;
        if(!recurrencePeriod.getValue().equals("null")){
            recurrenceDuration = Duration.parse(recurrencePeriod.getValue());
        }

        ScheduledBillboard billboard = new ScheduledBillboard(name.getValue(), ZonedDateTime.parse(initialShowing.getValue()),
                Long.parseLong(displayDuration.getValue()), recurrenceDuration);

        return new ScheduleBillboardRequest(sessionToken.getValue(), billboard);
    }

    /**
     * Returns an empty success response from XML
     * @see billboard.common.api.EmptySuccessResponse
     */
    @Override
    public ScheduleBillboardResponse responseFromXML(Document document) {
        return new ScheduleBillboardResponse();
    }
}