package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.abstractXML.Attribute;
import billboard.common.abstractXML.Element;
import billboard.common.abstractXML.StringOutput;
import billboard.common.api.Request;
import org.w3c.dom.Document;

/**
 * Method class for creating and serialising/de-serialising a UserLogout request.
 * Serialised request documents get sent to the server via the client.
 */
public class UserLogoutRequest implements Request<UserLogoutResponse> {

    private String sessionToken;

    /**
     * Creates a new user logout request.
     *
     * @param sessionToken The session token of the user making the request.
     */
    public UserLogoutRequest(String sessionToken){
        this.sessionToken = sessionToken;
    }

    /**
     * Returns the session token for the user making the request.
     *
     * @return The session token for the user making the request.
     */
    public String getSessionToken(){
        return sessionToken;
    }

    /**
     * Serialise's this request into an XML document.
     * @see billboard.common.api.XMLSerialisable
     */
    @Override
    public Document toXML() {
        return new Element("request", new Attribute[] {
                new Attribute("type", "userLogout"),
                new Attribute("sessionToken", this.getSessionToken()), },
                new Element[]{
                }).getDocument();
    }

    /**
     * Deserialises a user logout request from XML.
     *
     * @param document The request XML document to deserialise.
     * @return The deserialised request.
     * @throws ParseException The request had an invalid type, or the document was in an invalid format.
     */
    public static UserLogoutRequest fromXML(Document document) throws ParseException{
        StringOutput type = new StringOutput();
        StringOutput sessionToken = new StringOutput();

        new Element("request",
                new Attribute[]{
                        new Attribute("type", type),
                        new Attribute("sessionToken", sessionToken)
                },
                new Element[]{
                }).parseDocument(document);

        if(!type.getValue().equals("userLogout")) {
            throw new ParseException(String.format("Invalid request type. Expected type was 'userLogout', but received '%s'", type.getValue()));
        }

        return new UserLogoutRequest(sessionToken.getValue());
    }

    /**
     * Returns an empty success response from XML
     * @see billboard.common.api.EmptySuccessResponse
     */
    @Override
    public UserLogoutResponse responseFromXML(Document document) {
        return new UserLogoutResponse();
    }


}
