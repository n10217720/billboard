package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.abstractXML.*;
import billboard.common.api.Response;
import billboard.server.Permission;
import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Method class for creating and serialising/de-serialising a GetUserPermissions response.
 * Serialised response documents get sent to the client via the server.
 */
public class GetUserPermissionsResponse implements Response {
    private HashSet<Permission> permissionList;

    /**
     * Creates a new set user permissions response.
     *
     * @param permissionList A HashSet of the permissions to be granted.
     */
    public GetUserPermissionsResponse(HashSet<Permission> permissionList){
        this.permissionList = permissionList;
    }

    /**
     * Returns a HashSet of the users permissions.
     *
     * @return A HashSet of the users permissions.
     */
    public HashSet<Permission> getPermissionList(){
        return permissionList;
    }

    @Override
    public Document toXML() {
        List<String> permissionsString = permissionList.stream()
                .map(Permission::toString)
                .collect(Collectors.toList());

        return new Element("response",
                new Attribute[]{
                        new Attribute("status", "success")
                }, new Element[]{
                new Element("permissions", new ElementBase[]{
                        new Elements("permission", permissionsString)
                })
        }).getDocument();
    }

    /**
     * Deserialises a get user permissions response from XML.
     *
     * @param document The response XML document to deserialise.
     * @return The deserialised response.
     * @throws ParseException The request had an invalid type, or the document was in an invalid format.
     */
    public static GetUserPermissionsResponse fromXML(Document document) throws ParseException{
        StringOutput status = new StringOutput();
        List<String> permissionsString = new ArrayList<>();
        HashSet<Permission> permissions = new HashSet<>();
        new Element("response",
                new Attribute[]{
                        new Attribute("status", status)
                },
                new Element[]{
                        new Element("permissions", new ElementBase[]{
                                new Elements("permission", permissionsString)
                        })
                }
        ).parseDocument(document);

        if(!status.getValue().equals("success")) {
            throw new ParseException(String.format("Invalid response status. Expected status was 'success', but received '%s'", status.getValue()));
        }

        for (String perm : permissionsString){
            permissions.add(Permission.fromString(perm));
        }
        return new GetUserPermissionsResponse(permissions);
    }
}
