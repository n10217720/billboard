package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.TestUtils;
import billboard.server.Permission;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestSetUserPermissionsRequest {
    private SetUserPermissionsRequest request;
    private HashSet<Permission> permissions;
    private ArrayList<String> permissionList;

    @BeforeEach
    public void beforeEach() {
        this.permissions = new HashSet<>(Arrays.asList(Permission.EDIT_USERS, Permission.CREATE_BILLBOARD));
        this.permissionList = new ArrayList<>(Arrays.asList("EDIT_USERS", "CREATE_BILLBOARD"));
        this.request = new SetUserPermissionsRequest("ABCDEF", "username", this.permissions);
    }

    @Test
    public void getSessionToken() {
        assertEquals("ABCDEF", this.request.getSessionToken());
    }

    @Test
    public void getUsername() {
        assertEquals("username", this.request.getUsername());
    }

    @Test
    public void getPermissionList() {
        assertEquals(this.permissions, this.request.getPermissionList());
    }

    @Test
    public void toXML() {
        Document requestXML = this.request.toXML();

        Element requestEl = (Element) requestXML.getElementsByTagName("request").item(0);
        assertEquals("setUserPermissions", requestEl.getAttribute("type"));
        assertEquals("ABCDEF", requestEl.getAttribute("sessionToken"));

        Element usernameEl = (Element) requestEl.getElementsByTagName("username").item(0);
        assertEquals("username", usernameEl.getTextContent());

        Element permissionsEl = (Element) requestEl.getElementsByTagName("permissions").item(0);
        assertEquals(2, permissionsEl.getChildNodes().getLength());

        NodeList children = permissionsEl.getElementsByTagName("permission");
        for (int i = 0; i < children.getLength(); ++i) {
            Element child = (Element) children.item(i);
            assertTrue(this.permissionList.contains(child.getTextContent()));
        }
    }

    @Test
    public void fromXML() throws ParseException {
        SetUserPermissionsRequest request = SetUserPermissionsRequest.fromXML(TestUtils.fromXMLString(
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                        "<request type=\"setUserPermissions\" sessionToken=\"ABCDEF\">\n" +
                        "\t<username>username</username>\n" +
                            "\t<permissions>\n" +
                                "\t<permission>EDIT_USERS</permission>\n" +
                                "\t<permission>CREATE_BILLBOARD</permission>\n" +
                            "\t</permissions>\n" +
                        "</request>"
        ));

        assertEquals("username", request.getUsername());
        assertEquals("ABCDEF", request.getSessionToken());
        assertEquals(this.permissions, request.getPermissionList());
    }

    @Test
    public void fromXMLInvalidType() {
        assertThrows(ParseException.class, () -> {
            SetUserPermissionsRequest request = SetUserPermissionsRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"invalid\" sessionToken=\"ABCDEF\">\n" +
                            "\t<username>username</username>\n" +
                            "\t<permissions>\n" +
                            "\t<permission>EDIT_USERS</permission>\n" +
                            "\t<permission>CREATE_BILLBOARD</permission>\n" +
                            "\t</permissions>\n" +
                            "</request>"
            ));
        });
    }

    @Test
    public void fromXMLNoRequestElement() {
        assertThrows(ParseException.class, () -> {
            SetUserPermissionsRequest request = SetUserPermissionsRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<xrequest type=\"setUserPermissions\" sessionToken=\"ABCDEF\">\n" +
                            "\t<username>username</username>\n" +
                            "\t<permissions>\n" +
                            "\t<permission>EDIT_USERS</permission>\n" +
                            "\t<permission>CREATE_BILLBOARD</permission>\n" +
                            "\t</permissions>\n" +
                            "</xrequest>"
            ));
        });
    }

    @Test
    public void fromXMLNoUsernameElement() {
        assertThrows(ParseException.class, () -> {
            SetUserPermissionsRequest request = SetUserPermissionsRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"setUserPermissions\" sessionToken=\"ABCDEF\">\n" +
                                "\t<xusername>username</xusername>\n" +
                                "\t<permissions>\n" +
                                    "\t<permission>EDIT_USERS</permission>\n" +
                                    "\t<permission>CREATE_BILLBOARD</permission>\n" +
                                "\t</permissions>\n" +
                            "</request>"
            ));
        });
    }

    @Test
    public void fromXMLNoPermissionsElement() {
        assertThrows(ParseException.class, () -> {
            SetUserPermissionsRequest request = SetUserPermissionsRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"setUserPermissions\" sessionToken=\"ABCDEF\">\n" +
                                "\t<username>username</username>\n" +
                                "\t<xpermissions>\n" +
                                    "\t<permission>EDIT_USERS</permission>\n" +
                                    "\t<permission>CREATE_BILLBOARD</permission>\n" +
                                "\t</xpermissions>\n" +
                            "</request>"
            ));
        });
    }
}
