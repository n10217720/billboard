package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.ScheduledBillboard;
import billboard.common.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestViewScheduleResponse {
    private ViewScheduleResponse response;
    private List<ScheduledBillboard> scheduledBillboards;
    private List<String> billboardNames;
    private List<String> creators;
    private List<ZonedDateTime> initialShowing;
    private List<Long> durations;
    private List<Duration> recurrences;

    @BeforeEach
    public void beforeEach() {
        this.billboardNames = List.of("Billboard 1", "Billboard 2");
        this.initialShowing = List.of(ZonedDateTime.of(2020, 10, 4, 1, 0, 0, 0, ZoneId.systemDefault()),
                ZonedDateTime.of(2020, 10, 4, 2, 0, 0, 0, ZoneId.systemDefault()));
        this.durations = List.of((long) 15, (long) 30);
        this.recurrences = List.of(Duration.ofHours(1), Duration.ofHours(23));
        this.scheduledBillboards = List.of(new ScheduledBillboard(billboardNames.get(0), initialShowing.get(0), durations.get(0), recurrences.get(0)));
        this.creators = List.of("creator1", "creator2");

        this.response = new ViewScheduleResponse(this.scheduledBillboards, this.creators);
    }

    @Test
    public void getBillboards() {
        assertEquals(this.scheduledBillboards, response.getScheduledBillboards());
        assertEquals(this.creators, response.getBillboardCreators());
        System.out.println(this.recurrences.get(1).toString());
    }

    @Test
    public void toXML() {
        Document responseXML = this.response.toXML();

        Element responseEl = (Element) responseXML.getElementsByTagName("response").item(0);
        assertEquals("success", responseEl.getAttribute("status"));

        NodeList billboardEls = responseEl.getElementsByTagName("entry");
        for (int i = 0; i < billboardEls.getLength(); ++i) {
            Element billboardEl = (Element) billboardEls.item(i);
            Element nameEl = (Element) billboardEl.getElementsByTagName("billboardName").item(0);
            Element initialShowingEl = (Element) billboardEl.getElementsByTagName("initialShowing").item(0);
            Element displayDurationEl = (Element) billboardEl.getElementsByTagName("displayDuration").item(0);
            Element recurrencePeriodEl = (Element) billboardEl.getElementsByTagName("recurrencePeriod").item(0);
            Element creatorsEl = (Element) billboardEl.getElementsByTagName("creator").item(0);

            assertEquals(nameEl.getTextContent(), this.billboardNames.get(i));
            assertEquals(initialShowingEl.getTextContent(), this.initialShowing.get(i).toString());
            assertEquals(displayDurationEl.getTextContent(), this.durations.get(i).toString());
            assertEquals(recurrencePeriodEl.getTextContent(), this.recurrences.get(i).toString());
            assertEquals(creatorsEl.getTextContent(), this.creators.get(i));
        }
    }

    @Test
    public void fromXML() throws ParseException {
        ViewScheduleResponse response = ViewScheduleResponse.fromXML(TestUtils.fromXMLString(
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                        "<response status=\"success\">\n" +
                        "\t<entry>\n" +
                        "\t<billboardName>Billboard 1</billboardName>\n" +
                        "\t<initialShowing>2020-10-04T01:00+10:00[Australia/Brisbane]</initialShowing>\n" +
                        "\t<displayDuration>15</displayDuration>\n" +
                        "\t<recurrencePeriod>PT1H</recurrencePeriod>\n" +
                        "\t<creator>creator1</creator>\n" +
                        "\t</entry>\n" +
                        "\t<entry>\n" +
                        "\t<billboardName>Billboard 2</billboardName>\n" +
                        "\t<initialShowing>2020-10-04T02:00+10:00[Australia/Brisbane]</initialShowing>\n" +
                        "\t<displayDuration>30</displayDuration>\n" +
                        "\t<recurrencePeriod>PT23H</recurrencePeriod>\n" +
                        "\t<creator>creator2</creator>\n" +
                        "\t</entry>\n" +
                        "</response>"
        ));

        assertEquals(this.billboardNames, response.getScheduledBillboards().stream().map(ScheduledBillboard::getName).collect(Collectors.toList()));
        assertEquals(this.initialShowing, response.getScheduledBillboards().stream().map(ScheduledBillboard::getInitialShowing).collect(Collectors.toList()));
        assertEquals(this.durations, response.getScheduledBillboards().stream().map(ScheduledBillboard::getDuration).collect(Collectors.toList()));
        assertEquals(this.recurrences, response.getScheduledBillboards().stream().map(ScheduledBillboard::getRecurrencePeriod).collect(Collectors.toList()));
        assertEquals(this.creators, response.getBillboardCreators());
    }

    @Test
    public void fromXMLInvalidStatus() {
        assertThrows(ParseException.class, () -> {
            ViewScheduleResponse response = ViewScheduleResponse.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<response status=\"invalid\">\n" +
                            "\t<entry>\n" +
                            "\t<billboardName>Billboard 1</billboardName>\n" +
                            "\t<initialShowing>2020-10-04T01:00+10:00[Australia/Brisbane]</initialShowing>\n" +
                            "\t<displayDuration>15</displayDuration>\n" +
                            "\t<recurrencePeriod>PT1H</recurrencePeriod>\n" +
                            "\t<creator>creator1</creator>\n" +
                            "\t</entry>\n" +
                            "\t<entry>\n" +
                            "\t<billboardName>Billboard 2</billboardName>\n" +
                            "\t<initialShowing>2020-10-04T02:00+10:00[Australia/Brisbane]</initialShowing>\n" +
                            "\t<displayDuration>30</displayDuration>\n" +
                            "\t<recurrencePeriod>PT23H</recurrencePeriod>\n" +
                            "\t<creator>creator2</creator>\n" +
                            "\t</entry>\n" +
                            "</response>"
            ));
        });
    }

    @Test
    public void fromXMLNoResponseElement() {
        assertThrows(ParseException.class, () -> {
            ViewScheduleResponse response = ViewScheduleResponse.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<xresponse status=\"success\">\n" +
                            "\t<entry>\n" +
                            "\t<billboardName>Billboard 1</billboardName>\n" +
                            "\t<initialShowing>2020-10-04T01:00+10:00[Australia/Brisbane]</initialShowing>\n" +
                            "\t<displayDuration>15</displayDuration>\n" +
                            "\t<recurrencePeriod>PT1H</recurrencePeriod>\n" +
                            "\t<creator>creator1</creator>\n" +
                            "\t</entry>\n" +
                            "\t<entry>\n" +
                            "\t<billboardName>Billboard 2</billboardName>\n" +
                            "\t<initialShowing>2020-10-04T02:00+10:00[Australia/Brisbane]</initialShowing>\n" +
                            "\t<displayDuration>30</displayDuration>\n" +
                            "\t<recurrencePeriod>PT23H</recurrencePeriod>\n" +
                            "\t<creator>creator2</creator>\n" +
                            "\t</entry>\n" +
                            "</xresponse>"
            ));
        });
    }
}
