package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestSetUserPasswordRequest {
    private SetUserPasswordRequest request;

    @BeforeEach
    public void beforeEach() {
        this.request = new SetUserPasswordRequest("ABCDEF", "username", "5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8");
    }

    @Test
    public void getSessionToken() {
        assertEquals("ABCDEF", this.request.getSessionToken());
    }

    @Test
    public void getUsername() {
        assertEquals("username", this.request.getUsername());
    }

    @Test
    public void getNewHashedPassword() {
        assertEquals("5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8", this.request.getNewHashedPassword());
    }

    @Test
    public void toXML() {
        Document requestXML = this.request.toXML();

        Element requestEl = (Element) requestXML.getElementsByTagName("request").item(0);
        assertEquals("setUserPassword", requestEl.getAttribute("type"));
        assertEquals("ABCDEF", requestEl.getAttribute("sessionToken"));

        Element usernameEl = (Element) requestEl.getElementsByTagName("username").item(0);
        assertEquals("username", usernameEl.getTextContent());

        Element newHashedPasswordEl = (Element) requestEl.getElementsByTagName("newHashedPassword").item(0);
        assertEquals("5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8", newHashedPasswordEl.getTextContent());
    }

    @Test
    public void fromXML() throws ParseException {
        SetUserPasswordRequest request = SetUserPasswordRequest.fromXML(TestUtils.fromXMLString(
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                        "<request type=\"setUserPassword\" sessionToken=\"ABCDEF\">\n" +
                        "\t<username>username</username>\n" +
                        "\t<newHashedPassword>5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8</newHashedPassword>\n" +
                        "</request>"
        ));

        assertEquals("username", request.getUsername());
        assertEquals("ABCDEF", request.getSessionToken());
        assertEquals("5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8", request.getNewHashedPassword());
    }

    @Test
    public void fromXMLInvalidType() {
        assertThrows(ParseException.class, () -> {
            SetUserPasswordRequest request = SetUserPasswordRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"invalid\" sessionToken=\"ABCDEF\">\n" +
                            "\t<username>username</username>\n" +
                            "\t<newHashedPassword>5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8</newHashedPassword>\n" +
                            "</request>"
            ));
        });
    }

    @Test
    public void fromXMLNoRequestElement() {
        assertThrows(ParseException.class, () -> {
            SetUserPasswordRequest request = SetUserPasswordRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<xrequest type=\"setUserPassword\" sessionToken=\"ABCDEF\">\n" +
                            "\t<username>username</username>\n" +
                            "\t<newHashedPassword>5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8</newHashedPassword>\n" +
                            "</xrequest>"
            ));
        });
    }

    @Test
    public void fromXMLNoUsernameElement() {
        assertThrows(ParseException.class, () -> {
            SetUserPasswordRequest request = SetUserPasswordRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"setUserPassword\" sessionToken=\"ABCDEF\">\n" +
                            "\t<xusername>username</xusername>\n" +
                            "\t<newHashedPassword>5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8</newHashedPassword>\n" +
                            "</request>"
            ));
        });
    }
}
