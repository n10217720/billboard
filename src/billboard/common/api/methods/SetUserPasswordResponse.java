package billboard.common.api.methods;

import billboard.common.api.EmptySuccessResponse;

/**
 * Method class for creating and serialising/de-serialising a SetUserPassword response.
 * Serialised response documents get sent to the client via the server.
 * Extends EmptySuccessResponse as the response requires no information to send back.
 */
public class SetUserPasswordResponse extends EmptySuccessResponse {}
