package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestGetCurrentBillboardRequest {
    private GetCurrentBillboardRequest request;

    @BeforeEach
    public void beforeEach() {
        this.request = new GetCurrentBillboardRequest();
    }

    @Test
    public void toXML() {
        Document requestXML = this.request.toXML();

        Element requestEl = (Element) requestXML.getElementsByTagName("request").item(0);
        assertEquals("getCurrentBillboard", requestEl.getAttribute("type"));
    }

    @Test
    public void fromXMLInvalidType() {
        assertThrows(ParseException.class, () -> {
            GetCurrentBillboardRequest request = GetCurrentBillboardRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"invalid\">" +
                            "</request>"

            ));
        });
    }


    @Test
    public void fromXMLNoRequestElement() {
        assertThrows(ParseException.class, () -> {
            GetBillboardRequest request = GetBillboardRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<xrequest type=\"getCurrentBillboard\">" +
                            "</xrequest>"
            ));
        });
    }
}
