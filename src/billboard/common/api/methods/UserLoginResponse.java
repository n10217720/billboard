package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.abstractXML.Attribute;
import billboard.common.abstractXML.Element;
import billboard.common.abstractXML.StringOutput;
import billboard.common.api.Response;
import org.w3c.dom.Document;

/**
 * Method class for creating and serialising/de-serialising a UserLogin response.
 * Serialised response documents get sent to the client via the server.
 */
public class UserLoginResponse implements Response {
    private String sessionToken;

    /**
     * Creates a user login response.
     *
     * @param sessionToken The session token generated from the login.
     */
    public UserLoginResponse(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    /**
     * Returns the generated session token.
     *
     * @return The generated session token.
     */
    public String getSessionToken() {
        return sessionToken;
    }

    /**
     * Serialise's this request into an XML document.
     * @see billboard.common.api.XMLSerialisable
     */
    @Override
    public Document toXML() {
        return new Element("response",
                new Attribute[]{
                        new Attribute("status", "success")
                },
                new Element[]{
                        new Element("sessionToken", this.getSessionToken())
                }
        ).getDocument();
    }

    /**
     * Deserialises a user login response from XML.
     *
     * @param document The response XML document to deserialise.
     * @return The deserialised response.
     * @throws ParseException The request had an invalid type, or the document was in an invalid format.
     */
    public static UserLoginResponse fromXML(Document document) throws ParseException {
        StringOutput status = new StringOutput();
        StringOutput sessionToken = new StringOutput();
        new Element("response",
                new Attribute[]{
                        new Attribute("status", status)
                },
                new Element[]{
                        new Element("sessionToken", sessionToken)
                }
        ).parseDocument(document);

        if(!status.getValue().equals("success")) {
            throw new ParseException(String.format("Invalid response status. Expected status was 'success', but received '%s'", status.getValue()));
        }

        return new UserLoginResponse(sessionToken.getValue());
    }
}