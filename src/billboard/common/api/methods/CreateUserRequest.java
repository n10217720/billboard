package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.abstractXML.*;
import billboard.common.api.Request;
import billboard.server.Permission;
import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Method class for creating and serialising/de-serialising a createUser request.
 * Serialised request documents get sent to the server via the client.
 */
public class CreateUserRequest implements Request<CreateUserResponse> {
    private String sessionToken;
    private String username;
    private String hashedPassword;
    private HashSet<Permission> permissionList;

    /**
     * Creates a create user request.
     *
     * @param sessionToken The user's hashed and salted password.
     * @param username The user's username.
     * @param hashedPassword The user's hashedPassword.
     * @param permissionList A hashset of the user's permissions.
     */
    public CreateUserRequest(String sessionToken, String username, String hashedPassword, HashSet<Permission> permissionList){
        this.sessionToken = sessionToken;
        this.username = username;
        this.hashedPassword = hashedPassword;
        this.permissionList = permissionList;
    }

    /**
     * Returns the session token for the user making the request.
     *
     * @return The session token for the user making the request.
     */
    public String getSessionToken(){
        return sessionToken;
    }

    /**
     * Returns the username.
     *
     * @return The username.
     */
    public String getUsername(){
        return username;
    }

    /**
     * Returns the hashed password.
     *
     * @return The hashed password.
     */
    public String getHashedPassword(){
        return hashedPassword;
    }

    /**
     * Returns the hashset of permissions.
     *
     * @return The hashset of permissions.
     */
    public HashSet<Permission> getPermissions(){
        return permissionList;
    }

    /**
     * Serialise's this request into an XML document.
     * @see billboard.common.api.XMLSerialisable
     */
    @Override
    public Document toXML() {
        List<String> permissionsString = permissionList.stream()
                .map(Permission::toString)
                .collect(Collectors.toList());

        return new Element("request", new Attribute[] {
                new Attribute("type", "createUser"),
                new Attribute("sessionToken", this.getSessionToken())
        }, new Element[] {
                new Element("username", this.getUsername()),
                new Element("hashedPassword", this.getHashedPassword()),
                new Element("permissions", new ElementBase[] {
                        new Elements("permission", permissionsString)
                })
        }).getDocument();
    }

    /**
     * Deserialises a create user request from XML.
     *
     * @param document The request XML document to deserialise.
     * @return The deserialised request.
     * @throws ParseException The request had an invalid type, or the document was in an invalid format.
     */
    public static CreateUserRequest fromXML(Document document) throws ParseException{
        StringOutput type = new StringOutput();
        StringOutput sessionToken = new StringOutput();
        StringOutput username = new StringOutput();
        StringOutput hashedPassword = new StringOutput();
        HashSet<Permission> permissions = new HashSet<>();
        List<String> permissionsString = new ArrayList<>();

        new Element("request", new Attribute[] {
                new Attribute("type", type),
                new Attribute("sessionToken", sessionToken)
        }, new Element[] {
                new Element("username", username),
                new Element("hashedPassword", hashedPassword),
                new Element("permissions", new ElementBase[] {
                        new Elements("permission", permissionsString)
                })
        }).parseDocument(document);

        if(!type.getValue().equals("createUser")) {
            throw new ParseException(String.format("Invalid request type. Expected type was 'createUser', but received '%s'", type.getValue()));
        }

        for (String perm : permissionsString){
            permissions.add(Permission.fromString(perm));
        }

        return new CreateUserRequest(sessionToken.getValue(), username.getValue(), hashedPassword.getValue(), permissions);
    }

    /**
     * Returns an empty success response from XML
     * @see billboard.common.api.EmptySuccessResponse
     */
    @Override
    public CreateUserResponse responseFromXML(Document document) {
        return new CreateUserResponse();
    }
}
