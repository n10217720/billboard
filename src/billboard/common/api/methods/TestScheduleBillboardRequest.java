package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.ScheduledBillboard;
import billboard.common.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;

import static org.junit.jupiter.api.Assertions.*;

public class TestScheduleBillboardRequest {
    private ScheduleBillboardRequest requestOccurrenceRepeat;
    private ScheduleBillboardRequest requestOccurrenceOnce;

    private ScheduledBillboard billboardOccurrenceRepeat;
    private ScheduledBillboard billboardOccurrenceOnce;

    private ZonedDateTime initialShowing = ZonedDateTime.of(2020, 10, 4, 1, 45, 0, 0, ZoneId.systemDefault());
    private long duration = 5;
    private Duration occurrence = Duration.ofHours(23);

    @BeforeEach
    public void beforeEach() {
        billboardOccurrenceRepeat = new ScheduledBillboard("billboard", initialShowing, duration, occurrence);

        this.requestOccurrenceRepeat = new ScheduleBillboardRequest("ABCDEF", billboardOccurrenceRepeat);

        billboardOccurrenceOnce = new ScheduledBillboard("billboard", initialShowing, duration, null);

        this.requestOccurrenceOnce = new ScheduleBillboardRequest("ABCDEF", billboardOccurrenceOnce);
    }

    @Test
    public void getSessionToken() {
        assertEquals("ABCDEF", this.requestOccurrenceRepeat.getSessionToken());
    }

    @Test
    public void getScheduledBillboardRepeating(){
        assertEquals(billboardOccurrenceRepeat, this.requestOccurrenceRepeat.getScheduledBillboard());
    }

    @Test
    public void getScheduledBillboardOnce(){
        assertEquals(billboardOccurrenceOnce, this.requestOccurrenceOnce.getScheduledBillboard());

    }

    @Test
    public void toXMLOccurrenceRepeating() {
        Document requestXML = this.requestOccurrenceRepeat.toXML();

        Element requestEl = (Element) requestXML.getElementsByTagName("request").item(0);
        assertEquals("scheduleBillboard", requestEl.getAttribute("type"));
        assertEquals("ABCDEF", requestEl.getAttribute("sessionToken"));

        Element nameEl = (Element) requestEl.getElementsByTagName("name").item(0);
        assertEquals("billboard", nameEl.getTextContent());

        Element initialShowingEl = (Element) requestEl.getElementsByTagName("initialShowing").item(0);
        assertEquals("2020-10-04T01:45+10:00[Australia/Brisbane]", initialShowingEl.getTextContent());

        Element durationEl = (Element) requestEl.getElementsByTagName("displayDuration").item(0);
        assertEquals("" + duration, durationEl.getTextContent());

        Element recurrencePeriodEl = (Element) requestEl.getElementsByTagName("recurrencePeriod").item(0);
        assertEquals(occurrence.toString(), recurrencePeriodEl.getTextContent());

    }

    @Test
    public void toXMLOccurrenceOnce() {
        Document requestXML = this.requestOccurrenceOnce.toXML();

        Element requestEl = (Element) requestXML.getElementsByTagName("request").item(0);
        assertEquals("scheduleBillboard", requestEl.getAttribute("type"));
        assertEquals("ABCDEF", requestEl.getAttribute("sessionToken"));

        Element nameEl = (Element) requestEl.getElementsByTagName("name").item(0);
        assertEquals("billboard", nameEl.getTextContent());

        Element initialShowingEl = (Element) requestEl.getElementsByTagName("initialShowing").item(0);
        assertEquals("2020-10-04T01:45+10:00[Australia/Brisbane]", initialShowingEl.getTextContent());

        Element durationEl = (Element) requestEl.getElementsByTagName("displayDuration").item(0);
        assertEquals("" + duration, durationEl.getTextContent());

        Element recurrencePeriodEl = (Element) requestEl.getElementsByTagName("recurrencePeriod").item(0);
        assertEquals("null", recurrencePeriodEl.getTextContent());
    }

    @Test
    public void fromXMLRepeating() throws ParseException {
        ScheduleBillboardRequest request = ScheduleBillboardRequest.fromXML(TestUtils.fromXMLString(
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                        "<request type=\"scheduleBillboard\" sessionToken=\"ABCDEF\">\n" +
                        "\t<name>billboard</name>\n" +
                        "\t<initialShowing>2020-10-04T01:45+10:00[Australia/Brisbane]</initialShowing>\n" +
                        "\t<displayDuration>5</displayDuration>\n" +
                        "\t<recurrencePeriod>PT23H</recurrencePeriod>\n" +
                        "</request>"
        ));
        assertEquals("ABCDEF", request.getSessionToken());
        assertEquals("billboard", request.getScheduledBillboard().getName());
        assertEquals("2020-10-04T01:45+10:00[Australia/Brisbane]", request.getScheduledBillboard().getInitialShowing().toString());
        assertEquals("5", "" + request.getScheduledBillboard().getDuration());
        assertEquals("PT23H", request.getScheduledBillboard().getRecurrencePeriod().toString());
    }

    @Test
    public void fromXMLOnce() throws ParseException {
        ScheduleBillboardRequest request = ScheduleBillboardRequest.fromXML(TestUtils.fromXMLString(
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                        "<request type=\"scheduleBillboard\" sessionToken=\"ABCDEF\">\n" +
                        "\t<name>billboard</name>\n" +
                        "\t<initialShowing>2020-10-04T01:45+10:00[Australia/Brisbane]</initialShowing>\n" +
                        "\t<displayDuration>5</displayDuration>\n" +
                        "\t<recurrencePeriod>null</recurrencePeriod>\n" +
                        "</request>"
        ));
        assertEquals("ABCDEF", request.getSessionToken());
        assertEquals("billboard", request.getScheduledBillboard().getName());
        assertEquals("2020-10-04T01:45+10:00[Australia/Brisbane]", request.getScheduledBillboard().getInitialShowing().toString());
        assertEquals("5", "" + request.getScheduledBillboard().getDuration());
        assertNull(request.getScheduledBillboard().getRecurrencePeriod());
    }

    @Test
    public void fromXMLInvalidType() {
        assertThrows(ParseException.class, () -> {
            ScheduleBillboardRequest request = ScheduleBillboardRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"invalid\" sessionToken=\"ABCDEF\">\n" +
                            "\t<name>billboard</name>\n" +
                            "\t<initialShowing>2020-10-04T01:45+10:00[Australia/Brisbane]</initialShowing>\n" +
                            "\t<displayDuration>5</displayDuration>\n" +
                            "\t<recurrencePeriod>null</recurrencePeriod>\n" +
                            "</request>"
            ));
        });
    }

    @Test
    public void fromXMLNoRequestElement() {
        assertThrows(ParseException.class, () -> {
            ScheduleBillboardRequest request = ScheduleBillboardRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<xrequest type=\"scheduleBillboard\" sessionToken=\"ABCDEF\">\n" +
                            "\t<name>billboard</name>\n" +
                            "\t<initialShowing>2020-10-04T01:45+10:00[Australia/Brisbane]</initialShowing>\n" +
                            "\t<displayDuration>5</displayDuration>\n" +
                            "\t<recurrencePeriod>null</recurrencePeriod>\n" +
                            "</xrequest>"
            ));
        });
    }

    @Test
    public void fromXMLNoNameElement() {
        assertThrows(ParseException.class, () -> {
            ScheduleBillboardRequest request = ScheduleBillboardRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"scheduleBillboard\" sessionToken=\"ABCDEF\">\n" +
                            "\t<xname>billboard</xname>\n" +
                            "\t<initialShowing>2020-10-04T01:45+10:00[Australia/Brisbane]</initialShowing>\n" +
                            "\t<displayDuration>5</displayDuration>\n" +
                            "\t<recurrencePeriod>null</recurrencePeriod>\n" +
                            "</request>"
            ));
        });
    }

    @Test
    public void fromXMLInitialShowingElement() {
        assertThrows(ParseException.class, () -> {
            ScheduleBillboardRequest request = ScheduleBillboardRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"scheduleBillboard\" sessionToken=\"ABCDEF\">\n" +
                            "\t<name>billboard</name>\n" +
                            "\t<xinitialShowing>2020-10-04T01:45+10:00[Australia/Brisbane]</xinitialShowing>\n" +
                            "\t<displayDuration>5</displayDuration>\n" +
                            "\t<recurrencePeriod>null</recurrencePeriod>\n" +
                            "</request>"
            ));
        });
    }

    @Test
    public void fromXMLNoDisplayDurationElement() {
        assertThrows(ParseException.class, () -> {
            ScheduleBillboardRequest request = ScheduleBillboardRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"scheduleBillboard\" sessionToken=\"ABCDEF\">\n" +
                            "\t<name>billboard</name>\n" +
                            "\t<initialShowing>2020-10-04T01:45+10:00[Australia/Brisbane]</initialShowing>\n" +
                            "\t<xdisplayDuration>5</xdisplayDuration>\n" +
                            "\t<recurrencePeriod>null</recurrencePeriod>\n" +
                            "</request>"
            ));
        });
    }

    @Test
    public void fromXMLNoRecurrencePeriodElement() {
        assertThrows(ParseException.class, () -> {
            ScheduleBillboardRequest request = ScheduleBillboardRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"scheduleBillboard\" sessionToken=\"ABCDEF\">\n" +
                            "\t<name>billboard</name>\n" +
                            "\t<initialShowing>2020-10-04T01:45+10:00[Australia/Brisbane]</initialShowing>\n" +
                            "\t<displayDuration>5</displayDuration>\n" +
                            "\t<xrecurrencePeriod>null</xrecurrencePeriod>\n" +
                            "</request>"
            ));
        });
    }
}