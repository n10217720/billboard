package billboard.common.api.methods;

import billboard.common.api.EmptySuccessResponse;
import billboard.common.api.Response;
import org.w3c.dom.Document;

/**
 * Method class for creating and serialising/de-serialising a ScheduleBillboard response.
 * Serialised response documents get sent to the client via the server.
 * Extends EmptySuccessResponse as the response requires no information to send back.
 */
public class ScheduleBillboardResponse extends EmptySuccessResponse {

}
