package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.abstractXML.Attribute;
import billboard.common.abstractXML.Element;
import billboard.common.abstractXML.StringOutput;
import billboard.common.api.Request;
import org.w3c.dom.Document;

/**
 * Method class for creating and serialising/de-serialising a getBillboard request.
 * Serialised request documents get sent to the server via the client.
 */
public class GetBillboardRequest implements Request<GetBillboardResponse> {
    private String sessionToken;
    private String name;

    /**
     * Creates a new get billboard request.
     *
     * @param sessionToken The session token of the logged in user.
     * @param name The name of the billboard to retrieve.
     */
    public GetBillboardRequest(String sessionToken, String name) {
        this.sessionToken = sessionToken;
        this.name = name;
    }

    /**
     * Returns the session token for the user making the request.
     *
     * @return The session token for the user making the request.
     */
    public String getSessionToken() {
        return sessionToken;
    }

    /**
     * Returns the name of the billboard being retrieved.
     *
     * @return The name of the billboard being retrieved.
     */
    public String getName() {
        return name;
    }

    public static GetBillboardRequest fromXML(Document document) throws ParseException {
        StringOutput type = new StringOutput();
        StringOutput name = new StringOutput();
        StringOutput sessionToken = new StringOutput();
        new Element("request",
                new Attribute[]{
                        new Attribute("type", type),
                        new Attribute("sessionToken", sessionToken)
                },
                new Element[]{
                        new Element("name", name)
                }
        ).parseDocument(document);

        if(!type.getValue().equals("getBillboard")){
            throw new ParseException(String.format("Invalid request type. Expected type was 'getBillboard', but received '%s'", type.getValue()));
        }

        return new GetBillboardRequest(sessionToken.getValue(), name.getValue());
    }

    /**
     * Serialise's this request into an XML document.
     * @see billboard.common.api.XMLSerialisable
     */
    @Override
    public Document toXML() {
        return new Element("request",
                new Attribute[]{
                        new Attribute("type", "getBillboard"),
                        new Attribute("sessionToken", this.getSessionToken())
                },
                new Element[]{
                        new Element("name", this.getName())
                }
        ).getDocument();
    }

    /**
     * Returns an empty success response from XML
     * @see billboard.common.api.EmptySuccessResponse
     */
    @Override
    public GetBillboardResponse responseFromXML(Document document) throws ParseException {
        return GetBillboardResponse.fromXML(document);
    }
}
