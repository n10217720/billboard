package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestGetBillboardRequest {
    private GetBillboardRequest request;

    @BeforeEach
    public void beforeEach() {
        this.request = new GetBillboardRequest("ABCDEF", "billboard name");
    }

    @Test
    public void getSessionToken() {
        assertEquals("ABCDEF", this.request.getSessionToken());
    }

    @Test
    public void getName() {
        assertEquals("billboard name", this.request.getName());
    }

    @Test
    public void toXML() {
        Document requestXML = this.request.toXML();

        Element requestEl = (Element) requestXML.getElementsByTagName("request").item(0);
        assertEquals("getBillboard", requestEl.getAttribute("type"));
        assertEquals("ABCDEF", requestEl.getAttribute("sessionToken"));

        Element nameEl = (Element) requestXML.getElementsByTagName("name").item(0);
        assertEquals("billboard name", nameEl.getTextContent());
    }

    @Test
    public void fromXML() throws ParseException {
        GetBillboardRequest request = GetBillboardRequest.fromXML(TestUtils.fromXMLString(
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                        "<request type=\"getBillboard\" sessionToken=\"ABCDEF\">" +
                        "<name>billboard name</name>" +
                        "</request>"

        ));

        assertEquals("ABCDEF", request.getSessionToken());
        assertEquals("billboard name", request.getName());
    }

    @Test
    public void fromXMLInvalidType() {
        assertThrows(ParseException.class, () -> {
            GetBillboardRequest request = GetBillboardRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"invalid\" sessionToken=\"ABCDEF\">" +
                            "<name>billboard name</name>" +
                            "</request>"
            ));
        });
    }

    @Test
    public void fromXMLNoNameElement() {
        assertThrows(ParseException.class, () -> {
            GetBillboardRequest request = GetBillboardRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<xrequest type=\"getBillboard\" sessionToken=\"ABCDEF\"></xrequest>"
            ));
        });
    }

    @Test
    public void fromXMLNoRequestElement() {
        assertThrows(ParseException.class, () -> {
            GetBillboardRequest request = GetBillboardRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<xrequest type=\"getBillboard\" sessionToken=\"ABCDEF\">" +
                            "<name>billboard name</name>" +
                            "</xrequest>"
            ));
        });
    }
}
