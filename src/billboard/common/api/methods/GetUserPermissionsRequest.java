package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.abstractXML.Attribute;
import billboard.common.abstractXML.Element;
import billboard.common.abstractXML.StringOutput;
import billboard.common.api.Request;
import org.w3c.dom.Document;

/**
 * Method class for creating and serialising/de-serialising a GetUserPermissions request.
 * Serialised request documents get sent to the server via the client.
 */
public class GetUserPermissionsRequest implements Request<GetUserPermissionsResponse> {
    private String sessionToken;
    private String username;

    /**
     * Creates a new get user permissions request.
     *
     * @param sessionToken The session token of the user making the request.
     * @param username The username of the user whose permissions are to be listed.
     */
    public GetUserPermissionsRequest(String sessionToken, String username){
        this.sessionToken = sessionToken;
        this.username = username;
    }

    /**
     * Returns the session token for the user making the request.
     *
     * @return The session token for the user making the request.
     */
    public String getSessionToken(){
        return sessionToken;
    }

    /**
     * Returns the username of the user whose permissions are to be listed.
     *
     * @return The username of the user whose permissions are to be listed.
     */
    public String getUsername(){
        return username;
    }

    /**
     * Serialise's this request into an XML document.
     * @see billboard.common.api.XMLSerialisable
     */
    @Override
    public Document toXML() {
        return new Element("request", new Attribute[] {
                new Attribute("type", "getUserPermissions"),
                new Attribute("sessionToken", this.getSessionToken())
        }, new Element[] {
                new Element("username", this.getUsername())
        }).getDocument();
    }

    /**
     * Deserialises a get user permissions request from XML.
     *
     * @param document The request XML document to deserialise.
     * @return The deserialised request.
     * @throws ParseException The request had an invalid type, or the document was in an invalid format.
     */
    public static GetUserPermissionsRequest fromXML(Document document) throws ParseException {
        StringOutput type = new StringOutput();
        StringOutput sessionToken = new StringOutput();
        StringOutput username = new StringOutput();

        new Element("request",
                new Attribute[]{
                        new Attribute("type", type),
                        new Attribute("sessionToken", sessionToken)
                },
                new Element[]{
                        new Element("username", username)
                }
        ).parseDocument(document);

        if(!type.getValue().equals("getUserPermissions")) {
            throw new ParseException(String.format("Invalid request type. Expected type was 'getUserPermissions', but received '%s'", type.getValue()));
        }

        return new GetUserPermissionsRequest(sessionToken.getValue(), username.getValue());
    }

    /**
     * Returns an appropriate for this request, response parsed from XML.
     * @see billboard.common.api.Request
     */
    @Override
    public GetUserPermissionsResponse responseFromXML(Document document) throws ParseException {
        return GetUserPermissionsResponse.fromXML(document);
    }
}
