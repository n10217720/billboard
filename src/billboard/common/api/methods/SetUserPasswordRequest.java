package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.abstractXML.Attribute;
import billboard.common.abstractXML.Element;
import billboard.common.abstractXML.StringOutput;
import billboard.common.api.Request;
import org.w3c.dom.Document;

/**
 * Method class for creating and serialising/de-serialising a SetUserPassword request.
 * Serialised request documents get sent to the server via the client.
 */
public class SetUserPasswordRequest implements Request<SetUserPasswordResponse> {
    private String sessionToken;
    private String username;
    private String newHashedPassword;

    /**
     * Creates a new set user password request.
     *
     * @param sessionToken The session token of the user making the request.
     * @param username The username of the user whose password is to be changed.
     * @param newHashedPassword The hash of the user's new password.
     */
    public SetUserPasswordRequest(String sessionToken, String username, String newHashedPassword) {
        this.sessionToken = sessionToken;
        this.username = username;
        this.newHashedPassword = newHashedPassword;
    }

    /**
     * Returns the session token for the user making the request.
     *
     * @return The session token for the user making the request.
     */
    public String getSessionToken() {
        return sessionToken;
    }

    /**
     * Returns the username of the user whose password is to be changed.
     *
     * @return The username of the user whose password is to be changed.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Returns the hash of the user's new password.
     *
     * @return The hash of the user's new password.
     */
    public String getNewHashedPassword() {
        return newHashedPassword;
    }

    /**
     * Serialise's this request into an XML document.
     * @see billboard.common.api.XMLSerialisable
     */
    @Override
    public Document toXML() {
        return new Element("request", new Attribute[] {
                new Attribute("type", "setUserPassword"),
                new Attribute("sessionToken", this.getSessionToken())
        }, new Element[] {
                new Element("username", this.getUsername()),
                new Element("newHashedPassword", this.getNewHashedPassword())
        }).getDocument();
    }

    /**
     * Deserialises a set user password request from XML.
     *
     * @param document The request XML document to deserialise.
     * @return The deserialised request.
     * @throws ParseException The request had an invalid type, or the document was in an invalid format.
     */
    public static SetUserPasswordRequest fromXML(Document document) throws ParseException {
        StringOutput type = new StringOutput();
        StringOutput sessionToken = new StringOutput();
        StringOutput username = new StringOutput();
        StringOutput newHashedPassword = new StringOutput();
        new Element("request",
                new Attribute[]{
                        new Attribute("type", type),
                        new Attribute("sessionToken", sessionToken)
                },
                new Element[]{
                        new Element("username", username),
                        new Element("newHashedPassword", newHashedPassword),
                }
        ).parseDocument(document);

        if(!type.getValue().equals("setUserPassword")) {
            throw new ParseException(String.format("Invalid request type. Expected type was 'userLogin', but received '%s'", type.getValue()));
        }

        return new SetUserPasswordRequest(sessionToken.getValue(), username.getValue(), newHashedPassword.getValue());
    }

    /**
     * Returns an empty success response from XML
     * @see billboard.common.api.EmptySuccessResponse
     */
    @Override
    public SetUserPasswordResponse responseFromXML(Document document) {
        return new SetUserPasswordResponse();
    }
}
