package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestUserLoginRequest {
    private UserLoginRequest request;

    @BeforeEach
    public void beforeEach() {
        this.request = new UserLoginRequest("username", "5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8");
    }

    @Test
    public void getUsername() {
        assertEquals("username", this.request.getUsername());
    }

    @Test
    public void getHashedPassword() {
        assertEquals("5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8", this.request.getHashedPassword());
    }

    @Test
    public void toXML() {
        Document requestXML = this.request.toXML();

        Element requestEl = (Element) requestXML.getElementsByTagName("request").item(0);
        assertEquals("userLogin", requestEl.getAttribute("type"));

        Element usernameEl = (Element) requestEl.getElementsByTagName("username").item(0);
        assertEquals("username", usernameEl.getTextContent());

        Element hashedPasswordEl = (Element) requestEl.getElementsByTagName("hashedPassword").item(0);
        assertEquals("5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8", hashedPasswordEl.getTextContent());
    }

    @Test
    public void fromXML() throws ParseException {
        UserLoginRequest request = UserLoginRequest.fromXML(TestUtils.fromXMLString(
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                        "<request type=\"userLogin\">\n" +
                        "\t<username>username</username>\n" +
                        "\t<hashedPassword>5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8</hashedPassword>\n" +
                        "</request>"
        ));

        assertEquals("username", request.getUsername());
        assertEquals("5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8", request.getHashedPassword());
    }

    @Test
    public void fromXMLInvalidType() {
        assertThrows(ParseException.class, () -> {
            UserLoginRequest request = UserLoginRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"invalid\">\n" +
                            "\t<username>username</username>\n" +
                            "\t<hashedPassword>5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8</hashedPassword>\n" +
                            "</request>"
            ));
        });
    }

    @Test
    public void fromXMLNoRequestElement() {
        assertThrows(ParseException.class, () -> {
            UserLoginRequest request = UserLoginRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<xrequest type=\"userLogin\">\n" +
                            "\t<username>username</username>\n" +
                            "\t<hashedPassword>5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8</hashedPassword>\n" +
                            "</xrequest>"
            ));
        });
    }

    @Test
    public void fromXMLNoUsernameElement() {
        assertThrows(ParseException.class, () -> {
            UserLoginRequest request = UserLoginRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"userLogin\">\n" +
                            "\t<xusername>username</xusername>\n" +
                            "\t<hashedPassword>5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8</hashedPassword>\n" +
                            "</request>"
            ));
        });
    }
}
