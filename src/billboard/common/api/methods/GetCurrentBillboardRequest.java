package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.abstractXML.Attribute;
import billboard.common.abstractXML.Element;
import billboard.common.abstractXML.StringOutput;
import billboard.common.api.Request;
import org.w3c.dom.Document;

/**
 * Method class for creating and serialising/de-serialising a GetCurrentBillboard request.
 * Serialised request documents get sent to the server via the client.
 */
public class GetCurrentBillboardRequest implements Request<GetCurrentBillboardResponse> {

    /**
     * Creates an empty get current showing billboard request constructor.
     */
    public GetCurrentBillboardRequest(){}

    /**
     * Returns an appropriate for this request, response parsed from XML.
     * @see billboard.common.api.Request
     */
    @Override
    public GetCurrentBillboardResponse responseFromXML(Document document) throws ParseException {
        return GetCurrentBillboardResponse.fromXML(document);
    }

    /**
     * Serialise's this request into an XML document.
     * @see billboard.common.api.XMLSerialisable
     */
    @Override
    public Document toXML() {
        return new Element("request", new Attribute[] {
                new Attribute("type", "getCurrentBillboard")
                 },
                new Element[]{
                }).getDocument();
    }

    /**
     * Deserialises a get current showing billboard request from XML.
     *
     * @param document The request XML document to deserialise.
     * @return The deserialised request.
     * @throws ParseException The request had an invalid type, or the document was in an invalid format.
     */
    public static GetCurrentBillboardRequest fromXML(Document document) throws ParseException {
        StringOutput type = new StringOutput();

        new Element("request",
                new Attribute[]{
                        new Attribute("type", type)
                },
                new Element[]{
                }).parseDocument(document);

        if(!type.getValue().equals("getCurrentBillboard")) {
            throw new ParseException(String.format("Invalid request type. Expected type was 'getCurrentBillboard', but received '%s'", type.getValue()));
        }

        return new GetCurrentBillboardRequest();
    }
}
