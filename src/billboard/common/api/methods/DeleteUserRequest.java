package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.abstractXML.Attribute;
import billboard.common.abstractXML.Element;
import billboard.common.abstractXML.StringOutput;
import billboard.common.api.Request;

import org.w3c.dom.Document;

/**
 * Method class for creating and serialising/de-serialising a deleteUser request.
 * Serialised request documents get sent to the server via the client.
 */
public class DeleteUserRequest implements Request<DeleteUserResponse> {
    private String username;
    private String sessionToken;

    /**
     * Creates a delete user request.
     *
     * @param sessionToken The user's hashed and salted password.
     * @param username The user's username.
     */
    public DeleteUserRequest(String sessionToken, String username){
        this.sessionToken = sessionToken;
        this.username = username;
    }

    /**
     * Returns the session token for the user making the request.
     *
     * @return The session token for the user making the request.
     */
    public String getSessionToken(){
        return sessionToken;
    }

    /**
     * Returns the username.
     *
     * @return The username.
     */
    public String getUsername(){
        return username;
    }

    /**
     * Serialise's this request into an XML document.
     * @see billboard.common.api.XMLSerialisable
     */
    @Override
    public Document toXML() {
        return new Element("request", new Attribute[] {
                new Attribute("type", "deleteUser"),
                new Attribute("sessionToken", this.getSessionToken())
        }, new Element[] {
                new Element("username", this.getUsername())
        }).getDocument();
    }

    /**
     * Deserialises a delete user request from XML.
     *
     * @param document The request XML document to deserialise.
     * @return The deserialised request.
     * @throws ParseException The request had an invalid type, or the document was in an invalid format.
     */
    public static DeleteUserRequest fromXML(Document document) throws ParseException{
        StringOutput type = new StringOutput();
        StringOutput username = new StringOutput();
        StringOutput sessionToken = new StringOutput();
        new Element("request",
                new Attribute[]{
                        new Attribute("type", type),
                        new Attribute("sessionToken", sessionToken)
                },
                new Element[]{
                        new Element("username", username)
                }
        ).parseDocument(document);

        if(!type.getValue().equals("deleteUser")){
            throw new ParseException(String.format("Invalid request type. Expected type was 'deleteUser', but received '%s'", type.getValue()));
        }

        return new DeleteUserRequest(sessionToken.getValue(), username.getValue());
    }

    /**
     * Returns an empty success response from XML
     * @see billboard.common.api.EmptySuccessResponse
     */
    @Override
    public DeleteUserResponse responseFromXML(Document document) {
        return new DeleteUserResponse();
    }
}
