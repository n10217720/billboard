package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.abstractXML.Attribute;
import billboard.common.abstractXML.Element;
import billboard.common.abstractXML.StringOutput;
import billboard.common.api.Request;
import org.w3c.dom.Document;

/**
 * Method class for creating and serialising/de-serialising a ListUsers request.
 * Serialised request documents get sent to the server via the client.
 */
public class ListUsersRequest implements Request<ListUsersResponse> {
    private String sessionToken;

    /**
     * Creates a new list users request.
     *
     * @param sessionToken The session token of the user making the request.
     */
    public ListUsersRequest(String sessionToken){
        this.sessionToken = sessionToken;
    }

    /**
     * Returns the session token for the user making the request.
     *
     * @return The session token for the user making the request.
     */
    public String getSessionToken(){
        return sessionToken;
    }

    /**
     * Serialise's this request into an XML document.
     * @see billboard.common.api.XMLSerialisable
     */
    @Override
    public Document toXML() {
        return new Element("request", new Attribute[] {
                new Attribute("type", "listUsers"),
                new Attribute("sessionToken", this.getSessionToken()), },
                new Element[]{
                }).getDocument();
    }

    /**
     * Deserialises a list users permissions request from XML.
     *
     * @param document The request XML document to deserialise.
     * @return The deserialised request.
     * @throws ParseException The request had an invalid type, or the document was in an invalid format.
     */
    public static ListUsersRequest fromXML(Document document) throws ParseException {
        StringOutput type = new StringOutput();
        StringOutput sessionToken = new StringOutput();

        new Element("request",
                new Attribute[]{
                        new Attribute("type", type),
                        new Attribute("sessionToken", sessionToken)
                },
                new Element[]{
                }).parseDocument(document);

        if(!type.getValue().equals("listUsers")) {
            throw new ParseException(String.format("Invalid request type. Expected type was 'listUsers', but received '%s'", type.getValue()));
        }

        return new ListUsersRequest(sessionToken.getValue());
    }

    /**
     * Returns an appropriate for this request, response parsed from XML.
     * @see billboard.common.api.Request
     */
    @Override
    public ListUsersResponse responseFromXML(Document document) throws ParseException {
        return ListUsersResponse.fromXML(document);
    }
}
