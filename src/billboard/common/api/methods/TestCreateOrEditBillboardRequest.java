package billboard.common.api.methods;

import billboard.common.Billboard;
import billboard.common.ColouredText;
import billboard.common.ParseException;
import billboard.common.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestCreateOrEditBillboardRequest {
    private static final String BILLBOARD_XML_TEXT = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?><billboard background=\"#FF0000\"><message colour=\"#FFFFFF\">Message</message></billboard>";
    private static final String BILLBOARD_XML_TEXT_ESCAPED = "&lt;?xml version=&quot;1.0&quot; encoding=&quot;UTF-8&quot; standalone=&quot;no&quot;?&gt;&lt;billboard background=&quot;#FF0000&quot;&gt;&lt;message colour=&quot;#FFFFFF&quot;&gt;Message&lt;/message&gt;&lt;/billboard&gt;";
    private Billboard billboard;
    private CreateOrEditBillboardRequest request;

    @BeforeEach
    public void beforeEach() {
        this.billboard = new Billboard();
        this.billboard.backgroundColour = Color.RED;
        this.billboard.message = new ColouredText("Message", Color.WHITE);
        this.request = new CreateOrEditBillboardRequest("ABCDEF", "billboard name", this.billboard);
    }

    @Test
    public void getSessionToken() {
        assertEquals("ABCDEF", this.request.getSessionToken());
    }

    @Test
    public void getName() {
        assertEquals("billboard name", this.request.getName());
    }

    @Test
    public void getBillboard() {
        assertEquals(BILLBOARD_XML_TEXT, this.request.getBillboard());
    }

    @Test
    public void toXML() {
        Document requestXML = this.request.toXML();

        Element requestEl = (Element) requestXML.getElementsByTagName("request").item(0);
        assertEquals("createOrEditBillboard", requestEl.getAttribute("type"));
        assertEquals("ABCDEF", requestEl.getAttribute("sessionToken"));

        Element nameEl = (Element) requestXML.getElementsByTagName("name").item(0);
        assertEquals("billboard name", nameEl.getTextContent());

        Element billboardEl = (Element) requestXML.getElementsByTagName("billboard").item(0);
        assertEquals(BILLBOARD_XML_TEXT, billboardEl.getTextContent());
    }

    @Test
    public void fromXML() throws ParseException {
        CreateOrEditBillboardRequest request = CreateOrEditBillboardRequest.fromXML(TestUtils.fromXMLString(
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                        "<request type=\"createOrEditBillboard\" sessionToken=\"ABCDEF\">" +
                        "<name>billboard name</name>" +
                        "<billboard>" + BILLBOARD_XML_TEXT_ESCAPED + "</billboard>" +
                        "</request>"

        ));

        assertEquals("ABCDEF", request.getSessionToken());
        assertEquals("billboard name", request.getName());
        assertEquals(BILLBOARD_XML_TEXT, request.getBillboard());
    }

    @Test
    public void fromXMLInvalidType() {
        assertThrows(ParseException.class, () -> {
            CreateOrEditBillboardRequest request = CreateOrEditBillboardRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<request type=\"invalid\" sessionToken=\"ABCDEF\">" +
                            "<name>billboard name</name>" +
                            "</request>"
            ));
        });
    }

    @Test
    public void fromXMLNoNameElement() {
        assertThrows(ParseException.class, () -> {
            CreateOrEditBillboardRequest request = CreateOrEditBillboardRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<xrequest type=\"createOrEditBillboard\" sessionToken=\"ABCDEF\">" +
                            "<billboard>" + BILLBOARD_XML_TEXT_ESCAPED + "</billboard>" +
                            "</xrequest>"
            ));
        });
    }

    @Test
    public void fromXMLNoBillboardElement() {
        assertThrows(ParseException.class, () -> {
            CreateOrEditBillboardRequest request = CreateOrEditBillboardRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<xrequest type=\"createOrEditBillboard\" sessionToken=\"ABCDEF\">" +
                            "<name>billboard name</name>" +
                            "</xrequest>"
            ));
        });
    }

    @Test
    public void fromXMLNoRequestElement() {
        assertThrows(ParseException.class, () -> {
            CreateOrEditBillboardRequest request = CreateOrEditBillboardRequest.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<xrequest type=\"createOrEditBillboard\" sessionToken=\"ABCDEF\">" +
                            "<name>billboard name</name>" +
                            "<billboard>" + BILLBOARD_XML_TEXT_ESCAPED + "</billboard>" +
                            "</xrequest>"
            ));
        });
    }
}
