package billboard.common.api.methods;

import billboard.common.ParseException;
import billboard.common.abstractXML.*;
import billboard.common.api.Request;
import billboard.server.Permission;
import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Method class for creating and serialising/de-serialising a SetUserPermissions request.
 * Serialised request documents get sent to the server via the client.
 */
public class SetUserPermissionsRequest implements Request<SetUserPermissionsResponse> {
    private String sessionToken;
    private String username;
    private HashSet<Permission> permissionList;

    /**
     * Creates a new set user permissions request.
     *
     * @param sessionToken The session token of the user making the request.
     * @param username The username of the user whose permissions are to be changed.
     * @param permissionList A HashSet of the permissions to be granted.
     */
    public SetUserPermissionsRequest(String sessionToken, String username, HashSet<Permission> permissionList){
        this.sessionToken = sessionToken;
        this.username = username;
        this.permissionList = permissionList;
    }

    /**
     * Returns the session token for the user making the request.
     *
     * @return The session token for the user making the request.
     */
    public String getSessionToken(){
        return sessionToken;
    }

    /**
     * Returns the username of the user whose permissions are to be changed.
     *
     * @return The username of the user whose permissions are to be changed.
     */
    public String getUsername(){
        return username;
    }

    /**
     * Returns a HashSet of the permissions to be granted.
     *
     * @return A HashSet of the permissions to be granted.
     */
    public HashSet<Permission> getPermissionList(){
        return permissionList;
    }

    /**
     * Serialise's this request into an XML document.
     * @see billboard.common.api.XMLSerialisable
     */
    @Override
    public Document toXML() {
        List<String> permissionsString = permissionList.stream()
                .map(Permission::toString)
                .collect(Collectors.toList());

        return new Element("request", new Attribute[] {
                new Attribute("type", "setUserPermissions"),
                new Attribute("sessionToken", this.getSessionToken())
        }, new Element[] {
                new Element("username", this.getUsername()),
                new Element("permissions", new ElementBase[] {
                        new Elements("permission", permissionsString)
                })
        }).getDocument();
    }

    /**
     * Deserialises a set user permissions request from XML.
     *
     * @param document The response XML document to deserialise.
     * @return The deserialised response.
     * @throws ParseException The request had an invalid type, or the document was in an invalid format.
     */
    public static SetUserPermissionsRequest fromXML(Document document) throws ParseException{
        StringOutput type = new StringOutput();
        StringOutput sessionToken = new StringOutput();
        StringOutput username = new StringOutput();
        HashSet<Permission> permissions = new HashSet<>();
        List<String> permissionsString = new ArrayList<>();

        new Element("request", new Attribute[] {
                new Attribute("type", type),
                new Attribute("sessionToken", sessionToken)
        }, new Element[] {
                new Element("username", username),
                new Element("permissions", new ElementBase[] {
                        new Elements("permission", permissionsString)
                })
        }).parseDocument(document);

        if(!type.getValue().equals("setUserPermissions")) {
            throw new ParseException(String.format("Invalid request type. Expected type was 'setUserPermissions', but received '%s'", type.getValue()));
        }

        for (String perm : permissionsString){
            permissions.add(Permission.fromString(perm));
        }

        return new SetUserPermissionsRequest(sessionToken.getValue(), username.getValue(), permissions);
    }

    /**
     * Returns an empty success response from XML
     * @see billboard.common.api.EmptySuccessResponse
     */
    @Override
    public SetUserPermissionsResponse responseFromXML(Document document) {
        return new SetUserPermissionsResponse();
    }
}
