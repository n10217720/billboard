package billboard.common.api;

/**
 * A response which can be issued to a client.
 */
public interface Response extends XMLSerialisable {}
