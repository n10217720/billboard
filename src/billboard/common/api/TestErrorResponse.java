package billboard.common.api;

import billboard.common.ParseException;
import billboard.common.TestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class TestErrorResponse {
    private ErrorResponse errorResponse;

    @BeforeEach
    public void beforeEach() {
        this.errorResponse = new ErrorResponse("This is the message");
    }

    @Test
    public void getMessage() {
        assertEquals("This is the message", this.errorResponse.getMessage());
    }

    @Test
    public void toXML() {
        Document document = this.errorResponse.toXML();

        Element responseEl = (Element) document.getElementsByTagName("response").item(0);
        assertEquals("error", responseEl.getAttribute("status"));

        Element messageEl = (Element) responseEl.getElementsByTagName("message").item(0);
        assertEquals("This is the message", messageEl.getTextContent());
    }

    @Test
    public void fromXML() throws ParseException {
        ErrorResponse response = ErrorResponse.fromXML(TestUtils.fromXMLString(
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                        "<response status=\"error\">\n" +
                        "\t<message>Error message</message>\n" +
                        "</response>"
        ));

        assertEquals("Error message", response.getMessage());
    }

    @Test
    public void fromXMLInvalidStatus() {
        assertThrows(ParseException.class, () -> {
            ErrorResponse response = ErrorResponse.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<response status=\"success\">\n" +
                            "</response>"
            ));
        });
    }

    @Test
    public void fromXMLNoResponseElement() {
        assertThrows(ParseException.class, () -> {
            ErrorResponse response = ErrorResponse.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<xresponse status=\"error\">\n" +
                            "\t<message>Error message</message>\n" +
                            "</xresponse>"
            ));
        });
    }

    @Test
    public void fromXMLNoMessageElement() {
        assertThrows(ParseException.class, () -> {
            ErrorResponse response = ErrorResponse.fromXML(TestUtils.fromXMLString(
                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                            "<response status=\"error\">\n" +
                            "</response>"
            ));
        });
    }
}
