package billboard.common.api;

import billboard.common.ParseException;
import org.w3c.dom.Document;

/**
 * A request which can be issued to a server.
 *
 * @param <TResponse> The type of the response the server returns.
 */
public interface Request<TResponse extends Response> extends XMLSerialisable {
    /**
     * Parses the response associated with this request.
     *
     * @param document The XML document containing the response.
     * @return The parsed response.
     * @throws ParseException Thrown if parsing the response XML failed.
     */
    TResponse responseFromXML(Document document) throws ParseException;
}
