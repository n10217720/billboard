package billboard.common.api.errors;

import billboard.common.api.ErrorResponse;
import billboard.server.Permission;

/**
 * A common error response used by the server when trying to receive and parse an XML request.
 */
public class PermissionRequiredErrorResponse extends ErrorResponse {
    private Permission missingPermission;

    /**
     * Creates an error response for when a required permission is not possessed by the requesting user.
     *
     * @param missingPermission The permission that was required, but not held.
     */
    public PermissionRequiredErrorResponse(Permission missingPermission) {
        super(String.format("Permission %s required", missingPermission));

        this.missingPermission = missingPermission;
    }

    /**
     * Returns the permission that was required, but not held.
     *
     * @return The permission that was required, but not held.
     */
    public Permission getMissingPermission() {
        return this.missingPermission;
    }
}
