package billboard.common.api.errors;

import billboard.common.api.ErrorResponse;

/**
 * A common error response used by the server when trying to receive and parse an XML request.
 */
public class AuthenticationRequiredErrorResponse extends ErrorResponse {
    /**
     * Creates an error response for when a request requires authentication (by way of a session token), but one was not
     * provided.
     */
    public AuthenticationRequiredErrorResponse() {
        super("Authentication required");
    }
}
