package billboard.common.api.errors;

import billboard.common.api.ErrorResponse;

/**
 * A common error response used by the server when trying to receive and parse an XML request.
 */
public class MalformedRequestErrorResponse extends ErrorResponse {
    /**
     * Creates an error response for when a request is malformed at a low-level, such as not being an XML document,
     * failing to specify a type, or having an invalid type.
     */
    public MalformedRequestErrorResponse() {
        super("Malformed request");
    }
}
