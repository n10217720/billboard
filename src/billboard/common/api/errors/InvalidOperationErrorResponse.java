package billboard.common.api.errors;

import billboard.common.api.ErrorResponse;

/**
 * A common error response used by the server when trying to receive and parse an XML request.
 */
public class InvalidOperationErrorResponse extends ErrorResponse {
    /**
     * Creates an error response for when a request attempts to perform an invalid operation, which likely could not
     * have been caused by a user's action.
     */
    public InvalidOperationErrorResponse() {
        super("Invalid operation");
    }
}
