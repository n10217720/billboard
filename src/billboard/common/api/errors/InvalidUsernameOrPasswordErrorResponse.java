package billboard.common.api.errors;

import billboard.common.api.ErrorResponse;

/**
 * A common error response used by the server when trying to receive and parse an XML request.
 */
public class InvalidUsernameOrPasswordErrorResponse extends ErrorResponse {
    /**
     * Creates an error response for a login request which fails due to an incorrect username or password.
     */
    public InvalidUsernameOrPasswordErrorResponse() {
        super("Invalid username or password");
    }
}
