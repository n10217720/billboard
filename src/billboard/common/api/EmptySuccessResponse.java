package billboard.common.api;

import billboard.common.abstractXML.Attribute;
import billboard.common.abstractXML.Element;
import org.w3c.dom.Document;

/**
 * A base class for responses which do not return any information.
 */
public abstract class EmptySuccessResponse implements Response {

    /**
     * Serialise's this response into an XML document.
     * @see billboard.common.api.XMLSerialisable
     */
    @Override
    public Document toXML() {
        return new Element("response", new Attribute[]{
                new Attribute("status", "success")
        }, new Element[]{}).getDocument();
    }
}
