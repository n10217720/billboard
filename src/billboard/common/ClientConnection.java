package billboard.common;

import billboard.common.abstractXML.DocumentToString;
import billboard.common.api.Request;
import billboard.common.api.Response;

import java.io.*;
import java.net.Socket;

/**
 * Represents a connection by a client to the server, allowing requests to be made both in a blocking and
 * semi-non-blocking manner.
 */
public class ClientConnection {
    private String host;
    private int port;

    /**
     * Creates a connection to a specified host, on a specified port. Does not open the connection until a request is
     * made.
     *
     * @param host The hostname of the server.
     * @param port The port to connect to the server using.
     */
    public ClientConnection(String host, int port) {
        this.host = host;
        this.port = port;
    }

    /**
     * Sends the specified stringified request to the server, and returns the string the server replied with.
     *
     * @param request The stringified request.
     * @return The server's response.
     * @throws RequestFailedException Thrown if connecting to, sending the request, or receiving the reply failed.
     */
    private String makeRequest(String request) throws RequestFailedException {
        String response;
        try (
                Socket socket = new Socket(this.host, this.port);
                PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        ) {
            out.println(request);
            response = in.readLine();
        } catch (IOException e) {
            throw new RequestFailedException("Could not connect to server", e);
        }

        if (response == null) {
            throw new RequestFailedException("Server did not send a response");
        }

        return response;
    }

    /**
     * Sends a request to the server, returning the associated response.
     *
     * A matching {@code TResponse} is always provided for an {@code Request}, and as such the correct response for the
     * request will always be returned.
     *
     * @param request The request to send.
     * @param <TResponse> The type of the response associated with the request.
     * @return The server's response to the request.
     * @throws RequestFailedException Thrown if the request could not be made, or the response failed or was invalid.
     */
    public<TResponse extends Response> TResponse makeRequest(Request<TResponse> request) throws RequestFailedException {
        String requestText = DocumentToString.documentToString(request.toXML());
        String responseText = makeRequest(requestText);
        return ClientProtocol.parseResponse(request, responseText);
    }

    /**
     * Sends a request to the server in the background, returning the associated response. Displays a modal dialog
     * informing the user that a request is in progress, and shows an error dialog if the request failed.
     *
     * A matching {@code TResponse} is always provided for an {@code Request}, and as such the correct response for the
     * request will always be returned.
     *
     * @param request The request to send.
     * @param <TResponse> The type of the response associated with the request.
     * @return The server's response to the request, or null if the request failed.
     */
    public<TResponse extends Response> TResponse makeRequestBackground(Request<TResponse> request) {
        return BackgroundRequest.makeRequestBackground(this, request);
    }
}
