package billboard.common;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * Class for handling the reading and loading of the connection properties for the server.
 */
public class ConnectionProperties {
    /** The default file name for the connection properties file. */
    public static final String DEFAULT_FILENAME = "connection.props";

    private String hostname;
    private int port;

    /**
     * Loads the properties of a connection (port and hostname) from a file.
     *
     * @param filename The path to the file to read the properties from.
     * @throws IOException The file could not be read.
     * @throws Exception An error occurred parsing the properties file contents.
     */
    public ConnectionProperties(String filename) throws IOException, Exception {
        Properties properties = new Properties();
        try (InputStream file = Files.newInputStream(Paths.get(filename))) {
            properties.load(file);
        }

        if (!properties.containsKey("hostname")) {
            throw new Exception("Properties file did not contain hostname");
        }

        this.hostname = properties.getProperty("hostname");

        if (!properties.containsKey("port")) {
            throw new Exception("Properties file did not contain port");
        }

        String portString = properties.getProperty("port");
        try {
            this.port = Integer.parseInt(portString);
        } catch (NumberFormatException e) {
            throw new Exception("Properties value contained non-numeric port value", e);
        }
    }

    /**
     * Returns the hostname from the properties file.
     *
     * @return The hostname from the properties file.
     */
    public String getHostname() {
        return hostname;
    }

    /**
     * Returns the port from the properties file.
     *
     * @return The port from the properties file.
     */
    public int getPort() {
        return port;
    }
}
