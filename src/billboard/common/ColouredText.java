package billboard.common;

import java.awt.*;

/**
 * Represents a piece of text which is drawn in a specified colour.
 */
public class ColouredText {
    public String text;
    public Color colour;

    /**
     * Creates an instance of {@link ColouredText}.
     *
     * @param text The text to be drawn.
     * @param colour The colour to draw the text in.
     */
    public ColouredText(String text, Color colour) {
        this.text = text;
        this.colour = colour;
    }
}
