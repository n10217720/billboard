package billboard.common;

import billboard.common.abstractXML.Attribute;
import billboard.common.abstractXML.Element;
import billboard.common.abstractXML.StringOutput;
import billboard.common.api.ErrorResponse;
import billboard.common.api.Request;
import billboard.common.api.Response;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * Handles the conversion of responses from string representations of their XML to XML objects, and into the response
 * type expected for the request that was issued.
 */
public class ClientProtocol {
    /**
     * Parses a server's response to the provided request. The request is used only to apply the correct response
     * parsing.
     *
     * @param request The request that has already been made.
     * @param responseText The response the server returned.
     * @param <TResponse> The type of the response associated with the request.
     * @return The parsed response.
     * @throws RequestFailedException Thrown if the response could not be parsed.
     */
    public static <TResponse extends Response> TResponse parseResponse(Request<TResponse> request, String responseText) throws RequestFailedException {
        // Attempt to parse the response into an XML document
        Document responseDocument;
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newDefaultInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            responseDocument = db.parse(new ByteArrayInputStream(responseText.getBytes("UTF-8")));
        } catch (ParserConfigurationException | SAXException | IOException e) {
            throw new RequestFailedException("Could not parse response as XML document", e);
        }

        // Extract the status of the response, failing if the response is already malformed at this stage
        StringOutput statusOutput = new StringOutput();
        try {
            new Element("response", new Attribute[] {
                    new Attribute("status", statusOutput)
            }, new Element[]{}).parseDocument(responseDocument);
        } catch (ParseException e) {
            throw new RequestFailedException("Response was not a valid response XML document", e);
        }

        String status = statusOutput.getValue();
        if (!status.equals("success") && !status.equals("error")) {
            throw new RequestFailedException("Response had invalid status");
        }

        // Our response is now definitely either an error response, or a success response
        if (status.equals("error")) {
            try {
                throw new RequestErrorException(ErrorResponse.fromXML(responseDocument));
            } catch (ParseException e) {
                throw new RequestFailedException("Could not parse error response", e);
            }
        }

        try {
            return request.responseFromXML(responseDocument);
        } catch (ParseException e) {
            throw new RequestFailedException("Could not parse response sub-type", e);
        }
    }
}
