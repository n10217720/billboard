package billboard.common;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.fail;

/**
 * A generic class providing common test functions specifically relating to XML parsing.
 */
public class TestUtils {
    /**
     * Reads a string representation of an XML document into a Document, failing the test if the document can not be
     * parsed.
     *
     * @param xml The string representation of the XML document.
     * @return The parsed XML document.
     */
    public static Document fromXMLString(String xml) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newDefaultInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            return db.parse(new ByteArrayInputStream(xml.getBytes("UTF-8")));
        } catch (ParserConfigurationException | SAXException | IOException e) {
            fail("Could not parse XML", e);

            // Should be unreachable, but still required
            return null;
        }
    }
}
