package billboard.common;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class TestScheduledBillboard {
    private static final ZoneId ZONE = ZoneId.systemDefault();

    private ScheduledBillboard sbOnce;
    private ScheduledBillboard sbRepeat;

    @BeforeEach
    public void beforeEach() {
        this.sbOnce = new ScheduledBillboard(
                "Test billboard",
                // 2020-01-01T08:00:00
                ZonedDateTime.of(2020, 1, 1, 8, 0, 0, 0, ZONE),
                5,
                null
        );
        this.sbRepeat =new ScheduledBillboard(
                "Test billboard",
                // 2020-01-01T08:00:00
                ZonedDateTime.of(2020, 1, 1, 8, 0, 0, 0, ZONE),
                5,
                Duration.ofHours(23)
        );
    }

    @Test
    public void once() {
        // Before showing
        assertFalse(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 1, 7, 0, 0, 0, ZONE)));
        assertFalse(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 1, 7, 59, 0, 0, ZONE)));
        assertFalse(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 1, 7, 59, 59, 0, ZONE)));
        assertFalse(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 1, 7, 59, 59, 999999999, ZONE)));

        // During showing
        assertTrue(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 1, 8, 0, 0, 0, ZONE)));
        assertTrue(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 1, 8, 2, 30, 0, ZONE)));
        assertTrue(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 1, 8, 4, 59, 999999999, ZONE)));

        // After showing
        assertFalse(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 1, 8, 5, 0, 0, ZONE)));
        assertFalse(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 1, 8, 6, 0, 0, ZONE)));
    }

    @Test
    public void onceGetShowingsNegativePeriodEmpty() {
        assertEquals(List.of(), this.sbOnce.getShowingsBetween(
                ZonedDateTime.of(2020, 1, 2, 0, 0, 0, 0, ZONE),
                ZonedDateTime.of(2020, 1, 1, 0, 0, 0, 0, ZONE)
        ));
    }

    @Test
    public void onceGetShowingsZeroPeriodEmpty() {
        assertEquals(List.of(), this.sbOnce.getShowingsBetween(
                ZonedDateTime.of(2020, 1, 1, 0, 0, 0, 0, ZONE),
                ZonedDateTime.of(2020, 1, 1, 0, 0, 0, 0, ZONE)
        ));
    }

    @Test
    public void onceGetShowingsBeforeInitialEmpty() {
        assertEquals(List.of(), this.sbOnce.getShowingsBetween(
                ZonedDateTime.of(2019, 1, 1, 0, 0, 0, 0, ZONE),
                ZonedDateTime.of(2019, 1, 2, 0, 0, 0, 0, ZONE)
        ));
    }

    @Test
    public void onceGetShowingsAfterInitialEmpty() {
        assertEquals(List.of(), this.sbOnce.getShowingsBetween(
                ZonedDateTime.of(2021, 1, 1, 0, 0, 0, 0, ZONE),
                ZonedDateTime.of(2021, 1, 2, 0, 0, 0, 0, ZONE)
        ));
    }

    @Test
    public void onceGetShowings() {
        assertEquals(List.of(
                ZonedDateTime.of(2020, 1, 1, 8, 0, 0, 0, ZONE)
        ), this.sbOnce.getShowingsBetween(
                ZonedDateTime.of(2020, 1, 1, 0, 0, 0, 0, ZONE),
                ZonedDateTime.of(2020, 1, 2, 0, 0, 0, 0, ZONE)
        ));

        // Inclusive of start point
        assertEquals(List.of(
                ZonedDateTime.of(2020, 1, 1, 8, 0, 0, 0, ZONE)
        ), this.sbOnce.getShowingsBetween(
                ZonedDateTime.of(2020, 1, 1, 8, 0, 0, 0, ZONE),
                ZonedDateTime.of(2020, 1, 2, 0, 0, 0, 0, ZONE)
        ));

        // Exclusive of end point
        assertEquals(List.of(), this.sbOnce.getShowingsBetween(
                ZonedDateTime.of(2020, 1, 1, 0, 0, 0, 0, ZONE),
                ZonedDateTime.of(2020, 1, 1, 8, 0, 0, 0, ZONE)
        ));
    }

    @Test
    public void repeatFirstShowing() {
        // Before showing
        assertFalse(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 1, 7, 0, 0, 0, ZONE)));
        assertFalse(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 1, 7, 59, 0, 0, ZONE)));
        assertFalse(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 1, 7, 59, 59, 0, ZONE)));
        assertFalse(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 1, 7, 59, 59, 999999999, ZONE)));

        // During showing
        assertTrue(this.sbRepeat.isShowingNow(ZonedDateTime.of(2020, 1, 1, 8, 0, 0, 0, ZONE)));
        assertTrue(this.sbRepeat.isShowingNow(ZonedDateTime.of(2020, 1, 1, 8, 2, 30, 0, ZONE)));
        assertTrue(this.sbRepeat.isShowingNow(ZonedDateTime.of(2020, 1, 1, 8, 4, 59, 999999999, ZONE)));

        // After showing
        assertFalse(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 1, 8, 5, 0, 0, ZONE)));
        assertFalse(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 1, 8, 6, 0, 0, ZONE)));
    }

    @Test
    public void repeatSecondShowing() {
        // Before showing
        assertFalse(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 2, 6, 0, 0, 0, ZONE)));
        assertFalse(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 2, 6, 59, 0, 0, ZONE)));
        assertFalse(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 2, 6, 59, 59, 0, ZONE)));
        assertFalse(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 2, 6, 59, 59, 999999999, ZONE)));

        // During showing
        assertTrue(this.sbRepeat.isShowingNow(ZonedDateTime.of(2020, 1, 2, 7, 0, 0, 0, ZONE)));
        assertTrue(this.sbRepeat.isShowingNow(ZonedDateTime.of(2020, 1, 2, 7, 2, 30, 0, ZONE)));
        assertTrue(this.sbRepeat.isShowingNow(ZonedDateTime.of(2020, 1, 2, 7, 4, 59, 999999999, ZONE)));

        // After showing
        assertFalse(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 2, 7, 5, 0, 0, ZONE)));
        assertFalse(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 2, 7, 6, 0, 0, ZONE)));
    }

    @Test
    public void repeatFourthShowing() {
        // Before showing
        assertFalse(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 4, 4, 0, 0, 0, ZONE)));
        assertFalse(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 4, 4, 59, 0, 0, ZONE)));
        assertFalse(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 4, 4, 59, 59, 0, ZONE)));
        assertFalse(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 4, 4, 59, 59, 999999999, ZONE)));

        // During showing
        assertTrue(this.sbRepeat.isShowingNow(ZonedDateTime.of(2020, 1, 4, 5, 0, 0, 0, ZONE)));
        assertTrue(this.sbRepeat.isShowingNow(ZonedDateTime.of(2020, 1, 4, 5, 2, 30, 0, ZONE)));
        assertTrue(this.sbRepeat.isShowingNow(ZonedDateTime.of(2020, 1, 4, 5, 4, 59, 999999999, ZONE)));

        // After showing
        assertFalse(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 4, 5, 5, 0, 0, ZONE)));
        assertFalse(this.sbOnce.isShowingNow(ZonedDateTime.of(2020, 1, 4, 5, 6, 0, 0, ZONE)));
    }

    @Test
    public void repeatGetShowingsNegativePeriodEmpty() {
        assertEquals(List.of(), this.sbRepeat.getShowingsBetween(
                ZonedDateTime.of(2020, 1, 2, 0, 0, 0, 0, ZONE),
                ZonedDateTime.of(2020, 1, 1, 0, 0, 0, 0, ZONE)
        ));
    }
    @Test
    public void repeatGetShowingsZeroPeriodEmpty() {
        assertEquals(List.of(), this.sbRepeat.getShowingsBetween(
                ZonedDateTime.of(2020, 1, 1, 0, 0, 0, 0, ZONE),
                ZonedDateTime.of(2020, 1, 1, 0, 0, 0, 0, ZONE)
        ));
    }

    @Test
    public void repeatGetShowingsBeforeInitialEmpty() {
        assertEquals(List.of(), this.sbRepeat.getShowingsBetween(
                ZonedDateTime.of(2019, 1, 1, 0, 0, 0, 0, ZONE),
                ZonedDateTime.of(2019, 1, 2, 0, 0, 0, 0, ZONE)
        ));

        assertEquals(List.of(), this.sbRepeat.getShowingsBetween(
                ZonedDateTime.of(2019, 12, 1, 0, 0, 0, 0, ZONE),
                ZonedDateTime.of(2020, 1, 1, 0, 0, 0, 0, ZONE)
        ));
    }

    @Test
    public void repeatGetShowingsFirst() {
        assertEquals(List.of(
                ZonedDateTime.of(2020, 1, 1, 8, 0, 0, 0, ZONE)
        ), this.sbRepeat.getShowingsBetween(
                ZonedDateTime.of(2020, 1, 1, 0, 0, 0, 0, ZONE),
                ZonedDateTime.of(2020, 1, 2, 0, 0, 0, 0, ZONE)
        ));

        // Inclusive of start point
        assertEquals(List.of(
                ZonedDateTime.of(2020, 1, 1, 8, 0, 0, 0, ZONE)
        ), this.sbRepeat.getShowingsBetween(
                ZonedDateTime.of(2020, 1, 1, 8, 0, 0, 0, ZONE),
                ZonedDateTime.of(2020, 1, 2, 0, 0, 0, 0, ZONE)
        ));

        // Exclusive of end point
        assertEquals(List.of(), this.sbRepeat.getShowingsBetween(
                ZonedDateTime.of(2020, 1, 1, 0, 0, 0, 0, ZONE),
                ZonedDateTime.of(2020, 1, 1, 8, 0, 0, 0, ZONE)
        ));
    }

    @Test
    public void repeatGetShowingsSecond() {
        assertEquals(List.of(
                ZonedDateTime.of(2020, 1, 2, 7, 0, 0, 0, ZONE)
        ), this.sbRepeat.getShowingsBetween(
                ZonedDateTime.of(2020, 1, 2, 0, 0, 0, 0, ZONE),
                ZonedDateTime.of(2020, 1, 3, 0, 0, 0, 0, ZONE)
        ));

        // Inclusive of start point
        assertEquals(List.of(
                ZonedDateTime.of(2020, 1, 2, 7, 0, 0, 0, ZONE)
        ), this.sbRepeat.getShowingsBetween(
                ZonedDateTime.of(2020, 1, 2, 7, 0, 0, 0, ZONE),
                ZonedDateTime.of(2020, 1, 3, 0, 0, 0, 0, ZONE)
        ));

        // Exclusive of end point
        assertEquals(List.of(), this.sbRepeat.getShowingsBetween(
                ZonedDateTime.of(2020, 1, 2, 0, 0, 0, 0, ZONE),
                ZonedDateTime.of(2020, 1, 2, 7, 0, 0, 0, ZONE)
        ));
    }

    @Test
    public void repeatGetShowingsFirstAndSecond() {
        assertEquals(List.of(
                ZonedDateTime.of(2020, 1, 1, 8, 0, 0, 0, ZONE),
                ZonedDateTime.of(2020, 1, 2, 7, 0, 0, 0, ZONE)
        ), this.sbRepeat.getShowingsBetween(
                ZonedDateTime.of(2020, 1, 1, 0, 0, 0, 0, ZONE),
                ZonedDateTime.of(2020, 1, 3, 0, 0, 0, 0, ZONE)
        ));

        // Inclusive of start point
        assertEquals(List.of(
                ZonedDateTime.of(2020, 1, 1, 8, 0, 0, 0, ZONE),
                ZonedDateTime.of(2020, 1, 2, 7, 0, 0, 0, ZONE)
        ), this.sbRepeat.getShowingsBetween(
                ZonedDateTime.of(2020, 1, 1, 8, 0, 0, 0, ZONE),
                ZonedDateTime.of(2020, 1, 3, 0, 0, 0, 0, ZONE)
        ));

        // Exclusive of end point
        assertEquals(List.of(
                ZonedDateTime.of(2020, 1, 1, 8, 0, 0, 0, ZONE)
        ), this.sbRepeat.getShowingsBetween(
                ZonedDateTime.of(2020, 1, 1, 0, 0, 0, 0, ZONE),
                ZonedDateTime.of(2020, 1, 2, 7, 0, 0, 0, ZONE)
        ));
    }

    @Test
    public void getName() {
        assertEquals("Test billboard", this.sbOnce.getName());
        assertEquals("Test billboard", this.sbRepeat.getName());
    }

    @Test
    public void getInitialShowing() {
        assertEquals(ZonedDateTime.of(2020, 1, 1, 8, 0, 0, 0, ZONE), this.sbOnce.getInitialShowing());
        assertEquals(ZonedDateTime.of(2020, 1, 1, 8, 0, 0, 0, ZONE), this.sbRepeat.getInitialShowing());
    }

    @Test
    public void getDuration() {
        assertEquals(5, this.sbOnce.getDuration());
        assertEquals(5, this.sbRepeat.getDuration());
    }

    @Test
    public void getRecurrencePeriod() {
        assertEquals(null, this.sbOnce.getRecurrencePeriod());
        assertEquals(Duration.ofHours(23), this.sbRepeat.getRecurrencePeriod());
    }
}
