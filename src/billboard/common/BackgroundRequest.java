package billboard.common;

import billboard.common.api.Request;
import billboard.common.api.Response;

import javax.swing.*;
import java.awt.*;

/**
 * Provides a method of issuing a request to the server in the background, initially blocking but displaying a dialog to
 * inform the user if the request takes longer than a certain period of time.
 *
 * @param <TResponse> The type of the response expected from the server.
 */
public class BackgroundRequest<TResponse extends Response> extends JDialog {
    private static final long MAX_NO_FEEDBACK_TIME_MS = 100;

    private boolean complete;
    private TResponse response;
    private RequestFailedException e;

    /**
     * Creates a new background request window.
     *
     * Visibility logic primarilly handled by {@code makeRequestBackground}.
     *
     * @param connection The connection to issue the request over.
     * @param request The request to issue.
     */
    private BackgroundRequest(ClientConnection connection, Request<TResponse> request) {
        super();

        this.complete = false;
        this.response = null;
        this.e = null;

        this.setModalityType(ModalityType.APPLICATION_MODAL);
        this.setModal(true);
        this.setTitle("Request in progress");

        this.setLayout(new BorderLayout());
        JLabel label = new JLabel("Please wait...");
        label.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        this.add(label, BorderLayout.CENTER);

        pack();

        SwingWorker<Object, Object> worker = new SwingWorker<>() {
            @Override
            protected Object doInBackground() {
                try {
                    BackgroundRequest.this.response = connection.makeRequest(request);
                } catch (RequestFailedException e) {
                    BackgroundRequest.this.e = e;
                }

                BackgroundRequest.this.complete = true;

                // We're not using the return value, since we return through the response object
                return null;
            }

            @Override
            protected void done() {
                // Hide ourselves, if we were even visible in the first place
                BackgroundRequest.this.dispose();
            }
        };

        worker.execute();
    }

    @Override
    public void setVisible(boolean b) {
        if (b && this.complete) {
            // We're already done. We shouldn't be shown.
            return;
        }

        super.setVisible(b);
    }

    /**
     * Sends a request to the server in the background, returning the associated response. Displays a modal dialog
     * informing the user that a request is in progress, and shows an error dialog if the request failed.
     *
     * A matching {@code TResponse} is always provided for an {@code Request}, and as such the correct response for the
     * request will always be returned.
     *
     * @param connection The connection to use for the request.
     * @param request The request to send.
     * @param <TResponse> The type of the response associated with the request.
     * @return The server's response to the request, or null if the request failed.
     */
    public static<TResponse extends Response> TResponse makeRequestBackground(ClientConnection connection, Request<TResponse> request) {
        BackgroundRequest<TResponse> worker = new BackgroundRequest<>(connection, request);

        long startTime = System.currentTimeMillis();
        while (((System.currentTimeMillis() - startTime) < MAX_NO_FEEDBACK_TIME_MS) && !worker.complete) {
            // Do nothing - busy wait for now.
        }

        // If we timed out, show the dialog, since the request is clearly taking a while.
        // Will be ignored if we're complete.
        worker.setVisible(true);

        if (worker.e != null) {
            if (worker.e instanceof RequestErrorException) {
                String errorMessage = ((RequestErrorException) worker.e).getErrorResponse().getMessage();
                JOptionPane.showMessageDialog(null, errorMessage, "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(null, worker.e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
            }

            return null;
        }

        return worker.response;
    }
}
