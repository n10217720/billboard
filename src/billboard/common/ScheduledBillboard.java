package billboard.common;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

/**
 * Class object for a scheduled billboard entry.
 * Contains all relevant schedule details for a billboard object to be added to a schedule.
 */
public class ScheduledBillboard {
    private String name;
    private ZonedDateTime initialShowing;
    private long duration;
    private Duration recurrencePeriod;

    /**
     * Creates a schedule entry for a billboard.
     *
     * @param name The billboard's name.
     * @param initialShowing The date and time of the first showing of the billboard.
     * @param duration The duration of the showing, in minutes.
     * @param recurrencePeriod How often the billboard is shown, or null for a one-off showing.
     */
    public ScheduledBillboard(String name, ZonedDateTime initialShowing, long duration, Duration recurrencePeriod) {
        this.name = name;
        this.initialShowing = initialShowing;
        this.duration = duration;
        this.recurrencePeriod = recurrencePeriod;
    }

    /**
     * Determines if the billboard is currently showing based on its initial showing date and time, duration and
     * recurrence period.
     *
     * @param now The current time.
     * @return True if the billboard is currently showing, false otherwise.
     */
    public boolean isShowingNow(ZonedDateTime now) {
        if (now.isBefore(this.initialShowing)) {
            return false;
        }

        // Initial and non-recurring showings
        long deltaMinutes = this.initialShowing.until(now, ChronoUnit.MINUTES);
        if (deltaMinutes < this.duration) {
            return true;
        }

        if (this.recurrencePeriod == null) {
            // We're not in the initial showing, and there's no other showings, so we can't possibly be showing now.
            return false;
        }

        // This logic is fun: let's explain
        // deltaMinutes % recurrenceMinutes basically wraps time around a continuous loop which is recurrenceMinutes
        // long. Thus, every showing starts at 0 and ends the instant before this.durationMinutes, and the rest of the
        // time the billboard isn't shown.
        // That maps well to our problem, since just like our showing, the loop we make has a period of
        // recurrenceMinutes!
        long recurrenceMinutes = this.recurrencePeriod.toMinutes();
        if (deltaMinutes % recurrenceMinutes < this.duration) {
            return true;
        }

        return false;
    }

    /**
     * Returns a list of showings which occur between two dates and times. The period is inclusive of the start point,
     * and exclusive of the end point.
     *
     * @param start The start of the period.
     * @param end The end of the period.
     * @return A possibly empty list of the showings which occur during the period.
     */
    public List<ZonedDateTime> getShowingsBetween(ZonedDateTime start, ZonedDateTime end) {
        if (start.isAfter(end)) {
            return List.of();
        }

        if (this.recurrencePeriod == null) {
            // Billboard is only shown once: if the period contains the initial showing, return the initial showing
            boolean startBeforeOrEqual = start.isBefore(this.initialShowing) || start.isEqual(this.initialShowing);
            boolean endAfter = end.isAfter(this.initialShowing);
            if (startBeforeOrEqual && endAfter) {
                return List.of(this.initialShowing);
            } else {
                return List.of();
            }
        }

        // The billboard recurs - find the first recurrence within the period (though not necessarily before the end of
        // the period yet)
        ZonedDateTime firstRecurrenceInPeriod;
        if (start.isBefore(this.initialShowing)) {
            // If the start point is before the initial showing, the first recurrence must be the initial showing.
            firstRecurrenceInPeriod = this.initialShowing;
        } else {
            // If the initial showing is before the start point, work out the minimum number of recurrences that must've
            // occurred to get us to the first showing just after the start of the period.
            // We can use minutes since the shortest recurrence period possible is 1 minute.
            long delta = this.initialShowing.until(start, ChronoUnit.MINUTES);
            long recurrences = (long) Math.ceil((double) delta / this.recurrencePeriod.toMinutes());
            firstRecurrenceInPeriod = this.initialShowing.plus(recurrences * this.recurrencePeriod.toMinutes(), ChronoUnit.MINUTES);
        }

        // Step one recurrence period at a time until we shoot past the end
        List<ZonedDateTime> showings = new ArrayList<>();
        ZonedDateTime nextRecurrence = firstRecurrenceInPeriod;
        while (nextRecurrence.isBefore(end)) {
            showings.add(nextRecurrence);
            nextRecurrence = nextRecurrence.plus(this.recurrencePeriod);
        }

        return showings;
    }

    /**
     * Returns the name of the billboard.
     *
     * @return The name of the billboard.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Returns the date and time of the initial showing.
     *
     * @return The date and time of the initial showing.
     */
    public ZonedDateTime getInitialShowing() {
        return this.initialShowing;
    }

    /**
     * Returns the duration of the billboard's showing in minutes.
     *
     * @return The duration of the billboard's showing in minutes.
     */
    public long getDuration() {
        return this.duration;
    }

    /**
     * Returns the billboard's recurrence period.
     *
     * @return The billboard's recurrence period.
     */
    public Duration getRecurrencePeriod() {
        return this.recurrencePeriod;
    }
}
