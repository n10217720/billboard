package billboard.common;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Class for handling the generation and creation of hashes using the SHA3-512 algorithm (used for hashing passwords).
 */
public class HashSHA3512 {
    private static MessageDigest digest;

    static {
        try {
            digest = MessageDigest.getInstance("SHA3-512");
        } catch (NoSuchAlgorithmException e) {
            // We absolutely can't function without this message digest provider. There's nothing better to do than
            // crash, because there's no way we can handle this.
            System.err.println("Could not get instance of MessageDigest which supports SHA3-512");
            e.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Hashes the provided string as a UTF-8 byte sequence using the SHA3-512 algorithm, and returns the resulting hash
     * as a lowercase hexidecimal string.
     *
     * @param plaintext The string to hash.
     * @return The resultant hash.
     */
    public static String generateHash(String plaintext) {
        digest.reset();
        byte[] hashBytes = digest.digest(plaintext.getBytes(StandardCharsets.UTF_8));

        StringBuilder hashString = new StringBuilder();
        for (byte hashByte : hashBytes) {
            hashString.append(String.format("%02x", hashByte));
        }
        return hashString.toString();
    }
}
