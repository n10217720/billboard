package billboard.common;

import javax.swing.*;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.AttributedCharacterIterator;
import java.text.AttributedString;

/**
 * A component which displays a billboard.
 */
public class BillboardRenderer extends JComponent {
    private static final int FONT_SIZE_MINIMUM = 5;
    private static final int FONT_SIZE_MAXIMUM = 200;
    private static final int PADDING = 10;
    private static final Font FONT = new Font("Arial", Font.PLAIN, 0); // size is always changed

    private Billboard billboard;
    private BufferedImage picture;

    public BillboardRenderer() {
        super();
    }

    /**
     * Displays the provided billboard. If passed {@code null}, a cross is displayed to indicate a missing billboard.
     *
     * A cross is also displayed if the billboard has a picture, and retrieving the picture failed.
     *
     * @param billboard The billboard to display.
     */
    public void setBillboard(Billboard billboard) {
        this.billboard = billboard;

        if (billboard != null && billboard.picture != null) {
            try {
                this.picture = billboard.picture.getImage();
            } catch (IOException e) {
                // Failed to load - display a cross (by setting ourselves to null)
                this.billboard = null;
            }
        }

        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2 = (Graphics2D) g;
        Dimension size = this.getSize();

        if (this.billboard == null) {
            // Draw a missing billboard symbol
            g2.setColor(Color.BLACK);
            g2.fillRect(0, 0, size.width, size.height);
            g2.setColor(Color.RED);
            g2.drawLine(0, 0, size.width, size.height);
            g2.drawLine(size.width, 0, 0, size.height);

            return;
        }

        boolean hasMessage = billboard.message != null;
        boolean hasPicture = billboard.picture != null;
        boolean hasInformation = billboard.information != null;

        Rectangle messageRegion = null;
        Rectangle pictureRegion = null;
        Rectangle informationRegion = null;

        if (hasPicture) {
            Dimension pictureDimensions = new Dimension(this.picture.getWidth(), this.picture.getHeight());

            Rectangle pictureContainer;
            if (!hasMessage && !hasInformation) {
                // Center, max 1/2
                int pictureWidth = size.width / 2;
                int pictureHeight = size.height / 2;
                pictureContainer = new Rectangle(
                        (size.width - pictureWidth) / 2, (size.height - pictureHeight) / 2,
                        pictureWidth, pictureHeight
                );
            } else if (hasMessage && !hasInformation) {
                // Center of bottom 2/3rds, max 1/2
                int pictureWidth = size.width / 2;
                int pictureHeight = size.height / 2;
                pictureContainer = new Rectangle(
                        (size.width - pictureWidth) / 2, size.height / 3 + (size.height * 2 / 3 - pictureHeight) / 2,
                        pictureWidth, pictureHeight
                );
            } else if (!hasMessage && hasInformation) {
                // Center of top 2/3rds, max 1/2
                int pictureWidth = size.width / 2;
                int pictureHeight = size.height / 2;
                pictureContainer = new Rectangle(
                        (size.width - pictureWidth) / 2, (size.height * 2 / 3 - pictureHeight) / 2,
                        pictureWidth, pictureHeight
                );
            } else {
                // Center, max 1/3
                int pictureWidth = size.width / 3;
                int pictureHeight = size.height / 3;
                pictureContainer = new Rectangle(
                        (size.width - pictureWidth) / 2, (size.height - pictureHeight) / 2,
                        pictureWidth, pictureHeight
                );
            }

            g2.drawRect(pictureContainer.x, pictureContainer.y, pictureContainer.width, pictureContainer.height);
            pictureRegion = getPictureDrawRegion(pictureDimensions, pictureContainer);

            if (hasMessage) {
                // All remaining space above the image
                messageRegion = new Rectangle(0, 0, size.width, pictureRegion.y);
            }

            if (hasInformation) {
                // All remaining vertical space below the image
                int y = pictureRegion.y + pictureRegion.height;
                informationRegion = new Rectangle(
                        0, y,
                        size.width, size.height - y
                );
            }
        } else if (hasMessage && !hasInformation) {
            // Entire billboard
            messageRegion = new Rectangle(0, 0, size.width, size.height);
        } else if (!hasMessage && hasInformation) {
            // 75% of width, 50% of height
            int width = size.width * 3 / 4;
            int height = size.height / 2;
            informationRegion = new Rectangle((size.width - width) / 2, (size.height - height) / 2, width, height);
        } else if (hasMessage && hasInformation) {
            // Half and half
            messageRegion = new Rectangle(0, 0, size.width, size.height / 2);
            informationRegion = new Rectangle(0, size.height / 2, size.width, size.height / 2);
        }

        g2.setColor(billboard.backgroundColour);
        g2.fillRect(0, 0, size.width, size.height);

        int messageFontSize = 0;
        if (messageRegion != null) {
            g2.setColor(billboard.message.colour);
            messageFontSize = drawStringOneLine(billboard.message.text, FONT, g2, messageRegion);
        }

        if (pictureRegion != null) {
            g2.drawImage(
                    this.picture,
                    pictureRegion.x, pictureRegion.y,
                    pictureRegion.x + pictureRegion.width, pictureRegion.y + pictureRegion.height,
                    0, 0,
                    this.picture.getWidth(), this.picture.getHeight(),
                    null
            );
        }

        if (informationRegion != null) {
            int informationFontSize = FONT_SIZE_MAXIMUM;
            if (messageFontSize != 0) {
                // Make sure the information text is always smaller than the message, but don't go below the minimum.
                informationFontSize = Math.max(FONT_SIZE_MINIMUM, messageFontSize - 10);
            }

            g2.setColor(billboard.information.colour);
            drawStringManyLines(billboard.information.text, FONT, g2, informationRegion, informationFontSize);
        }
    }

    /**
     * Draws {@code text} in the largest font size that can be used to render the string on one line inside
     * {@code region}.
     *
     * @param text The text to be drawn.
     * @param font The font to use.
     * @param g2 The graphics object which will perform the rendering.
     * @param region The region the text will be drawn in.
     * @return The font size that was used.
     */
    private int drawStringOneLine(String text, Font font, Graphics2D g2, Rectangle region) {
        FontRenderContext fontRenderContext = g2.getFontRenderContext();

        // Work down from the largest font size. Stop when we reach the first font that /does/ fit.
        int largestFontSize;
        for (largestFontSize = FONT_SIZE_MAXIMUM; largestFontSize >= FONT_SIZE_MINIMUM; --largestFontSize) {
            Font newFont = font.deriveFont((float) largestFontSize);
            TextLayout textLayout = new TextLayout(text, newFont, fontRenderContext);

            Rectangle2D bounds = textLayout.getBounds();

            int textWidth = (int) bounds.getWidth();
            int textHeight = (int) bounds.getHeight();
            if (textWidth + PADDING < region.getWidth() && textHeight + PADDING < region.getHeight()) {
                int x = region.x + (region.width - textWidth) / 2 - (int) bounds.getX();
                int y = region.y + region.height / 2 - textHeight / 2 - (int) bounds.getY();

                textLayout.draw(g2, x, y);

                break;
            }
        }

        return largestFontSize;
    }

    /**
     * Draws {@code text} in the largest font size that can be used to render the string on as many lines as required in
     * {@code region}.
     *
     * @param text The text to be drawn.
     * @param font The font to use.
     * @param g2 The graphics object which will perform the rendering.
     * @param region The region the text will be drawn in.
     * @param fontSizeMax The maximum font size to use.
     */
    private void drawStringManyLines(String text, Font font, Graphics2D g2, Rectangle region, int fontSizeMax) {
        FontRenderContext fontRenderContext = g2.getFontRenderContext();

        // Work down from the largest font size. Stop when we reach the first font that /does/ fit.
        int largestFontSize;
        for (largestFontSize = fontSizeMax; largestFontSize >= FONT_SIZE_MINIMUM; --largestFontSize) {
            Font newFont = font.deriveFont((float) largestFontSize);

            AttributedString as = new AttributedString(text);
            as.addAttribute(TextAttribute.FONT, newFont);

            AttributedCharacterIterator aci = as.getIterator();
            LineBreakMeasurer measurer = new LineBreakMeasurer(aci, fontRenderContext);

            int totalHeight = 0;
            while (measurer.getPosition() < aci.getEndIndex()) {
                TextLayout layout = measurer.nextLayout(region.width);
                totalHeight += layout.getAscent() + layout.getDescent() + layout.getLeading();
            }

            if (totalHeight + PADDING < region.height) {
                aci = as.getIterator();
                measurer = new LineBreakMeasurer(aci, fontRenderContext);

                int dy = (region.height - totalHeight) / 2;
                while (measurer.getPosition() < aci.getEndIndex()) {
                    TextLayout layout = measurer.nextLayout(region.width);

                    dy += layout.getAscent();

                    int dx = (region.width - (int) layout.getBounds().getWidth()) / 2 - (int) layout.getBounds().getX();
                    layout.draw(g2, region.x + dx, region.y + dy);

                    dy += layout.getDescent() + layout.getLeading();
                }

                break;
            }
        }
    }

    /**
     * Returns the rectangle in which the image should be drawn to maintain aspect ratio whilst displaying the image at
     * the largest scale possible.
     *
     * @param pictureDimensions The dimensions of the picture.
     * @param container The rectangle the picture is to be drawn in.
     * @return The region in which the picture should be drawn within the container.
     */
    protected static Rectangle getPictureDrawRegion(Dimension pictureDimensions, Rectangle container) {
        // Pretend to scale the image to the container dimensions, ignoring aspect ratio, then pick the dimension we
        // scaled that had the smallest scale factor.
        // That's the dimension that fit the worst, and is our limiting value. To maintain aspect ratio, scale the other
        // dimension by the same factor - it must fit, because if it wouldn't, it would have been our smallest scale
        // factor.
        double widthScaleFactor = (double) container.width / pictureDimensions.width;
        double heightScaleFactor = (double) container.height / pictureDimensions.height;

        int newWidth;
        int newHeight;

        if (widthScaleFactor <= heightScaleFactor) {
            newWidth = container.width;
            newHeight = (int) (pictureDimensions.height * widthScaleFactor);
        } else {
            newHeight = container.height;
            newWidth = (int) (pictureDimensions.width * heightScaleFactor);
        }

        // Center the image in the container.
        int x = container.x + (container.width - newWidth) / 2;
        int y = container.y + (container.height - newHeight) / 2;

        return new Rectangle(x, y, newWidth, newHeight);
    }
}
