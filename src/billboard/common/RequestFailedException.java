package billboard.common;

/**
 * Exception for requests to the server that fail.
 */
public class RequestFailedException extends Exception {

    /**
     * Construct a new RequestFailedException with a message.
     * @param message The message to display.
     */
    public RequestFailedException(String message) {
        super(message);
    }

    /**
     * Construct a new RequestFailedException with a message, and an Exception cause.
     * @param message The message to display.
     * @param cause The exception details to display.
     */
    public RequestFailedException(String message, Exception cause) {
        super(message, cause);
    }
}
