package billboard.common;

/**
 * An object that contains relevant metadata for a billboard object.
 */
public class BillboardMeta {
    private String name;
    private String creator;
    private boolean canEdit;
    private boolean canDelete;

    /**
     * Creates a metadata object for a billboard.
     *
     * @param name The billboard's name.
     * @param creator The username of the billboard's creator.
     * @param canEdit True if the user can edit the billboard.
     * @param canDelete True if the user can delete the billboard.
     */
    public BillboardMeta(String name, String creator, boolean canEdit, boolean canDelete) {
        this.name = name;
        this.creator = creator;
        this.canEdit = canEdit;
        this.canDelete = canDelete;
    }

    /**
     * Returns the billboard's name.
     *
     * @return The billboard's name.
     */
    public String getName() {
        return name;
    }

    /**
     * Returns the username of the billboard's creator.
     *
     * @return The username of the billboard's creator.
     */
    public String getCreator() {
        return creator;
    }

    /**
     * Returns true if the user can edit the billboard.
     *
     * @return True if the user can edit the billboard.
     */
    public boolean canEdit() {
        return canEdit;
    }

    /**
     * Sets if the user can edit the billboard.
     *
     * @param canEdit True if the user can edit the billboard.
     */
    public void setCanEdit(boolean canEdit) {
        this.canEdit = canEdit;
    }

    /**
     * Returns true if the user can delete the billboard.
     *
     * @return True if the user can delete the billboard.
     */
    public boolean canDelete() {
        return canDelete;
    }

    /**
     * Sets if the user can delete the billboard.
     *
     * @param canDelete True if the user can delete the billboard.
     */
    public void setCanDelete(boolean canDelete) {
        this.canDelete = canDelete;
    }

    @Override
    public String toString() {
        return String.format("%s (creator: %s)", this.name, this.creator);
    }
}
