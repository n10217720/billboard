package billboard.common;

import billboard.common.api.Request;
import billboard.common.api.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;

import static org.junit.jupiter.api.Assertions.*;

public class TestClientProtocol {
    private FakeRequest request;

    @BeforeEach
    public void beforeEach() {
        this.request = new FakeRequest();
    }

    @Test
    public void parseResponseInvalidXML() {
        assertThrows(RequestFailedException.class, () -> {
            ClientProtocol.parseResponse(request, "this is invalid XML");
        });
    }

    @Test
    public void parseResponseNotAResponse() {
        assertThrows(RequestFailedException.class, () -> {
            ClientProtocol.parseResponse(request, "<xresponse status=\"success\"></xresponse>");
        });
    }

    @Test
    public void parseResponseInvalidStatus() {
        assertThrows(RequestFailedException.class, () -> {
            ClientProtocol.parseResponse(request, "<response status=\"invalid\"></response>");
        });
    }

    @Test
    public void parseResponseError() {
        RequestErrorException e = assertThrows(RequestErrorException.class, () -> {
            ClientProtocol.parseResponse(request, "<response status=\"error\"><message>Error message</message></response>");
        });

        assertEquals(e.getErrorResponse().getMessage(), "Error message");
    }

    @Test
    public void parseResponseSuccess() throws RequestFailedException {
        FakeResponse r = ClientProtocol.parseResponse(request, "<response status=\"success\"></response>");
        assertNotNull(r);
    }

    static class FakeRequest implements Request<FakeResponse> {
        @Override
        public Document toXML() {
            throw new Error("Method should not be called");
        }

        @Override
        public FakeResponse responseFromXML(Document document) throws ParseException {
            return FakeResponse.fromXML(document);
        }
    }

    static class FakeResponse implements Response {
        @Override
        public Document toXML() {
            throw new Error("Method should not be called");
        }

        public static FakeResponse fromXML(Document document) throws ParseException {
            return new FakeResponse();
        }
    }
}
