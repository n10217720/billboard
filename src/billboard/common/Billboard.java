package billboard.common;

import billboard.common.abstractXML.*;
import org.w3c.dom.Document;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

/**
 * An object representing a billboard in the system.
 * Contains all relevant billboard object information, such as text, images, colouring, etc.
 */
public class Billboard {
    private static final Color DEFAULT_BACKGROUND_COLOUR = Color.BLACK;
    private static final Color DEFAULT_TEXT_COLOUR = Color.WHITE;

    public Color backgroundColour;

    public ColouredText message;
    public BillboardPicture picture;
    public ColouredText information;

    /**
     * Creates an empty billboard.
     */
    public Billboard() {
        this(null, null, null);
    }

    /**
     * Creates a billboard.
     *
     * @param message The message to display.
     * @param picture The picture to display.
     * @param information The information text to display.
     */
    public Billboard(ColouredText message, BillboardPicture picture, ColouredText information) {
        this.message = message;
        this.picture = picture;
        this.information = information;
    }
    /**
     * Creates a billboard.
     *
     * @param backgroundColour The background colour.
     * @param message The message to display.
     * @param picture The picture to display.
     * @param information The information text to display.
     */
    public Billboard(Color backgroundColour, ColouredText message, BillboardPicture picture, ColouredText information) {
        this.backgroundColour = backgroundColour;
        this.message = message;
        this.picture = picture;
        this.information = information;
    }

    /**
     * Returns the provided colour, decoded, or the default text colour if the colour was empty.
     *
     * @param colour The colour to decode.
     * @return The decoded colour, or the default text colour.
     */
    private static Color decodeOrDefaultTextColour(String colour) {
        if (colour != null && !colour.isEmpty()) {
            return Color.decode(colour);
        } else {
            return DEFAULT_TEXT_COLOUR;
        }
    }

    /**
     * Encodes a colour into an uppercase, '#'-prefixed hexadecimal string.
     *
     * @param colour The colour to encode.
     * @return The uppercase, '#'-prefixed hexidecimal string.
     */
    private static String encode(Color colour) {
        return String.format("#%02X%02X%02X", colour.getRed(), colour.getGreen(), colour.getBlue());
    }

    /**
     * Converts the billboard into an XML document.
     *
     * @return The billboard as an XML document.
     */
    public Document toXML() {
        List<Element> elements = new ArrayList<>();

        if (this.message != null) {
            elements.add(new Element("message", new Attribute[] {
                    new Attribute("colour", encode(this.message.colour))
            }, this.message.text));
        }

        if (this.picture != null) {
            String attributeName;
            String attributeData;

            if (this.picture instanceof BillboardPictureURL) {
                attributeName = "url";
                attributeData = ((BillboardPictureURL) this.picture).getURL();
            } else {
                attributeName = "data";
                attributeData = ((BillboardPictureData) this.picture).getData();
            }

            elements.add(new Element("picture", new Attribute[] {
                    new Attribute(attributeName, attributeData)
            }, new Element[] {}));
        }

        if (this.information != null) {
            elements.add(new Element("information", new Attribute[] {
                    new Attribute("colour", encode(this.information.colour))
            }, this.information.text));
        }

        Element[] elementsArray = new Element[elements.size()];
        elements.toArray(elementsArray);

        return new Element("billboard", new Attribute[] {
                new Attribute("background", encode(this.backgroundColour))
        }, elementsArray).getDocument();
    }

    /**
     * Parses a billboard from an XML file, substituting default values where required but not specified.
     *
     * @param document The XML document to parse.
     * @return The billboard parsed from the XML file.
     * @throws ParseException The XML document was malformed.
     */
    public static Billboard fromXML(Document document) throws ParseException {
        StringOutput background = new StringOutput();
        StringOutput messageColour = new StringOutput();
        StringOutput messageText = new StringOutput();
        StringOutput pictureURL = new StringOutput();
        StringOutput pictureData = new StringOutput();
        StringOutput informationColour = new StringOutput();
        StringOutput informationText = new StringOutput();

        new Element("billboard", new Attribute[] {
                new Attribute("background", background)
        }, new Element[] {
                new Element("message", new Attribute[] {
                        new Attribute("colour", messageColour)
                }, messageText).markOptional(),
                new Element("picture", new Attribute[] {
                        new Attribute("url", pictureURL),
                        new Attribute("data", pictureData)
                }, new Element[] {}).markOptional(),
                new Element("information", new Attribute[] {
                        new Attribute("colour", informationColour)
                }, informationText).markOptional()
        }).parseDocument(document);

        Billboard billboard = new Billboard();

        if (!background.getValue().isEmpty()) {
            billboard.backgroundColour = Color.decode(background.getValue());
        } else {
            billboard.backgroundColour = DEFAULT_BACKGROUND_COLOUR;
        }

        if (messageText.getValue() != null) {
            billboard.message = new ColouredText(messageText.getValue(), decodeOrDefaultTextColour(messageColour.getValue()));
        }

        if (pictureURL.getValue() != null && !pictureURL.getValue().isEmpty()) {
            billboard.picture = new BillboardPictureURL(pictureURL.getValue());
        }

        if (pictureData.getValue() != null && !pictureData.getValue().isEmpty()) {
            billboard.picture = new BillboardPictureData(pictureData.getValue());
        }

        if (informationText.getValue() != null) {
            billboard.information = new ColouredText(informationText.getValue(), decodeOrDefaultTextColour(informationColour.getValue()));
        }

        if (!billboard.isValid()) {
            throw new ParseException("Billboard was not valid (no components or missing text)");
        }

        return billboard;
    }

    /**
     * Determines if this billboard is valid, i.e. the combination of components is valid.
     *
     * @return True if the billboard is valid, false otherwise.
     */
    public boolean isValid() {
        boolean hasMessage = this.message != null;
        boolean hasPicture = this.picture != null;
        boolean hasInformation = this.information != null;

        // If either is missing text, even if the component combination is OK, the billboard isn't valid.
        if ((hasMessage && this.message.text.isEmpty()) || (hasInformation && this.information.text.isEmpty())) {
            return false;
        }

        // The billboard is then valid if we have at least one component.
        return hasMessage || hasPicture || hasInformation;
    }

    /**
     * An abstract base class for classes which represent pictures which can be displayed on the billboard, from
     * different sources. Provides memoization.
     */
    public abstract static class BillboardPicture {
        private BufferedImage image = null;

        /**
         * Returns the image object that stores this picture.
         *
         * @return The image object that stores this picture.
         * @throws IOException The image could not be retrieved.
         */
        public BufferedImage getImage() throws IOException {
            if (this.image != null) {
                return this.image;
            }

            this.image = getImageOnce();

            return this.image;
        }

        /**
         * Returns the image object that stores this picture.
         *
         * This method is only ever called once, on the first call to {@code getImage}. The image this function returns
         * is then stored and that object returned.
         *
         * @return The image object that stores this picture.
         * @throws IOException The image could not be retrieved.
         */
        protected abstract BufferedImage getImageOnce() throws IOException;
    }

    public static class BillboardPictureURL extends BillboardPicture {
        private String url;

        /**
         * Creates a billboard picture which is stored on the internet at the provided URL.
         *
         * @param url The URL of the picture.
         */
        public BillboardPictureURL(String url) {
            this.url = url;
        }

        /**
         * Returns this picture's URL.
         *
         * @return This picture's URL.
         */
        public String getURL() {
            return this.url;
        }

        @Override
        protected BufferedImage getImageOnce() throws IOException {
            return ImageIO.read(new URL(this.url));
        }

        @Override
        public boolean equals(Object other) {
            if (other == null || other.getClass() != this.getClass()) {
                return false;
            }

            return ((BillboardPictureURL) other).url.equals(this.url);
        }
    }

    public static class BillboardPictureData extends BillboardPicture {
        private String data;

        /**
         * Creates a billboard picture which is stored as an image file, base 64 encoded.
         *
         * @param data The base64 encoded image file.
         */
        public BillboardPictureData(String data) {
            this.data = data;
        }

        /**
         * Creates a billboard picture which is stored as an image file, base 64 encoded. Retrieves image data from the
         * provided file.
         *
         * @param file The file to read the image data from.
         * @throws IOException The file couldn't be read.
         */
        public BillboardPictureData(File file) throws IOException {
            this(Base64.getEncoder().encodeToString(new FileInputStream(file).readAllBytes()));
        }

        /**
         * Retrieves this picture's base 64 data.
         *
         * @return This picture's base 64 data.
         */
        public String getData() {
            return this.data;
        }

        @Override
        protected BufferedImage getImageOnce() throws IOException {
            byte[] imageData = Base64.getDecoder().decode(this.data);
            return ImageIO.read(new ByteArrayInputStream(imageData));
        }

        /**
         * Compares this picture's base 64 data with another.
         *
         * @return True if the data matches, false if it does not or if the object is not the same type.
         * @see java.lang.Object
         */
        @Override
        public boolean equals(Object other) {
            if (other == null || other.getClass() != this.getClass()) {
                return false;
            }

            return ((BillboardPictureData) other).data.equals(this.data);
        }
    }
}
