package billboard.common;

import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestBillboardRenderer {
    @Test
    public void getPictureDrawRegionNoChange() {
        Dimension pictureDimensions = new Dimension(100, 200);
        Rectangle container = new Rectangle(100, 200);

        Rectangle pictureRegion = BillboardRenderer.getPictureDrawRegion(pictureDimensions, container);

        assertEquals(new Rectangle(0, 0, 100, 200), pictureRegion);
    }

    @Test
    public void getPictureDrawRegionScaleUpSameAspectRatio() {
        Dimension pictureDimensions = new Dimension(100, 200);
        Rectangle container = new Rectangle(200, 400);

        Rectangle pictureRegion = BillboardRenderer.getPictureDrawRegion(pictureDimensions, container);

        assertEquals(new Rectangle(0, 0, 200, 400), pictureRegion);
    }

    @Test
    public void getPictureDrawRegionScaleDownSameAspectRatio() {
        Dimension pictureDimensions = new Dimension(100, 200);
        Rectangle container = new Rectangle(50, 100);

        Rectangle pictureRegion = BillboardRenderer.getPictureDrawRegion(pictureDimensions, container);

        assertEquals(new Rectangle(0, 0, 50, 100), pictureRegion);
    }

    @Test
    public void getPictureDrawRegionScaleUpExtraWidth() {
        Dimension pictureDimensions = new Dimension(100, 200);
        Rectangle container = new Rectangle(250, 400);

        Rectangle pictureRegion = BillboardRenderer.getPictureDrawRegion(pictureDimensions, container);

        assertEquals(new Rectangle(25, 0, 200, 400), pictureRegion);
    }

    @Test
    public void getPictureDrawRegionScaleUpExtraHeight() {
        Dimension pictureDimensions = new Dimension(100, 200);
        Rectangle container = new Rectangle(200, 500);

        Rectangle pictureRegion = BillboardRenderer.getPictureDrawRegion(pictureDimensions, container);

        assertEquals(new Rectangle(0, 50, 200, 400), pictureRegion);
    }

    @Test
    public void getPictureDrawRegionScaleDownExtraWidth() {
        Dimension pictureDimensions = new Dimension(100, 200);
        Rectangle container = new Rectangle(100, 100);

        Rectangle pictureRegion = BillboardRenderer.getPictureDrawRegion(pictureDimensions, container);

        assertEquals(new Rectangle(25, 0, 50, 100), pictureRegion);
    }

    @Test
    public void getPictureDrawRegionScaleDownExtraHeight() {
        Dimension pictureDimensions = new Dimension(100, 200);
        Rectangle container = new Rectangle(50, 200);

        Rectangle pictureRegion = BillboardRenderer.getPictureDrawRegion(pictureDimensions, container);

        assertEquals(new Rectangle(0, 50, 50, 100), pictureRegion);
    }
}
