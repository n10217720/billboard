package billboard.common;

import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

public class TestBillboard {
    private final Billboard billboardMessageOnly = new Billboard(
            new Color(0x00, 0x00, 0xFF),
            new ColouredText("Welcome to the ____ Corporation's Annual Fundraiser!", new Color(0xFF, 0xFF, 0x00)),
            null,
            null
    );
    private final Document billboardXMLMessageOnly = TestUtils.fromXMLString(
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                    "<billboard background=\"#0000FF\">\n" +
                    "\t<message colour=\"#FFFF00\">Welcome to the ____ Corporation's Annual Fundraiser!</message>\n" +
                    "</billboard>"
    );
    private final Billboard billboardPictureOnly = new Billboard(
            new Color(0x00, 0x00, 0xFF),
            null,
            new Billboard.BillboardPictureURL("https://example.com/fundraiser_image.jpg"),
            null
    );
    private final Document billboardXMLPictureOnly = TestUtils.fromXMLString(
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                    "<billboard background=\"#0000FF\">\n" +
                    "\t<picture url=\"https://example.com/fundraiser_image.jpg\" />\n" +
                    "</billboard>"
    );
    private final Billboard billboardInformationOnly = new Billboard(
            new Color(0x00, 0x00, 0xFF),
            null,
            null,
            new ColouredText("Be sure to check out https://example.com/ for more information.", new Color(0x00, 0xFF, 0xFF))
    );
    private final Document billboardXMLInformationOnly = TestUtils.fromXMLString(
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                    "<billboard background=\"#0000FF\">\n" +
                    "\t<information colour=\"#00FFFF\">Be sure to check out https://example.com/ for more information.</information>\n" +
                    "</billboard>"
    );
    private final Billboard billboardPictureURL = new Billboard(
            new Color(0x00, 0x00, 0xFF),
            new ColouredText("Welcome to the ____ Corporation's Annual Fundraiser!", new Color(0xFF, 0xFF, 0x00)),
            new Billboard.BillboardPictureURL("https://example.com/fundraiser_image.jpg"),
            new ColouredText("Be sure to check out https://example.com/ for more information.", new Color(0x00, 0xFF, 0xFF))
    );
    private final Document billboardXMLPictureURL = TestUtils.fromXMLString(
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                    "<billboard background=\"#0000FF\">\n" +
                    "\t<message colour=\"#FFFF00\">Welcome to the ____ Corporation's Annual Fundraiser!</message>\n" +
                    "\t<picture url=\"https://example.com/fundraiser_image.jpg\" />\n" +
                    "\t<information colour=\"#00FFFF\">Be sure to check out https://example.com/ for more information.</information>\n" +
                    "</billboard>"
    );
    private final Billboard billboardPictureData = new Billboard(
            new Color(0x00, 0x00, 0xFF),
            new ColouredText("Welcome to the ____ Corporation's Annual Fundraiser!", new Color(0xFF, 0xFF, 0x00)),
            new Billboard.BillboardPictureData("iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAIAAABLbSncAAAALHRFWHRDcmVhdGlvbiBUaW1lAE1vbiAxNiBNYXIgMjAyMCAxMDowNTo0NyArMTAwMNQXthkAAAAHdElNRQfkAxAABh+N6nQIAAAACXBIWXMAAAsSAAALEgHS3X78AAAABGdBTUEAALGPC/xhBQAAADVJREFUeNp1jkEKADAIwxr//+duIIhumJMUNUWSbU2AyPROFeVqaIH/T7JeRBd0DY+8SrLVPbTmFQ1iRvw3AAAAAElFTkSuQmCC"),
            new ColouredText("Be sure to check out https://example.com/ for more information.", new Color(0x00, 0xFF, 0xFF))
    );
    private final Document billboardXMLPictureData = TestUtils.fromXMLString(
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                    "<billboard background=\"#0000FF\">\n" +
                    "\t<message colour=\"#FFFF00\">Welcome to the ____ Corporation's Annual Fundraiser!</message>\n" +
                    "\t<picture data=\"iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAIAAABLbSncAAAALHRFWHRDcmVhdGlvbiBUaW1lAE1vbiAxNiBNYXIgMjAyMCAxMDowNTo0NyArMTAwMNQXthkAAAAHdElNRQfkAxAABh+N6nQIAAAACXBIWXMAAAsSAAALEgHS3X78AAAABGdBTUEAALGPC/xhBQAAADVJREFUeNp1jkEKADAIwxr//+duIIhumJMUNUWSbU2AyPROFeVqaIH/T7JeRBd0DY+8SrLVPbTmFQ1iRvw3AAAAAElFTkSuQmCC\" />\n" +
                    "\t<information colour=\"#00FFFF\">Be sure to check out https://example.com/ for more information.</information>\n" +
                    "</billboard>"
    );
    private final Document billboardXMLNoBackground = TestUtils.fromXMLString(
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                    "<billboard>\n" +
                    "\t<message colour=\"#FFFF00\">Welcome to the ____ Corporation's Annual Fundraiser!</message>\n" +
                    "</billboard>"
    );
    private final Document billboardXMLNoMessageColour = TestUtils.fromXMLString(
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"+
                    "<billboard background=\"#0000FF\">\n" +
                    "\t<message>Welcome to the ____ Corporation's Annual Fundraiser!</message>\n" +
                    "</billboard>"
    );

    @Test
    public void isValid() {
        ColouredText text = new ColouredText("placeholder", Color.WHITE);
        ColouredText emptyText = new ColouredText("", Color.WHITE);
        Billboard.BillboardPicture picture = new Billboard.BillboardPictureURL("http://example.org/test.jpg");

        assertTrue(new Billboard(text, null, null).isValid());
        assertTrue(new Billboard(null, picture, null).isValid());
        assertTrue(new Billboard(null, null, text).isValid());
        assertTrue(new Billboard(text, picture, null).isValid());
        assertTrue(new Billboard(text, null, text).isValid());
        assertTrue(new Billboard(null, picture, text).isValid());
        assertTrue(new Billboard(text, picture, text).isValid());

        assertFalse(new Billboard(null, null, null).isValid());

        // Empty text should always cause a billboard to be invalid
        assertFalse(new Billboard(emptyText, null, null).isValid());
        assertFalse(new Billboard(null, null, emptyText).isValid());
        assertFalse(new Billboard(emptyText, null, emptyText).isValid());
        assertFalse(new Billboard(text, null, emptyText).isValid());
        assertFalse(new Billboard(emptyText, null, text).isValid());
    }

    @Test
    public void toXMLMessageOnly() {
        Document document = billboardMessageOnly.toXML();

        Element billboardEl = (Element) document.getElementsByTagName("billboard").item(0);
        assertEquals("#0000FF", billboardEl.getAttribute("background"));

        Element messageEl = (Element) billboardEl.getElementsByTagName("message").item(0);
        assertEquals("#FFFF00", messageEl.getAttribute("colour"));
        assertEquals("Welcome to the ____ Corporation's Annual Fundraiser!", messageEl.getTextContent());
    }

    @Test
    public void fromXMLMessageOnly() throws ParseException {
        Billboard billboard = Billboard.fromXML(billboardXMLMessageOnly);

        assertEquals(new Color(0x00, 0x00, 0xFF), billboard.backgroundColour);

        assertEquals("Welcome to the ____ Corporation's Annual Fundraiser!", billboard.message.text);
        assertEquals(new Color(0xFF, 0xFF, 0x00), billboard.message.colour);

        assertNull(billboard.picture);
        assertNull(billboard.information);
    }

    @Test
    public void toXMLPictureOnly() {
        Document document = billboardPictureOnly.toXML();

        Element billboardEl = (Element) document.getElementsByTagName("billboard").item(0);
        assertEquals("#0000FF", billboardEl.getAttribute("background"));

        Element pictureEl = (Element) billboardEl.getElementsByTagName("picture").item(0);
        assertEquals("https://example.com/fundraiser_image.jpg", pictureEl.getAttribute("url"));
    }

    @Test
    public void fromXMLPictureOnly() throws ParseException {
        Billboard billboard = Billboard.fromXML(billboardXMLPictureOnly);

        assertEquals(new Color(0x00, 0x00, 0xFF), billboard.backgroundColour);

        assertNull(billboard.message);

        assertEquals("https://example.com/fundraiser_image.jpg", ((Billboard.BillboardPictureURL) billboard.picture).getURL());

        assertNull(billboard.information);
    }

    @Test
    public void toXMLInformationOnly() {
        Document document = billboardInformationOnly.toXML();

        Element billboardEl = (Element) document.getElementsByTagName("billboard").item(0);
        assertEquals("#0000FF", billboardEl.getAttribute("background"));

        Element informationEl = (Element) billboardEl.getElementsByTagName("information").item(0);
        assertEquals("#00FFFF", informationEl.getAttribute("colour"));
        assertEquals("Be sure to check out https://example.com/ for more information.", informationEl.getTextContent());
    }

    @Test
    public void fromXMLInformationOnly() throws ParseException {
        Billboard billboard = Billboard.fromXML(billboardXMLInformationOnly);

        assertEquals(new Color(0x00, 0x00, 0xFF), billboard.backgroundColour);

        assertNull(billboard.message);
        assertNull(billboard.picture);

        assertEquals("Be sure to check out https://example.com/ for more information.", billboard.information.text);
        assertEquals(new Color(0x00, 0xFF, 0xFF), billboard.information.colour);
    }

    @Test
    public void toXMLPictureURL() {
        Document document = billboardPictureURL.toXML();

        Element billboardEl = (Element) document.getElementsByTagName("billboard").item(0);
        assertEquals("#0000FF", billboardEl.getAttribute("background"));

        Element messageEl = (Element) billboardEl.getElementsByTagName("message").item(0);
        assertEquals("#FFFF00", messageEl.getAttribute("colour"));
        assertEquals("Welcome to the ____ Corporation's Annual Fundraiser!", messageEl.getTextContent());

        Element pictureEl = (Element) billboardEl.getElementsByTagName("picture").item(0);
        assertEquals("https://example.com/fundraiser_image.jpg", pictureEl.getAttribute("url"));

        Element informationEl = (Element) billboardEl.getElementsByTagName("information").item(0);
        assertEquals("#00FFFF", informationEl.getAttribute("colour"));
        assertEquals("Be sure to check out https://example.com/ for more information.", informationEl.getTextContent());
    }

    @Test
    public void fromXMLPictureURL() throws ParseException {
        Billboard billboard = Billboard.fromXML(billboardXMLPictureURL);

        assertEquals(new Color(0x00, 0x00, 0xFF), billboard.backgroundColour);

        assertEquals("Welcome to the ____ Corporation's Annual Fundraiser!", billboard.message.text);
        assertEquals(new Color(0xFF, 0xFF, 0x00), billboard.message.colour);

        assertEquals("https://example.com/fundraiser_image.jpg", ((Billboard.BillboardPictureURL) billboard.picture).getURL());

        assertEquals("Be sure to check out https://example.com/ for more information.", billboard.information.text);
        assertEquals(new Color(0x00, 0xFF, 0xFF), billboard.information.colour);
    }

    @Test
    public void toXMLPictureData() {
        Document document = billboardPictureData.toXML();

        Element billboardEl = (Element) document.getElementsByTagName("billboard").item(0);
        assertEquals("#0000FF", billboardEl.getAttribute("background"));

        Element messageEl = (Element) billboardEl.getElementsByTagName("message").item(0);
        assertEquals("#FFFF00", messageEl.getAttribute("colour"));
        assertEquals("Welcome to the ____ Corporation's Annual Fundraiser!", messageEl.getTextContent());

        Element pictureEl = (Element) billboardEl.getElementsByTagName("picture").item(0);
        assertEquals("iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAIAAABLbSncAAAALHRFWHRDcmVhdGlvbiBUaW1lAE1vbiAxNiBNYXIgMjAyMCAxMDowNTo0NyArMTAwMNQXthkAAAAHdElNRQfkAxAABh+N6nQIAAAACXBIWXMAAAsSAAALEgHS3X78AAAABGdBTUEAALGPC/xhBQAAADVJREFUeNp1jkEKADAIwxr//+duIIhumJMUNUWSbU2AyPROFeVqaIH/T7JeRBd0DY+8SrLVPbTmFQ1iRvw3AAAAAElFTkSuQmCC", pictureEl.getAttribute("data"));

        Element informationEl = (Element) billboardEl.getElementsByTagName("information").item(0);
        assertEquals("#00FFFF", informationEl.getAttribute("colour"));
        assertEquals("Be sure to check out https://example.com/ for more information.", informationEl.getTextContent());
    }

    @Test
    public void fromXMLPictureData() throws ParseException {
        Billboard billboard = Billboard.fromXML(billboardXMLPictureData);

        assertEquals(new Color(0x00, 0x00, 0xFF), billboard.backgroundColour);

        assertEquals("Welcome to the ____ Corporation's Annual Fundraiser!", billboard.message.text);
        assertEquals(new Color(0xFF, 0xFF, 0x00), billboard.message.colour);

        assertEquals("iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAIAAABLbSncAAAALHRFWHRDcmVhdGlvbiBUaW1lAE1vbiAxNiBNYXIgMjAyMCAxMDowNTo0NyArMTAwMNQXthkAAAAHdElNRQfkAxAABh+N6nQIAAAACXBIWXMAAAsSAAALEgHS3X78AAAABGdBTUEAALGPC/xhBQAAADVJREFUeNp1jkEKADAIwxr//+duIIhumJMUNUWSbU2AyPROFeVqaIH/T7JeRBd0DY+8SrLVPbTmFQ1iRvw3AAAAAElFTkSuQmCC", ((Billboard.BillboardPictureData) billboard.picture).getData());

        assertEquals("Be sure to check out https://example.com/ for more information.", billboard.information.text);
        assertEquals(new Color(0x00, 0xFF, 0xFF), billboard.information.colour);
    }

    @Test
    public void fromXMLNoBackground() throws ParseException {
        Billboard billboard = Billboard.fromXML(billboardXMLNoBackground);

        // We don't really care what the colour is, just that one has actually been set
        assertNotNull(billboard.backgroundColour);
    }

    @Test
    public void fromXMLNoMessageColour() throws ParseException {
        Billboard billboard = Billboard.fromXML(billboardXMLNoMessageColour);

        // We don't really care what the colour is, just that one has actually been set
        assertNotNull(billboard.message.colour);
    }
}
