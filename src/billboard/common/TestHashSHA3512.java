package billboard.common;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestHashSHA3512 {
    @Test
    public void generateHash() {
        // Test cases generated with https://emn178.github.io/online-tools/sha3_512.html
        assertEquals(
                "e9a75486736a550af4fea861e2378305c4a555a05094dee1dca2f68afea49cc3a50e8de6ea131ea521311f4d6fb054a146e8282f8e35ff2e6368c1a62e909716",
                HashSHA3512.generateHash("password")
        );
        assertEquals(
                "6856043d8c72383b878cd25e443fda922dd0cb3391d4a3a923a1873549d05a43654bd0c3c6c38371b11f37c55b40b15fb42a78fc52043b1fd06fb9ba93fbd81a",
                HashSHA3512.generateHash("testing123")
        );
    }
}
