package billboard.server;

import billboard.common.*;
import billboard.common.api.Response;
import billboard.common.api.errors.AuthenticationRequiredErrorResponse;
import billboard.common.api.errors.InvalidOperationErrorResponse;
import billboard.common.api.errors.InvalidUsernameOrPasswordErrorResponse;
import billboard.common.api.errors.PermissionRequiredErrorResponse;
import billboard.common.api.methods.*;

import java.awt.*;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

/**
 * Class which contains handler methods for requests made by the client, either by user actions or client systems.
 * Communicates with the database to retrieve/write data relevant to the request and response.
 * Each handler sends back a response to the client based on the request information, and if it was successful or not.
 */
public class Server {
    private static final long SESSION_TOKEN_EXPIRY = 24;
    private static final TemporalUnit SESSION_TOKEN_EXPIRY_UNIT = ChronoUnit.HOURS;

    private Random random;
    private Database database;
    private SessionTokenStore<String> sessionTokenStore;

    /**
     * Constructs a new instance of the server, linking to the provided database.
     *
     * @param database The database for the server to link and work of.
     */
    public Server(Database database) {
        this.random = new Random();
        this.database = database;
        this.sessionTokenStore = new SessionTokenStore<>(
                SESSION_TOKEN_EXPIRY, SESSION_TOKEN_EXPIRY_UNIT,
                new RealTimeSource(), this.random
        );
    }

    /**
     * Handler for logging a user into the server. Checks the database based on information given in the request,
     * and returns an appropriate response.
     *
     * @param request The userLogin request to process.
     * @return An error response if the username/password is wrong, successful response containing the users' session token otherwise.
     */
    public Response handleUserLogin(UserLoginRequest request) {
        HashedSaltedPassword hsp = this.database.getHashedSaltedPassword(request.getUsername());
        if (hsp != null) {
            String providedHashedSaltedPassword = HashSHA3512.generateHash(request.getHashedPassword() + hsp.getSalt());
            if (hsp.getHashedSaltedPassword().equals(providedHashedSaltedPassword)) {
                String sessionToken = sessionTokenStore.createToken(request.getUsername());

                return new UserLoginResponse(sessionToken);
            }
        }

        return new InvalidUsernameOrPasswordErrorResponse();
    }

    /**
     * Handler for logging a user out of the server. Checks for if the user is authenticated,
     * and invalidates the users' session token when successful.
     *
     * @param request The userLogout request to process.
     * @return An error response if the user is not authenticated (invalid session token),
     * successful response otherwise.
     */
    public Response handleUserLogout(UserLogoutRequest request){
        String requestUser = this.sessionTokenStore.getTokenValue(request.getSessionToken());
        if (requestUser == null) {
            return new AuthenticationRequiredErrorResponse();
        }
        sessionTokenStore.invalidateToken(request.getSessionToken());
        return new UserLogoutResponse();
    }

    /**
     * Handler for setting a users' password. Checks for if the request user is authenticated, and
     * if they have the correct permissions.
     *
     * @param request The setUserPassword request to process.
     * @return An error response if the user is not authenticated (invalid session token)
     * or does not have the right permissions, successful response otherwise.
     */
    public Response handleSetUserPassword(SetUserPasswordRequest request) {
        String requestUser = this.sessionTokenStore.getTokenValue(request.getSessionToken());
        if (requestUser == null) {
            return new AuthenticationRequiredErrorResponse();
        }

        boolean operationPermitted = false;
        operationPermitted |= requestUser.equals(request.getUsername());
        operationPermitted |= this.database.hasPermission(requestUser, Permission.EDIT_USERS);
        if (!operationPermitted) {
            return new PermissionRequiredErrorResponse(Permission.EDIT_USERS);
        }

        HashedSaltedPassword newPassword = HashedSaltedPassword.newFromHashedPassword(request.getNewHashedPassword(), this.random);
        this.database.setUserPassword(request.getUsername(), newPassword);

        this.sessionTokenStore.updateLastUsed(request.getSessionToken());
        return new SetUserPasswordResponse();
    }

    /**
     * Handler for setting a users' permisisons. Checks for if the request user is authenticated, and
     * if they have the correct permissions.
     *
     * @param request The setUserPermissions request to process.
     * @return An error response if the user is not authenticated (invalid session token),
     * if the requested user is the user making the request and is trying to remove EDIT_USERS,
     * the specified user does not exist, or the user does not have the right permissions, successful response otherwise.
     */
    public Response handleSetUserPermissions(SetUserPermissionsRequest request){
        String requestUser = this.sessionTokenStore.getTokenValue(request.getSessionToken());
        if (requestUser == null) {
            return new AuthenticationRequiredErrorResponse();
        }

        if(!this.database.doesUserExist(request.getUsername())){
            return new InvalidOperationErrorResponse();
        }

        boolean operationPermitted;
        operationPermitted = this.database.hasPermission(requestUser, Permission.EDIT_USERS);
        if (!operationPermitted) {
            return new PermissionRequiredErrorResponse(Permission.EDIT_USERS);
        }

        HashSet<Permission> currentPermissions = request.getPermissionList();
        HashSet<Permission> previousPermissions = this.database.getUserPermissions(request.getUsername());

        if(requestUser.equals(request.getUsername())){
            // Don't allow a user to remove their own EDIT_USERS permission
            if (previousPermissions.contains(Permission.EDIT_USERS) && !currentPermissions.contains(Permission.EDIT_USERS)) {
                return new InvalidOperationErrorResponse();
            }
        }

        this.database.setUserPermissions(request.getUsername(), currentPermissions);

        this.sessionTokenStore.updateLastUsed(request.getSessionToken());
        return new SetUserPermissionsResponse();
    }

    /**
     * Handler for getting a users' permisisons. Checks for if the request user is authenticated, and
     * if they have the correct permissions.
     *
     * @param request The getUserPermissions request to process.
     * @return An error response if the user is not authenticated (invalid session token),
     * the specified user does not exist or the user does not have the right permissions,
     * successful response containing the users permissions otherwise.
     */
    public Response handleGetUserPermissions(GetUserPermissionsRequest request){
        String requestUser = this.sessionTokenStore.getTokenValue(request.getSessionToken());
        if (requestUser == null) {
            return new AuthenticationRequiredErrorResponse();
        }

        if(!this.database.doesUserExist(request.getUsername())){
            return new InvalidOperationErrorResponse();
        }

        boolean operationPermitted = false;
        operationPermitted |= requestUser.equals(request.getUsername());
        operationPermitted |= this.database.hasPermission(requestUser, Permission.EDIT_USERS);
        if (!operationPermitted) {
            return new PermissionRequiredErrorResponse(Permission.EDIT_USERS);
        }

        this.sessionTokenStore.updateLastUsed(request.getSessionToken());
        return new GetUserPermissionsResponse(this.database.getUserPermissions(request.getUsername()));
    }

    /**
     * Handler for deleting an existing user. Checks for if the request user is authenticated, and
     * if they have the correct permissions.
     *
     * @param request The deleteUser request to process.
     * @return An error response if the user is not authenticated (invalid session token),
     * the specified user does not exist, the specified user is the request user, or the user does not have the right permissions,
     * successful response otherwise.
     */
    public Response handleDeleteUser(DeleteUserRequest request){
        String requestUser = this.sessionTokenStore.getTokenValue(request.getSessionToken());
        if (requestUser == null) {
            return new AuthenticationRequiredErrorResponse();
        }

        if(!this.database.doesUserExist(request.getUsername())){
            return new InvalidOperationErrorResponse();
        }

        boolean operationPermitted;
        operationPermitted = this.database.hasPermission(requestUser, Permission.EDIT_USERS);
        if (!operationPermitted) {
            return new PermissionRequiredErrorResponse(Permission.EDIT_USERS);
        }

        if(requestUser.equals(request.getUsername())){
            return new InvalidOperationErrorResponse();
        }

        this.database.deleteUser(request.getUsername());

        this.sessionTokenStore.updateLastUsed(request.getSessionToken());
        return new DeleteUserResponse();
    }

    /**
     * Handler for listing all existing users. Checks for if the request user is authenticated, and
     * if they have the correct permissions.
     *
     * @param request The listUsers request to process.
     * @return An error response if the user is not authenticated (invalid session token),
     * or the user does not have the right permissions, successful response containing all usernames otherwise.
     */
    public Response handleListUsers(ListUsersRequest request){
        String requestUser = this.sessionTokenStore.getTokenValue(request.getSessionToken());
        if (requestUser == null) {
            return new AuthenticationRequiredErrorResponse();
        }

        boolean operationPermitted;
        operationPermitted = this.database.hasPermission(requestUser, Permission.EDIT_USERS);
        if (!operationPermitted) {
            return new ListUsersResponse(List.of(requestUser));
        }

        this.sessionTokenStore.updateLastUsed(request.getSessionToken());
        return new ListUsersResponse(this.database.listUsers());
    }

    /**
     * Handler for creating a new user. Checks for if the request user is authenticated, and
     * if they have the correct permissions.
     *
     * @param request The createUser request to process.
     * @return An error response if the user is not authenticated (invalid session token),
     * the requested username already exists, or the user does not have the right permissions, successful response otherwise.
     */
    public Response handleCreateUser(CreateUserRequest request){
        String requestUser = this.sessionTokenStore.getTokenValue((request.getSessionToken()));
        if (requestUser == null) {
            return new AuthenticationRequiredErrorResponse();
        }

        boolean operationPermitted;
        operationPermitted = this.database.hasPermission(requestUser, Permission.EDIT_USERS);
        if (!operationPermitted) {
            return new PermissionRequiredErrorResponse(Permission.EDIT_USERS);
        }

        if(request.getUsername().isEmpty() || this.database.doesUserExist(request.getUsername())){
            return new InvalidOperationErrorResponse();
        }

        HashedSaltedPassword password = HashedSaltedPassword.newFromHashedPassword(request.getHashedPassword(), this.random);

        this.database.createUser(request.getUsername(), password, request.getPermissions());
        this.sessionTokenStore.updateLastUsed(request.getSessionToken());

        return new CreateUserResponse();
    }

    /**
     * Handler for deleting an existing billboard. Checks for if the request user is authenticated, and
     * if they have the correct permissions.
     *
     * @param request The deleteBillboard request to process.
     * @return An error response if the user is not authenticated (invalid session token),
     * the requested billboard does not exist, or the user does not have the right permissions, successful response otherwise.
     */
    public Response handleDeleteBillboard(DeleteBillboardRequest request){
        String requestUser = this.sessionTokenStore.getTokenValue((request.getSessionToken()));
        if (requestUser == null) {
            return new AuthenticationRequiredErrorResponse();
        }

        String billboardName = request.getBillboardName();

        if(!this.database.doesBillboardExist(billboardName)){
            return new InvalidOperationErrorResponse();
        }

        boolean operationPermitted;
        boolean isScheduled = this.database.isBillboardScheduled(billboardName);

        //If billboard is scheduled
        if(isScheduled){
            operationPermitted = this.database.hasPermission(requestUser, Permission.EDIT_BILLBOARD);
            if(!operationPermitted){
                return new PermissionRequiredErrorResponse(Permission.EDIT_BILLBOARD);
            }
        }
        //If billboard is unscheduled
        else{
            String billboardOwner = this.database.getBillboardCreator(billboardName);

            //If owner is the current user
            if(billboardOwner.equals(requestUser)){
                operationPermitted = this.database.hasPermission(requestUser, Permission.CREATE_BILLBOARD);
                if(!operationPermitted){
                    return new PermissionRequiredErrorResponse(Permission.CREATE_BILLBOARD);
                }
            }

            //If owner is not the current user
            if(!billboardOwner.equals(requestUser)){
                operationPermitted = this.database.hasPermission(requestUser, Permission.EDIT_BILLBOARD);
                if(!operationPermitted){
                    return new PermissionRequiredErrorResponse(Permission.EDIT_BILLBOARD);
                }
            }
        }

        this.database.deleteBillboard(billboardName);
        this.sessionTokenStore.updateLastUsed(request.getSessionToken());

        return new DeleteBillboardResponse();
    }

    /**
     * Handler for listing all existing billboards. Checks for if the request user is authenticated, and
     * if they have the correct permissions.
     *
     * @param request The listBillboards request to process.
     * @return An error response if the user is not authenticated (invalid session token)
     * or the user does not have the right permissions, successful response containing all billboard metadata otherwise.
     */
    public Response handleListBillboards(ListBillboardsRequest request) {
        String requestUser = this.sessionTokenStore.getTokenValue((request.getSessionToken()));
        if (requestUser == null) {
            return new AuthenticationRequiredErrorResponse();
        }

        boolean hasEditAllPermission = this.database.hasPermission(requestUser, Permission.EDIT_BILLBOARD);
        boolean hasCreatePermission = this.database.hasPermission(requestUser, Permission.CREATE_BILLBOARD);

        List<BillboardMeta> metadata = this.database.listBillboardMetadata();
        for (BillboardMeta meta : metadata) {
            boolean isScheduled = this.database.isBillboardScheduled(meta.getName());

            boolean canModify = false;
            canModify |= meta.getCreator().equals(requestUser) && !isScheduled && hasCreatePermission;
            canModify |= hasEditAllPermission;

            meta.setCanEdit(canModify);
            meta.setCanDelete(canModify);
        }

        this.sessionTokenStore.updateLastUsed(request.getSessionToken());
        return new ListBillboardsResponse(metadata);
    }

    /**
     * Handler for getting a billboard by name. Checks for if the request user is authenticated, and
     * if the billboard exists.
     *
     * @param request The getBillboard request to process.
     * @return An error response if the user is not authenticated (invalid session token)
     * or if the billboard does not exist or could not be parsed, successful response containing the billboard otherwise.
     */
    public Response handleGetBillboard(GetBillboardRequest request) {
        String requestUser = this.sessionTokenStore.getTokenValue((request.getSessionToken()));
        if (requestUser == null) {
            return new AuthenticationRequiredErrorResponse();
        }

        String billboardText = this.database.getBillboard(request.getName());

        if (billboardText == null) {
            return new InvalidOperationErrorResponse();
        }

        this.sessionTokenStore.updateLastUsed(request.getSessionToken());

        try {
            return new GetBillboardResponse(billboardText);
        } catch (ParseException e) {
            e.printStackTrace();
            return new InvalidOperationErrorResponse();
        }
    }

    /**
     * Handler for getting the currently showing billboard, or a default billboard if none are showing. (used by the viewer application).
     *
     * @param request The getCurrentBillboard request to process.
     * @return An error response if the billboard request could not be parsed, successful response otherwise, containing either
     * the currently showing billboard or a default billboard.
     */
    public Response handleGetCurrentBillboard(GetCurrentBillboardRequest request){
        List<ScheduledBillboard> scheduledBillboards = this.database.getSchedule();
        String currentBillboardText;
        for (ScheduledBillboard billboard : scheduledBillboards){
            if(billboard.isShowingNow(ZonedDateTime.now())){
                currentBillboardText = this.database.getBillboard(billboard.getName());
                try {
                    return new GetCurrentBillboardResponse(currentBillboardText);
                } catch (ParseException e) {
                    e.printStackTrace();
                    return new InvalidOperationErrorResponse();
                }
            }
        }
        //Return a default billboard if no billboard is currently showing
        ColouredText informationText = new ColouredText("No currently showing billboard", Color.WHITE);
        Billboard defaultBillboard = new Billboard(null, null, informationText);
        defaultBillboard.backgroundColour = Color.BLACK;
        return new GetCurrentBillboardResponse(defaultBillboard);
    }

    /**
     * Handler for creating a new billboard or editing an existing one. Checks for if the request user is authenticated, and
     * if they have the correct permissions.
     *
     * @param request The createOrEdit request to process.
     * @return An error response if the user is not authenticated (invalid session token)
     * or the user does not have the right permissions, successful response otherwise.
     */
    public Response handleCreateOrEditBillboard(CreateOrEditBillboardRequest request) {
        String requestUser = this.sessionTokenStore.getTokenValue((request.getSessionToken()));
        if (requestUser == null) {
            return new AuthenticationRequiredErrorResponse();
        }

        String billboardName = request.getName();

        if (!this.database.doesBillboardExist(billboardName)) {
            // Creation request
            if (!this.database.hasPermission(requestUser, Permission.CREATE_BILLBOARD)) {
                return new PermissionRequiredErrorResponse(Permission.CREATE_BILLBOARD);
            }

            this.database.createBillboard(billboardName, requestUser, request.getBillboard());
            return new CreateOrEditBillboardResponse();
        } else {
            // Edit request
            String billboardCreator = this.database.getBillboardCreator(billboardName);
            boolean isOwnBillboard = billboardCreator.equals(requestUser);
            boolean isScheduled = this.database.isBillboardScheduled(billboardName);
            boolean requestUserHasEditPermission = this.database.hasPermission(requestUser, Permission.EDIT_BILLBOARD);
            boolean requestUserHasCreatePermission = this.database.hasPermission(requestUser, Permission.CREATE_BILLBOARD);

            boolean operationPermitted = false;
            operationPermitted |= isOwnBillboard && !isScheduled && requestUserHasCreatePermission;
            operationPermitted |= requestUserHasEditPermission;

            if (operationPermitted) {
                this.database.setBillboard(billboardName, request.getBillboard());
                return new CreateOrEditBillboardResponse();
            } else {
                return new InvalidOperationErrorResponse();
            }
        }
    }

    /**
     * Handler for scheduling a billboard for the viewer to display. Checks for if the request user is authenticated, and
     * if they have the correct permissions.
     *
     * @param request The scheduleBillboard request to process.
     * @return An error response if the user is not authenticated (invalid session token)
     * or the user does not have the right permissions, successful response otherwise.
     */
    public Response handleScheduleBillboard(ScheduleBillboardRequest request){
        String requestUser = this.sessionTokenStore.getTokenValue((request.getSessionToken()));
        if (requestUser == null) {
            return new AuthenticationRequiredErrorResponse();
        }

        boolean operationPermitted;
        operationPermitted = this.database.hasPermission(requestUser, Permission.SCHEDULE_BILLBOARD);
        if (!operationPermitted) {
            return new PermissionRequiredErrorResponse(Permission.SCHEDULE_BILLBOARD);
        }


        this.database.scheduleBillboard(
                request.getScheduledBillboard().getName(), request.getScheduledBillboard().getInitialShowing(),
                request.getScheduledBillboard().getDuration(), request.getScheduledBillboard().getRecurrencePeriod(),
                Timestamp.from(Instant.now())
        );
        this.sessionTokenStore.updateLastUsed(request.getSessionToken());

        return new ScheduleBillboardResponse();
    }

    /**
     * Handler for removing a billboard from the schedule. Checks for if the request user is authenticated,
     * if they have the correct permissions, and if the billboard exists and is scheduled.
     *
     * @param request The removeFromSchedule request to process.
     * @return An error response if the user is not authenticated (invalid session token), the billboard does not exist,
     * the billboard isn't scheduled, or the user does not have the right permissions, successful response otherwise.
     */
    public Response handleRemoveFromSchedule(RemoveFromScheduleRequest request){
        String requestUser = this.sessionTokenStore.getTokenValue((request.getSessionToken()));
        if (requestUser == null) {
            return new AuthenticationRequiredErrorResponse();
        }

        String billboardName = request.getScheduledBillboard().getName();

        if(!this.database.doesBillboardExist(billboardName) || !this.database.isBillboardScheduled(billboardName)){
            return new InvalidOperationErrorResponse();
        }

        boolean operationPermitted;
        operationPermitted = this.database.hasPermission(requestUser, Permission.SCHEDULE_BILLBOARD);
        if (!operationPermitted) {
            return new PermissionRequiredErrorResponse(Permission.SCHEDULE_BILLBOARD);
        }

        this.database.removeFromSchedule(request.getScheduledBillboard().getName(), request.getScheduledBillboard().getInitialShowing());
        this.sessionTokenStore.updateLastUsed(request.getSessionToken());

        return new RemoveFromScheduleResponse();
    }

    /**
     * Handler for viewing the current schedule. Checks for if the request user is authenticated,
     * and if they have the correct permissions.
     *
     * @param request The viewSchedule request to process.
     * @return An error response if the user is not authenticated (invalid session token),
     * or the user does not have the right permissions, successful response with the schedule otherwise.
     */
    public Response handleViewSchedule(ViewScheduleRequest request){
        String requestUser = this.sessionTokenStore.getTokenValue((request.getSessionToken()));
        if (requestUser == null) {
            return new AuthenticationRequiredErrorResponse();
        }

        boolean operationPermitted;
        operationPermitted = this.database.hasPermission(requestUser, Permission.SCHEDULE_BILLBOARD);
        if (!operationPermitted) {
            return new PermissionRequiredErrorResponse(Permission.SCHEDULE_BILLBOARD);
        }

        List<ScheduledBillboard> scheduledBillboards = this.database.getSchedule();
        List<String> creators = new ArrayList<>();

        for (ScheduledBillboard billboard : scheduledBillboards){
            creators.add(this.database.getBillboardCreator(billboard.getName()));
        }

        this.sessionTokenStore.updateLastUsed(request.getSessionToken());

        return new ViewScheduleResponse(scheduledBillboards, creators);

    }
}
