package billboard.server;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * A class for parsing and storing parameters relevant to connecting to the database.
 */
public class DatabaseConnectionProperties {
    /** The default file name for the database connection properties file. */
    public static final String DEFAULT_FILENAME = "db.props";

    private String url;

    /**
     * Loads the properties of a database connection (URL) from a file.
     *
     * @param filename The path to the file to read the properties from.
     * @throws IOException The file could not be read.
     * @throws Exception An error occurred parsing the properties file contents.
     */
    public DatabaseConnectionProperties(String filename) throws IOException, Exception {
        Properties properties = new Properties();
        try (InputStream file = Files.newInputStream(Paths.get(filename))) {
            properties.load(file);
        }

        if (!properties.containsKey("jdbc.url")) {
            throw new Exception("Properties file did not contain URL (jdbc.url)");
        }

        this.url = properties.getProperty("jdbc.url");
    }

    /**
     * Returns the URL from the properties file.
     *
     * @return The URL from the properties file.
     */
    public String getURL() {
        return url;
    }
}
