package billboard.server;

import java.time.Instant;

/**
 * Generic class which implements server.TimeSource to provide the true, current time.
 *
 * @see billboard.server.TimeSource
 */
public class RealTimeSource implements TimeSource {
    @Override
    public Instant now() {
        return Instant.now();
    }
}
