package billboard.server;

import java.time.Instant;
import java.time.temporal.TemporalUnit;
import java.util.HashMap;
import java.util.Random;

/**
 * Creation, storage and expiry management of session tokens.
 *
 * @param <T> The type of the value associated with each session token.
 */
public class SessionTokenStore<T> {
    /** The length of a session token string. */
    private static final int TOKEN_LENGTH = 32;
    /** Characters permitted to appear in a token. */
    private static final char[] TOKEN_ALPHABET = "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray();

    private final long expiry;
    private final TemporalUnit expiryUnit;
    private TimeSource timeSource;
    private Random random;

    private HashMap<String, SessionTokenData> store;

    /**
     * Creates a session token store with the specified token expiry period, time source, and source of random data.
     *
     * @param expiry The amount of time after which a token expires.
     * @param expiryUnit The time unit of {@code expiry}.
     * @param timeSource A source of time used to evaluate token expiry.
     * @param random An instance of {@link Random} which provides random data for token generation.
     */
    public SessionTokenStore(long expiry, TemporalUnit expiryUnit, TimeSource timeSource, Random random) {
        this.expiry = expiry;
        this.expiryUnit = expiryUnit;
        this.timeSource = timeSource;
        this.random = random;

        this.store = new HashMap<>();
    }

    /**
     * Creates a session token.
     *
     * Initialises the token's last used time to the current time.
     *
     * @param value The value to associate with the session token.
     * @return The created session token.
     */
    public String createToken(T value) {
        // Our token length and alphabet should be long enough that it's unlikely two randomly generated tokens collide,
        // but we'll loop until we *definitely* have a unique token to make sure.
        String token;
        do {
            StringBuilder tokenBuilder = new StringBuilder();
            for (int i = 0; i < TOKEN_LENGTH; ++i) {
                char c = TOKEN_ALPHABET[random.nextInt(TOKEN_ALPHABET.length)];
                tokenBuilder.append(c);
            }

            token = tokenBuilder.toString();
        } while (this.store.containsKey(token));

        this.store.put(token, new SessionTokenData(value, timeSource.now()));

        return token;
    }

    /**
     * Returns the {@link SessionTokenData} for the provided token, or {@code null} if the token does not exist or has
     * expired.
     *
     * @param token The token to retrieve the {@link SessionTokenData} for.
     * @return The {@link SessionTokenData}, or {@code null}.
     */
    private SessionTokenData getTokenData(String token) {
        SessionTokenData data = this.store.get(token);
        if (data == null || data.hasExpired()) {
            return null;
        }

        return data;
    }

    /**
     * Retrieves the value associated with the provided session token, or {@code null} if the token does not exist or
     * has expired.
     *
     * @param token The token to retrieve the associated value of.
     * @return The value associated with the token, or {@code null}.
     */
    public T getTokenValue(String token) {
        SessionTokenData data = this.getTokenData(token);
        if (data == null) {
            return null;
        }

        return data.value;
    }

    /**
     * Updates the last used time of a token to the current time.
     *
     * @param token The token to update the last used time of.
     */
    public void updateLastUsed(String token) {
        SessionTokenData data = this.getTokenData(token);
        if (data == null) {
            return;
        }

        data.lastUsed = timeSource.now();
    }

    /**
     * Invalidates a token, removing it from the store.
     *
     * @param token The token to invalidate.
     */
    public void invalidateToken(String token) {
        this.store.remove(token);
    }

    private class SessionTokenData {
        public T value;
        public Instant lastUsed;

        public SessionTokenData(T value, Instant lastUsed) {
            this.value = value;
            this.lastUsed = lastUsed;
        }

        /**
         * Determines if this token has expired.
         *
         * @return True if the token has expired, false otherwise.
         */
        public boolean hasExpired() {
            return this.lastUsed.until(timeSource.now(), expiryUnit) >= expiry;
        }
    }
}
