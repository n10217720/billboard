package billboard.server;

import billboard.common.ParseException;
import billboard.common.abstractXML.Attribute;
import billboard.common.abstractXML.DocumentToString;
import billboard.common.abstractXML.Element;
import billboard.common.abstractXML.StringOutput;
import billboard.common.api.Response;
import billboard.common.api.errors.MalformedRequestErrorResponse;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.function.Function;

/**
 * Class which handles parsing XML strings representing requests, hands them to the Server class,
 * and converts the response back into an XML string.
 */
public class Protocol {
    private HashMap<String, Function<Document, Response>> handlers;

    /**
     * Creates a protocol management object, which dispatches requests to the provided handlers.
     *
     * @param handlers A mapping between a request type and a handler.
     */
    public Protocol(HashMap<String, Function<Document, Response>> handlers) {
        this.handlers = handlers;
    }

    /**
     * Processes a request, dispatching to a handler if the request was valid.
     *
     * @param requestText The text of the request.
     * @return The text of the response.
     */
    public String process(String requestText) {
        // Attempt to parse the request into an XML document
        Document requestDocument;
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newDefaultInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            requestDocument = db.parse(new ByteArrayInputStream(requestText.getBytes("UTF-8")));
        } catch (ParserConfigurationException | SAXException | IOException e) {
            return DocumentToString.documentToString(new MalformedRequestErrorResponse().toXML());
        }

        // Extract the type of the request, failing if the request is already malformed at this stage
        StringOutput requestTypeOutput = new StringOutput();
        try {
            new Element("request", new Attribute[] {
                    new Attribute("type", requestTypeOutput)
            }, new Element[]{}).parseDocument(requestDocument);
        } catch (ParseException e) {
            return DocumentToString.documentToString(new MalformedRequestErrorResponse().toXML());
        }

        String requestType = requestTypeOutput.getValue();
        if (requestType.isEmpty()) {
            return DocumentToString.documentToString(new MalformedRequestErrorResponse().toXML());
        }

        // Perform dispatch
        Function<Document, Response> handler = this.handlers.get(requestType);
        if (handler == null) {
            return DocumentToString.documentToString(new MalformedRequestErrorResponse().toXML());
        }

        // Actually handle the request!
        Response response = handler.apply(requestDocument);

        return DocumentToString.documentToString(response.toXML());
    }
}
