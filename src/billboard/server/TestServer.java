package billboard.server;

import billboard.common.*;
import billboard.common.abstractXML.DocumentToString;
import billboard.common.api.errors.AuthenticationRequiredErrorResponse;
import billboard.common.api.errors.InvalidOperationErrorResponse;
import billboard.common.api.errors.InvalidUsernameOrPasswordErrorResponse;
import billboard.common.api.errors.PermissionRequiredErrorResponse;
import billboard.common.api.methods.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.*;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class TestServer {
    private MockDatabase database;
    private Server server;

    @BeforeEach
    public void beforeEach() {
        this.database = new MockDatabase();
        this.server = new Server(this.database);
    }

    //UserLogin Tests
    @Test
    public void userLoginInvalidUsername() {
        UserLoginRequest request = new UserLoginRequest("fake", HashSHA3512.generateHash("password"));

        InvalidUsernameOrPasswordErrorResponse response = (InvalidUsernameOrPasswordErrorResponse) server.handleUserLogin(request);
        assertNotNull(response);
    }

    @Test
    public void userLoginInvalidPassword() {
        UserLoginRequest request = new UserLoginRequest("admin", HashSHA3512.generateHash("fake"));

        InvalidUsernameOrPasswordErrorResponse response = (InvalidUsernameOrPasswordErrorResponse) server.handleUserLogin(request);
        assertNotNull(response);
    }

    @Test
    public void userLoginSuccess() {
        UserLoginRequest request = new UserLoginRequest("admin", HashSHA3512.generateHash("password"));

        UserLoginResponse response = (UserLoginResponse) server.handleUserLogin(request);
        assertNotNull(response);

        assertFalse(response.getSessionToken().isEmpty());
    }

    //SetUserPassword Tests
    @Test
    public void setUserPasswordUnauthenticated() {
        SetUserPasswordRequest request = new SetUserPasswordRequest("fake session token", "admin", HashSHA3512.generateHash("newPassword"));

        AuthenticationRequiredErrorResponse response = (AuthenticationRequiredErrorResponse) server.handleSetUserPassword(request);
        assertNotNull(response);
    }

    @Test
    public void setUserPasswordOwn() {
        String token = authenticateAs("user", "userPassword");
        SetUserPasswordRequest request = new SetUserPasswordRequest(token, "user", HashSHA3512.generateHash("newPassword"));

        SetUserPasswordResponse response = (SetUserPasswordResponse) server.handleSetUserPassword(request);
        assertNotNull(response);

        HashedSaltedPassword hsp = this.database.getHashedSaltedPassword("user");
        assertEquals(
                HashSHA3512.generateHash(HashSHA3512.generateHash("newPassword") + hsp.getSalt()),
                hsp.getHashedSaltedPassword()
        );
    }

    @Test
    public void setUserPasswordOtherUserPermitted() {
        String token = authenticateAs("admin", "password");
        SetUserPasswordRequest request = new SetUserPasswordRequest(token, "user", HashSHA3512.generateHash("newPassword"));

        SetUserPasswordResponse response = (SetUserPasswordResponse) server.handleSetUserPassword(request);
        assertNotNull(response);

        HashedSaltedPassword hsp = this.database.getHashedSaltedPassword("user");
        assertEquals(
                HashSHA3512.generateHash(HashSHA3512.generateHash("newPassword") + hsp.getSalt()),
                hsp.getHashedSaltedPassword()
        );
    }

    @Test
    public void setUserPasswordOtherUserDenied() {
        String token = authenticateAs("user", "userPassword");
        SetUserPasswordRequest request = new SetUserPasswordRequest(token, "admin", HashSHA3512.generateHash("newPassword"));

        PermissionRequiredErrorResponse response = (PermissionRequiredErrorResponse) server.handleSetUserPassword(request);
        assertNotNull(response);

        assertEquals(Permission.EDIT_USERS, response.getMissingPermission());
    }

    //Set Permissions Tests
    @Test
    public void setUserPermissionsOwn(){
        String token = authenticateAs("admin", "password");
        HashSet<Permission> newPermissions = new HashSet<>(Arrays.asList(Permission.EDIT_USERS,
                Permission.CREATE_BILLBOARD, Permission.SCHEDULE_BILLBOARD, Permission.EDIT_BILLBOARD));

        SetUserPermissionsRequest request = new SetUserPermissionsRequest(token, "admin", newPermissions);

        SetUserPermissionsResponse response = (SetUserPermissionsResponse) server.handleSetUserPermissions(request);
        assertNotNull(response);

        assertEquals(newPermissions, this.database.getUserPermissions("admin"));
    }

    @Test
    public void setUserPermissionsOtherPermitted(){
        String token = authenticateAs("admin", "password");
        HashSet<Permission> newPermissions = new HashSet<>(Arrays.asList(Permission.EDIT_USERS,
                Permission.CREATE_BILLBOARD, Permission.SCHEDULE_BILLBOARD, Permission.EDIT_BILLBOARD));

        SetUserPermissionsRequest request = new SetUserPermissionsRequest(token, "user", newPermissions);

        SetUserPermissionsResponse response = (SetUserPermissionsResponse) server.handleSetUserPermissions(request);
        assertNotNull(response);

        assertEquals(newPermissions, this.database.getUserPermissions("user"));
    }

    @Test
    public void setUserPermissionsOtherDenied(){
        String token = authenticateAs("user", "userPassword");
        HashSet<Permission> newPermissions = new HashSet<>(Arrays.asList(Permission.EDIT_USERS,
                Permission.CREATE_BILLBOARD, Permission.SCHEDULE_BILLBOARD, Permission.EDIT_BILLBOARD));

        SetUserPermissionsRequest request = new SetUserPermissionsRequest(token, "admin", newPermissions);

        PermissionRequiredErrorResponse response = (PermissionRequiredErrorResponse) server.handleSetUserPermissions(request);
        assertNotNull(response);

        assertEquals(Permission.EDIT_USERS, response.getMissingPermission());
    }

    @Test
    public void setUserPermissionsOtherEmptyPerms(){
        String token = authenticateAs("admin", "password");
        HashSet<Permission> newPermissions = new HashSet<>();

        SetUserPermissionsRequest request = new SetUserPermissionsRequest(token, "user", newPermissions);

        SetUserPermissionsResponse response = (SetUserPermissionsResponse) server.handleSetUserPermissions(request);
        assertNotNull(response);

        assertEquals(newPermissions, this.database.getUserPermissions("user"));
    }

    @Test
    public void setUserPermissionsInvalidUsername(){
        String token = authenticateAs("admin", "password");
        SetUserPermissionsRequest request = new SetUserPermissionsRequest(token, "fake", new HashSet<>());

        InvalidOperationErrorResponse response = (InvalidOperationErrorResponse) server.handleSetUserPermissions(request);
        assertNotNull(response);
    }

    @Test
    public void setUserPermissionsErrorRemoveFromSelf(){
        String token = authenticateAs("admin", "password");
        HashSet<Permission> newPermissions = new HashSet<>(Arrays.asList(Permission.CREATE_BILLBOARD));

        SetUserPermissionsRequest request = new SetUserPermissionsRequest(token, "admin", newPermissions);

        InvalidOperationErrorResponse response = (InvalidOperationErrorResponse) server.handleSetUserPermissions(request);
        assertNotNull(response);
    }

    @Test
    public void setUserPermissionsUnauthenticated(){
        SetUserPermissionsRequest request = new SetUserPermissionsRequest("fake", "admin", new HashSet<>());

        AuthenticationRequiredErrorResponse response = (AuthenticationRequiredErrorResponse) server.handleSetUserPermissions(request);
        assertNotNull(response);
    }

    //Get Permissions Tests
    @Test
    public void getUserPermissionsOwn(){
        String token = authenticateAs("user", "userPassword");
        GetUserPermissionsRequest request = new GetUserPermissionsRequest(token, "user");

        GetUserPermissionsResponse response = (GetUserPermissionsResponse) server.handleGetUserPermissions(request);
        assertNotNull(response);
    }

    @Test
    public void getUserPermissionsOtherPermitted(){
        String token = authenticateAs("admin", "password");
        GetUserPermissionsRequest request = new GetUserPermissionsRequest(token, "user");

        GetUserPermissionsResponse response = (GetUserPermissionsResponse) server.handleGetUserPermissions(request);
        assertNotNull(response);
    }

    @Test
    public void getUserPermissionsOtherDenied(){
        String token = authenticateAs("user", "userPassword");
        GetUserPermissionsRequest request = new GetUserPermissionsRequest(token, "admin");

        PermissionRequiredErrorResponse response = (PermissionRequiredErrorResponse) server.handleGetUserPermissions(request);
        assertNotNull(response);

        assertEquals(Permission.EDIT_USERS, response.getMissingPermission());
    }

    @Test
    public void getUserPermissionsInvalidUsername(){
        String token = authenticateAs("admin", "password");
        GetUserPermissionsRequest request = new GetUserPermissionsRequest(token, "fake");

        InvalidOperationErrorResponse response = (InvalidOperationErrorResponse) server.handleGetUserPermissions(request);
        assertNotNull(response);
    }

    @Test
    public void getUserPermissionsUnauthenticated(){
        GetUserPermissionsRequest request = new GetUserPermissionsRequest("fake", "admin");

        AuthenticationRequiredErrorResponse response = (AuthenticationRequiredErrorResponse) server.handleGetUserPermissions(request);
        assertNotNull(response);
    }

    //DeleteUser Tests
    @Test
    public void deleteUser(){
        String token = authenticateAs("admin", "password");
        DeleteUserRequest request = new DeleteUserRequest(token, "user");

        DeleteUserResponse response = (DeleteUserResponse) server.handleDeleteUser(request);
        assertNotNull(response);

        assertFalse(this.database.doesUserExist(request.getUsername()));
    }

    @Test
    public void deleteUserErrorCurrentUser(){
        String token = authenticateAs("admin", "password");
        DeleteUserRequest request = new DeleteUserRequest(token, "admin");

        InvalidOperationErrorResponse response = (InvalidOperationErrorResponse) server.handleDeleteUser(request);
        assertNotNull(response);
    }

    @Test
    public void deleteUserInvalidUsername(){
        String token = authenticateAs("admin", "password");
        DeleteUserRequest request = new DeleteUserRequest(token, "eh");

        InvalidOperationErrorResponse response = (InvalidOperationErrorResponse) server.handleDeleteUser(request);
        assertNotNull(response);
    }

    @Test
    public void deleteUserPermissionDenied(){
        String token = authenticateAs("user", "userPassword");
        DeleteUserRequest request = new DeleteUserRequest(token, "admin");

        PermissionRequiredErrorResponse response = (PermissionRequiredErrorResponse) server.handleDeleteUser(request);
        assertNotNull(response);
    }

    @Test
    public void deleteUserUnauthenticated(){
        DeleteUserRequest request = new DeleteUserRequest("fake", "user");

        AuthenticationRequiredErrorResponse response = (AuthenticationRequiredErrorResponse) server.handleDeleteUser(request);
        assertNotNull(response);
    }

    //ListUsers Tests
    @Test
    public void listUsers(){
        String token = authenticateAs("admin", "password");
        ListUsersRequest request = new ListUsersRequest(token);

        ListUsersResponse response = (ListUsersResponse) server.handleListUsers(request);
        assertNotNull(response);
    }

    @Test
    public void listUsersNoEditUsersPermission(){
        String token = authenticateAs("user", "userPassword");
        ListUsersRequest request = new ListUsersRequest(token);

        ListUsersResponse response = (ListUsersResponse) server.handleListUsers(request);
        assertNotNull(response);

        assertEquals(List.of("user"), response.getUsernames());
    }

    @Test
    public void listUsersUnauthenticated(){
        ListUsersRequest request = new ListUsersRequest("fake");

        AuthenticationRequiredErrorResponse response = (AuthenticationRequiredErrorResponse) server.handleListUsers(request);
        assertNotNull(response);
    }

    //CreateUser Tests
    @Test
    public void createUser(){
        String token = authenticateAs("admin", "password");
        HashSet<Permission> permissions = new HashSet<>(Arrays.asList(Permission.EDIT_USERS,
                Permission.CREATE_BILLBOARD, Permission.SCHEDULE_BILLBOARD, Permission.EDIT_BILLBOARD));
        CreateUserRequest request = new CreateUserRequest(token, "joe", HashSHA3512.generateHash("newPassword"), permissions);

        CreateUserResponse response = (CreateUserResponse) server.handleCreateUser(request);
        assertNotNull(response);

        assertTrue(this.database.doesUserExist("joe"));

        HashedSaltedPassword storedHSP = this.database.getHashedSaltedPassword("joe");
        assertEquals(
                HashSHA3512.generateHash(HashSHA3512.generateHash("newPassword") + storedHSP.getSalt()),
                storedHSP.getHashedSaltedPassword()
        );
    }

    @Test
    public void createUserPermissionDenied(){
        String token = authenticateAs("user", "userPassword");
        CreateUserRequest request = new CreateUserRequest(token, "joe", HashSHA3512.generateHash("newPassword"), new HashSet<>());

        PermissionRequiredErrorResponse response = (PermissionRequiredErrorResponse) server.handleCreateUser(request);
        assertNotNull(response);
    }

    @Test
    public void createUserUnauthenticated(){
        CreateUserRequest request = new CreateUserRequest("fake", "joe", HashSHA3512.generateHash("newPassword"), new HashSet<>());

        AuthenticationRequiredErrorResponse response = (AuthenticationRequiredErrorResponse) server.handleCreateUser(request);
        assertNotNull(response);
    }

    @Test
    public void createUserNameAlreadyTaken(){
        String token = authenticateAs("admin", "password");
        CreateUserRequest request = new CreateUserRequest(token, "user", HashSHA3512.generateHash("newPassword"), new HashSet<>());

        InvalidOperationErrorResponse response = (InvalidOperationErrorResponse) server.handleCreateUser(request);
        assertNotNull(response);
    }

    @Test
    public void createUserEmptyUsername(){
        String token = authenticateAs("admin", "password");
        CreateUserRequest request = new CreateUserRequest(token, "", HashSHA3512.generateHash("newPassword"), new HashSet<>());

        InvalidOperationErrorResponse response = (InvalidOperationErrorResponse) server.handleCreateUser(request);
        assertNotNull(response);
    }

    @Test
    public void createUserEmptyPerms(){
        String token = authenticateAs("admin", "password");
        HashSet<Permission> permissions = new HashSet<>();
        CreateUserRequest request = new CreateUserRequest(token, "joe", HashSHA3512.generateHash("newPassword"), permissions);

        CreateUserResponse response = (CreateUserResponse) server.handleCreateUser(request);
        assertNotNull(response);

        assertTrue(this.database.doesUserExist("joe"));
        assertEquals(permissions, this.database.getUserPermissions("joe"));
    }

    //DeleteBillboard Tests
    @Test
    public void deleteBillboardScheduled(){
        String token = authenticateAs("admin", "password");
        DeleteBillboardRequest request = new DeleteBillboardRequest(token, "scheduledBillboard");

        DeleteBillboardResponse response = (DeleteBillboardResponse) server.handleDeleteBillboard(request);
        assertNotNull(response);

        assertFalse(this.database.doesBillboardExist("scheduledBillboard"));
    }

    //I assume that we should not allow a user to delete their own billboard if they no longer have the permission to do so.
    @Test
    public void deleteBillboardScheduledError(){
        String token = authenticateAs("user", "userPassword");
        DeleteBillboardRequest request = new DeleteBillboardRequest(token, "scheduledBillboard");

        PermissionRequiredErrorResponse response = (PermissionRequiredErrorResponse) server.handleDeleteBillboard(request);
        assertNotNull(response);
    }

    @Test
    public void deleteBillboardUnscheduledCurrentUserCreator(){
        String token = authenticateAs("admin", "password");
        DeleteBillboardRequest request = new DeleteBillboardRequest(token, "billboardAdmin");

        DeleteBillboardResponse response = (DeleteBillboardResponse) server.handleDeleteBillboard(request);
        assertNotNull(response);

        assertFalse(this.database.doesBillboardExist("billboardAdmin"));
    }

    @Test
    public void deleteBillboardUnscheduledCurrentUserCreatorError(){
        String token = authenticateAs("user", "userPassword");
        DeleteBillboardRequest request = new DeleteBillboardRequest(token, "billboard");

        PermissionRequiredErrorResponse response = (PermissionRequiredErrorResponse) server.handleDeleteBillboard(request);
        assertNotNull(response);
    }

    @Test
    public void deleteBillboardUnscheduledAnotherUserCreator(){
        String token = authenticateAs("admin", "password");

        DeleteBillboardRequest request = new DeleteBillboardRequest(token, "billboard");

        DeleteBillboardResponse response = (DeleteBillboardResponse) server.handleDeleteBillboard(request);
        assertNotNull(response);

        assertFalse(this.database.doesBillboardExist("billboard"));
    }

    @Test
    public void deleteBillboardUnscheduledAnotherUserCreatorError(){
        String token = authenticateAs("user", "userPassword");
        DeleteBillboardRequest request = new DeleteBillboardRequest(token, "billboardAdmin");

        PermissionRequiredErrorResponse response = (PermissionRequiredErrorResponse) server.handleDeleteBillboard(request);
        assertNotNull(response);
    }

    @Test
    public void deleteBillboardInvalidName(){
        String token = authenticateAs("admin", "password");
        DeleteBillboardRequest request = new DeleteBillboardRequest(token, "fake");

        InvalidOperationErrorResponse response = (InvalidOperationErrorResponse) server.handleDeleteBillboard(request);
        assertNotNull(response);
    }

    @Test
    public void deleteBillboardUnauthenticated(){
        DeleteBillboardRequest request = new DeleteBillboardRequest("fake", "fake");

        AuthenticationRequiredErrorResponse response = (AuthenticationRequiredErrorResponse) server.handleDeleteBillboard(request);
        assertNotNull(response);
    }

    // ListBillboards tests
    @Test
    public void listBillboards(){
        String token = authenticateAs("admin", "password");

        ListBillboardsRequest request = new ListBillboardsRequest(token);

        ListBillboardsResponse response = (ListBillboardsResponse) server.handleListBillboards(request);
        assertNotNull(response);

        List<String> names = response.getBillboards().stream().map(BillboardMeta::getName).collect(Collectors.toList());
        assertTrue(names.contains("billboard"));
        assertTrue(names.contains("billboardAdmin"));
        assertTrue(names.contains("scheduledBillboard"));

        List<String> creators = response.getBillboards().stream().map(BillboardMeta::getCreator).collect(Collectors.toList());
        assertTrue(creators.contains("admin"));
        assertTrue(creators.contains("user"));

        List<Boolean> canEdits = response.getBillboards().stream().map(BillboardMeta::canEdit).collect(Collectors.toList());
        assertEquals(List.of(true, true, true, true), canEdits);

        List<Boolean> canDeletes = response.getBillboards().stream().map(BillboardMeta::canDelete).collect(Collectors.toList());
        assertEquals(List.of(true, true, true, true), canDeletes);
    }

    // GetBillboard tests
    @Test
    public void getBillboard(){
        String token = authenticateAs("admin", "password");

        GetBillboardRequest request = new GetBillboardRequest(token, "billboard");

        GetBillboardResponse response = (GetBillboardResponse) server.handleGetBillboard(request);
        assertNotNull(response);

        assertEquals(Color.RED, response.getBillboard().backgroundColour);
        assertEquals("Message", response.getBillboard().message.text);
        assertEquals(Color.WHITE, response.getBillboard().message.colour);
        assertNull(response.getBillboard().picture);
        assertNull(response.getBillboard().information);
    }

    // GetCurrentBillboard tests
    @Test
    public void getCurrentBillboardNoShowing(){
        GetCurrentBillboardRequest request = new GetCurrentBillboardRequest();

        GetCurrentBillboardResponse response = (GetCurrentBillboardResponse) server.handleGetCurrentBillboard(request);
        assertNotNull(response);

        assertEquals(Color.BLACK, response.getBillboard().backgroundColour);
        assertEquals("No currently showing billboard", response.getBillboard().information.text);
        assertEquals(Color.WHITE, response.getBillboard().information.colour);
        assertNull(response.getBillboard().picture);
        assertNull(response.getBillboard().message);
    }

    // CreateOrEditBillboard tests
    @Test
    public void createOrEditBillboardCreate() {
        String token = authenticateAs("admin", "password");

        Billboard billboard = new Billboard();
        billboard.backgroundColour = Color.WHITE;
        billboard.message = new ColouredText("Message", Color.BLACK);
        CreateOrEditBillboardRequest request = new CreateOrEditBillboardRequest(token, "new billboard", billboard);

        CreateOrEditBillboardResponse response = (CreateOrEditBillboardResponse) server.handleCreateOrEditBillboard(request);
        assertNotNull(response);

        assertEquals(DocumentToString.documentToString(billboard.toXML()), database.getBillboard("new billboard"));
    }

    @Test
    public void createOrEditBillboardCreateDenied() {
        String token = authenticateAs("user", "userPassword");

        Billboard billboard = new Billboard();
        billboard.backgroundColour = Color.WHITE;
        billboard.message = new ColouredText("Message", Color.BLACK);
        CreateOrEditBillboardRequest request = new CreateOrEditBillboardRequest(token, "new billboard", billboard);

        PermissionRequiredErrorResponse response = (PermissionRequiredErrorResponse) server.handleCreateOrEditBillboard(request);
        assertNotNull(response);
    }

    @Test
    public void createOrEditBillboardEditOwn() {
        String token = authenticateAs("admin", "password");

        Billboard billboard = new Billboard();
        billboard.backgroundColour = Color.WHITE;
        billboard.message = new ColouredText("Message", Color.BLACK);
        CreateOrEditBillboardRequest request = new CreateOrEditBillboardRequest(token, "billboardAdmin", billboard);

        CreateOrEditBillboardResponse response = (CreateOrEditBillboardResponse) server.handleCreateOrEditBillboard(request);
        assertNotNull(response);

        assertEquals(DocumentToString.documentToString(billboard.toXML()), database.getBillboard("billboardAdmin"));
    }

    @Test
    public void createOrEditBillboardEditOtherDenied() {
        String token = authenticateAs("user", "userPassword");

        Billboard billboard = new Billboard();
        billboard.backgroundColour = Color.WHITE;
        billboard.message = new ColouredText("Message", Color.BLACK);
        CreateOrEditBillboardRequest request = new CreateOrEditBillboardRequest(token, "billboardAdmin", billboard);

        InvalidOperationErrorResponse response = (InvalidOperationErrorResponse) server.handleCreateOrEditBillboard(request);
        assertNotNull(response);
    }

    // ScheduleBillboard Tests
    @Test
    public void scheduleBillboard(){
        String token = authenticateAs("admin", "password");

        ScheduledBillboard billboard = new ScheduledBillboard("billboardAdmin", ZonedDateTime.now(), 5, Duration.ofHours(23));
        ScheduleBillboardRequest request = new ScheduleBillboardRequest(token, billboard);

        ScheduleBillboardResponse response = (ScheduleBillboardResponse) server.handleScheduleBillboard(request);
        assertNotNull(response);

        assertTrue(database.isBillboardScheduled("billboardAdmin"));
    }

    @Test
    public void scheduleBillboardPermissionRequired(){
        String token = authenticateAs("user", "userPassword");

        ScheduledBillboard billboard = new ScheduledBillboard("billboardAdmin", ZonedDateTime.now(), 5, Duration.ofHours(23));
        ScheduleBillboardRequest request = new ScheduleBillboardRequest(token, billboard);

        PermissionRequiredErrorResponse response = (PermissionRequiredErrorResponse) server.handleScheduleBillboard(request);
        assertNotNull(response);
    }

    @Test
    public void scheduleBillboardUnauthenticated(){
        ScheduledBillboard billboard = new ScheduledBillboard("billboardAdmin", ZonedDateTime.now(), 5, Duration.ofHours(23));
        ScheduleBillboardRequest request = new ScheduleBillboardRequest("token", billboard);

        AuthenticationRequiredErrorResponse response = (AuthenticationRequiredErrorResponse) server.handleScheduleBillboard(request);
        assertNotNull(response);
    }

    //RemoveFromSchedule Tests
    @Test
    public void removeFromSchedule(){
        String token = authenticateAs("admin", "password");

        ScheduledBillboard billboard = new ScheduledBillboard("scheduledBillboard", ZonedDateTime.now(), 5, Duration.ofHours(23));
        RemoveFromScheduleRequest request = new RemoveFromScheduleRequest(token, billboard);

        RemoveFromScheduleResponse response = (RemoveFromScheduleResponse) server.handleRemoveFromSchedule(request);
        assertNotNull(response);

        assertFalse(database.isBillboardScheduled("scheduledBillboard"));
    }

    @Test
    public void removeFromScheduleUnscheduledError(){
        String token = authenticateAs("admin", "password");

        ScheduledBillboard billboard = new ScheduledBillboard("billboardAdmin", ZonedDateTime.now(), 5, Duration.ofHours(23));
        RemoveFromScheduleRequest request = new RemoveFromScheduleRequest(token, billboard);

        InvalidOperationErrorResponse response = (InvalidOperationErrorResponse) server.handleRemoveFromSchedule(request);
        assertNotNull(response);
    }

    @Test
    public void removeFromScheduleInvalidNameError(){
        String token = authenticateAs("admin", "password");

        ScheduledBillboard billboard = new ScheduledBillboard("name", ZonedDateTime.now(), 5, Duration.ofHours(23));
        RemoveFromScheduleRequest request = new RemoveFromScheduleRequest(token, billboard);

        InvalidOperationErrorResponse response = (InvalidOperationErrorResponse) server.handleRemoveFromSchedule(request);
        assertNotNull(response);
    }

    @Test
    public void removeFromSchedulePermissionRequired(){
        String token = authenticateAs("user", "userPassword");

        ScheduledBillboard billboard = new ScheduledBillboard("scheduledBillboard", ZonedDateTime.now(), 5, Duration.ofHours(23));
        RemoveFromScheduleRequest request = new RemoveFromScheduleRequest(token, billboard);

        PermissionRequiredErrorResponse response = (PermissionRequiredErrorResponse) server.handleRemoveFromSchedule(request);
        assertNotNull(response);
    }

    @Test
    public void removeFromScheduleUnauthenticated(){
        ScheduledBillboard billboard = new ScheduledBillboard("scheduledBillboard", ZonedDateTime.now(), 5, Duration.ofHours(23));
        RemoveFromScheduleRequest request = new RemoveFromScheduleRequest("token", billboard);

        AuthenticationRequiredErrorResponse response = (AuthenticationRequiredErrorResponse) server.handleRemoveFromSchedule(request);
        assertNotNull(response);
    }

    //ViewSchedule
    @Test
    public void viewSchedule(){
        String token = authenticateAs("admin", "password");
        ViewScheduleRequest request = new ViewScheduleRequest(token);

        ViewScheduleResponse response = (ViewScheduleResponse) server.handleViewSchedule(request);
        assertNotNull(response);

        assertEquals(response.getScheduledBillboards().get(0).getName(), this.database.getSchedule().get(0).getName());
        assertEquals(response.getScheduledBillboards().get(1).getName(), this.database.getSchedule().get(1).getName());

        assertEquals(response.getScheduledBillboards().get(0).getInitialShowing(), this.database.getSchedule().get(0).getInitialShowing());
        assertEquals(response.getScheduledBillboards().get(1).getInitialShowing(), this.database.getSchedule().get(1).getInitialShowing());

        assertEquals(response.getScheduledBillboards().get(0).getDuration(), this.database.getSchedule().get(0).getDuration());
        assertEquals(response.getScheduledBillboards().get(1).getDuration(), this.database.getSchedule().get(1).getDuration());

        assertEquals(response.getScheduledBillboards().get(0).getRecurrencePeriod(), this.database.getSchedule().get(0).getRecurrencePeriod());
        assertEquals(response.getScheduledBillboards().get(1).getRecurrencePeriod(), this.database.getSchedule().get(1).getRecurrencePeriod());

        assertTrue(response.getBillboardCreators().contains("admin"));
    }

    @Test
    public void viewSchedulePermissionRequired(){
        String token = authenticateAs("user", "userPassword");
        ViewScheduleRequest request = new ViewScheduleRequest(token);

        PermissionRequiredErrorResponse response = (PermissionRequiredErrorResponse) server.handleViewSchedule(request);
        assertNotNull(response);
    }

    @Test
    public void viewScheduleUnauthenticated(){
        ViewScheduleRequest request = new ViewScheduleRequest("token");

        AuthenticationRequiredErrorResponse response = (AuthenticationRequiredErrorResponse) server.handleViewSchedule(request);
        assertNotNull(response);
    }

    /**
     * Helper method to return a valid session token for the provided user.
     *
     * @param username The username of the user to authenticate as.
     * @param password The password of the user to authenticate as.
     * @return A session token for the user.
     */
    private String authenticateAs(String username, String password) {
        UserLoginRequest request = new UserLoginRequest(username, HashSHA3512.generateHash(password));
        UserLoginResponse response = (UserLoginResponse) server.handleUserLogin(request);
        return response.getSessionToken();
    }
}

class MockDatabase implements Database {
    private static final String SALT = "salt";
    private HashMap<String, HashedSaltedPassword> passwords;
    private HashMap<String, HashSet<Permission>> permissions;

    private HashMap<String, String> billboards;
    private HashMap<String, String> billboardOwners;

    private HashMap<String, List<ZonedDateTime>> billboardStartTimes;
    private HashMap<String, List<Long>> billboardDurations;
    private HashMap<String, List<Duration>> billboardRecurrences;

    public MockDatabase() {
        this.passwords = new HashMap<>();
        this.permissions = new HashMap<>();
        this.billboards = new HashMap<>();
        this.billboardOwners = new HashMap<>();

        this.billboardStartTimes = new HashMap<>();
        this.billboardDurations = new HashMap<>();
        this.billboardRecurrences = new HashMap<>();

        this.passwords.put("admin", new HashedSaltedPassword(
                HashSHA3512.generateHash(HashSHA3512.generateHash("password") + SALT),
                SALT
        ));
        this.passwords.put("user", new HashedSaltedPassword(
                HashSHA3512.generateHash(HashSHA3512.generateHash("userPassword") + SALT),
                SALT
        ));

        this.permissions.put("admin", new HashSet<>(Arrays.asList(Permission.EDIT_USERS, Permission.EDIT_BILLBOARD, Permission.CREATE_BILLBOARD, Permission.SCHEDULE_BILLBOARD)));
        this.permissions.put("user", new HashSet<>(Arrays.asList()));

        Billboard billboard = new Billboard();
        billboard.backgroundColour = Color.RED;
        billboard.message = new ColouredText("Message", Color.WHITE);

        this.billboards.put("billboard", DocumentToString.documentToString(billboard.toXML()));
        this.billboardOwners.put("billboard", "user");

        billboard = new Billboard();
        billboard.backgroundColour = Color.ORANGE;
        billboard.message = new ColouredText("Message", Color.WHITE);

        this.billboards.put("billboardAdmin", DocumentToString.documentToString(billboard.toXML()));
        this.billboardOwners.put("billboardAdmin", "admin");

        billboard = new Billboard();
        billboard.backgroundColour = Color.GREEN;
        billboard.message = new ColouredText("Message", Color.WHITE);

        this.billboards.put("scheduledBillboard", DocumentToString.documentToString(billboard.toXML()));
        this.billboardOwners.put("scheduledBillboard", "admin");

        billboard = new Billboard();
        billboard.backgroundColour = Color.BLUE;
        billboard.message = new ColouredText("Message", Color.WHITE);

        this.billboards.put("scheduledBillboard2", DocumentToString.documentToString(billboard.toXML()));
        this.billboardOwners.put("scheduledBillboard2", "admin");

        this.billboardStartTimes.put("scheduledBillboard", List.of(ZonedDateTime.of(2020, 10, 4, 1, 45, 0, 0, ZoneId.systemDefault())));
        this.billboardDurations.put("scheduledBillboard", List.of((long) 5));
        this.billboardRecurrences.put("scheduledBillboard", List.of(Duration.ofHours(23)));

        this.billboardStartTimes.put("scheduledBillboard2", List.of(ZonedDateTime.of(2020, 10, 4, 5, 0, 0, 0, ZoneId.systemDefault())));
        this.billboardDurations.put("scheduledBillboard2", List.of((long) 10));
        this.billboardRecurrences.put("scheduledBillboard2", List.of(Duration.ofHours(23)));

    }

    @Override
    public HashedSaltedPassword getHashedSaltedPassword(String username) {
        return this.passwords.get(username);
    }

    @Override
    public void setUserPassword(String username, HashedSaltedPassword newPassword) {
        this.passwords.put(username, newPassword);
    }

    @Override
    public boolean hasPermission(String username, Permission permission) {
        if (this.permissions.containsKey(username)) {
            return this.permissions.get(username).contains(permission);
        }
        return false;
    }

    @Override
    public void setUserPermissions(String username, HashSet<Permission> permissions) {
        this.permissions.put(username, permissions);
    }

    @Override
    public HashSet<Permission> getUserPermissions(String username) {
        return this.permissions.get(username);
    }

    @Override
    public boolean doesUserExist(String username) {
        return this.passwords.containsKey(username);
    }

    @Override
    public void deleteUser(String username) {
        this.permissions.remove(username);
        this.passwords.remove(username);
    }

    @Override
    public ArrayList<String> listUsers() {
        return new ArrayList<>(this.passwords.keySet());
    }

    @Override
    public void createUser(String username, HashedSaltedPassword password, HashSet<Permission> permissions) {
        this.passwords.put(username, password);
        this.permissions.put(username, permissions);
    }

    @Override
    public void deleteBillboard(String name) {
        this.billboards.remove(name);
        this.billboardOwners.remove(name);
    }

    @Override
    public boolean doesBillboardExist(String billboardName) {
        return this.billboards.containsKey(billboardName);
    }

    @Override
    public boolean isBillboardScheduled(String billboardName) {
        return billboardStartTimes.containsKey(billboardName);
    }

    @Override
    public String getBillboardCreator(String billboardName) {
        return this.billboardOwners.get(billboardName);
    }

    @Override
    public List<BillboardMeta> listBillboardMetadata() {
        List<BillboardMeta> metadata = new ArrayList<>();
        for (String billboardName : this.billboards.keySet()) {
            metadata.add(new BillboardMeta(
                    billboardName,
                    this.billboardOwners.get(billboardName),
                    true, false
            ));
        }
        return metadata;
    }

    @Override
    public String getBillboard(String name) {
        if (this.billboards.containsKey(name)) {
            return this.billboards.get(name);
        }

        return null;
    }

    @Override
    public void createBillboard(String billboardName, String creator, String billboard) {
        this.billboards.put(billboardName, billboard);
        this.billboardOwners.put(billboardName, creator);
    }

    @Override
    public void setBillboard(String billboardName, String billboard) {
        this.billboards.remove(billboardName);
        this.billboards.put(billboardName, billboard);
    }

    @Override
    public void scheduleBillboard(String billboardName, ZonedDateTime startingTimeDate, long duration, Duration recurrencePeriod, Timestamp now) {
        this.billboardStartTimes.put(billboardName, List.of(startingTimeDate));
        this.billboardDurations.put(billboardName, List.of(duration));
        this.billboardRecurrences.put(billboardName, List.of(recurrencePeriod));
    }

    @Override
    public void removeFromSchedule(String billboardName, ZonedDateTime timeDate) {
        this.billboardStartTimes.remove(billboardName);
        this.billboardDurations.remove(billboardName);
        this.billboardRecurrences.remove(billboardName);
    }

    @Override
    public List<ScheduledBillboard> getSchedule() {
        List<ScheduledBillboard> billboards = new ArrayList<>();
        for(String billboardName : this.billboardStartTimes.keySet()){
            for(int i = 0; i < this.billboardStartTimes.get(billboardName).size(); ++i){
                billboards.add(new ScheduledBillboard(billboardName,
                        this.billboardStartTimes.get(billboardName).get(i),
                        this.billboardDurations.get(billboardName).get(i),
                        this.billboardRecurrences.get(billboardName).get(i)));
            }
        }
        return billboards;
    }
}
