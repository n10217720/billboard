package billboard.server;

import billboard.common.ConnectionProperties;
import billboard.common.ParseException;
import billboard.common.api.Request;
import billboard.common.api.Response;
import billboard.common.api.errors.MalformedRequestErrorResponse;
import billboard.common.api.methods.*;
import org.w3c.dom.Document;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.function.Function;

/**
 * Provides the entry point for the server.
 * Opens a listening socket, accepts client connections, processes the client’s request and returns an appropriate response.
 */
public class Main {
    public static void main(String[] args) {
        ConnectionProperties connectionProperties = null;
        try {
            connectionProperties = new ConnectionProperties(ConnectionProperties.DEFAULT_FILENAME);
        } catch (Exception e) {
            System.err.println("Failed to load connection properties file.");
            e.printStackTrace();
            System.exit(1);
        }

        DatabaseConnectionProperties dbConnectionProperties = null;
        try {
            dbConnectionProperties = new DatabaseConnectionProperties(DatabaseConnectionProperties.DEFAULT_FILENAME);
        } catch (Exception e) {
            System.err.println("Failed to load database connection properties file.");
            e.printStackTrace();
            System.exit(1);
        }

        String dbURL = dbConnectionProperties.getURL();
        try (SQLiteDatabase db = new SQLiteDatabase(dbURL)) {
            Server server = new Server(db);
            Protocol protocol = new Protocol(getHandlers(server));

            int port = connectionProperties.getPort();
            System.out.println(String.format("Server started listening on port %d", port));

            // Server will run forever: users wishing to quit it will force terminate, so we're safe to loop infinitely such
            // that we're always ready to accept a new client.
            while (true) {
                try (ServerSocket serverSocket = new ServerSocket(port)) {
                    try (
                            Socket client = serverSocket.accept();
                            BufferedReader clientIn = new BufferedReader(new InputStreamReader(client.getInputStream()));
                            PrintWriter clientOut = new PrintWriter(client.getOutputStream(), true);
                    ) {
                        System.out.println(String.format("Accepted client: %s", client.getRemoteSocketAddress().toString()));

                        // Can return null if client prematurely closes connection
                        String requestText = clientIn.readLine();
                        if (requestText != null) {
                            String responseText = protocol.process(requestText);
                            clientOut.println(responseText);
                        }

                        System.out.println(String.format("Client finished: %s", client.getRemoteSocketAddress().toString()));
                    }
                } catch (IOException e) {
                    System.err.println("Server failed to create required server socket. Server cannot start.");
                    e.printStackTrace();
                    System.exit(1);
                }
            }
        } catch (SQLException e) {
            System.err.println("Server failed to open connection to database. Server cannot start.");
            e.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Returns the mapping between request types and handlers which operate on the provided server.
     *
     * @param server The server handling the requests.
     * @return The request type to handler mapping.
     */
    private static HashMap<String, Function<Document, Response>> getHandlers(Server server) {
        HashMap<String, Function<Document, Response>> handlers = new HashMap<>();

        handlers.put("userLogin", document -> {
            UserLoginRequest request = null;
            try {
                request = UserLoginRequest.fromXML(document);
            } catch (ParseException e) {
                return new MalformedRequestErrorResponse();
            }
            return server.handleUserLogin(request);
        });
        handlers.put("setUserPassword", document -> {
            SetUserPasswordRequest request = null;
            try {
                request = SetUserPasswordRequest.fromXML(document);
            } catch (ParseException e) {
                return new MalformedRequestErrorResponse();
            }
            return server.handleSetUserPassword(request);
        });
        handlers.put("deleteUser", document -> {
            DeleteUserRequest request = null;
            try {
                request = DeleteUserRequest.fromXML(document);
            } catch (ParseException e){
                return new MalformedRequestErrorResponse();
            }
            return server.handleDeleteUser(request);
        });
        handlers.put("listUsers", document -> {
            ListUsersRequest request = null;
            try {
                request = ListUsersRequest.fromXML(document);
            } catch (ParseException e){
                return new MalformedRequestErrorResponse();
            }
            return server.handleListUsers(request);
        });
        handlers.put("setUserPermissions", document -> {
            SetUserPermissionsRequest request = null;
            try {
                request = SetUserPermissionsRequest.fromXML(document);
            } catch (ParseException e){
                return new MalformedRequestErrorResponse();
            }
            return server.handleSetUserPermissions(request);
        });
        handlers.put("getUserPermissions", document -> {
            GetUserPermissionsRequest request = null;
            try {
                request = GetUserPermissionsRequest.fromXML(document);
            } catch (ParseException e){
                return new MalformedRequestErrorResponse();
            }
            return server.handleGetUserPermissions(request);
        });
        handlers.put("createUser", document -> {
            CreateUserRequest request = null;
            try{
                request = CreateUserRequest.fromXML(document);
            } catch (ParseException e) {
                return new MalformedRequestErrorResponse();
            }
            return server.handleCreateUser(request);
        });
        handlers.put("userLogout", document -> {
            UserLogoutRequest request = null;
            try {
                request = UserLogoutRequest.fromXML(document);
            } catch (Exception e) {
                return new MalformedRequestErrorResponse();
            }
            return server.handleUserLogout(request);
        });
        handlers.put("listBillboards", document -> {
            ListBillboardsRequest request = null;
            try {
                request = ListBillboardsRequest.fromXML(document);
            } catch (Exception e) {
                return new MalformedRequestErrorResponse();
            }
            return server.handleListBillboards(request);
        });
        handlers.put("getBillboard", document -> {
            GetBillboardRequest request = null;
            try {
                request = GetBillboardRequest.fromXML(document);
            } catch (Exception e) {
                return new MalformedRequestErrorResponse();
            }
            return server.handleGetBillboard(request);
        });
        handlers.put("deleteBillboard", document -> {
            DeleteBillboardRequest request = null;
            try {
                request = DeleteBillboardRequest.fromXML(document);
            } catch (Exception e) {
                return new MalformedRequestErrorResponse();
            }
            return server.handleDeleteBillboard(request);
        });
        handlers.put("createOrEditBillboard", document -> {
            CreateOrEditBillboardRequest request = null;
            try {
                request = CreateOrEditBillboardRequest.fromXML(document);
            } catch (Exception e) {
                return new MalformedRequestErrorResponse();
            }
            return server.handleCreateOrEditBillboard(request);
        });
        handlers.put("scheduleBillboard", document -> {
            ScheduleBillboardRequest request = null;
            try {
                request = ScheduleBillboardRequest.fromXML(document);
            } catch (Exception e) {
                return new MalformedRequestErrorResponse();
            }
            return server.handleScheduleBillboard(request);
        });
        handlers.put("removeFromSchedule", document -> {
            RemoveFromScheduleRequest request = null;
            try {
                request = RemoveFromScheduleRequest.fromXML(document);
            } catch (Exception e) {
                return new MalformedRequestErrorResponse();
            }
            return server.handleRemoveFromSchedule(request);
        });
        handlers.put("viewSchedule", document -> {
            ViewScheduleRequest request = null;
            try {
                request = ViewScheduleRequest.fromXML(document);
            } catch (Exception e) {
                return new MalformedRequestErrorResponse();
            }
            return server.handleViewSchedule(request);
        });
        handlers.put("getCurrentBillboard", document -> {
            GetCurrentBillboardRequest request = null;
            try {
                request = GetCurrentBillboardRequest.fromXML(document);
            } catch (Exception e) {
                return new MalformedRequestErrorResponse();
            }
            return server.handleGetCurrentBillboard(request);
        });

        return handlers;
    }
}
