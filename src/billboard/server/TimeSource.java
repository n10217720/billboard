package billboard.server;

import java.time.Instant;

/**
 * A source of the current time.
 */
public interface TimeSource {
    /**
     * Returns the current time.
     *
     * @return The current time.
     */
    Instant now();
}
