package billboard.server;

import billboard.common.HashSHA3512;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

public class TestHashedSaltedPassword {
    private HashedSaltedPassword hsp = new HashedSaltedPassword(
            HashSHA3512.generateHash(HashSHA3512.generateHash("password") + "salt"),
            "salt"
    );

    @Test
    public void getHashedSaltedPassword() {
        assertEquals(hsp.getHashedSaltedPassword(), HashSHA3512.generateHash(HashSHA3512.generateHash("password") + "salt"));
    }

    @Test
    public void getSalt() {
        assertEquals(hsp.getSalt(), "salt");
    }

    @Test
    public void newFromHashedPassword() {
        HashedSaltedPassword newPassword = HashedSaltedPassword.newFromHashedPassword(
                HashSHA3512.generateHash("password"),
                new Random()
        );

        String expectedHashedSaltedPassword = HashSHA3512.generateHash(
                HashSHA3512.generateHash("password") + newPassword.getSalt()
        );

        assertFalse(newPassword.getSalt().isEmpty());
        // We don't care what the salt was (as long as there is one), as long as it's used to generate the hashed and
        // salted password
        assertEquals(newPassword.getHashedSaltedPassword(), expectedHashedSaltedPassword);
    }
}
