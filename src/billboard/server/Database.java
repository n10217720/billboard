package billboard.server;

import billboard.common.BillboardMeta;
import billboard.common.ScheduledBillboard;

import java.sql.Timestamp;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * An interface which abstracts over the implementation of a database which the server utilises.
 */
public interface Database {
    /**
     * Returns a user's salted password hash and salt if the provided user exists.
     *
     * @param username The username.
     * @return The user's salted password hash and salt, or null if the user does not exist.
     */
    HashedSaltedPassword getHashedSaltedPassword(String username);

    /**
     * Sets a user's password.
     *
     * @param username The username of the user to set the password of.
     * @param newPassword The new salted hash and salt of the user's password.
     */
    void setUserPassword(String username, HashedSaltedPassword newPassword);

    /**
     * Determines if the provided user has the specified permission.
     *
     * @param username The username of the user to check.
     * @param permission The permission to check if the user holds.
     * @return True if the user has the permission, false otherwise.
     */
    boolean hasPermission(String username, Permission permission);

    /**
     * Modifies specified permissions for the specified user.
     *
     * @param username The username of the user to modify.
     * @param permissions The permissions to apply to the user.
     */
    void setUserPermissions(String username, HashSet<Permission> permissions);

    /**
     * Returns the permissions of the specified user.
     *
     * @param username The username of the user to grab the permissions from.
     * @return A hashset containing the users permissions.
     */
    HashSet<Permission> getUserPermissions(String username);

    /**
     * Checks if a user exists in the database.
     *
     * @param username The username to check for.
     * @return True if the user exists, false otherwise.
     */
    boolean doesUserExist(String username);

    /**
     * Deletes the specified user from the database/server.
     *
     * @param username The username of the user profile to delete.
     */
    void deleteUser(String username);

    /**
     * Lists all usernames in the database.
     *
     * @return A list of usernames in the database.
     */
    ArrayList<String> listUsers();

    /**
     * Create a new user profile to add to the database.
     *
     * @param username The username of the new user.
     * @param password The salted hash and salt of the new users' password.
     * @param permissions The permissions to give the new user.
     */
    void createUser(String username, HashedSaltedPassword password, HashSet<Permission> permissions);

    /**
     * Deletes the specified billboard from the database.
     *
     * @param billboardName The billboard to delete.
     */
    void deleteBillboard(String billboardName);

    /**
     * Checks the database for the provided billboard name, to see if it exists.
     *
     * @param billboardName The billboard to check for.
     * @return True if the billboard exits, false otherwise.
     */
    boolean doesBillboardExist(String billboardName);

    /**
     * Checks the database for if the billboard is currently scheduled.
     *
     * @param billboardName The username of the new user.
     * @return True if the billboard is currently scheduled, false otherwise.
     */
    boolean isBillboardScheduled(String billboardName);

    /**
     * Gets the specified billboards' creator.
     *
     * @param billboardName The username of the new user.
     * @return The billboard creators username as a String.
     */
    String getBillboardCreator(String billboardName);

    /**
     * Gets and returns all the currently existing billboards' metadata.
     *
     * @return A list of BillboardMeta objects.
     */
    List<BillboardMeta> listBillboardMetadata();

    /**
     * Gets and returns a billboard objects properties.
     *
     * @param name The name of the billboard to return.
     * @return The billboard and its properties in XML string.
     */
    String getBillboard(String name);

    /**
     * Creates a new billboard object to store in the database.
     *
     * @param billboardName The name for the new billboard.
     * @param creator The creators' username for the new billboard.
     * @param billboard The new billboard's raw XML string.
     */
    void createBillboard(String billboardName, String creator, String billboard);

    /**
     * Sets a specified billboards properties, for when it has been edited or replaced.
     *
     * @param billboardName The name of the billboard to set.
     * @param billboard The billboards raw XML string to set.
     */
    void setBillboard(String billboardName, String billboard);

    /**
     * Adds a scheduled billboard entry to the display schedule database.
     *
     * @param billboardName The name of the billboard to schedule.
     * @param startingTimeDate The starting time for the billboard to begin displaying at.
     * @param duration The duration for the billboard to display for (in minutes).
     * @param recurrencePeriod The period for which the billboard to re-occur it's displaying (can be null).
     * @param now The time in which the schedule entry was added, generated server side.
     */
    void scheduleBillboard(String billboardName, ZonedDateTime startingTimeDate, long duration, Duration recurrencePeriod, Timestamp now);

    /**
     * Removes a scheduled billboard entry from the display schedule database.
     *
     * @param billboardName The name of the scheduled billboard to remove.
     * @param timeDate The time to check to remove the billboard from showing.
     */
    void removeFromSchedule(String billboardName, ZonedDateTime timeDate);

    /**
     * Gets a list of all the currently scheduled billboard entries, effectively getting the schedule.
     *
     * @return The list of scheduled billboard objects in the db.
     */
    List<ScheduledBillboard> getSchedule();
}
