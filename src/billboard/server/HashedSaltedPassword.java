package billboard.server;

import billboard.common.HashSHA3512;

import java.util.Random;

/**
 * A class for storing and generating hashed, salted passwords, used for user passwords.
 */
public class HashedSaltedPassword {
    /** The length of a salt. */
    private static final int SALT_LENGTH = 8;
    /** Characters permitted to appear in a salt. */
    private static final char[] SALT_ALPHABET = "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray();

    private String hashedSaltedPassword;
    private String salt;

    /**
     * Constructs a new hashedSaltedPassword object, storing the hashedSaltedPassword and the salt.
     *
     * @param hashedSaltedPassword The password, hashed once with no salt.
     * @param salt The salt used with the hashedSaltedPassword.
     * @return A hashed, salted password generated from the provided hashed password.
     */
    public HashedSaltedPassword(String hashedSaltedPassword, String salt) {
        this.hashedSaltedPassword = hashedSaltedPassword;
        this.salt = salt;
    }

    /**
     * Creates a new hashed, salted password, generating a random salt.
     *
     * @param hashedPassword The password, hashed once with no salt.
     * @param random The random instance to use to generate the salt.
     * @return A hashed, salted password generated from the provided hashed password.
     */
    public static HashedSaltedPassword newFromHashedPassword(String hashedPassword, Random random) {
        StringBuilder saltBuilder = new StringBuilder();
        for (int i = 0; i < SALT_LENGTH; ++i) {
            char c = SALT_ALPHABET[random.nextInt(SALT_ALPHABET.length)];
            saltBuilder.append(c);
        }
        String salt = saltBuilder.toString();

        return new HashedSaltedPassword(
                HashSHA3512.generateHash(hashedPassword + salt),
                salt
        );
    }

    /**
     * Returns the hashedSaltedPassword that's been stored/constructed.
     *
     * @return The hashedSaltedPassword that's been stored/constructed.
     */
    public String getHashedSaltedPassword() {
        return this.hashedSaltedPassword;
    }

    /**
     * Returns the salt used with the hashedSaltedPassword.
     *
     * @return The salt used with the hashedSaltedPassword.
     */
    public String getSalt() {
        return this.salt;
    }
}
