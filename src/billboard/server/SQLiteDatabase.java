package billboard.server;

import billboard.common.BillboardMeta;
import billboard.common.HashSHA3512;
import billboard.common.ScheduledBillboard;

import java.sql.*;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

/**
 * Class which implements server.Database for an SQLite-backed database.
 * Creates tables and user accounts on first launch if they do not exist, and connects to the SQL database.
 */
public class SQLiteDatabase implements Database, AutoCloseable {
    private static final String DEFAULT_USER_USERNAME = "admin";
    private static final String DEFAULT_USER_PASSWORD = "password";

    private Connection connection;

    /**
     * Opens a connection to the specified SQLite database.
     *
     * If the database file does not exist, it will be created. If the database is empty or missing required tables, all
     * required tables will be created and a default user added.
     *
     * @param connectionString The connection string for the desired SQLite database.
     * @throws SQLException An error occurred while connecting to the database.
     */
    public SQLiteDatabase(String connectionString) throws SQLException {
        this.connection = DriverManager.getConnection(connectionString);

        performInitialisation();
    }

    /**
     * Performs basic initialisation such as creating tables if required.
     *
     * @throws SQLException An error occurred while performing initialisation.
     */
    private void performInitialisation() throws SQLException {
        Statement enableFKs = this.connection.createStatement();
        enableFKs.execute("PRAGMA foreign_keys = ON");

        Statement createUsersTable = this.connection.createStatement();
        createUsersTable.execute("CREATE TABLE IF NOT EXISTS users (" +
                "username TEXT NOT NULL PRIMARY KEY," +
                "hashedSaltedPassword TEXT NOT NULL," +
                "salt TEXT NOT NULL," +
                "hasEditUsers BOOL NOT NULL," +
                "hasCreateBillboard BOOL NOT NULL," +
                "hasEditBillboard BOOL NOT NULL," +
                "hasScheduleBillboard BOOL NOT NULL" +
                ")");

        Statement createBillboardsTable = this.connection.createStatement();
        createBillboardsTable.execute("CREATE TABLE IF NOT EXISTS billboards (" +
                "name TEXT NOT NULL PRIMARY KEY," +
                "creator TEXT NOT NULL," +
                "xml TEXT NOT NULL," +
                "FOREIGN KEY (creator) REFERENCES users(username)" +
                "ON DELETE CASCADE" +
                ")");

        Statement createScheduleTable = this.connection.createStatement();
        createScheduleTable.execute("CREATE TABLE IF NOT EXISTS schedule (" +
                "billboardName TEXT NOT NULL," +
                "startTimestamp TEXT NOT NULL," +
                "displayTime INTEGER NOT NULL," +
                "repeatRate INTEGER," +
                "createdAt TIMESTAMP NOT NULL," +
                "FOREIGN KEY (billboardName) REFERENCES billboards(name)" +
                "ON DELETE CASCADE" +
                ")");

        // Do we need to create a user? The users table will only be empty if we just created it.
        Statement isUsersTableEmpty = this.connection.createStatement();
        int numUsers = isUsersTableEmpty.executeQuery("SELECT COUNT(*) FROM users").getInt(1);

        if (numUsers == 0) {
            HashedSaltedPassword hsp = HashedSaltedPassword.newFromHashedPassword(
                    HashSHA3512.generateHash(DEFAULT_USER_PASSWORD),
                    // We're only ever going to need to use a random value once here, so it's safe enough to just create
                    // a random instance to use once
                    new Random()
            );

            createUser(
                    DEFAULT_USER_USERNAME,
                    hsp,
                    new HashSet<>(List.of(
                            Permission.EDIT_USERS,
                            Permission.CREATE_BILLBOARD,
                            Permission.EDIT_BILLBOARD,
                            Permission.SCHEDULE_BILLBOARD
                    ))
            );
        }
    }

    @Override
    public void close() throws SQLException {
        if (this.connection != null) {
            this.connection.close();
        }
    }

    @Override
    public HashedSaltedPassword getHashedSaltedPassword(String username) {
        try {
            PreparedStatement s = this.connection.prepareStatement("SELECT * FROM users WHERE username = ?");
            s.clearParameters();
            s.setString(1, username);

            ResultSet rs = s.executeQuery();
            if (!rs.next()) {
                return null;
            }

            return new HashedSaltedPassword(rs.getString("hashedSaltedPassword"), rs.getString("salt"));
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void setUserPassword(String username, HashedSaltedPassword newPassword) {
        try {
            PreparedStatement createDefaultUser = this.connection.prepareStatement(
                    "UPDATE users SET hashedSaltedPassword = ?, salt = ? WHERE username = ?"
            );
            createDefaultUser.clearParameters();
            createDefaultUser.setString(1, newPassword.getHashedSaltedPassword());
            createDefaultUser.setString(2, newPassword.getSalt());
            createDefaultUser.setString(3, username);
            createDefaultUser.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean hasPermission(String username, Permission permission) {
        return this.getUserPermissions(username).contains(permission);
    }

    @Override
    public void setUserPermissions(String username, HashSet<Permission> permissions) {
        try {
            PreparedStatement s = this.connection.prepareStatement("UPDATE users SET hasEditUsers = ?, hasCreateBillboard = ?, hasEditBillboard = ?, hasScheduleBillboard = ? WHERE username = ?");

            s.setBoolean(1, permissions.contains(Permission.EDIT_USERS));
            s.setBoolean(2, permissions.contains(Permission.CREATE_BILLBOARD));
            s.setBoolean(3, permissions.contains(Permission.EDIT_BILLBOARD));
            s.setBoolean(4, permissions.contains(Permission.SCHEDULE_BILLBOARD));

            s.setString(5, username);

            s.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public HashSet<Permission> getUserPermissions(String username) {
        try {
            PreparedStatement s = this.connection.prepareStatement("SELECT hasEditUsers, hasCreateBillboard, hasEditBillboard, hasScheduleBillboard FROM users WHERE username = ?");
            s.clearParameters();
            s.setString(1, username);

            ResultSet rs = s.executeQuery();
            if (rs == null) {
                return null;
            }

            HashSet<Permission> permissions = new HashSet<>();

            if (rs.getBoolean("hasEditUsers")) {
                permissions.add(Permission.EDIT_USERS);
            }
            if (rs.getBoolean("hasCreateBillboard")) {
                permissions.add(Permission.CREATE_BILLBOARD);
            }
            if (rs.getBoolean("hasEditBillboard")) {
                permissions.add(Permission.EDIT_BILLBOARD);
            }
            if (rs.getBoolean("hasScheduleBillboard")) {
                permissions.add(Permission.SCHEDULE_BILLBOARD);
            }

            return permissions;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean doesUserExist(String username) {
        try {
            PreparedStatement s = this.connection.prepareStatement("SELECT 1 FROM users WHERE username = ?");
            s.clearParameters();
            s.setString(1, username);

            ResultSet rs = s.executeQuery();
            return rs.next();

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void deleteUser(String username) {
        try {
            PreparedStatement s = this.connection.prepareStatement("DELETE FROM users WHERE username = ?");
            s.clearParameters();
            s.setString(1, username);
            s.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<String> listUsers() {
        try {
            PreparedStatement s = this.connection.prepareStatement("SELECT username FROM users");
            ResultSet rs = s.executeQuery();
            if(rs == null){
                return null;
            }

            ArrayList<String> users = new ArrayList<>();
            while(rs.next()) {
                users.add(rs.getString("username"));
            }

            return users;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void createUser(String username, HashedSaltedPassword password, HashSet<Permission> permissions) {
        try {
            PreparedStatement s = this.connection.prepareStatement("INSERT INTO users (username, hashedSaltedPassword," +
                    " salt, hasEditUsers, hasCreateBillboard, hasEditBillboard, hasScheduleBillboard) VALUES (?, ?, ?, ?, ?, ?, ?)");

            s.clearParameters();
            s.setString(1, username);
            s.setString(2, password.getHashedSaltedPassword());
            s.setString(3, password.getSalt());
            s.setBoolean(4, permissions.contains(Permission.EDIT_USERS));
            s.setBoolean(5, permissions.contains(Permission.CREATE_BILLBOARD));
            s.setBoolean(6, permissions.contains(Permission.EDIT_BILLBOARD));
            s.setBoolean(7, permissions.contains(Permission.SCHEDULE_BILLBOARD));

            s.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void deleteBillboard(String billboardName) {
        try {
            PreparedStatement s = this.connection.prepareStatement("DELETE FROM billboards WHERE name = ?");

            s.clearParameters();
            s.setString(1, billboardName);

            s.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean doesBillboardExist(String billboardName) {
        try {
            PreparedStatement s = this.connection.prepareStatement("SELECT 1 FROM billboards WHERE name = ?");

            s.clearParameters();
            s.setString(1, billboardName);

            ResultSet rs = s.executeQuery();
            return rs.next();

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public boolean isBillboardScheduled(String billboardName) {
        try {
            PreparedStatement s = this.connection.prepareStatement("SELECT 1 FROM schedule WHERE billboardName = ?");

            s.clearParameters();
            s.setString(1, billboardName);

            ResultSet rs = s.executeQuery();
            return rs.next();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public String getBillboardCreator(String billboardName) {
        try {
            PreparedStatement s = this.connection.prepareStatement("SELECT creator FROM billboards WHERE name = ?");

            s.clearParameters();
            s.setString(1, billboardName);

            ResultSet rs = s.executeQuery();
            if (rs == null || !rs.next()) {
                return null;
            }

            return rs.getString("creator");
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public List<BillboardMeta> listBillboardMetadata() {
        try {
            PreparedStatement s = this.connection.prepareStatement("SELECT name, creator FROM billboards");
            ResultSet rs = s.executeQuery();
            if(rs == null){
                return null;
            }

            ArrayList<BillboardMeta> metadata = new ArrayList<>();
            while (rs.next()) {
                metadata.add(new BillboardMeta(
                        rs.getString("name"),
                        rs.getString("creator"),
                        false, false
                ));
            }

            return metadata;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public String getBillboard(String name) {
        try {
            PreparedStatement s = this.connection.prepareStatement("SELECT xml FROM billboards WHERE name = ?");

            s.clearParameters();
            s.setString(1, name);

            ResultSet rs = s.executeQuery();
            if (rs == null || !rs.next()) {
                return null;
            }

            return rs.getString("xml");
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void createBillboard(String billboardName, String creator, String billboard) {
        try {
            PreparedStatement s = this.connection.prepareStatement("INSERT INTO billboards (name, creator, xml) VALUES (?, ?, ?)");

            s.clearParameters();
            s.setString(1, billboardName);
            s.setString(2, creator);
            s.setString(3, billboard);

            s.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setBillboard(String billboardName, String billboard) {
        try {
            PreparedStatement s = this.connection.prepareStatement("UPDATE billboards SET xml = ? WHERE name = ?");

            s.clearParameters();
            s.setString(1, billboard);
            s.setString(2, billboardName);

            s.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void scheduleBillboard(String billboardName, ZonedDateTime startingTimeDate, long duration, Duration recurrencePeriod, Timestamp now) {
        try {
            PreparedStatement s = this.connection.prepareStatement("INSERT INTO schedule (billboardName, startTimestamp, displayTime, repeatRate, createdAt)" +
                    "VALUES (?, ?, ?, ?, ?)");

            s.clearParameters();
            s.setString(1, billboardName);
            s.setString(2, startingTimeDate.toString());
            s.setLong(3, duration);
            if (recurrencePeriod == null) {
                s.setNull(4, Types.INTEGER);
            } else {
                s.setLong(4, recurrencePeriod.toMinutes());
            }
            s.setTimestamp(5, now);

            s.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void removeFromSchedule(String billboardName, ZonedDateTime timeDate) {
        try {
            PreparedStatement s = this.connection.prepareStatement("DELETE FROM schedule WHERE billboardName = ? AND startTimestamp = ?");

            s.clearParameters();
            s.setString(1, billboardName);
            s.setString(2, timeDate.toString());

            s.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<ScheduledBillboard> getSchedule() {
        try {
            // Put the most recently created schedule entry at the top, so it's the first to be considered for display
            // purposes.
            PreparedStatement s = this.connection.prepareStatement(
                    "SELECT billboardName, startTimestamp, displayTime, repeatRate FROM schedule ORDER BY createdAt DESC"
            );

            ResultSet rs = s.executeQuery();
            if(rs == null){
                return null;
            }

            ArrayList<ScheduledBillboard> schedule = new ArrayList<>();
            while (rs.next()) {
                // Kinda horrible: best way to check for null is to read the field, do whatever with it, then ask if it
                // was null. Null should propagate, so propagate it.
                Duration repeatRate = Duration.ofMinutes(rs.getInt("repeatRate"));
                if (rs.wasNull()) {
                    repeatRate = null;
                }

                schedule.add(new ScheduledBillboard(
                        rs.getString("billboardName"),
                        ZonedDateTime.parse(rs.getString("startTimestamp")),
                        rs.getInt("displayTime"),
                        repeatRate
                ));
            }

            return schedule;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
