package billboard.server;

/**
 * An enum that captures all the relevant permissions.
 * Used by the users of the client when trying to perform permission required actions.
 */
public enum Permission {
    EDIT_USERS, CREATE_BILLBOARD, EDIT_BILLBOARD, SCHEDULE_BILLBOARD;

    @Override
    public String toString(){
        return this.name();
    }

    /**
     * Returns a permission value from a valid string format.
     *
     * @return A permission value converted from a string.
     */
    public static Permission fromString(String perm){
        return Permission.valueOf(perm);
    }
}
