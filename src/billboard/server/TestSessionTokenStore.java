package billboard.server;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

public class TestSessionTokenStore {
    private static final long EXPIRY = 5;
    private static final TemporalUnit EXPIRY_UNIT = ChronoUnit.MINUTES;

    private MockTimeSource timeSource;
    private SessionTokenStore<String> sts;

    @BeforeEach
    public void createSessionTokenStore() {
        timeSource = new MockTimeSource(Instant.EPOCH);
        sts = new SessionTokenStore<>(EXPIRY, EXPIRY_UNIT, timeSource, new Random());
    }

    @Test
    public void createTokenUnique() {
        String token1 = sts.createToken("value1");
        String token2 = sts.createToken("value2");

        assertNotEquals(token1, token2);
    }

    @Test
    public void getTokenValue() {
        String token1 = sts.createToken("value1");
        String token2 = sts.createToken("value2");

        assertEquals("value1", sts.getTokenValue(token1));
        assertEquals("value2", sts.getTokenValue(token2));
    }

    @Test
    public void getTokenValueMissing() {
        assertNull(sts.getTokenValue("fake, non-existent token string"));
    }

    @Test
    public void getTokenValueAfterExpiry() {
        String token = sts.createToken("value");

        timeSource.tick(EXPIRY - 1, EXPIRY_UNIT);
        assertEquals("value", sts.getTokenValue(token));

        timeSource.tick(1, EXPIRY_UNIT);
        assertNull(sts.getTokenValue(token));
    }

    @Test
    public void updateLastUse() {
        String token = sts.createToken("value");

        // Wait until just before the token should expire, and mark it as having been used
        timeSource.tick(EXPIRY - 1, EXPIRY_UNIT);
        sts.updateLastUsed(token);

        // Wait until the time the token would have expired had it not been updated, and ensure it is still valid
        timeSource.tick(1, EXPIRY_UNIT);
        assertEquals("value", sts.getTokenValue(token));

        // But the token should still expire, just based on the new last used time
        timeSource.tick(EXPIRY, EXPIRY_UNIT);
        assertNull(sts.getTokenValue(token));
    }

    @Test
    public void updateLastUseExpired() {
        String token = sts.createToken("value");

        // Let the token expire, then mark it as having been used
        timeSource.tick(EXPIRY, EXPIRY_UNIT);
        sts.updateLastUsed(token);

        // If the update went through, the token should be valid after less than the expiry period, but the update
        // shouldn't have gone through so make sure it *isn't* valid
        timeSource.tick(EXPIRY - 1, EXPIRY_UNIT);
        assertNull(sts.getTokenValue(token));
    }

    @Test
    public void invalidateToken() {
        String token = sts.createToken("value");

        sts.invalidateToken(token);

        assertNull(sts.getTokenValue(token));
    }
}

class MockTimeSource implements TimeSource {
    private Instant currentTime;

    /**
     * Creates a mock time source which can be manually incremented.
     *
     * @param epoch The initial current time.
     */
    public MockTimeSource(Instant epoch) {
        this.currentTime = epoch;
    }

    @Override
    public Instant now() {
        return currentTime;
    }

    /**
     * Increments the current time.
     *
     * @param amount The amount of time to increment by.
     * @param unit The unit of {@code amount}.
     */
    public void tick(long amount, TemporalUnit unit) {
        currentTime = currentTime.plus(amount, unit);
    }
}
