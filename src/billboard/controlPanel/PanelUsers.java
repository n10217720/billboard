package billboard.controlPanel;

import billboard.common.*;
import billboard.common.api.methods.*;
import billboard.server.Permission;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.HashSet;

/**
 * Generates the admin panel for the ControlPanel class.
 */
public class PanelUsers extends JPanel {
    private static Insets BUTTON_MARGINS = new Insets(10, 10, 10, 10);
    private static Insets PERMISSIONS_MARGINS = new Insets(0, 10, 10, 10);

    private ClientConnection connection;
    private LoginParams loginParams;

    private JButton deleteUser;

    private JCheckBox createBoards;
    private  JCheckBox editAllBoards;
    private JCheckBox scheduleBoards;
    private JCheckBox editUsers;

    private JSplitPane tabSplit;
    private JPanel listPanel;
    private JPanel infoPanel;
    private JPanel permissionsPanel;
    private DefaultListModel<String> userListModel;
    private JList<String> userList;

    /**
     * Creates a panel containing GUI elements for the admin tab.
     *
     * @param connection The connection to the billboard server.
     * @param loginParams Parameters for the currently logged in user.
     */
    public PanelUsers(ClientConnection connection, LoginParams loginParams) {
        this.connection = connection;
        this.loginParams = loginParams;

        createListPanel();
        createInfoPanel();
        this.tabSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, listPanel, infoPanel);

        this.setLayout(new BorderLayout());
        this.add(tabSplit, BorderLayout.CENTER);

        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                PanelUsers.this.updateFromServer();
            }
        });
    }

    /**
     * Updates the displayed user list from the server.
     */
    private void updateFromServer() {
        ListUsersResponse response = this.connection.makeRequestBackground(new ListUsersRequest(
                this.loginParams.getSessionToken()
        ));
        if (response != null) {
            this.userListModel.clear();
            this.userListModel.addAll(response.getUsernames());

            // We're guaranteed to always have one user, so select the first one in the list so that the info panel is
            // populated with useful data (this triggers the selection changed handler)
            this.userList.setSelectedIndex(0);
        }
    }

    /**
     * Updates the info panel based on the provided user's information from the server.
     *
     * @param username The selected user's username.
     */
    private void updateInfoPanel(String username) {
        boolean isOwnAccount = username.equals(this.loginParams.getUsername());

        boolean canModifyPermissions = this.loginParams.hasPermission(Permission.EDIT_USERS);
        permissionsPanel.setVisible(canModifyPermissions);
        if (canModifyPermissions) {
            GetUserPermissionsResponse response = this.connection.makeRequestBackground(
                    new GetUserPermissionsRequest(this.loginParams.getSessionToken(), username)
            );

            if (response != null) {
                HashSet<Permission> permissions = response.getPermissionList();
                createBoards.setSelected(permissions.contains(Permission.CREATE_BILLBOARD));
                editAllBoards.setSelected(permissions.contains(Permission.EDIT_BILLBOARD));
                scheduleBoards.setSelected(permissions.contains(Permission.SCHEDULE_BILLBOARD));
                editUsers.setSelected(permissions.contains(Permission.EDIT_USERS));
            }

            // Only allow editing edit users on other accounts
            editUsers.setEnabled(!isOwnAccount);
        }

        boolean canDelete = this.loginParams.hasPermission(Permission.EDIT_USERS) && !isOwnAccount;
        deleteUser.setVisible(canDelete);
    }

    /**
     * Generates panel containing user information (change pass, delete, permissions).
     */
    private void createInfoPanel() {
        this.infoPanel = new JPanel();
        this.infoPanel.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));

        JButton changePass = new JButton("Change Password");
        changePass.addActionListener(e -> {
            ChangePassDialog changePassDialog = new ChangePassDialog(null);
            changePassDialog.setVisible(true);

            String username = userList.getSelectedValue();

            if (changePassDialog.getDialogResult() == DialogResult.OK) {
                connection.makeRequestBackground(new SetUserPasswordRequest(
                        this.loginParams.getSessionToken(),
                        username,
                        HashSHA3512.generateHash(changePassDialog.getNewPassword())
                ));
            }
        });
        buttonPanel.add(changePass);

        this.deleteUser = new JButton("Delete User");
        this.deleteUser.addActionListener(e -> {
            String username = userList.getSelectedValue();

            connection.makeRequestBackground(new DeleteUserRequest(this.loginParams.getSessionToken(), username));

            updateFromServer();
        });
        buttonPanel.add(this.deleteUser);

        permissionsPanel = new JPanel();
        TitledBorder permissionsBorder;
        permissionsBorder = BorderFactory.createTitledBorder("Permissions");
        permissionsPanel.setBorder(permissionsBorder);
        permissionsPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        permissionsPanel.add(checkPanel());

        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.insets = BUTTON_MARGINS;
        this.infoPanel.add(buttonPanel, constraints);

        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;
        constraints.gridy = 1;
        constraints.insets = PERMISSIONS_MARGINS;
        this.infoPanel.add(permissionsPanel, constraints);
    }

    /**
     * Generates the checkbox panel to be added to the user info.
     * @return Panel containing checkboxes.
     */
    private JPanel checkPanel() {
        JPanel newPanel = new JPanel();
        this.createBoards = new JCheckBox("Create Billboards");
        this.editAllBoards = new JCheckBox("Edit All Billboards");
        this.scheduleBoards = new JCheckBox("Schedule Billboards");
        this.editUsers = new JCheckBox("Edit Users");

        ActionListener applyNewPermissions = e -> {
            String username = userList.getSelectedValue();
            HashSet<Permission> permissions = new HashSet<>();

            if (this.createBoards.isSelected()) {
                permissions.add(Permission.CREATE_BILLBOARD);
            }
            if (this.editAllBoards.isSelected()) {
                permissions.add(Permission.EDIT_BILLBOARD);
            }
            if (this.scheduleBoards.isSelected()) {
                permissions.add(Permission.SCHEDULE_BILLBOARD);
            }
            if (this.editUsers.isSelected()) {
                permissions.add(Permission.EDIT_USERS);
            }

            this.connection.makeRequestBackground(new SetUserPermissionsRequest(
                    this.loginParams.getSessionToken(),
                    username,
                    permissions
            ));
        };

        this.createBoards.addActionListener(applyNewPermissions);
        this.editAllBoards.addActionListener(applyNewPermissions);
        this.scheduleBoards.addActionListener(applyNewPermissions);
        this.editUsers.addActionListener(applyNewPermissions);

        newPanel.setLayout(new BoxLayout(newPanel, BoxLayout.Y_AXIS));
        newPanel.add(createBoards);
        newPanel.add(editAllBoards);
        newPanel.add(scheduleBoards);
        newPanel.add(editUsers);
        return newPanel;
    }

    /**
     * Generates the list of billboards, and new billboard button.
     */
    private void createListPanel() {
        this.listPanel = new JPanel();
        this.listPanel.setLayout(new GridBagLayout());

        //Create JList to be displayed.
        this.userListModel = new DefaultListModel<>();
        this.userList = new JList<>(userListModel);
        userList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        userList.setLayoutOrientation(JList.VERTICAL);
        JScrollPane scrollList = new JScrollPane(userList);

        userList.addListSelectionListener(e -> {
            String username = userList.getSelectedValue();

            // Selected item is null if no item is selected
            if (username != null) {
                PanelUsers.this.updateInfoPanel(username);
            }
        });

        //Constraints for scrolling list.
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weighty = 1.0;
        constraints.weightx = 1.0;
        this.listPanel.add(scrollList, constraints);

        if (this.loginParams.hasPermission(Permission.EDIT_USERS)) {
            JButton buttonNew = new JButton("New User");
            buttonNew.addActionListener(e -> {
                CreateUserDialog createUserDialog = new CreateUserDialog(null);
                createUserDialog.setVisible(true);

                if (createUserDialog.getDialogResult() == DialogResult.OK) {
                    connection.makeRequestBackground(new CreateUserRequest(
                            loginParams.getSessionToken(),
                            createUserDialog.getUsername(),
                            HashSHA3512.generateHash(createUserDialog.getPassword()),
                            createUserDialog.getPermissions()
                    ));

                    updateFromServer();
                }
            });

            //Constraints for add button.
            constraints.fill = GridBagConstraints.HORIZONTAL;
            constraints.weighty = 0;
            constraints.weightx = 1.0;
            constraints.insets = BUTTON_MARGINS;
            constraints.gridy = 1;
            this.listPanel.add(buttonNew, constraints);
        }
    }
}
