package billboard.controlPanel;

import billboard.common.Billboard;
import billboard.common.BillboardMeta;
import billboard.common.BillboardRenderer;
import billboard.common.ClientConnection;
import billboard.common.api.methods.*;
import billboard.server.Permission;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

/**
 * Generates the GUI panel for the billboard tab of the ControlPanel class.
 */
public class PanelBillboard extends JPanel {
    private ClientConnection connection;
    private LoginParams loginParams;

    private JSplitPane tabSplit;
    private JPanel listPanel;
    private JList<BillboardMeta> billboardList;
    private JPanel buttonsPanel;
    private JButton buttonEdit;
    private JButton buttonDelete;
    private BillboardRenderer billboardRenderer;
    private DefaultListModel<BillboardMeta> billboardListModel;

    private JFrame owner;

    /**
     * Creates a panel containing the GUI elements for the billboard tab.
     *
     * @param o The JFrame the panel will be attached to.
     * @param connection The connection to the billboard server.
     * @param loginParams Parameters for the currently logged in user.
     */
    public PanelBillboard(JFrame o, ClientConnection connection, LoginParams loginParams) {
        this.connection = connection;
        this.loginParams = loginParams;

        owner = o;

        createListPanel();

        // Display an empty billboard to start, in case there are none
        this.billboardRenderer = new BillboardRenderer();
        this.billboardRenderer.setBillboard(null);

        tabSplit = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, listPanel, billboardRenderer);

        this.setLayout(new BorderLayout());
        this.add(tabSplit, BorderLayout.CENTER);

        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                PanelBillboard.this.updateFromServer();
            }
        });
    }

    /**
     * Updates the displayed billboard list from the server.
     */
    private void updateFromServer() {
        ListBillboardsResponse response = this.connection.makeRequestBackground(new ListBillboardsRequest(
                this.loginParams.getSessionToken()
        ));
        if (response != null) {
            this.billboardListModel.clear();
            this.billboardListModel.addAll(response.getBillboards());

            if (response.getBillboards().size() != 0) {
                this.billboardList.setSelectedIndex(0);
            } else {
                // Clear the display if we aren't left with any billboards
                this.billboardRenderer.setBillboard(null);

                // Can't edit or delete something that doesn't exist
                buttonEdit.setVisible(false);
                buttonDelete.setVisible(false);
            }
        }
    }

    /**
     * Generates the list of billboards, and new billboard button.
     */
    private void createListPanel() {
        GridBagConstraints constraints = new GridBagConstraints();

        this.listPanel = new JPanel();
        this.listPanel.setLayout(new GridBagLayout());

        this.buttonsPanel = new JPanel();
        this.buttonsPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

        if (this.loginParams.hasPermission(Permission.CREATE_BILLBOARD)) {
            JButton buttonNew = new JButton("New");
            buttonNew.addActionListener(e -> {
                EditBillboardDialog editDialog = new EditBillboardDialog(owner, new Billboard(), null);
                editDialog.setVisible(true);

                if (editDialog.getDialogResult() == DialogResult.OK) {
                    connection.makeRequestBackground(new CreateOrEditBillboardRequest(
                            loginParams.getSessionToken(), editDialog.getName(), editDialog.getBillboard()
                    ));

                    updateFromServer();
                }
            });

            this.buttonsPanel.add(buttonNew);
        }

        this.buttonEdit = new JButton("Edit");
        this.buttonEdit.addActionListener(e -> {
            BillboardMeta selectedBillboard = billboardList.getSelectedValue();
            GetBillboardResponse response = connection.makeRequestBackground(new GetBillboardRequest(
                    loginParams.getSessionToken(), selectedBillboard.getName()
            ));

            if (response != null) {
                EditBillboardDialog editDialog = new EditBillboardDialog(owner, response.getBillboard(), selectedBillboard.getName());
                editDialog.setVisible(true);

                if (editDialog.getDialogResult() == DialogResult.OK) {
                    connection.makeRequestBackground(new CreateOrEditBillboardRequest(
                            loginParams.getSessionToken(), editDialog.getName(), editDialog.getBillboard()
                    ));

                    updateFromServer();
                }
            }
        });
        this.buttonsPanel.add(this.buttonEdit);

        this.buttonDelete = new JButton("Delete");
        this.buttonDelete.addActionListener(e -> {
            BillboardMeta selectedBillboard = billboardList.getSelectedValue();
            connection.makeRequestBackground(new DeleteBillboardRequest(
                    loginParams.getSessionToken(), selectedBillboard.getName()
            ));

            updateFromServer();
        });
        this.buttonsPanel.add(this.buttonDelete);

        //Constraints for add button.
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weighty = 0;
        constraints.weightx = 1;
        addToPanel(this.listPanel, this.buttonsPanel, constraints, 0, 1, new Insets(10, 10, 10, 10));

        //Create JList to be displayed.
        this.billboardListModel = new DefaultListModel<>();
        billboardList = new JList<>(billboardListModel);
        billboardList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        billboardList.setLayoutOrientation(JList.VERTICAL);

        billboardList.addListSelectionListener(e -> {
            buttonEdit.setVisible(false);

            BillboardMeta selectedBillboard = billboardList.getSelectedValue();

            // Selection is null if nothing is selected
            if (selectedBillboard != null) {
                GetBillboardResponse response = this.connection.makeRequestBackground(new GetBillboardRequest(
                        this.loginParams.getSessionToken(), selectedBillboard.getName()
                ));

                if (response != null) {
                    this.billboardRenderer.setBillboard(response.getBillboard());

                    buttonEdit.setVisible(selectedBillboard.canEdit());
                    buttonDelete.setVisible(selectedBillboard.canDelete());
                }
            }
        });

        JScrollPane scrollList = new JScrollPane(billboardList);

        //Constraints for scrolling list.
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weighty = 1;
        constraints.weightx = 1;
        addToPanel(this.listPanel, scrollList, constraints, 0, 0, new Insets(0, 0, 0, 0));
    }

    /**
     * Function to add a component to a JPanel with some inset padding.
     * @param jp JPanel to be added to.
     * @param c Component to be added.
     * @param constraints GridBag constraints.
     * @param x GridBag column.
     * @param y GridBag row.
     * @param inset Inset of component.
     */
    private void addToPanel (JPanel jp, Component c, GridBagConstraints constraints, int x, int y, Insets inset) {
        constraints.insets = inset;
        constraints.gridx = x;
        constraints.gridy = y;
        jp.add(c, constraints);
    }
}
