package billboard.controlPanel;

import billboard.server.Permission;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.util.HashSet;

/**
 * Modal dialog which lets user create a new user in the server.
 */
public class CreateUserDialog extends DialogWithResult {
    public static final int WIDTH = 300;
    public static final int HEIGHT = 300;

    private static Insets FULL_MARGINS = new Insets(5, 5, 5, 5);
    private static Insets FIELD_MARGINS = new Insets(5, 5, 0, 0);
    private static Insets LABEL_MARGINS = new Insets(5, 0, 0, 0);

    private JTextField userField;
    private JPasswordField passField;

    private JCheckBox createCheck;
    private JCheckBox editBoardsCheck;
    private JCheckBox scheduleCheck;
    private JCheckBox editUsersCheck;

    /**
     * Creates the dialog for a user to create a new user.
     *
     * @param owner The dialog's owner.
     */
    public CreateUserDialog(Frame owner) {
        super(owner, true);

        this.setMinimumSize(new Dimension(WIDTH, HEIGHT));
        this.setTitle("Create User");

        this.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.BOTH;
        constraints.insets = FULL_MARGINS;
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;
        this.add(detailsPanel(), constraints);
        constraints.gridy = 1;
        this.add(permissionsPanel(), constraints);
        constraints.weighty = 0;
        constraints.gridy = 2;
        this.add(confirmPanel(), constraints);
    }

    /**
     * Creates a panel containing the username and password fields for new user.
     * @return The panel containing username/password fields.
     */
    private JPanel detailsPanel() {
        JPanel detailsPanel = new JPanel();
        detailsPanel.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;

        JLabel userLabel = new JLabel("Username:", SwingConstants.RIGHT);
        JLabel passLabel = new JLabel("Password:", SwingConstants.RIGHT);
        userField = new JTextField();
        passField = new JPasswordField();

        addToPanel(detailsPanel, userLabel, constraints, 0, 0, constraints.insets = LABEL_MARGINS);
        addToPanel(detailsPanel, passLabel, constraints, 0, 1, constraints.insets = LABEL_MARGINS);

        constraints.weightx = 1;
        addToPanel(detailsPanel, userField, constraints, 1, 0, constraints.insets = FIELD_MARGINS);
        addToPanel(detailsPanel, passField, constraints, 1, 1, constraints.insets = FIELD_MARGINS);

        return detailsPanel;
    }

    /**
     * Creates a panel containing permission check boxes for new user.
     * @return The panel containing permission check boxes.
     */
    private JPanel permissionsPanel() {
        JPanel permissionsPanel = new JPanel();
        permissionsPanel.setLayout(new BoxLayout(permissionsPanel, BoxLayout.Y_AXIS));

        createCheck = new JCheckBox("Create Billboards");
        editBoardsCheck = new JCheckBox("Edit All Billboards");
        scheduleCheck = new JCheckBox("Schedule Billboards");
        editUsersCheck = new JCheckBox("Edit Users");

        permissionsPanel.add(createCheck);
        permissionsPanel.add(editBoardsCheck);
        permissionsPanel.add(scheduleCheck);
        permissionsPanel.add(editUsersCheck);

        TitledBorder permissionsBorder;
        permissionsBorder = BorderFactory.createTitledBorder("Permissions");
        permissionsPanel.setBorder(permissionsBorder);

        return permissionsPanel;
    }

    /**
     * Creates panel with cancel/confirm buttons for dialog.
     * @return The panel with cancel/confirm buttons.
     */
    private JPanel confirmPanel() {
        JPanel confirmPanel = new JPanel();
        confirmPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        JButton confirmButton = new JButton("Confirm");
        confirmButton.addActionListener(e -> {
            if (this.getUsername().isEmpty()) {
                JOptionPane.showMessageDialog(this, "Cannot finish: username is empty", "Create user", JOptionPane.ERROR_MESSAGE);
                return;
            }

            if (this.getPassword().isEmpty()) {
                JOptionPane.showMessageDialog(this, "Cannot finish: password is empty", "Create user", JOptionPane.ERROR_MESSAGE);
                return;
            }

            closeWithResult(DialogResult.OK);
        });
        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(e -> {
            closeWithResult(DialogResult.CANCEL);
        });
        confirmPanel.add(confirmButton);
        confirmPanel.add(cancelButton);

        return confirmPanel;
    }

    /**
     * Function to add a component to a JPanel with some specifiable GridBagConstraint values.
     * @param jp JPanel to be added to.
     * @param c Component to be added.
     * @param constraints GridBag constraints.
     * @param x GridBag column.
     * @param y GridBag row.
     * @param inset Inset of component.
     */
    private void addToPanel (JPanel jp, Component c, GridBagConstraints constraints, int x, int y, Insets inset) {
        constraints.insets = inset;
        constraints.gridx = x;
        constraints.gridy = y;
        jp.add(c, constraints);
    }

    /**
     * Returns the username of the new user.
     *
     * @return The username of the new user.
     */
    public String getUsername() {
        return this.userField.getText();
    }

    /**
     * Returns the password of the new user.
     *
     * @return The password of the new user.
     */
    public String getPassword() {
        return new String(this.passField.getPassword());
    }

    /**
     * Returns the permissions of the new user.
     *
     * @return The permissions of the new user.
     */
    public HashSet<Permission> getPermissions() {
        HashSet<Permission> permissions = new HashSet<>();

        if (this.createCheck.isSelected()) {
            permissions.add(Permission.CREATE_BILLBOARD);
        }
        if (this.editBoardsCheck.isSelected()) {
            permissions.add(Permission.EDIT_BILLBOARD);
        }
        if (this.scheduleCheck.isSelected()) {
            permissions.add(Permission.SCHEDULE_BILLBOARD);
        }
        if (this.editUsersCheck.isSelected()) {
            permissions.add(Permission.EDIT_USERS);
        }


        return permissions;
    }
}
