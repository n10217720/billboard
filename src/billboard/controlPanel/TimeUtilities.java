package billboard.controlPanel;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.ZonedDateTime;

/**
 * Utility functions for acquiring time information relevant to scheduling.
 */
public class TimeUtilities {
    /**
     * Gets the Monday of the week a given date is in.
     * @param currentDate the date to find the week of.
     * @return the date of the start of the week (the Monday).
     */
    public static ZonedDateTime getWeekOf(ZonedDateTime currentDate) {
        DayOfWeek currentDay = currentDate.getDayOfWeek();
        ZonedDateTime currentWeek = currentDate;
        switch (currentDay) {
            case TUESDAY:
                currentWeek = currentDate.minusDays(1);
                break;
            case WEDNESDAY:
                currentWeek = currentDate.minusDays(2);
                break;
            case THURSDAY:
                currentWeek = currentDate.minusDays(3);
                break;
            case FRIDAY:
                currentWeek = currentDate.minusDays(4);
                break;
            case SATURDAY:
                currentWeek = currentDate.minusDays(5);
                break;
            case SUNDAY:
                currentWeek = currentDate.minusDays(6);
                break;
        }
        return currentWeek;
    }

    /**
     * Returns the date of the Monday of the week after a given date.
     * @param currentDate the date that the following week will be found for.
     * @return the date of the Monday of the following week.
     */
    public static ZonedDateTime nextWeek(ZonedDateTime currentDate) {
        ZonedDateTime currentWeek = getWeekOf(currentDate);
        currentWeek = currentWeek.plusDays(7);
        return currentWeek;
    }

    /**
     * Returns the date of the Monday of the week before a given date.
     * @param currentDate the date that the previous week will be found for.
     * @return the date of the Monday of the previous week.
     */
    public static ZonedDateTime prevWeek(ZonedDateTime currentDate) {
        ZonedDateTime currentWeek = getWeekOf(currentDate);
        currentWeek = currentWeek.minusDays(7);
        return currentWeek;
    }

    /**
     * Formats a duration as a human readable string in the format "10m", "5h", "2d", etc.
     *
     * Durations are displayed in the unit of their most granular non-zero field, i.e. 1 minute and 50 seconds is
     * formatted as "110s". Does not support zero durations.
     *
     * @param duration The duration to format.
     * @return The duration as a human readable string.
     */
    public static String durationAsHumanReadableString(Duration duration) {
        long days = duration.toDaysPart();
        String daysPart = String.format("%dd", days);

        long hours = duration.toHoursPart();
        String hoursPart = String.format("%dh", hours);

        long minutes = duration.toMinutesPart();
        String minutesPart = String.format("%dm", minutes);

        StringBuilder result = new StringBuilder();

        if (days != 0) result.append(daysPart);
        if (hours != 0) result.append(hoursPart);
        if (minutes != 0) result.append(minutesPart);

        return result.toString();
    }
}
