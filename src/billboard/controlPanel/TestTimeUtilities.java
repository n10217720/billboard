package billboard.controlPanel;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class TestTimeUtilities {
    @Test
    public void getWeek() {
        ZonedDateTime weekOf = TimeUtilities.getWeekOf(ZonedDateTime.of(2020, 1, 14, 1, 1, 1, 1, ZoneId.systemDefault()));
        Assertions.assertEquals(ZonedDateTime.of(2020, 1, 13, 1, 1, 1, 1, ZoneId.systemDefault()), weekOf);
    }

    @Test
    public void getWeek2() {
        ZonedDateTime weekOf = TimeUtilities.getWeekOf(ZonedDateTime.of(2020, 1, 1, 1, 1, 1, 1, ZoneId.systemDefault()));
        Assertions.assertEquals(ZonedDateTime.of(2019, 12, 30, 1, 1, 1, 1, ZoneId.systemDefault()), weekOf);
    }

    @Test
    public void nextWeek() {
        ZonedDateTime weekOf = TimeUtilities.getWeekOf(ZonedDateTime.of(2020, 5, 14, 1, 1, 1, 1, ZoneId.systemDefault()));
        ZonedDateTime nextWeekOf = TimeUtilities.nextWeek(weekOf);
        Assertions.assertEquals(ZonedDateTime.of(2020, 5, 18, 1, 1, 1, 1, ZoneId.systemDefault()), nextWeekOf);
    }

    @Test
    public void nextWeek2() {
        ZonedDateTime weekOf = TimeUtilities.getWeekOf(ZonedDateTime.of(2019, 12, 30, 1, 1, 1, 1, ZoneId.systemDefault()));
        ZonedDateTime nextWeekOf = TimeUtilities.nextWeek(weekOf);
        Assertions.assertEquals(ZonedDateTime.of(2020, 1, 6, 1, 1, 1, 1, ZoneId.systemDefault()), nextWeekOf);
    }

    @Test
    public void prevWeek() {
        ZonedDateTime weekOf = TimeUtilities.getWeekOf(ZonedDateTime.of(2020, 5, 14, 1, 1, 1, 1, ZoneId.systemDefault()));
        ZonedDateTime nextWeekOf = TimeUtilities.prevWeek(weekOf);
        Assertions.assertEquals(ZonedDateTime.of(2020, 5, 4, 1, 1, 1, 1, ZoneId.systemDefault()), nextWeekOf);
    }

    @Test
    public void prevWeek2() {
        ZonedDateTime weekOf = TimeUtilities.getWeekOf(ZonedDateTime.of(2020, 1, 6, 1, 1, 1, 1, ZoneId.systemDefault()));
        ZonedDateTime nextWeekOf = TimeUtilities.prevWeek(weekOf);
        Assertions.assertEquals(ZonedDateTime.of(2019, 12, 30, 1, 1, 1, 1, ZoneId.systemDefault()), nextWeekOf);
    }

    @Test
    public void durationAsHumanReadableStringBasicMinutes() {
        Assertions.assertEquals("1m", TimeUtilities.durationAsHumanReadableString(Duration.ofMinutes(1)));
        Assertions.assertEquals("10m", TimeUtilities.durationAsHumanReadableString(Duration.ofMinutes(10)));
        Assertions.assertEquals("25m", TimeUtilities.durationAsHumanReadableString(Duration.ofMinutes(25)));
        Assertions.assertEquals("59m", TimeUtilities.durationAsHumanReadableString(Duration.ofMinutes(59)));
    }

    @Test
    public void durationAsHumanReadableStringBasicHours() {
        Assertions.assertEquals("1h", TimeUtilities.durationAsHumanReadableString(Duration.ofHours(1)));
        Assertions.assertEquals("10h", TimeUtilities.durationAsHumanReadableString(Duration.ofHours(10)));
        Assertions.assertEquals("23h", TimeUtilities.durationAsHumanReadableString(Duration.ofHours(23)));
    }

    @Test
    public void durationAsHumanReadableStringBasicDays() {
        Assertions.assertEquals("1d", TimeUtilities.durationAsHumanReadableString(Duration.ofDays(1)));
        Assertions.assertEquals("10d", TimeUtilities.durationAsHumanReadableString(Duration.ofDays(10)));
        Assertions.assertEquals("50d", TimeUtilities.durationAsHumanReadableString(Duration.ofDays(50)));
    }

    @Test
    public void durationAsHumanReadableStringBasicHoursAndMinutes() {
        Assertions.assertEquals("1h10m", TimeUtilities.durationAsHumanReadableString(Duration.ofMinutes(1 * 60 + 10)));
        Assertions.assertEquals("5h59m", TimeUtilities.durationAsHumanReadableString(Duration.ofMinutes(5 * 60 + 59)));
        Assertions.assertEquals("23h59m", TimeUtilities.durationAsHumanReadableString(Duration.ofMinutes(23 * 60 + 59)));
    }

    @Test
    public void durationAsHumanReadableStringBasicDaysAndHours() {
        Assertions.assertEquals("1d10h", TimeUtilities.durationAsHumanReadableString(Duration.ofHours(1 * 24 + 10)));
        Assertions.assertEquals("5d15h", TimeUtilities.durationAsHumanReadableString(Duration.ofHours(5 * 24 + 15)));
        Assertions.assertEquals("50d23h", TimeUtilities.durationAsHumanReadableString(Duration.ofHours(50 * 24 + 23)));
    }

    @Test
    public void durationAsHumanReadableStringBasicDaysHoursAndMinutes() {
        Assertions.assertEquals("1d10h5m", TimeUtilities.durationAsHumanReadableString(Duration.ofMinutes(1 * 24 * 60 + 10 * 60 + 5)));
        Assertions.assertEquals("50d23h59m", TimeUtilities.durationAsHumanReadableString(Duration.ofMinutes(50 * 24 * 60 + 23 * 60 + 59)));
    }

    @Test
    public void durationAsHumanReadableStringBasicDaysAndMinutes() {
        Assertions.assertEquals("1d5m", TimeUtilities.durationAsHumanReadableString(Duration.ofMinutes(1 * 24 * 60 + 5)));
        Assertions.assertEquals("50d59m", TimeUtilities.durationAsHumanReadableString(Duration.ofMinutes(50 * 24 * 60 + 59)));
    }
}
