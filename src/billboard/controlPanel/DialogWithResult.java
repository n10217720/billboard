package billboard.controlPanel;

import javax.swing.*;
import java.awt.*;

/**
 * A dialog which returns a result.
 */
public abstract class DialogWithResult extends JDialog {
    private DialogResult dialogResult;

    /**
     * Creates a dialog with result with application modality.
     */
    public DialogWithResult() {
        // Casting to null ensures the JDialog constructor which takes a Dialog is called, which makes the dialog appear
        // on the taskbar.
        super((Dialog) null, true);

        this.dialogResult = DialogResult.CANCEL;

        this.setModalityType(ModalityType.APPLICATION_MODAL);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    /**
     * Creates a dialog with result with a parent.
     *
     * @param owner The dialog's owner.
     * @param modal True if the dialog should be modal, false otherwise.
     */
    public DialogWithResult(Frame owner, boolean modal) {
        super(owner, modal);

        this.dialogResult = DialogResult.CANCEL;

        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }

    /**
     * Closes the dialog, and reports the provided dialog result.
     *
     * @param dialogResult The result to report.
     */
    protected void closeWithResult(DialogResult dialogResult) {
        this.dialogResult = dialogResult;
        this.dispose();
    }

    /**
     * Returns the result of the dialog.
     *
     * If the dialog has not been opened, the result is {@code CANCEL} by default.
     *
     * @return The dialog's result.
     */
    public DialogResult getDialogResult() {
        return this.dialogResult;
    }
}
