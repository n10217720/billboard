package billboard.controlPanel;

/**
 * Possible results that a dialog can have.
 */
public enum DialogResult {
    OK,
    CANCEL
}
