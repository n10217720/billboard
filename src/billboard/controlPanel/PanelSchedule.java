package billboard.controlPanel;

import billboard.common.*;
import billboard.common.api.methods.*;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Creates the schedule tab GUI in the ControlPanel class.
 */
public class PanelSchedule extends JPanel {
    private static final int RENDER_WIDTH = 0;
    private static final int RENDER_HEIGHT = 100;
    private static final double BUTTON_WEIGHT = 0.08;
    private static final int NUM_DAYS = 7;
    private static final String[] DAY_NAMES = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"};

    private ClientConnection connection;
    private LoginParams loginParams;

    private ZonedDateTime currentMonday;
    private List<ScheduledBillboard> currentSchedule;
    private List<String> currentCreators;

    private JSplitPane tabSplit;
    private JPanel displayPanel;
    private BillboardRenderer billboardRenderer;
    private JPanel schedulePanel;
    private JLabel week;
    private List<JLabel> dayHeadings;
    private List<JList<ScheduleEntry>> dayListLists;
    private List<DefaultListModel<ScheduleEntry>> dayLists;

    /**
     * Creates a panel containing the GUI elements of the Schedule tab.
     *
     * @param connection The connection to the billboard server.
     * @param loginParams Parameters for the currently logged in user.
     */
    public PanelSchedule(ClientConnection connection, LoginParams loginParams) {
        this.connection = connection;
        this.loginParams = loginParams;

        ZonedDateTime monday = TimeUtilities.getWeekOf(ZonedDateTime.now());
        this.currentMonday = ZonedDateTime.of(monday.getYear(), monday.getMonthValue(), monday.getDayOfMonth(), 0, 0, 0, 0, monday.getZone());

        dayLists = new ArrayList<>(NUM_DAYS);
        for (int i = 0; i < NUM_DAYS; ++i) {
            this.dayLists.add(new DefaultListModel<>());
        }

        this.dayHeadings = new ArrayList<>(NUM_DAYS);
        this.dayListLists = new ArrayList<>(NUM_DAYS);

        createDisplayPanel();
        createSchedulePanel();

        tabSplit = new JSplitPane(JSplitPane.VERTICAL_SPLIT, displayPanel, schedulePanel);
        this.setLayout(new BorderLayout());
        this.add(tabSplit, BorderLayout.CENTER);

        this.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentShown(ComponentEvent e) {
                PanelSchedule.this.updateFromServer();
            }
        });
    }

    /**
     * Updates the displayed billboard schedule from the server.
     */
    private void updateFromServer() {
        ViewScheduleResponse response = this.connection.makeRequestBackground(new ViewScheduleRequest(
                this.loginParams.getSessionToken()
        ));
        if (response != null) {
            this.currentSchedule = response.getScheduledBillboards();
            this.currentCreators = response.getBillboardCreators();

            updateFromSchedule(this.currentSchedule, this.currentCreators);
        }
    }

    /**
     * Updates the schedule from the list of scheduled billboards and their creators.
     *
     * @param schedule The schedule.
     * @param creators The creators of the scheduled billboards.
     */
    private void updateFromSchedule(List<ScheduledBillboard> schedule, List<String> creators) {
        week.setText(String.format(
                "Week of %d/%d/%d",
                this.currentMonday.getDayOfMonth(),
                this.currentMonday.getMonthValue(),
                this.currentMonday.getYear()
        ));

        List<List<ScheduleEntry>> newDayLists = new ArrayList<>(NUM_DAYS);
        for (int i = 0; i < NUM_DAYS; ++i) {
            dayHeadings.get(i).setText(String.format(
                    "%s (%d)",
                    DAY_NAMES[i],
                    this.currentMonday.plus(i, ChronoUnit.DAYS).getDayOfMonth()
            ));
            newDayLists.add(new ArrayList<>());
        }

        ZonedDateTime end = this.currentMonday.plus(7, ChronoUnit.DAYS);
        for (int i = 0; i < schedule.size(); ++i) {
            ScheduledBillboard scheduledBillboard = schedule.get(i);
            String creator = creators.get(i);

            List<ZonedDateTime> showings = scheduledBillboard.getShowingsBetween(this.currentMonday, end);
            for (ZonedDateTime showing : showings) {
                int dayListIndex = showing.getDayOfWeek().getValue() - 1;
                newDayLists.get(dayListIndex).add(new ScheduleEntry(scheduledBillboard, showing, creator));
            }
        }

        for (int i = 0; i < NUM_DAYS; ++i) {
            this.dayLists.get(i).clear();
            newDayLists.get(i).sort(Comparator.comparing(o -> o.showing));
            this.dayLists.get(i).addAll(newDayLists.get(i));
        }
    }

    /**
     * Event listener fired when a schedule entry is selected.
     */
    private void onScheduleEntrySelected() {
        ScheduleEntry entry = getSelectedScheduleEntry();

        // No item selected: bail
        if (entry == null) {
            return;
        }

        GetBillboardResponse response = BackgroundRequest.makeRequestBackground(this.connection, new GetBillboardRequest(
                this.loginParams.getSessionToken(),
                entry.scheduledBillboard.getName()
        ));
        if (response != null) {
            billboardRenderer.setBillboard(response.getBillboard());
        }
    }

    /**
     * Returns the schedule entry currently selected, or null if no entry is selected.
     *
     * @return The schedule entry currently selected.
     */
    private ScheduleEntry getSelectedScheduleEntry() {
        ScheduleEntry entry = null;
        for (int i = 0; i < NUM_DAYS; ++i) {
            JList<ScheduleEntry> dayList = this.dayListLists.get(i);
            if (!dayList.isSelectionEmpty()) {
                entry = dayList.getSelectedValue();
                break;
            }
        }
        return entry;
    }

    /**
     * Creates a panel to display the billboard of a selected viewing
     */
    private void createDisplayPanel() {
        this.displayPanel = new JPanel();
        this.displayPanel.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        billboardRenderer = new BillboardRenderer();
        billboardRenderer.setBillboard(null);
        billboardRenderer.setMinimumSize(new Dimension(RENDER_WIDTH, RENDER_HEIGHT));
        constraints.anchor = GridBagConstraints.CENTER;
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;
        addToPanel(this.displayPanel, billboardRenderer, constraints, 0, 0);
    }

    /**
     * Creates the panel containing the schedule view.
     */
    private void createSchedulePanel() {
        this.schedulePanel = new JPanel();
        this.schedulePanel.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.BOTH;
        constraints.anchor = GridBagConstraints.CENTER;

        JButton addButton = new JButton("Schedule Billboard");
        addButton.addActionListener(e -> {
            ListBillboardsResponse response = BackgroundRequest.makeRequestBackground(connection, new ListBillboardsRequest(
                    loginParams.getSessionToken()
            ));

            if (response == null) {
                return;
            }

            if (response.getBillboards().isEmpty()) {
                JOptionPane.showMessageDialog(
                        this,
                        "No billboards available to schedule.",
                        "Schedule billboard.",
                        JOptionPane.ERROR_MESSAGE
                );
                return;
            }

            List<String> billboards = response.getBillboards().stream()
                    .map(BillboardMeta::getName)
                    .distinct()
                    .collect(Collectors.toList());
            ScheduleBillboardDialog newViewing = new ScheduleBillboardDialog(billboards);
            newViewing.setVisible(true);

            if (newViewing.getDialogResult() == DialogResult.OK) {
                ScheduledBillboard sb = newViewing.getScheduledBillboard();

                BackgroundRequest.makeRequestBackground(connection, new ScheduleBillboardRequest(
                        loginParams.getSessionToken(),
                        sb
                ));

                updateFromServer();
            }
        });

        JButton deleteShowingButton = new JButton("Delete Showing");
        deleteShowingButton.addActionListener(e -> {
            ScheduleEntry entry = getSelectedScheduleEntry();

            if (entry == null) {
                JOptionPane.showMessageDialog(
                        this,
                        "No schedule entry selected: cannot delete.",
                        "Schedule billboard.",
                        JOptionPane.ERROR_MESSAGE
                );
                return;
            }

            this.connection.makeRequestBackground(new RemoveFromScheduleRequest(
                    this.loginParams.getSessionToken(),
                    entry.scheduledBillboard
            ));

            updateFromServer();

            // Deletion deselects the entry, so display an empty billboard.
            this.billboardRenderer.setBillboard(null);
        });

        this.week = new JLabel();
        this.week.setHorizontalAlignment(JLabel.CENTER);

        JButton lastWeekButton = new JButton("<");
        lastWeekButton.addActionListener(e -> {
            currentMonday = TimeUtilities.prevWeek(currentMonday);
            updateFromSchedule(currentSchedule, currentCreators);
        });

        JButton nextWeekButton = new JButton(">");
        nextWeekButton.addActionListener(e -> {
            currentMonday = TimeUtilities.nextWeek(currentMonday);
            updateFromSchedule(currentSchedule, currentCreators);
        });

        Border blackBorder = BorderFactory.createLineBorder(Color.black);
        addButton.setBorder(blackBorder);
        deleteShowingButton.setBorder(blackBorder);
        this.week.setBorder(blackBorder);
        lastWeekButton.setBorder(blackBorder);
        nextWeekButton.setBorder(blackBorder);

        constraints.weightx = BUTTON_WEIGHT;
        addToPanel(this.schedulePanel, lastWeekButton, constraints, 0, 0);
        addToPanel(this.schedulePanel, nextWeekButton, constraints, 1, 0);

        JPanel scheduleButtons = new JPanel();
        scheduleButtons.setLayout(new GridBagLayout());
        addToPanel(scheduleButtons, addButton, constraints, 0, 0);
        addToPanel(scheduleButtons, deleteShowingButton, constraints, 0, 1);
        addToPanel(this.schedulePanel, scheduleButtons, constraints, 3, 0);
        constraints.weightx = 1.0;
        addToPanel(this.schedulePanel, week, constraints, 2, 0);

        JPanel listPanel = new JPanel();
        JPanel headings = new JPanel();
        JPanel lists = new JPanel();

        headings.setLayout(new GridLayout(1, 7));
        lists.setLayout(new GridLayout(1, 7));

        for (int i = 0; i < NUM_DAYS; i++) {
            JLabel heading = new JLabel();
            heading.setBorder(blackBorder);
            dayHeadings.add(heading);
            headings.add(heading);

            JList<ScheduleEntry> newList = new JList<>(this.dayLists.get(i));
            newList.setBorder(blackBorder);
            newList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

            int thisIndex = i;
            newList.addListSelectionListener(e -> {
                // The ListSelectionEvent gives us a range of indices where the list selection changed.
                // Of course there isn't just a method which fires when an item is *selected* - of course not.
                // So to work out if we have a selected item, we need to go through the whole range of indices that the
                // event tells us changed, and work out if **ANY** of them are selected.
                boolean anySelected = false;
                for (int idx = e.getFirstIndex(); idx <= e.getLastIndex(); ++idx) {
                    anySelected |= newList.isSelectedIndex(idx);
                }

                if (e.getSource() != newList || newList.isSelectionEmpty() || !anySelected) {
                    // If we've not been selected, ignore the event
                    return;
                }

                for (int j = 0; j < NUM_DAYS; ++j) {
                    // Don't deselect ourselves
                    if (j != thisIndex) {
                        dayListLists.get(j).clearSelection();
                    }
                }

                onScheduleEntrySelected();
            });

            dayListLists.add(newList);
            lists.add(newList);
        }

        listPanel.setLayout(new GridBagLayout());
        addToPanel(listPanel, headings, constraints, 0, 0);
        constraints.weighty = 1.0;
        addToPanel(listPanel, lists, constraints, 0, 1);

        JScrollPane scheduleView = new JScrollPane(listPanel);
        scheduleView.setBorder(blackBorder);
        constraints.gridwidth = 5;
        addToPanel(this.schedulePanel, scheduleView, constraints, 0, 1);
    }

    /**
     * Adds component to gridbag panel at given x/y coordinates.
     * @param jp jpanel to be added to.
     * @param c component to be added.
     * @param constraints constraints of component.
     * @param x column of component.
     * @param y row of component.
     */
    private void addToPanel (JPanel jp, Component c, GridBagConstraints constraints, int x, int y) {
        constraints.gridx = x;
        constraints.gridy = y;
        jp.add(c, constraints);
    }

    private static class ScheduleEntry {
        ScheduledBillboard scheduledBillboard;
        ZonedDateTime showing;
        String creator;

        public ScheduleEntry(ScheduledBillboard scheduledBillboard, ZonedDateTime showing, String creator) {
            this.scheduledBillboard = scheduledBillboard;
            this.showing = showing;
            this.creator = creator;
        }

        @Override
        public String toString() {
            Duration recurrencePeriod = this.scheduledBillboard.getRecurrencePeriod();

            String recurrenceString = "";
            if (recurrencePeriod != null) {
                recurrenceString = String.format(", every %s", TimeUtilities.durationAsHumanReadableString(recurrencePeriod));
            }

            return String.format(
                    "<html>%02d:%02d (+%dm)%s<br>%s (Creator: %s)</html>",
                    this.showing.getHour(),
                    this.showing.getMinute(),
                    this.scheduledBillboard.getDuration(),
                    recurrenceString,
                    this.scheduledBillboard.getName(),
                    this.creator
            );
        }
    }
}
