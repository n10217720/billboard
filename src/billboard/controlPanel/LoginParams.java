package billboard.controlPanel;

import billboard.server.Permission;

import java.util.HashSet;

/**
 * Parameters pertaining to an active connection to the server.
 */
public class LoginParams {
    private String sessionToken;
    private String username;
    private HashSet<Permission> permissions;

    /**
     * Creates a LoginParams object.
     *
     * @param sessionToken The user's session token.
     * @param username The user's username.
     * @param permissions The set of the user's current permissions.
     */
    public LoginParams(String sessionToken, String username, HashSet<Permission> permissions) {
        this.sessionToken = sessionToken;
        this.username = username;
        this.permissions = permissions;
    }

    /**
     * Returns the user's session token.
     *
     * @return The user's session token.
     */
    public String getSessionToken() {
        return sessionToken;
    }

    /**
     * Returns the user's username.
     *
     * @return The user's username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Returns the user's permissions.
     *
     * @return The user's permissions.
     */
    public HashSet<Permission> getPermissions() {
        return permissions;
    }

    /**
     * Returns true if the user has the specified permission, or false otherwise.
     *
     * @param permission The permission to check.
     * @return True if the user has the permission, false otherwise.
     */
    public boolean hasPermission(Permission permission) {
        return this.permissions.contains(permission);
    }
}
