package billboard.controlPanel;

import billboard.common.ScheduledBillboard;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.time.DateTimeException;
import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.List;
import java.util.Vector;

/**
 * GUI Dialog for users of the ControlPanel to schedule billboard viewings.
 */
public class ScheduleBillboardDialog extends DialogWithResult {
    private static final int WIDTH = 300;
    private static final int HEIGHT = 400;
    private static Insets LEFT_MARGINS = new Insets(5, 5, 5, 0);
    private static Insets RIGHT_MARGINS = new Insets(5, 5, 5, 5);
    private static Insets INDENT_MARGINS = new Insets(5, 15, 5, 0);

    private JComboBox<String> billboardList;
    private JComboBox<Integer> dayCombo;
    private JComboBox<Integer> monthCombo;
    private JComboBox<Integer> yearCombo;
    private JTextField timeFieldHr;
    private JTextField timeFieldMin;
    private JTextField durationField;
    private JRadioButton onceRadio;
    private JRadioButton everyRadio;
    private JTextField occurrenceField;
    private JComboBox<TemporalUnit> periodCombo;

    /**
     * Creates a dialog object that allows a user to schedule a billboard viewing (with or without repeats).
     *
     * @param billboardNames The names of the billboards which can be scheduled.
     */
    public ScheduleBillboardDialog(List<String> billboardNames) {
        this.setMinimumSize(new Dimension(WIDTH, HEIGHT));
        this.setTitle("Schedule Billboard");
        this.setModalityType(ModalityType.APPLICATION_MODAL);

        this.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 1;
        constraints.weighty = 1;
        constraints.insets = RIGHT_MARGINS;

        this.add(boardPanel(billboardNames), constraints);
        constraints.gridy = 1;
        this.add(timePanel(), constraints);
        constraints.gridy = 2;
        this.add(occurrencePanel(), constraints);
        constraints.weighty = 0;
        constraints.gridy = 3;
        this.add(confirmPanel(), constraints);

        ActionListener recurrenceListener = e -> {
            boolean isSelected = everyRadio.isSelected();
            occurrenceField.setEnabled(isSelected);
            periodCombo.setEnabled(isSelected);
        };
        onceRadio.addActionListener(recurrenceListener);
        everyRadio.addActionListener(recurrenceListener);

        // Ensure state of fields matches UI state on initialisation
        recurrenceListener.actionPerformed(null);
    }

    /**
     * Creates a panel containing the UI elements for selecting a billboard to schedule a viewing for.
     *
     * @param billboardNames The names of the billboards which can be scheduled.
     * @return panel containing UI for selecting billboards.
     */
    private JPanel boardPanel(List<String> billboardNames) {
        JPanel billboardPanel = new JPanel();
        billboardPanel.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;

        JLabel boardLabel = new JLabel("Billboard:");
        addToPanel(billboardPanel, boardLabel, constraints, 0, 0, LEFT_MARGINS);

        this.billboardList = new JComboBox<>(new Vector<>(billboardNames));
        constraints.weightx = 1;
        addToPanel(billboardPanel, this.billboardList, constraints, 1, 0, RIGHT_MARGINS);

        return billboardPanel;
    }

    /**
     * Creates panel containing UI elements for the initial time of a billboard (date and 24hr time)
     * @return panel containing UI elements.
     */
    private JPanel timePanel() {
        JPanel timePanel = new JPanel();
        timePanel.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;

        constraints.weightx = 0;
        JLabel dateLabel = new JLabel("Date:");
        addToPanel(timePanel, dateLabel, constraints, 0, 0, RIGHT_MARGINS);
        JLabel dividerLabel = new JLabel("/");
        addToPanel(timePanel, dividerLabel, constraints, 2, 0, LEFT_MARGINS);
        JLabel dividerLabel2 = new JLabel("/");
        addToPanel(timePanel, dividerLabel2, constraints, 4, 0, LEFT_MARGINS);

        ZonedDateTime now = ZonedDateTime.now();

        Integer[] days = new Integer[31];
        for (int i = 0; i < 31; ++i) {
            days[i] = i + 1;
        }
        dayCombo = new JComboBox<>(days);
        dayCombo.setSelectedIndex(now.getDayOfMonth() - 1);

        Integer[] months = new Integer[12];
        for (int i = 0; i < 12; ++i) {
            months[i] = i + 1;
        }
        monthCombo = new JComboBox<>(months);
        monthCombo.setSelectedIndex(now.getMonthValue() - 1);

        int currentYear = now.getYear();
        Integer[] years = new Integer[3];
        for (int i = 0; i < 3; ++i) {
            years[i] = currentYear + i;
        }
        yearCombo = new JComboBox<>(years);

        constraints.weightx = 1;
        addToPanel(timePanel, dayCombo, constraints, 1, 0, LEFT_MARGINS);
        addToPanel(timePanel, monthCombo, constraints, 3, 0, LEFT_MARGINS);
        addToPanel(timePanel, yearCombo, constraints, 5, 0, RIGHT_MARGINS);

        constraints.weightx = 0;
        JPanel specificTimePanel = new JPanel();
        specificTimePanel.setLayout(new GridBagLayout());
        JLabel timeLabel = new JLabel("Time:");
        addToPanel(specificTimePanel, timeLabel, constraints, 0, 0, RIGHT_MARGINS);
        JLabel dividerLabel3 = new JLabel(":");
        addToPanel(specificTimePanel, dividerLabel3, constraints, 2, 0, LEFT_MARGINS);
        constraints.weightx = 1;
        timeFieldHr = new JTextField("12", 4);
        timeFieldHr.setHorizontalAlignment(SwingConstants.RIGHT);
        timeFieldMin = new JTextField("00", 4);
        addToPanel(specificTimePanel, timeFieldHr, constraints, 1, 0, LEFT_MARGINS);
        addToPanel(specificTimePanel, timeFieldMin, constraints, 3, 0, RIGHT_MARGINS);
        constraints.gridwidth = 6;
        addToPanel(timePanel, specificTimePanel, constraints, 0, 1, new Insets(0, 0, 0, 0));

        TitledBorder timeBorder;
        timeBorder = BorderFactory.createTitledBorder("Initial Time");
        timePanel.setBorder(timeBorder);

        return timePanel;
    }

    /**
     * Creates a panel containing the UI elements relating to billboard occurrence (duration, occurrence freq, etc.)
     * @return panel containing occurrence elements.
     */
    private JPanel occurrencePanel() {
        JPanel occurrencePanel = new JPanel();
        occurrencePanel.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        JLabel durationLabel = new JLabel("Duration:", SwingConstants.RIGHT);
        addToPanel(occurrencePanel, durationLabel, constraints, 0, 0, LEFT_MARGINS);
        JLabel minutesLabel = new JLabel("Minutes");
        addToPanel(occurrencePanel, minutesLabel, constraints, 2, 0, RIGHT_MARGINS);
        constraints.weightx = 1;
        durationField = new JTextField("1", 5);
        addToPanel(occurrencePanel, durationField, constraints, 1, 0, LEFT_MARGINS);

        constraints.gridwidth = 3;
        JLabel occurrenceLabel = new JLabel("Occurrence:");
        addToPanel(occurrencePanel, occurrenceLabel, constraints, 0, 1, RIGHT_MARGINS);
        onceRadio = new JRadioButton("Once");
        addToPanel(occurrencePanel, onceRadio, constraints, 0, 2, INDENT_MARGINS);
        constraints.gridwidth = 1;
        constraints.weightx = 0;
        everyRadio = new JRadioButton("Every:");
        addToPanel(occurrencePanel, everyRadio, constraints, 0, 3, INDENT_MARGINS);
        periodCombo = new JComboBox<>(new TemporalUnit[] {
                ChronoUnit.MINUTES, ChronoUnit.HOURS, ChronoUnit.DAYS
        });
        addToPanel(occurrencePanel, periodCombo, constraints, 2, 3, RIGHT_MARGINS);
        constraints.weightx = 1;
        occurrenceField = new JTextField("1", 5);
        addToPanel(occurrencePanel, occurrenceField, constraints, 1, 3, LEFT_MARGINS);

        ButtonGroup radioButtons = new ButtonGroup();
        radioButtons.add(onceRadio);
        radioButtons.add(everyRadio);
        onceRadio.setSelected(true);

        TitledBorder occurrenceBorder;
        occurrenceBorder = BorderFactory.createTitledBorder("Timing");
        occurrencePanel.setBorder(occurrenceBorder);

        return occurrencePanel;
    }

    /**
     * Creates the panel containing the dialog confirm/cancel buttons.
     * @return panel containing buttons.
     */
    private JPanel confirmPanel() {
        JPanel confirmPanel = new JPanel();
        confirmPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));

        JButton confirmButton = new JButton("Confirm");
        confirmButton.addActionListener(e -> {
            try {
                // We're not using the result - just that the method throws if the input is invalid.
                getScheduledBillboardInternal();

                closeWithResult(DialogResult.OK);
            } catch (InvalidScheduleException ex) {
                JOptionPane.showMessageDialog(
                        this,
                        String.format("Cannot schedule: %s", ex.getMessage()),
                        "Schedule billboard",
                        JOptionPane.ERROR_MESSAGE
                );
            }
        });
        confirmPanel.add(confirmButton);


        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(e -> {
            closeWithResult(DialogResult.CANCEL);
        });
        confirmPanel.add(cancelButton);

        return confirmPanel;
    }

    /**
     * Function to add a component to a JPanel with some specifiable GridBagConstraint values.
     * @param jp JPanel to be added to.
     * @param c Component to be added.
     * @param constraints GridBag constraints.
     * @param x GridBag column.
     * @param y GridBag row.
     * @param inset Inset of component.
     */
    private void addToPanel (JPanel jp, Component c, GridBagConstraints constraints, int x, int y, Insets inset) {
        constraints.insets = inset;
        constraints.gridx = x;
        constraints.gridy = y;
        jp.add(c, constraints);
    }

    /**
     * Returns the {@link ScheduledBillboard} instance which the user created.
     *
     * @return The {@link ScheduledBillboard} instance which the user created.
     * @throws InvalidScheduleException The schedule the user created was invalid.
     */
    private ScheduledBillboard getScheduledBillboardInternal() throws InvalidScheduleException {
        String name = getSelectedItem(this.billboardList);

        int year = getSelectedItem(this.yearCombo);
        int month = getSelectedItem(this.monthCombo);
        int day = getSelectedItem(this.dayCombo);
        int hour;
        int minute;
        try {
            hour = Integer.parseInt(this.timeFieldHr.getText());
            minute = Integer.parseInt(this.timeFieldMin.getText());
        } catch (NumberFormatException ex) {
            throw new InvalidScheduleException("Invalid initial showing date (non-integer input)");
        }

        ZonedDateTime initialShowing;
        try {
            initialShowing = ZonedDateTime.of(
                    year, month, day,
                    hour, minute, 0, 0,
                    // The user is probably scheduling a billboard relative to their current timezone
                    ZoneId.systemDefault()
            );
        } catch (DateTimeException ex) {
            throw new InvalidScheduleException("Invalid initial showing date");
        }

        long duration;
        try {
            duration = Long.parseLong(this.durationField.getText());
        } catch (NumberFormatException ex) {
            throw new InvalidScheduleException("Invalid duration (non-integer input)");
        }

        if (duration < 1) {
            throw new InvalidScheduleException("Invalid duration (must be >= 1)");
        }

        Duration recurrencePeriod = null;
        if (this.everyRadio.isSelected()) {
            long amount;
            try {
                amount = Integer.parseInt(this.occurrenceField.getText());
            } catch (NumberFormatException ex) {
                throw new InvalidScheduleException("Invalid recurrence period (non-integer input)");
            }

            if (amount < 1) {
                throw new InvalidScheduleException("Invalid recurrence period (must be >= 1)");
            }

            if (amount <= duration) {
                throw new InvalidScheduleException("Cannot schedule billboard to recur at a shorter period than it displays for");
            }

            TemporalUnit unit = getSelectedItem(this.periodCombo);
            recurrencePeriod = Duration.of(amount, unit);
        }

        return new ScheduledBillboard(name, initialShowing, duration, recurrencePeriod);
    }

    /**
     * Returns the {@link ScheduledBillboard} instance which the user created.
     *
     * @return The {@link ScheduledBillboard} instance which the user created.
     */
    public ScheduledBillboard getScheduledBillboard() {
        try {
            return this.getScheduledBillboardInternal();
        } catch (InvalidScheduleException e) {
            // This method should never be called if the internal method throws, since the dialog should never return a
            // OK DialogResult if so
            e.printStackTrace();
            System.exit(1);
            return null;
        }
    }

    /**
     * Helper method which returns the selected item from a {@link JComboBox} in a type-safe way.
     *
     * @param comboBox The combo box to retrieve the selected item from.
     * @param <T> The type of the objects stored in the combo box.
     * @return The selected item.
     */
    private <T> T getSelectedItem(JComboBox<T> comboBox) {
        // For type safety, use getItemAt in combination with getSelectedIndex instead of casting getSelectedItem
        // https://stackoverflow.com/questions/7026230/why-isnt-getselecteditem-on-jcombobox-generic
        return comboBox.getItemAt(comboBox.getSelectedIndex());
    }

    private static class InvalidScheduleException extends Exception {
        public InvalidScheduleException(String message) {
            super(message);
        }
    }
}
