package billboard.controlPanel;

import billboard.common.ClientConnection;
import billboard.common.api.methods.UserLogoutRequest;
import billboard.server.Permission;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Creates a frame for the control panel.
 */
public class ControlPanel extends JFrame {
    private ClientConnection connection;
    private LoginParams loginParams;

    JTabbedPane tabsControlPanel;
    PanelBillboard billboardPanel;
    PanelUsers adminPanel;
    PanelSchedule schedulePanel;

    /**
     * Creates the main control panel window.
     *
     * @param connection The connection to the billboard server.
     * @param loginParams Parameters for the currently logged in user.
     */
    public ControlPanel(ClientConnection connection, LoginParams loginParams) {
        this.connection = connection;
        this.loginParams = loginParams;

        createUI();
    }

    /**
     * Creates the control panel GUI, which contains the specific feature sub-panels
     */
    private void createUI() {
        this.setTitle("Billboard Control Panel");
        setSize(640,480);
        setMinimumSize(new Dimension(640, 480));

        //Invalidate session token.
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                connection.makeRequestBackground(new UserLogoutRequest(loginParams.getSessionToken()));
                dispose();
            }
        });

        this.tabsControlPanel = new JTabbedPane();

        this.billboardPanel = new PanelBillboard(this, this.connection, this.loginParams);
        this.tabsControlPanel.addTab("Billboards", billboardPanel);

        if (this.loginParams.hasPermission(Permission.SCHEDULE_BILLBOARD)) {
            this.schedulePanel = new PanelSchedule(this.connection, this.loginParams);
            this.tabsControlPanel.addTab("Schedule", schedulePanel);
        }

        this.adminPanel = new PanelUsers(this.connection, this.loginParams);
        this.tabsControlPanel.addTab("Users", adminPanel);

        this.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();

        constraints.anchor = GridBagConstraints.LINE_END;
        JButton logoutButton = new JButton("Logout");
        this.add(logoutButton, constraints);
        logoutButton.addActionListener(e -> {
            connection.makeRequestBackground(new UserLogoutRequest(this.loginParams.getSessionToken()));
            dispose();
        });

        constraints.fill = GridBagConstraints.BOTH;
        constraints.anchor = GridBagConstraints.CENTER;
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;
        constraints.gridy = 1;
        this.add(tabsControlPanel, constraints);

        repaint();
        setVisible(true);
    }
}
