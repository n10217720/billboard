package billboard.controlPanel;

import billboard.common.Billboard;
import billboard.common.BillboardRenderer;

import javax.swing.*;
import java.awt.*;

/**
 * A modal dialog which allows a user to create/edit and preview a billboard.
 */
public class EditBillboardDialog extends DialogWithResult {
    public static final int WIDTH = 1000;
    public static final int HEIGHT = 600;

    private BillboardRenderer preview;

    private BillboardEditControls controls;

    private JPanel dialogButtons;
    private JButton cancel;
    private JButton commit;

    /**
     * Creates a modal dialog for creating/editing a billboard.
     *
     * {@code initialBillboard} must not be null. To specify that all defaults should be used, construct a
     * {@link Billboard} object with all fields set to null. Though this would ordinarily be an invalid billboard, this
     * class interprets such an object as a signal to initialise with all default values.
     *
     * @param owner The component which owns this dialog (to be used to make the dialog modal).
     * @param initialBillboard The billboard to initialise the edit controls with.
     * @param name The billboard's name, or {@code null} if the user is to specify one.
     */
    public EditBillboardDialog(Frame owner, Billboard initialBillboard, String name) {
        super(owner, true);
        this.setTitle("Edit Billboard");
        this.setPreferredSize(new Dimension(WIDTH, HEIGHT));

        GridBagLayout gbl = new GridBagLayout();
        this.setLayout(gbl);

        boolean isCreating = name == null;

        createPreview(gbl);
        createControls(gbl, initialBillboard, name);
        createDialogButtons(gbl, isCreating);

        pack();

        this.cancel.addActionListener(e -> {
            closeWithResult(DialogResult.CANCEL);
        });
        this.commit.addActionListener(e -> {
            if (!this.controls.getBillboard().isValid()) {
                JOptionPane.showMessageDialog(this, "Cannot finish: billboard is invalid.", "Billboard", JOptionPane.ERROR_MESSAGE);
                return;
            }

            if (this.controls.getName().isEmpty()) {
                JOptionPane.showMessageDialog(this, "Cannot finish: name not specified.", "Billboard", JOptionPane.ERROR_MESSAGE);
                return;
            }

            closeWithResult(DialogResult.OK);
        });

        Runnable onBillboardModified = () -> {
            Billboard b = this.controls.getBillboard();
            if (b.isValid()) {
                this.preview.setBillboard(b);
            } else {
                this.preview.setBillboard(null);
            }
        };
        this.controls.bindOnBillboardModified(onBillboardModified);

        // Run once to ensure we're displaying the current state correctly
        onBillboardModified.run();
    }

    /**
     * Creates and adds the billboard preview to the dialog.
     *
     * @param gbl The dialog's grid bag layout.
     */
    private void createPreview(GridBagLayout gbl) {
        this.preview = new BillboardRenderer();

        GridBagConstraints previewGBC = new GridBagConstraints();
        previewGBC.weightx = 1.0;
        previewGBC.weighty = 1.0;
        previewGBC.gridx = 0;
        previewGBC.gridy = 0;
        previewGBC.gridheight = 2;
        previewGBC.fill = GridBagConstraints.BOTH;
        gbl.setConstraints(this.preview, previewGBC);

        this.add(this.preview);
    }

    /**
     * Creates the edit controls for the dialog.
     * @param gbl the GridBagLayout to be used.
     * @param initialBillboard the billboard that is being edited.
     * @param name the name of the billboard.
     */
    private void createControls(GridBagLayout gbl, Billboard initialBillboard, String name) {
        this.controls = new BillboardEditControls(initialBillboard, name);

        GridBagConstraints controlsGBC = new GridBagConstraints();
        controlsGBC.gridx = 1;
        controlsGBC.gridy = 0;
        controlsGBC.fill = GridBagConstraints.BOTH;
        gbl.setConstraints(this.controls, controlsGBC);

        this.add(this.controls);
    }

    /**
     * Creates and adds the main dialog buttons to the dialog.
     *
     * @param gbl The dialog's grid bag layout.
     * @param isCreating True if the user is creating a new billboard, false if they are editing one.
     */
    private void createDialogButtons(GridBagLayout gbl, boolean isCreating) {
        this.dialogButtons = new JPanel();
        this.dialogButtons.setLayout(new FlowLayout(FlowLayout.RIGHT));
        this.dialogButtons.setAlignmentX(Component.LEFT_ALIGNMENT);

        this.commit = new JButton(isCreating ? "Create" : "Edit");
        this.dialogButtons.add(this.commit);

        this.cancel = new JButton("Cancel");
        this.dialogButtons.add(this.cancel);

        GridBagConstraints buttonsGBC = new GridBagConstraints();
        buttonsGBC.gridx = 1;
        buttonsGBC.gridy = 1;
        buttonsGBC.fill = GridBagConstraints.HORIZONTAL;
        buttonsGBC.anchor = GridBagConstraints.PAGE_END;
        gbl.setConstraints(this.dialogButtons, buttonsGBC);

        this.add(this.dialogButtons);
    }

    /**
     * Returns the billboard's name.
     *
     * @return The billboard's name.
     */
    public String getName() {
        return this.controls.getName();
    }

    /**
     * Returns the billboard the user created.
     *
     * @return The billboard the user created.
     */
    public Billboard getBillboard() {
        return this.controls.getBillboard();
    }
}
