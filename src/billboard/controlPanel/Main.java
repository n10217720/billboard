package billboard.controlPanel;

import billboard.common.*;
import billboard.common.api.methods.GetUserPermissionsRequest;
import billboard.common.api.methods.GetUserPermissionsResponse;
import billboard.common.api.methods.UserLoginRequest;
import billboard.common.api.methods.UserLoginResponse;

/**
 * Provides the entry point for the control panel.
 * Requests the user to log in, attempts to authenticate them against the server using the connection parameters read from the connection file,
 * and opens the control panel if authentication is successful.
 */
public class Main {
    public static void main(String[] args) {
        ConnectionProperties connectionProperties = null;
        try {
            connectionProperties = new ConnectionProperties(ConnectionProperties.DEFAULT_FILENAME);
        } catch (Exception e) {
            System.err.println("Failed to load connection properties file.");
            e.printStackTrace();
            System.exit(1);
        }

        ClientConnection connection = new ClientConnection(connectionProperties.getHostname(), connectionProperties.getPort());

        String username = null;
        String sessionToken = null;
        do {
            LoginUI loginDialog = new LoginUI();
            loginDialog.setVisible(true);

            if (loginDialog.getDialogResult() == DialogResult.CANCEL) {
                // The user cancelled the dialog - quit the application
                return;
            }

            username = loginDialog.getUsername();
            String password = loginDialog.getPassword();

            UserLoginResponse response = connection.makeRequestBackground(new UserLoginRequest(
                    username, HashSHA3512.generateHash(password)
            ));

            if (response != null) {
                sessionToken = response.getSessionToken();
            }
        } while (sessionToken == null);

        GetUserPermissionsResponse response = connection.makeRequestBackground(new GetUserPermissionsRequest(
                sessionToken, username
        ));

        LoginParams loginParams = new LoginParams(sessionToken, username, response.getPermissionList());
        ControlPanel controlPanel = new ControlPanel(connection, loginParams);
    }
}
