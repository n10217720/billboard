package billboard.controlPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Creates a login dialog.
 */
public class LoginUI extends DialogWithResult {
    public static final int WIDTH = 300;
    public static final int HEIGHT = 145;

    private static Insets FIELD_MARGINS = new Insets(5, 0, 5, 10);
    private static Insets LABEL_MARGINS = new Insets(5, 10, 5, 5);
    private static Insets BUTTONS_MARGINS = new Insets(0, 10, 5, 10);

    private JLabel lblLogin;
    private JLabel lblPass;
    private JTextField fieldUser;
    private JPasswordField fieldPass;

    private JPanel pnlBtn;
    private JButton btnLogin;
    private JButton btnCancel;

    /**
     * Creates a new login dialog.
     */
    public LoginUI() {
        this.setPreferredSize(new Dimension(WIDTH, HEIGHT));
        this.setTitle("Control Panel Login");
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setLayout(new GridBagLayout());
        this.setResizable(false);

        //Create components.
        pnlBtn = new JPanel();
        btnLogin = createButton("Login");
        btnCancel = createButton("Cancel");
        lblLogin = new JLabel("Username:");
        lblPass = new JLabel("Password:");

        fieldUser = new JTextField();
        fieldUser.addActionListener(fieldListener());

        fieldPass = new JPasswordField();
        fieldPass.addActionListener(fieldListener());

        //Layout and display UI
        layoutFrame();

        pack();
    }

    /**
     * Create a button.
     *
     * @param name the name of the button.
     * @return the JButton
     */
    private JButton createButton(String name) {
        JButton btn = new JButton(name);
        btn.addActionListener(e -> {
            if (e.getSource() == btnLogin) {
                closeWithResult(DialogResult.OK);
            }

            if (e.getSource() == btnCancel) {
                closeWithResult(DialogResult.CANCEL);
            }
        });
        return btn;
    }

    /**
     * Creates an action listener to be added to a field.
     *
     * @return the action listener to be added.
     */
    private ActionListener fieldListener() {
        return e -> {closeWithResult(DialogResult.OK);};
    }

    /**
     * Applies the layout to component on the frame.
     */
    private void layoutFrame() {
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;

        //Place labels to left of fields.
        constraints.insets = LABEL_MARGINS;
        addToFrame(lblLogin, constraints, 0, 0, 1);
        addToFrame(lblPass, constraints, 0, 1, 1);

        //Make fields width of grid.
        constraints.weightx = 1.0;
        constraints.insets = FIELD_MARGINS;
        addToFrame(fieldUser, constraints, 1, 0, 1);
        addToFrame(fieldPass, constraints, 1, 1, 1);

        //Add flow layout panel to bottom row.
        constraints.insets = BUTTONS_MARGINS;
        addToFrame(pnlBtn, constraints, 0, 2, 2);

        pnlBtn.setLayout(new FlowLayout(FlowLayout.TRAILING));
        pnlBtn.add(btnLogin);
        pnlBtn.add(btnCancel);
    }

    /**
     * Adds elements to the login frame.
     *
     * @param c component to be added.
     * @param constraints gridbag constraints of component to be added.
     * @param x grid column.
     * @param y grid row.
     * @param w grid width.
     */
    private void addToFrame(Component c, GridBagConstraints constraints, int x, int y, int w) {
        constraints.gridx = x;
        constraints.gridy = y;
        constraints.gridwidth = w;
        this.add(c, constraints);
    }

    /**
     * Returns the username the user entered.
     *
     * @return The username entered.
     */
    public String getUsername() {
        return fieldUser.getText();
    }

    /**
     * Returns the password the user entered.
     *
     * @return The password entered.
     */
    public String getPassword() {
        return new String(fieldPass.getPassword());
    }
}