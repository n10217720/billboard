package billboard.controlPanel;

import billboard.common.Billboard;
import billboard.common.ColouredText;
import billboard.common.ParseException;
import billboard.common.abstractXML.DocumentToString;
import billboard.common.api.errors.MalformedRequestErrorResponse;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Base64;

/**
 * GUI component which provides edit controls for a billboard.
 */
public class BillboardEditControls extends JPanel {
    private static final int TEXT_FIELD_WIDTH = 20;
    private static Insets LABEL_MARGINS = new Insets(0, 5, 5, 5);
    private static Insets FIELD_MARGINS = new Insets(0, 0, 5, 5);

    private static final Color DEFAULT_BACKGROUND_COLOUR = Color.WHITE;
    private static final ColouredText DEFAULT_COLOURED_TEXT = new ColouredText("", Color.BLACK);

    private Runnable onBillboardModified;

    private JPanel generalPanel;
    private JLabel nameLabel;
    private JTextField name;
    private JLabel backgroundColourLabel;
    private SimpleColourPicker backgroundColourPicker;

    private BillboardTextComponentEditor message;
    private BillboardPictureComponentEditor picture;
    private BillboardTextComponentEditor information;

    private JPanel xmlPanel;
    private JButton xmlImport;
    private JButton xmlExport;

    private Billboard.BillboardPicture billboardPicture;

    /**
     * Creates an instance of {@link BillboardEditControls} providing edit controls relevant to building a billboard.
     *
     * @param initialBillboard The billboard to initialise the edit controls with.
     * @param name The billboard's name, or {@code null} if the user is to specify one.
     */
    public BillboardEditControls(Billboard initialBillboard, String name) {
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        createGeneralControls();

        this.message = new BillboardTextComponentEditor("Message");
        this.message.setAlignmentX(Component.LEFT_ALIGNMENT);
        this.add(this.message);

        this.picture = new BillboardPictureComponentEditor("Picture");
        this.picture.setAlignmentX(Component.LEFT_ALIGNMENT);
        this.add(this.picture);

        this.information = new BillboardTextComponentEditor("Information");
        this.information.setAlignmentX(Component.LEFT_ALIGNMENT);
        this.add(this.information);

        createXMLControls();

        initialiseFrom(initialBillboard, name);

        // By default, perform no action when the billboard is modified.
        onBillboardModified = () -> {};
        bindModificationListeners();

        this.picture.fromFile.addActionListener(e -> {
            JFileChooser dialog = new JFileChooser();
            dialog.setDialogTitle("Billboard picture from file");
            dialog.setFileFilter(new FileNameExtensionFilter("Image files", "jpg", "jpeg", "png", "gif"));

            if (dialog.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                File file = dialog.getSelectedFile();
                try {
                    billboardPicture = new Billboard.BillboardPictureData(file);
                    onBillboardModified.run();
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(this, "Could not open file", "Billboard picture from file", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        this.picture.fromWeb.addActionListener(e -> {
            String url = JOptionPane.showInputDialog(this, "Enter image URL:", null);
            if (url != null) {
                this.billboardPicture = new Billboard.BillboardPictureURL(url);
                onBillboardModified.run();
            }
        });

        this.xmlImport.addActionListener(e -> {
            JFileChooser dialog = new JFileChooser();
            dialog.setDialogTitle("Import billboard from XML file");
            dialog.setFileFilter(new FileNameExtensionFilter("XML files", "xml"));

            if (dialog.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                File file = dialog.getSelectedFile();
                try {
                    DocumentBuilderFactory dbf = DocumentBuilderFactory.newDefaultInstance();
                    DocumentBuilder db = dbf.newDocumentBuilder();
                    Document billboardDocument = db.parse(Files.newInputStream(file.toPath()));

                    Billboard billboard = Billboard.fromXML(billboardDocument);
                    initialiseFrom(billboard, null);

                    onBillboardModified.run();
                } catch (ParserConfigurationException | SAXException | IOException ex) {
                    JOptionPane.showMessageDialog(
                            this,
                            String.format("Could not read billboard XML file (%s)", ex.getMessage()),
                            "Import billboard from XML file",
                            JOptionPane.ERROR_MESSAGE
                    );
                } catch (ParseException ex) {
                    JOptionPane.showMessageDialog(
                            this,
                            String.format("Failed to parse billboard XML file (%s)", ex.getMessage()),
                            "Import billboard from XML file",
                            JOptionPane.ERROR_MESSAGE
                    );
                }
            }
        });

        this.xmlExport.addActionListener(e -> {
            Billboard billboard = this.getBillboard();
            if (!billboard.isValid()) {
                JOptionPane.showMessageDialog(this, "Cannot export invalid billboard", "Export billboard to XML file", JOptionPane.ERROR_MESSAGE);
                return;
            }

            JFileChooser dialog = new JFileChooser();
            dialog.setDialogTitle("Export billboard to XML file");
            dialog.setFileFilter(new FileNameExtensionFilter("XML files", "xml"));

            if (dialog.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
                File file = dialog.getSelectedFile();
                String billboardString = DocumentToString.documentToString(billboard.toXML());

                try {
                    Files.writeString(file.toPath(), billboardString);

                    JOptionPane.showMessageDialog(
                            this,
                            String.format("Billboard successfully exported to %s", file.getName()),
                            "Export billboard to XML file",
                            JOptionPane.INFORMATION_MESSAGE
                    );
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(
                            this,
                            String.format("Could not write billboard XML file (%s)", ex.getMessage()),
                            "Export billboard to XML file",
                            JOptionPane.ERROR_MESSAGE
                    );
                }
            }
        });
    }

    /**
     * Adds several listeners to GUI elements to allow notification when the billboard is updated.
     */
    private void bindModificationListeners() {
        // Multiple classes of listener are required for different controls, but all simply call the lambda we've been
        // given
        Runnable runnable = () -> onBillboardModified.run();
        ActionListener actionListener = e -> onBillboardModified.run();
        DocumentListener documentListener = new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                onBillboardModified.run();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                onBillboardModified.run();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                onBillboardModified.run();
            }
        };

        // Note: we must pass a lambda to bindOnColourChanged, since the onBillboardModified variable could change -
        // which would not be propagated into the other functions if we didn't.
        this.name.addActionListener(actionListener);
        this.backgroundColourPicker.bindOnColourChanged(runnable);
        this.message.visible.addActionListener(actionListener);
        this.message.text.getDocument().addDocumentListener(documentListener);
        this.message.colourPicker.bindOnColourChanged(runnable);
        this.picture.visible.addActionListener(actionListener);
        this.information.visible.addActionListener(actionListener);
        this.information.text.getDocument().addDocumentListener(documentListener);
        this.information.colourPicker.bindOnColourChanged(runnable);
    }

    /**
     * Initialises the edit controls.
     *
     * @param initialBillboard The billboard to initialise from.
     * @param name The billboard's name, or {@code null} if the user is to specify one.
     */
    private void initialiseFrom(Billboard initialBillboard, String name) {
        if (name != null) {
            this.name.setText(name);
            this.name.setEnabled(false);
        }

        if (initialBillboard.backgroundColour != null) {
            backgroundColourPicker.setColour(initialBillboard.backgroundColour);
        } else {
            backgroundColourPicker.setColour(DEFAULT_BACKGROUND_COLOUR);
        }

        initialiseTCEFromColouredText(this.message, initialBillboard.message);

        this.billboardPicture = initialBillboard.picture;
        if (initialBillboard.picture != null) {
            this.picture.visible.setSelected(true);
        }

        initialiseTCEFromColouredText(this.information, initialBillboard.information);
    }

    /**
     * Initialises a {@link BillboardTextComponentEditor} from a {@link ColouredText}.
     *
     * If {@code colouredText} is {@code null}, a default text and colour is used.
     *
     * @param tce The component to initialise.
     * @param colouredText The {@link ColouredText} to initialise the component with.
     */
    private void initialiseTCEFromColouredText(BillboardTextComponentEditor tce, ColouredText colouredText) {
        boolean isVisible = colouredText != null;

        tce.visible.setSelected(isVisible);
        if (isVisible) {
            tce.text.setText(colouredText.text);
            tce.colourPicker.setColour(colouredText.colour);
        } else {
            // Could call back into initialiseTCEFromColouredText, but then the visible checkbox would be ticked
            tce.text.setText(DEFAULT_COLOURED_TEXT.text);
            tce.colourPicker.setColour(DEFAULT_COLOURED_TEXT.colour);
        }
    }

    /**
     * Returns the name of the billboard.
     *
     * @return The name of the billboard.
     */
    public String getName() {
        return this.name.getText();
    }

    /**
     * Returns a billboard object built from the information input into this component's edit controls.
     *
     * @return A billboard object built from the information input into this component's edit controls.
     */
    public Billboard getBillboard() {
        Billboard billboard = new Billboard();

        billboard.backgroundColour = backgroundColourPicker.getColour();
        billboard.message = message.getColouredText();

        if (picture.visible.isSelected()) {
            billboard.picture = billboardPicture;
        }

        billboard.information = information.getColouredText();

        return billboard;
    }

    /**
     * Calls the provided function when a property of the billboard is updated by the user using the edit controls.
     *
     * @param r The function to run when a property of the billboard is updated.
     */
    public void bindOnBillboardModified(Runnable r) {
        this.onBillboardModified = r;
    }

    /**
     * Creates and adds the general controls panel.
     */
    private void createGeneralControls() {
        GridBagLayout gbl = new GridBagLayout();
        this.generalPanel = new JPanel(gbl);
        this.generalPanel.setBorder(BorderFactory.createTitledBorder("General"));
        this.generalPanel.setAlignmentX(Component.LEFT_ALIGNMENT);

        this.nameLabel = new JLabel("Name:");

        GridBagConstraints gbcNameLabel = new GridBagConstraints();
        gbcNameLabel.anchor = GridBagConstraints.EAST;
        gbcNameLabel.insets = LABEL_MARGINS;
        gbl.setConstraints(this.nameLabel, gbcNameLabel);
        this.generalPanel.add(this.nameLabel);

        this.name = new JTextField(TEXT_FIELD_WIDTH);

        GridBagConstraints gbcName = new GridBagConstraints();
        gbcName.gridx = 1;
        gbcName.weightx = 1.0;
        gbcName.fill = GridBagConstraints.HORIZONTAL;
        gbcName.anchor = GridBagConstraints.WEST;
        gbcName.insets = FIELD_MARGINS;
        gbl.setConstraints(this.name, gbcName);
        this.generalPanel.add(this.name);

        this.backgroundColourLabel = new JLabel("Background colour:");

        GridBagConstraints gbcBackgroundColourLabel = new GridBagConstraints();
        gbcBackgroundColourLabel.gridy = 1;
        gbcBackgroundColourLabel.anchor = GridBagConstraints.EAST;
        gbcBackgroundColourLabel.insets = LABEL_MARGINS;
        gbl.setConstraints(this.backgroundColourLabel, gbcBackgroundColourLabel);
        this.generalPanel.add(this.backgroundColourLabel);

        this.backgroundColourPicker = new SimpleColourPicker("Select background colour");

        GridBagConstraints gbcBackgroundColourPicker = new GridBagConstraints();
        gbcBackgroundColourPicker.gridx = 1;
        gbcBackgroundColourPicker.gridy = 1;
        gbcBackgroundColourPicker.weightx = 1.0;
        gbcBackgroundColourPicker.anchor = GridBagConstraints.WEST;
        gbcBackgroundColourPicker.insets = FIELD_MARGINS;
        gbl.setConstraints(this.backgroundColourPicker, gbcBackgroundColourPicker);
        this.generalPanel.add(this.backgroundColourPicker);

        this.add(generalPanel);
    }

    /**
     * Creates and adds the XML controls panel.
     */
    private void createXMLControls() {
        this.xmlPanel = new JPanel(new FlowLayout());
        this.xmlPanel.setBorder(BorderFactory.createTitledBorder("XML Import/Export"));
        this.xmlPanel.setAlignmentX(Component.LEFT_ALIGNMENT);

        this.xmlImport = new JButton("Import...");
        this.xmlPanel.add(this.xmlImport);

        this.xmlExport = new JButton("Export...");
        this.xmlPanel.add(this.xmlExport);

        this.add(this.xmlPanel);
    }

    /**
     * A generic GUI component providing and a titled border and visible checkbox for editing a billboard component, and
     * a nested panel for additional edit controls.
     */
    private static abstract class BillboardComponentEditor extends JPanel {
        public JCheckBox visible;
        protected JPanel editControlsPanel;

        /**
         * Creates a component editor.
         *
         * @param title The title to display in the component editor's border.
         */
        public BillboardComponentEditor(String title) {
            this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
            this.setBorder(BorderFactory.createTitledBorder(title));

            createVisible();
            createEditControlsPanel();
        }

        /**
         * Creates and adds the visible checkbox.
         */
        private void createVisible() {
            this.visible = new JCheckBox("Visible");
            this.visible.setAlignmentX(Component.LEFT_ALIGNMENT);
            this.visible.setMargin(new Insets(5, 5, 5, 5));
            this.add(visible);
        }

        /**
         * Creates and adds the edit controls panel.
         */
        private void createEditControlsPanel() {
            this.editControlsPanel = new JPanel();
            this.editControlsPanel.setAlignmentX(Component.LEFT_ALIGNMENT);

            this.add(this.editControlsPanel);
        }
    }

    /**
     * GUI component allowing a text component of a billboard to be edited.
     */
    private static class BillboardTextComponentEditor extends BillboardComponentEditor {
        private JLabel textLabel;
        public JTextField text;
        private JLabel colourLabel;
        public SimpleColourPicker colourPicker;

        /**
         * Creates a text component editor.
         *
         * @param title The title to display in the component editor's border.
         */
        public BillboardTextComponentEditor(String title) {
            super(title);

            createEditControls();
        }

        /**
         * Returns a {@link ColouredText} which reflects the state of the text component edit controls, or null if the
         * component has not been set as visible.
         *
         * @return A {@link ColouredText} from the text component edit controls, or null if it is not visible.
         */
        public ColouredText getColouredText() {
            if (visible.isSelected()) {
                return new ColouredText(text.getText(), colourPicker.getColour());
            } else {
                return null;
            }
        }

        /**
         * Creates and adds the text edit control and associated label.
         */
        private void createText(GridBagLayout gbl) {
            this.textLabel = new JLabel("Text:");

            GridBagConstraints gbcTextLabel = new GridBagConstraints();
            gbcTextLabel.anchor = GridBagConstraints.EAST;
            gbcTextLabel.insets = LABEL_MARGINS;
            gbl.setConstraints(this.textLabel, gbcTextLabel);
            this.editControlsPanel.add(this.textLabel);

            this.text = new JTextField(TEXT_FIELD_WIDTH);

            GridBagConstraints gbcText = new GridBagConstraints();
            gbcText.gridx = 1;
            gbcText.weightx = 1.0;
            gbcText.fill = GridBagConstraints.HORIZONTAL;
            gbcText.anchor = GridBagConstraints.WEST;
            gbcText.insets = FIELD_MARGINS;
            gbl.setConstraints(this.text, gbcText);
            this.editControlsPanel.add(this.text);

            this.textLabel.setLabelFor(this.text);
        }

        /**
         * Creates and adds the colour edit control and associated label.
         */
        private void createColour(GridBagLayout gbl) {
            this.colourLabel = new JLabel("Text colour:");

            GridBagConstraints gbcColourLabel = new GridBagConstraints();
            gbcColourLabel.gridy = 1;
            gbcColourLabel.anchor = GridBagConstraints.EAST;
            gbcColourLabel.insets = LABEL_MARGINS;
            gbl.setConstraints(this.colourLabel, gbcColourLabel);
            this.editControlsPanel.add(this.colourLabel);

            this.colourPicker = new SimpleColourPicker("Select text colour");

            GridBagConstraints gbcColourPicker = new GridBagConstraints();
            gbcColourPicker.gridx = 1;
            gbcColourPicker.gridy = 1;
            gbcColourPicker.anchor = GridBagConstraints.WEST;
            gbcColourPicker.insets = FIELD_MARGINS;
            gbl.setConstraints(this.colourPicker, gbcColourPicker);
            this.editControlsPanel.add(this.colourPicker);

            this.colourLabel.setLabelFor(this.colourPicker);
        }

        /**
         * Creates and adds additional edit controls to the nested panel.
         */
        private void createEditControls() {
            GridBagLayout gbl = new GridBagLayout();
            this.editControlsPanel.setLayout(gbl);

            createText(gbl);
            createColour(gbl);

            //If text visible but no text provided, highlight field in red.
            Border invalidBorder = BorderFactory.createLineBorder(Color.RED, 2);
            Border defaultBorder = new JTextField().getBorder();
            Runnable flagInvalid = () -> {
                if (this.visible.isSelected() && this.text.getText().isEmpty()) {
                    text.setBorder(invalidBorder);
                }
                else {
                    this.text.setBorder(new JTextField().getBorder());
                }
            };
            ActionListener actionListener = e -> flagInvalid.run();
            DocumentListener documentListener = new DocumentListener() {
                @Override
                public void insertUpdate(DocumentEvent e) {
                    flagInvalid.run();
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    flagInvalid.run();
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    flagInvalid.run();
                }
            };

            this.text.getDocument().addDocumentListener(documentListener);
            this.visible.addActionListener(actionListener);
        }
    }

    /**
     * GUI component allowing a picture component of a billboard to be edited.
     */
    private static class BillboardPictureComponentEditor extends BillboardComponentEditor {
        private JButton fromFile;
        private JButton fromWeb;

        /**
         * Creates a picture component editor.
         *
         * @param title The title to display in the component editor's border.
         */
        public BillboardPictureComponentEditor(String title) {
            super(title);

            createEditControls();
        }

        /**
         * Creates and adds additional edit controls to the nested panel.
         */
        private void createEditControls() {
            this.editControlsPanel.setLayout(new FlowLayout());

            this.fromFile = new JButton("From file...");
            this.editControlsPanel.add(this.fromFile);

            this.fromWeb = new JButton("From web...");
            this.editControlsPanel.add(this.fromWeb);
        }
    }
}
