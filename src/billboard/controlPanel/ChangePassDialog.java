package billboard.controlPanel;

import javax.swing.*;
import java.awt.*;

/**
 * Creates a modal dialog allowing a user to change a password.
 */
public class ChangePassDialog extends DialogWithResult {
    public static final int WIDTH = 300;
    public static final int HEIGHT = 120;

    private static Insets LABEL_MARGINS = new Insets(10, 10, 10, 5);
    private static Insets FIELD_MARGINS = new Insets(10, 0, 10, 10);

    private JPasswordField passField;

    /**
     * Creates a modal dialog prompting the user to enter a new password.
     *
     * @param owner The dialog's owner.
     */
    public ChangePassDialog(Frame owner) {
        super(owner, true);

        this.setMinimumSize(new Dimension(WIDTH, HEIGHT));
        this.setTitle("Change Password");

        this.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.BOTH;
        constraints.weightx = 1.0;

        constraints.gridy = 1;
        this.add(confirmPanel(), constraints);
        constraints.weighty = 1.0;
        constraints.gridy = 0;
        this.add(passPanel(), constraints);
    }

    /**
     * Creates a panel containing components for a new password.
     * @return panel containing components for a new password.
     */
    private JPanel passPanel() {
        JPanel passPanel = new JPanel();
        passPanel.setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();

        JLabel passLabel = new JLabel("New Password:", SwingConstants.RIGHT);
        this.passField = new JPasswordField();


        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.insets = LABEL_MARGINS;
        passPanel.add(passLabel, constraints);

        constraints.insets = FIELD_MARGINS;
        constraints.weightx = 1.0;
        passPanel.add(this.passField, constraints);

        return passPanel;
    }

    /**
     * Creates panel with cancel/confirm buttons for dialog.
     * @return The panel with cancel/confirm buttons.
     */
    private JPanel confirmPanel() {
        JPanel confirmPanel = new JPanel();
        confirmPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        JButton confirmButton = new JButton("Confirm");
        confirmButton.addActionListener(e -> {
            closeWithResult(DialogResult.OK);
        });

        JButton cancelButton = new JButton("Cancel");
        cancelButton.addActionListener(e -> {
            closeWithResult(DialogResult.CANCEL);
        });

        confirmPanel.add(confirmButton);
        confirmPanel.add(cancelButton);

        return confirmPanel;
    }

    /**
     * Returns the new password the user entered.
     *
     * @return The new password the user entered.
     */
    public String getNewPassword() {
        return new String(passField.getPassword());
    }
}
