package billboard.controlPanel;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * A simple colour picker which draws only a preview, which when clicked allows a user to select a new colour.
 */
public class SimpleColourPicker extends JPanel {
    private static final Color DEFAULT_COLOUR = Color.WHITE;

    private Runnable onColourChanged;
    private Color colour;

    /**
     * Creates a simple colour picker. The picker is initialised with a default colour which can be accessed and changed
     * with {@link SimpleColourPicker#getColour} and {@link SimpleColourPicker#setColour}.
     *
     * @param dialogTitle The title to display in the title bar of the colour picker dialog.
     */
    public SimpleColourPicker(String dialogTitle) {
        // By default, do nothing
        this.onColourChanged = () -> {};

        this.setColour(DEFAULT_COLOUR);

        this.setPreferredSize(new Dimension(20, 20));
        this.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
        this.setCursor(new Cursor(Cursor.HAND_CURSOR));

        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                // Don't allow the user to change the colour if the component has been disabled
                if (!SimpleColourPicker.this.isEnabled()) {
                    return;
                }

                Color newColour = JColorChooser.showDialog(SimpleColourPicker.this, dialogTitle, SimpleColourPicker.this.getBackground());

                // JColorChooser returns null if the user cancelled the action, so only set the colour if the user did
                // intend to commit their change
                if (newColour != null) {
                    SimpleColourPicker.this.setColour(newColour);
                }
            }
        });
    }

    /**
     * Calls the provided function when the colour picker is invoked, and the colour changed.
     *
     * @param r The function to run when the colour is changed.
     */
    public void bindOnColourChanged(Runnable r) {
        this.onColourChanged = r;
    }

    /**
     * Returns the colour that has been selected, or the initial colour if the user has not already selected a colour.
     *
     * @return The colour selected, or the initial colour if a colour has not been selected.
     */
    public Color getColour() {
        return this.colour;
    }

    /**
     * Sets the colour and updates the preview.
     *
     * @param newColour The new colour.
     */
    public void setColour(Color newColour) {
        this.colour = newColour;
        this.setBackground(newColour);

        onColourChanged.run();
    }
}
