package billboard.viewer;

import billboard.common.*;
import billboard.common.api.methods.GetCurrentBillboardRequest;
import billboard.common.api.methods.GetCurrentBillboardResponse;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Creates a fullscreen frame displaying the currently showing billboard.
 */
public class ViewerFrame extends JFrame {
    private static final int REFRESH_INTERVAL_MS = 15000;

    /**
     * Creates a fullscreen frame displaying the currently showing billboard.
     *
     * @param connection The connection to the billboard server.
     */
    public ViewerFrame(ClientConnection connection) {
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setUndecorated(true);
        this.setVisible(true);

        BillboardRenderer billboardDisplay = new BillboardRenderer();
        billboardDisplay.setBillboard(null);
        this.add(billboardDisplay);

        //Close viewer on mouse click.
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                dispose();
            }
        });

        //Close viewer when escape key pressed.
        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    dispose();
                }
            }
        });

        ActionListener refreshDisplay = e -> {
            try {
                GetCurrentBillboardResponse response = connection.makeRequest(new GetCurrentBillboardRequest());
                billboardDisplay.setBillboard(response.getBillboard());
            } catch (RequestFailedException ex) {
                billboardDisplay.setBillboard(getErrorBillboard(ex));
            }
        };
        Timer requestTimer = new Timer(REFRESH_INTERVAL_MS, refreshDisplay);
        requestTimer.start();

        // Ensure display is updated immediately
        refreshDisplay.actionPerformed(null);
    }

    /**
     * Gets a billboard which communicates an error to the user.
     *
     * @param ex The error.
     * @return The billboard which communicates the error.
     */
    private Billboard getErrorBillboard(RequestFailedException ex) {
        final Color LIGHT_RED = new Color(0x55, 0x33, 0x33);

        Billboard billboard = new Billboard();
        billboard.backgroundColour = LIGHT_RED;

        if (ex instanceof RequestErrorException) {
            billboard.message = new ColouredText("Could not retrieve current billboard", Color.BLACK);
            billboard.information = new ColouredText(((RequestErrorException) ex).getErrorResponse().getMessage(), Color.BLACK);
        } else {
            billboard.information = new ColouredText(String.format("Error: %s", ex.getMessage()), Color.BLACK);
        }

        return billboard;
    }
}
