package billboard.viewer;

import billboard.common.ClientConnection;
import billboard.common.ConnectionProperties;

/**
 * Class for providing the entry point for the viewer.
 * Retrieves connection parameters from configuration files and launches the viewer window.
 */
public class Main {
    public static void main(String[] args) {
        ConnectionProperties connectionProperties = null;
        try {
            connectionProperties = new ConnectionProperties(ConnectionProperties.DEFAULT_FILENAME);
        } catch (Exception e) {
            System.err.println("Failed to load connection properties file.");
            e.printStackTrace();
            System.exit(1);
        }

        ClientConnection connection = new ClientConnection(connectionProperties.getHostname(), connectionProperties.getPort());
        ViewerFrame viewer = new ViewerFrame(connection);
    }
}
